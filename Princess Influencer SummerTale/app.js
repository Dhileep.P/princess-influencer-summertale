var Phaser;
var level = 3;
var pageNo = 0;
var firstTime = true;
var bgmusic;
var isMuted = false;
var i = 0;
var sno = 0;
var loadFinish = false
var soundMuted = false
var settings = {
    game_id: 'princess_girls_oscars_design',
    game_name: 'Princess Girls Oscars Design',
    game_size: '800x600',
    pauseSoundCallback: function () {
        soundMuted = isMuted
        if (!isMuted) {
            soundMuted = true
            music.pause()
            mute.setFrame("button_music_off")
        }
    },
    resumeSoundCallback: function () {
        soundMuted = false
        if (!isMuted) {
            music.resume()
            mute.setFrame("button_music_on")
        } else {
            music.pause()
            mute.setFrame("button_music_off")
        }
    },
    blockClicksCallback: function () {
        hitobject.visible = true
    },
    allowClicksCallback: function () {
        hitobject.visible = false
    }
}
var testing = true;
WitchSDK.initGame(settings, testing);
var brandingMaterials = WitchSDK.getBrandingMaterials();
var preloaderLogo1 = brandingMaterials.preloaderLogo.image;
var html5GamesButton1 = brandingMaterials.html5GamesButton.image;
var partnerLogo1 = brandingMaterials.partnerLogo.image;
var coverLogo1 = brandingMaterials.coverLogo.image;
var inGameLogo1 = brandingMaterials.inGameLogo.image;
var thumbnails1 = brandingMaterials.thumbnails[0].image;
var thumbnails2 = brandingMaterials.thumbnails[1].image;
var moreGamesButton1 = brandingMaterials.moreGamesButton.image;
var buttonjson = WitchSDK.getUI().jsonURL
var buttonimage = WitchSDK.getUI().imageURL
var bootState = new Phaser.Class({
    Extends: Phaser.Scene,
    initialize: function bootState() {
        Phaser.Scene.call(this, {
            key: 'bootState'
        });
    },
    preload: function () {
        this.load.crossOrigin = 'anonymous';
        if (brandingMaterials.preloaderLogo.display == true && brandingMaterials.preloaderLogo.image != undefined) {
            this.load.image('preloaderLogo1', preloaderLogo1);
        }
        this.load.atlas('uiButton', buttonimage, buttonjson);
    },
    create: function () {
        this.cameras.main.setBackgroundColor('#FFFFFF')
        this.scene.start('preLoader');
    }
});
var preLoader = new Phaser.Class({
    Extends: Phaser.Scene,
    initialize: function preLoader() {
        Phaser.Scene.call(this, {
            key: 'preLoader'
        });
    },
    preload: function () {},
    create: function () {
        this.cameras.main.setBackgroundColor('#FFFFFF')
        if (brandingMaterials.preloaderLogo.display == true && brandingMaterials.preloaderLogo.image != undefined) {
            preloaderLogo1 = this.add.image(400, 300, 'preloaderLogo1').setInteractive({
                pixelPerfect: true,
                useHandCursor: true
            })
            preloaderLogo1.setOrigin(0.5);
            preloaderLogo1.on('pointerdown', brandingMaterials.preloaderLogo.onClick);
        }
        loaderbar = this.add.sprite(399, 480, 'uiButton').setFrame("preloader_bar")
        loaderbar.x -= 200;
        maskShape = this.make.graphics();
        maskShape.fillRect(307, 466, 189, 29);
        maskShape.fillStyle(0x0000ff, 1);
        loaderbar.mask = new Phaser.Display.Masks.GeometryMask(loaderbar, maskShape);
        continuebtn = this.add.sprite(399, 480, 'uiButton').setFrame("continue-button").setInteractive({
            pixelPerfect: true,
            useHandCursor: true
        })
        continuebtn.setScale(1.1)
        continuebtn.visible = false
        WitchSDK.recordEvent(WitchSDK.Events.PRELOAD_START);
        this.load.on('progress', function (value) {
            loaderbar.x = 200 + parseInt(parseFloat(value / 1) * 200);
        });
        this.load.on('complete', function () {
            WitchSDK.recordEvent(WitchSDK.Events.PRELOAD_END);
            if (pageNo == 0) {
                continuebtn.visible = true
                continuebtn.on('pointerover', function () {
                    continuebtn.setScale(1.12)
                }, this);
                continuebtn.on('pointerout', function () {
                    continuebtn.setScale(1.1)
                }, this);
                continuebtn.once('pointerdown', function () {
                    preloaderLogo1.destroy();
                    continuebtn.destroy();
                    loaderbar.destroy();
                    this.scene.scene.stop('preLoader')
                    game.scene.start('titleScreen');
                }, this);
            } else {
                loadFinish = true;
            }
        });
        if (brandingMaterials.html5GamesButton.display == true && brandingMaterials.html5GamesButton.image != undefined) {
            this.load.image('html5GamesButton1', html5GamesButton1);
        }
        if (brandingMaterials.partnerLogo.display == true && brandingMaterials.partnerLogo.image != undefined) {
            this.load.image('partnerLogo1', partnerLogo1);
        }
        if (brandingMaterials.coverLogo.display == true && brandingMaterials.coverLogo.image != undefined) {
            this.load.image('coverLogo1', coverLogo1);
        }
        this.load.image('titlescreenbg', 'assets/titlescreen/background.jpg');
        this.load.image('rays1', 'assets/titlescreen/rays1.png');
        this.load.image('rays3', 'assets/titlescreen/rays3.png');
        this.load.audio('boden', ['assets/audio/bmusic.mp3', 'assets/audio/bmusic.ogg']);
        this.load.audio('clickss', ['assets/audio/click.mp3', 'assets/audio/click.ogg']);
        this.load.audio('glittersound', ['assets/audio/glitter.mp3', 'assets/audio/glitter.ogg']);
        this.load.image('transitionbackground1', 'assets/transition/background1.jpg');
        this.load.image('transitionbackground2', 'assets/transition/background2.jpg');
        this.load.image('transitionbackground3', 'assets/transition/background3.jpg');
        this.load.image('transitiondress', 'assets/transition/dress.png');
        this.load.image('transitionpanel', 'assets/transition/panel.png');
        this.load.image('transitionstar', 'assets/transition/star.png');
        this.load.image('title1', 'assets/titlescreen/title1.png');
        this.load.image('title2', 'assets/titlescreen/title2.png');
        this.load.image('title3', 'assets/titlescreen/title3.png');
        this.load.image('titleobj', 'assets/titlescreen/titleobj.png');
        this.load.image('titleobj1', 'assets/titlescreen/titleobj1.png');
        this.load.image('hitobject', 'assets/titlescreen/hitobject.png');
        this.load.spritesheet('glitter', 'assets/level1/gilter.png', {
            frameWidth: 432,
            frameHeight: 439
        });
        this.load.image('levelbackground', 'assets/levelselect/background.jpg');
        this.load.image('leveltick', 'assets/levelselect/tick.png');
        this.load.image('levellock', 'assets/levelselect/lock.png');
        this.load.spritesheet('level1', 'assets/levelselect/level1.png', {
            frameWidth: 183,
            frameHeight: 172
        });
        this.load.spritesheet('level2', 'assets/levelselect/level2.png', {
            frameWidth: 183,
            frameHeight: 172
        });
        this.load.spritesheet('level3', 'assets/levelselect/level3.png', {
            frameWidth: 189,
            frameHeight: 167
        });
        this.load.spritesheet('level4', 'assets/levelselect/level4.png', {
            frameWidth: 189,
            frameHeight: 167
        });
        this.load.spritesheet('level5', 'assets/levelselect/level5.png', {
            frameWidth: 189,
            frameHeight: 167
        });
        this.load.spritesheet('level6', 'assets/levelselect/level6.png', {
            frameWidth: 197,
            frameHeight: 186
        });
        this.load.spritesheet('level7', 'assets/levelselect/level7.png', {
            frameWidth: 197,
            frameHeight: 170
        });
        this.load.spritesheet('level8', 'assets/levelselect/level8.png', {
            frameWidth: 197,
            frameHeight: 170
        });
        this.load.image('level1body', 'assets/l1/doll1/level1body3.png');
        // this.load.spritesheet('level1lips', 'assets/l1/doll1/lilibs.png', {
        //     frameWidth: 216,
        //     frameHeight: 152
        // });

        this.load.spritesheet('level1lips', 'assets/l1/doll1/arial-lips.png', {
            frameWidth: 176,
            frameHeight: 94
        });

        this.load.image('l1eye_white', 'assets/l1/doll1/l1eye_white.png');
        // this.load.spritesheet('level1eye1', 'assets/level1/doll1/eye1.png', {
        //     frameWidth: 164,
        //     frameHeight: 79
        // });

        this.load.spritesheet('level1eye1', 'assets/l1/doll1/l1eye_lens.png', {
            frameWidth: 205,
            frameHeight: 134,
        });

        this.load.spritesheet('level1eye2', 'assets/level1/doll1/eye2.png', {
            frameWidth: 164,
            frameHeight: 79
        });
        this.load.image('level1toplayer', 'assets/l1/doll1/toplayer.png');
        this.load.spritesheet('level1eshade', 'assets/l1/doll1/eshade.png', {
            frameWidth: 218,
            frameHeight: 131
        });
        // this.load.spritesheet('level1elash', 'assets/level1/doll1/elash.png', {
        //     frameWidth: 162,
        //     frameHeight: 59
        // });

        // this.load.spritesheet('level1elash', 'assets/l1/doll1/l1eye_lash.png', {
        //     frameWidth: 205,
        //     frameHeight: 134
        // });

        this.load.spritesheet('level1elash', 'assets/l1/doll1/l1eye_lash1.png', {
            frameWidth: 232,
            frameHeight: 124
        });
        // this.load.spritesheet('level1blush', 'assets/level1/doll1/blush.png', {
        //     frameWidth: 164,
        //     frameHeight: 68
        // });

        this.load.spritesheet('level1blush', 'assets/l1/doll1/l1blush.png', {
            frameWidth: 205,
            frameHeight: 134
        });

        // this.load.spritesheet('level1ebrow', 'assets/level1/doll1/ebrow.png', {
        //     frameWidth: 167,
        //     frameHeight: 44
        // });
        this.load.image('l1b_hair', 'assets/l1/doll1/l1b_hair.png');
        this.load.image('level1hair', 'assets/l1/doll1/l1hair.png');
        this.load.image('l1dress', 'assets/l1/doll1/l1dress.png');

        this.load.spritesheet('level1ebrow', 'assets/l1/doll1/l1eyebrow.png', {
            frameWidth: 239,
            frameHeight: 104
        });

        // this.load.spritesheet('level1hair', 'assets/level1/doll1/hair.png', {
        //     frameWidth: 380,
        //     frameHeight: 748
        // });
        if (brandingMaterials.inGameLogo.display == true && brandingMaterials.inGameLogo.image != undefined) {
            this.load.image('inGameLogo1', inGameLogo1);
        }
        this.load.image('level1cupboard', 'assets/l1/cupboard.png');
        for (i = 1; i <= 6; i++) {
            this.load.image('level1item' + i, 'assets/level1/item' + i + '.png');
        }
        this.load.image('table', 'assets/level1/table.png');
        for (i = 1; i <= 6; i++) {
            this.load.image('category' + i, 'assets/l1/item/m_item' + i + '.png');
            console.log("category' + i,", i)
        }
        this.load.image('shadow', 'assets/level1/shadow.png');
        this.load.image('shadow1', 'assets/level1/shadow1.png');
        this.load.image('shadow2', 'assets/level1/shadow2.png');
        this.load.image('shadow3', 'assets/level1/shadow3.png');
        this.load.image('shadow4', 'assets/level1/shadow4.png');
        this.load.image('elsahairbtn', 'assets/level1/elsahairbtn.png');
        this.load.image('arielhairbtn', 'assets/level1/arielhairbtn.png');
        this.load.image('annahairbtn', 'assets/level1/annahairbtn.png');
        this.load.image('rapunhairbtn', 'assets/level1/rapunhairbtn.png');
        for (i = 1; i <= 7; i++) {
            this.load.image('elash' + i, 'assets/l1/item/mascara' + i + '.png');
        }
        for (i = 1; i <= 10; i++) {
            this.load.image('blush' + i, 'assets/l1/item/blush' + i + '.png');
        }
        for (i = 1; i <= 10; i++) {
            this.load.image('lip' + i, 'assets/l1/item/lips' + i + '.png');
        }
        for (i = 1; i <= 10; i++) {
            this.load.image('eye' + i, 'assets/l1/item/e_lens' + i + '.png');
        }
        for (i = 1; i <= 10; i++) {
            this.load.image('eshade' + i, 'assets/l1/item/eyeshadow' + i + '.png');
        }
        for (i = 1; i <= 10; i++) {
            this.load.image('ebrow' + i, 'assets/l1/item/pencil' + i + '.png');
        }
        for (i = 1; i <= 5; i++) {
            this.load.image('elsahair' + i, 'assets/level1/elsahair' + i + '.png');
        }
        for (i = 1; i <= 5; i++) {
            this.load.image('arielhair' + i, 'assets/level1/arielhair' + i + '.png');
        }
        for (i = 1; i <= 5; i++) {
            this.load.image('annahair' + i, 'assets/level1/annahair' + i + '.png');
        }
        for (i = 1; i <= 5; i++) {
            this.load.image('rapunhair' + i, 'assets/level1/rapunhair' + i + '.png');
        }
        //dressupcupboard
        this.load.image('elsabackground1', 'assets/l1/bg1.png');
        this.load.image('dressuppanel', 'assets/l1/cupboard.png');
        this.load.image('colortext', 'assets/level2/cupboard1/colortext.png');
        this.load.spritesheet('tailorbtn', 'assets/level2/cupboard1/tailorbtn.png', {
            frameWidth: 97,
            frameHeight: 98
        });
        this.load.spritesheet('cutbtn', 'assets/level2/cupboard1/cutbtn.png', {
            frameWidth: 97,
            frameHeight: 98
        });
        this.load.image('color1', 'assets/level2/cupboard1/color1.png');
        this.load.image('color2', 'assets/level2/cupboard1/color2.png');
        this.load.image('color3', 'assets/level2/cupboard1/color3.png');
        this.load.image('color4', 'assets/level2/cupboard1/color4.png');
        this.load.image('color5', 'assets/level2/cupboard1/color5.png');
        this.load.image('color6', 'assets/level2/cupboard1/color6.png');
        this.load.image('color7', 'assets/level2/cupboard1/color7.png');
        this.load.image('color8', 'assets/level2/cupboard1/color8.png');
        this.load.image('color9', 'assets/level2/cupboard1/color9.png');
        this.load.image('color10', 'assets/level2/cupboard1/color10.png');
        this.load.image('color11', 'assets/level2/cupboard1/color11.png');
        this.load.image('color12', 'assets/level2/cupboard1/color12.png');
        this.load.image('color13', 'assets/level2/cupboard1/color13.png');
        this.load.image('color14', 'assets/level2/cupboard1/color14.png');
        this.load.image('color15', 'assets/level2/cupboard1/color15.png');
        this.load.image('color16', 'assets/level2/cupboard1/color16.png');
        this.load.image('color17', 'assets/level2/cupboard1/color17.png');
        this.load.image('dressphotoframe', 'assets/level2/cupboard1/photoframe.png');
        this.load.image('arrow', 'assets/level2/cupboard1/arrow.png');
        this.load.spritesheet('doll', 'assets/level2/cupboard1/doll.png', {
            frameWidth: 165,
            frameHeight: 280
        });
        //elsadoll
        this.load.image('elsabody', 'assets/level2/cupboard1/doll/body.png');
        this.load.spritesheet('elsalips', 'assets/level2/cupboard1/doll/lips.png', {
            frameWidth: 52,
            frameHeight: 30
        });
        this.load.spritesheet('elsaeye1', 'assets/level2/cupboard1/doll/eye1.png', {
            frameWidth: 55,
            frameHeight: 23
        });
        this.load.spritesheet('elsaeye2', 'assets/level2/cupboard1/doll/eye2.png', {
            frameWidth: 55,
            frameHeight: 23
        });
        this.load.image('elsatoplayer', 'assets/level2/cupboard1/doll/toplayer.png');
        this.load.spritesheet('elsaeshade', 'assets/level2/cupboard1/doll/eshade.png', {
            frameWidth: 72,
            frameHeight: 28
        });
        this.load.spritesheet('elsaelash', 'assets/level2/cupboard1/doll/elash.png', {
            frameWidth: 71,
            frameHeight: 28
        });
        this.load.spritesheet('elsablush', 'assets/level2/cupboard1/doll/blush.png', {
            frameWidth: 68,
            frameHeight: 30
        });
        this.load.spritesheet('elsaebrow', 'assets/level2/cupboard1/doll/ebrow.png', {
            frameWidth: 69,
            frameHeight: 21
        });
        this.load.spritesheet('elsahair', 'assets/level2/cupboard1/doll/hair.png', {
            frameWidth: 164,
            frameHeight: 299
        });
        this.load.image('elsachain', 'assets/level2/cupboard1/doll/chain.png');
        this.load.image('elsaline', 'assets/level2/cupboard1/doll/line.png');
        this.load.image('elsahit1', 'assets/level2/cupboard1/hit.png');
        this.load.spritesheet('elsasegment1dress1', 'assets/level2/cupboard1/doll/elsasegment1dress1.png', {
            frameWidth: 116,
            frameHeight: 84
        });
        this.load.spritesheet('elsasegment2dress2', 'assets/level2/cupboard1/doll/elsasegment2dress2.png', {
            frameWidth: 74,
            frameHeight: 41
        });
        this.load.spritesheet('elsasegment3dress3', 'assets/level2/cupboard1/doll/elsasegment3dress3.png', {
            frameWidth: 152,
            frameHeight: 175
        });
        this.load.spritesheet('elsasegment4dress4', 'assets/level2/cupboard1/doll/elsasegment4dress4.png', {
            frameWidth: 159,
            frameHeight: 147
        });
        this.load.spritesheet('elsasegment5dress5', 'assets/level2/cupboard1/doll/elsasegment5dress5.png', {
            frameWidth: 192,
            frameHeight: 143
        });
        this.load.spritesheet('elsasegment6dress6', 'assets/level2/cupboard1/doll/elsasegment6dress6.png', {
            frameWidth: 69,
            frameHeight: 104
        });
        this.load.spritesheet('elsasegment7dress7', 'assets/level2/cupboard1/doll/elsasegment7dress7.png', {
            frameWidth: 91,
            frameHeight: 133
        });
        this.load.spritesheet('elsasegment8dress8', 'assets/level2/cupboard1/doll/elsasegment8dress8.png', {
            frameWidth: 128,
            frameHeight: 146
        });
        this.load.spritesheet('elsasegment9dress9', 'assets/level2/cupboard1/doll/elsasegment9dress9.png', {
            frameWidth: 78,
            frameHeight: 114
        });
        this.load.spritesheet('elsasegment10dress10', 'assets/level2/cupboard1/doll/elsasegment10dress10.png', {
            frameWidth: 102,
            frameHeight: 114
        });
        this.load.spritesheet('elsasegment11dress11', 'assets/level2/cupboard1/doll/elsasegment11dress11.png', {
            frameWidth: 103,
            frameHeight: 132
        });
        //arieldoll
        this.load.image('arielbackground1', 'assets/level2/cupboard2/background.jpg');
        this.load.spritesheet('arieldoll', 'assets/level2/cupboard2/doll.png', {
            frameWidth: 156,
            frameHeight: 254
        });
        this.load.spritesheet('arielbhair', 'assets/level2/cupboard2/doll/bhair.png', {
            frameWidth: 125,
            frameHeight: 103
        });
        this.load.image('arielgilter', 'assets/level2/cupboard2/doll/gilter.png');
        this.load.image('arielbody', 'assets/level2/cupboard2/doll/body.png');
        this.load.image('arielcrown', 'assets/level2/cupboard2/doll/crown.png');
        this.load.spritesheet('ariellips', 'assets/level2/cupboard2/doll/lips.png', {
            frameWidth: 58,
            frameHeight: 34
        });
        this.load.spritesheet('arieleye1', 'assets/level2/cupboard2/doll/eye1.png', {
            frameWidth: 54,
            frameHeight: 25
        });
        this.load.spritesheet('arieleye2', 'assets/level2/cupboard2/doll/eye2.png', {
            frameWidth: 54,
            frameHeight: 25
        });
        this.load.spritesheet('arielblush', 'assets/level2/cupboard2/doll/blush.png', {
            frameWidth: 63,
            frameHeight: 28
        });
        this.load.image('arieltoplayer', 'assets/level2/cupboard2/doll/toplayer.png');
        this.load.spritesheet('arieleshade', 'assets/level2/cupboard2/doll/eshade.png', {
            frameWidth: 69,
            frameHeight: 27
        });
        this.load.spritesheet('arielelash', 'assets/level2/cupboard2/doll/elash.png', {
            frameWidth: 68,
            frameHeight: 29
        });
        this.load.spritesheet('arielebrow', 'assets/level2/cupboard2/doll/ebrow.png', {
            frameWidth: 64,
            frameHeight: 18
        });
        this.load.spritesheet('arielhair', 'assets/level2/cupboard2/doll/hair.png', {
            frameWidth: 155,
            frameHeight: 259
        });
        this.load.spritesheet('arielsegment1dress1', 'assets/level2/cupboard2/doll/arielsegment1dress1.png', {
            frameWidth: 135,
            frameHeight: 92
        });
        this.load.spritesheet('arielsegment2dress2', 'assets/level2/cupboard2/doll/arielsegment2dress2.png', {
            frameWidth: 75,
            frameHeight: 44
        });
        this.load.spritesheet('arielsegment3dress3', 'assets/level2/cupboard2/doll/arielsegment3dress3.png', {
            frameWidth: 160,
            frameHeight: 185
        });
        this.load.spritesheet('arielsegment4dress4', 'assets/level2/cupboard2/doll/arielsegment4dress4.png', {
            frameWidth: 206,
            frameHeight: 177
        });
        this.load.spritesheet('arielsegment5dress5', 'assets/level2/cupboard2/doll/arielsegment5dress5.png', {
            frameWidth: 213,
            frameHeight: 167
        });
        this.load.spritesheet('arielsegment6dress6', 'assets/level2/cupboard2/doll/arielsegment6dress6.png', {
            frameWidth: 103,
            frameHeight: 135
        });
        this.load.spritesheet('arielsegment7dress7', 'assets/level2/cupboard2/doll/arielsegment7dress7.png', {
            frameWidth: 101,
            frameHeight: 160
        });
        this.load.spritesheet('arielsegment8dress8', 'assets/level2/cupboard2/doll/arielsegment8dress8.png', {
            frameWidth: 113,
            frameHeight: 149
        });
        this.load.spritesheet('arielsegment9dress9', 'assets/level2/cupboard2/doll/arielsegment9dress9.png', {
            frameWidth: 101,
            frameHeight: 131
        });
        this.load.spritesheet('arielsegment10dress10', 'assets/level2/cupboard2/doll/arielsegment10dress10.png', {
            frameWidth: 109,
            frameHeight: 147
        });
        this.load.spritesheet('arielsegment11dress11', 'assets/level2/cupboard2/doll/arielsegment11dress11.png', {
            frameWidth: 120,
            frameHeight: 142
        });
        //annadoll
        this.load.image('annabackground1', 'assets/l3/background.png');
        this.load.spritesheet('annadoll', 'assets/level2/cupboard3/doll.png', {
            frameWidth: 165,
            frameHeight: 271
        });
        this.load.image('annabody', 'assets/level2/cupboard3/doll/body.png');
        this.load.image('annachain', 'assets/level2/cupboard3/doll/chain.png');
        this.load.image('annahand', 'assets/level2/cupboard3/doll/hand.png');
        this.load.image('annahand1', 'assets/level2/cupboard3/doll/hand1.png');
        this.load.spritesheet('annalips', 'assets/level2/cupboard3/doll/lips.png', {
            frameWidth: 51,
            frameHeight: 23
        });
        this.load.spritesheet('annaeye1', 'assets/level2/cupboard3/doll/eye1.png', {
            frameWidth: 55,
            frameHeight: 24
        });
        this.load.spritesheet('annaeye2', 'assets/level2/cupboard3/doll/eye2.png', {
            frameWidth: 55,
            frameHeight: 24
        });
        this.load.spritesheet('annablush', 'assets/level2/cupboard3/doll/blush.png', {
            frameWidth: 69,
            frameHeight: 35
        });
        this.load.image('annatoplayer', 'assets/level2/cupboard3/doll/toplayer.png');
        this.load.spritesheet('annaeshade', 'assets/level2/cupboard3/doll/eshade.png', {
            frameWidth: 71,
            frameHeight: 30
        });
        this.load.spritesheet('annaelash', 'assets/level2/cupboard3/doll/elash.png', {
            frameWidth: 67,
            frameHeight: 34
        });
        this.load.spritesheet('annaebrow', 'assets/level2/cupboard3/doll/ebrow.png', {
            frameWidth: 63,
            frameHeight: 19
        });
        this.load.spritesheet('annahair', 'assets/level2/cupboard3/doll/hair.png', {
            frameWidth: 137,
            frameHeight: 243
        });
        this.load.spritesheet('annasegment1dress1', 'assets/level2/cupboard3/doll/annasegment1dress1.png', {
            frameWidth: 143,
            frameHeight: 107
        });
        this.load.spritesheet('annasegment2dress2', 'assets/level2/cupboard3/doll/annasegment2dress2.png', {
            frameWidth: 74,
            frameHeight: 37
        });
        this.load.spritesheet('annasegment3dress3', 'assets/level2/cupboard3/doll/annasegment3dress3.png', {
            frameWidth: 169,
            frameHeight: 177
        });
        this.load.spritesheet('annasegment4dress4', 'assets/level2/cupboard3/doll/annasegment4dress4.png', {
            frameWidth: 187,
            frameHeight: 151
        });
        this.load.spritesheet('annasegment5dress5', 'assets/level2/cupboard3/doll/annasegment5dress5.png', {
            frameWidth: 209,
            frameHeight: 172
        });
        this.load.spritesheet('annasegment6dress6', 'assets/level2/cupboard3/doll/annasegment6dress6.png', {
            frameWidth: 94,
            frameHeight: 122
        });
        this.load.spritesheet('annasegment7dress7', 'assets/level2/cupboard3/doll/annasegment7dress7.png', {
            frameWidth: 132,
            frameHeight: 173
        });
        this.load.spritesheet('annasegment8dress8', 'assets/level2/cupboard3/doll/annasegment8dress8.png', {
            frameWidth: 108,
            frameHeight: 122
        });
        this.load.spritesheet('annasegment9dress9', 'assets/level2/cupboard3/doll/annasegment9dress9.png', {
            frameWidth: 159,
            frameHeight: 150
        });
        //rapundoll
        this.load.image('rapunbackground1', 'assets/level2/cupboard4/background.jpg');
        this.load.spritesheet('rapundoll', 'assets/level2/cupboard4/doll.png', {
            frameWidth: 132,
            frameHeight: 268
        });
        this.load.spritesheet('rapunbhair', 'assets/level2/cupboard4/doll/bhair.png', {
            frameWidth: 140,
            frameHeight: 137
        });
        this.load.image('rapunbody', 'assets/level2/cupboard4/doll/body.png');
        this.load.image('rapuncrown', 'assets/level2/cupboard4/doll/crown.png');
        this.load.spritesheet('rapunlips', 'assets/level2/cupboard4/doll/lips.png', {
            frameWidth: 61,
            frameHeight: 36
        });
        this.load.spritesheet('rapuneye1', 'assets/level2/cupboard4/doll/eye1.png', {
            frameWidth: 57,
            frameHeight: 23
        });
        this.load.spritesheet('rapuneye2', 'assets/level2/cupboard4/doll/eye2.png', {
            frameWidth: 57,
            frameHeight: 23
        });
        this.load.spritesheet('rapunblush', 'assets/level2/cupboard4/doll/blush.png', {
            frameWidth: 68,
            frameHeight: 31
        });
        this.load.image('rapuntoplayer', 'assets/level2/cupboard4/doll/toplayer.png');
        this.load.spritesheet('rapuneshade', 'assets/level2/cupboard4/doll/eshade.png', {
            frameWidth: 77,
            frameHeight: 35
        });
        this.load.spritesheet('rapunelash', 'assets/level2/cupboard4/doll/elash.png', {
            frameWidth: 71,
            frameHeight: 27
        });
        this.load.spritesheet('rapunebrow', 'assets/level2/cupboard4/doll/ebrow.png', {
            frameWidth: 74,
            frameHeight: 18
        });
        this.load.spritesheet('rapunhair', 'assets/level2/cupboard4/doll/hair.png', {
            frameWidth: 155,
            frameHeight: 290
        });
        this.load.spritesheet('rapunsegment1dress1', 'assets/level2/cupboard4/doll/rapunsegment1dress1.png', {
            frameWidth: 128,
            frameHeight: 116
        });
        this.load.spritesheet('rapunsegment2dress2', 'assets/level2/cupboard4/doll/rapunsegment2dress2.png', {
            frameWidth: 73,
            frameHeight: 43
        });
        this.load.spritesheet('rapunsegment3dress3', 'assets/level2/cupboard4/doll/rapunsegment3dress3.png', {
            frameWidth: 181,
            frameHeight: 179
        });
        this.load.spritesheet('rapunsegment4dress4', 'assets/level2/cupboard4/doll/rapunsegment4dress4.png', {
            frameWidth: 186,
            frameHeight: 158
        });
        this.load.spritesheet('rapunsegment5dress5', 'assets/level2/cupboard4/doll/rapunsegment5dress5.png', {
            frameWidth: 226,
            frameHeight: 162
        });
        this.load.spritesheet('rapunsegment6dress6', 'assets/level2/cupboard4/doll/rapunsegment6dress6.png', {
            frameWidth: 113,
            frameHeight: 151
        });
        this.load.spritesheet('rapunsegment7dress7', 'assets/level2/cupboard4/doll/rapunsegment7dress7.png', {
            frameWidth: 116,
            frameHeight: 170
        });
        this.load.image('level2bhair', 'assets/l3/hairlayer.png')
        this.load.image('raqdress','assets/l3/raqdress.png');
        //  {
        //     frameWidth: 308,
        //     frameHeight: 272
        // });
        this.load.image('level2body', 'assets/l3/raqdoll.png');
        // this.load.spritesheet('level2lips', 'assets/level1/doll2/lips.png', {
        //     frameWidth: 143,
        //     frameHeight: 85
        // });

        this.load.spritesheet('level2lips','assets/l3/scene1/sprites/lipssheet.png',{
            frameWidth:199,
            frameHeight:100
        
        })

        // this.load.spritesheet('level2eye1', 'assets/level1/doll2/eye1.png', {
        //     frameWidth: 126,
        //     frameHeight: 44
        // });

        this.load.spritesheet('level2eye1','assets/l3/scene1/sprites/eyelenssheet.png',{
            frameWidth:208,
            frameHeight:64
        
        })
        
        this.load.spritesheet('level2eye2', 'assets/level1/doll2/eye2.png', {
            frameWidth: 126,
            frameHeight: 44
        });
        // this.load.spritesheet('level2blush', 'assets/level1/doll2/blush.png', {
        //     frameWidth: 146,
        //     frameHeight: 65
        // });

        this.load.spritesheet('level2blush','assets/l3/scene1/sprites/blushsheet.png',{
            frameWidth: 146,
            frameHeight: 65
        
        })

        // this.load.spritesheet('level2eshade', 'assets/level1/doll2/eshade.png', {
        //     frameWidth: 172,
        //     frameHeight: 75
        // });

        // this.load.spritesheet('level2eshade','assets/l3/scene1/sprites/eyebrowsheet.png',{
        //     frameWidth: 172,
        //     frameHeight: 75
        
        // }) 

        this.load.spritesheet('level2eshade','assets/l3/scene1/sprites/eyeshadowsheet.png',{
            frameWidth:259,
            frameHeight:136
        
        })

        // this.load.spritesheet('level2elash', 'assets/level1/doll2/elash.png', {
        //     frameWidth: 166,
        //     frameHeight: 68
        // });

        this.load.spritesheet('level2elash','assets/l3/scene1/sprites/mascarasheet.png',{
            frameWidth:200,
            frameHeight:87
        
        })

        // this.load.spritesheet('level2ebrow', 'assets/level1/doll2/ebrow.png', {
        //     frameWidth: 156,
        //     frameHeight: 35
        // });

        this.load.spritesheet('level2ebrow','assets/l3/scene1/sprites/eyebrowsheet.png',{
            frameWidth:235,
            frameHeight:70
        
        }) 
        // this.load.spritesheet('level2hair', 'assets/level1/doll2/hair.png', {
        //     frameWidth: 404,
        //     frameHeight: 648
        // });

        this.load.spritesheet('level2hair','assets/l3/scene1/sprites/hairsheet.png',{
            frameWidth:496,
            frameHeight:727
        
        })

        this.load.image('level2toplayer', 'assets/l3/eyelayer.png');
        this.load.image('level3body', 'assets/l3/raqdoll.png');
        // this.load.image('raqdress','assets/l3/raqdress.png');
        this.load.image('level3hand', 'assets/level1/doll3/hand.png');
        this.load.spritesheet('level3eye1', 'assets/level1/doll3/eye1.png', {
            frameWidth: 120,
            frameHeight: 44
        });
        this.load.spritesheet('level3eye2', 'assets/level1/doll3/eye2.png', {
            frameWidth: 120,
            frameHeight: 44
        });
        this.load.spritesheet('level3eshade', 'assets/level1/doll3/eshade.png', {
            frameWidth: 162,
            frameHeight: 67
        });
        this.load.image('level3toplayer', 'assets/level1/doll3/toplayer.png');
        this.load.spritesheet('level3lips', 'assets/level1/doll3/lips.png', {
            frameWidth: 124,
            frameHeight: 54
        });
        this.load.spritesheet('level3ebrow', 'assets/level1/doll3/ebrow.png', {
            frameWidth: 149,
            frameHeight: 37
        });
        this.load.spritesheet('level3elash', 'assets/level1/doll3/elash.png', {
            frameWidth: 162,
            frameHeight: 65
        });
        this.load.spritesheet('level3blush', 'assets/level1/doll3/blush.png', {
            frameWidth: 169,
            frameHeight: 92
        });
        this.load.spritesheet('level3hair', 'assets/level1/doll3/hair.png', {
            frameWidth: 349,
            frameHeight: 612
        });
        this.load.spritesheet('level4bhair', 'assets/level1/doll4/bhair.png', {
            frameWidth: 362,
            frameHeight: 322
        });
        this.load.image('level4body', 'assets/level1/doll4/body.png');
        this.load.spritesheet('level4blush', 'assets/level1/doll4/blush.png', {
            frameWidth: 153,
            frameHeight: 63
        });
        this.load.spritesheet('level4eye1', 'assets/level1/doll4/eye1.png', {
            frameWidth: 143,
            frameHeight: 56
        });
        this.load.spritesheet('level4eye2', 'assets/level1/doll4/eye2.png', {
            frameWidth: 143,
            frameHeight: 56
        });
        this.load.image('level4toplayer', 'assets/level1/doll4/toplayer.png');
        this.load.spritesheet('level4eshade', 'assets/level1/doll4/eshade.png', {
            frameWidth: 197,
            frameHeight: 79
        });
        this.load.spritesheet('level4lips', 'assets/level1/doll4/lips.png', {
            frameWidth: 142,
            frameHeight: 84
        });
        this.load.spritesheet('level4elash', 'assets/level1/doll4/elash.png', {
            frameWidth: 178,
            frameHeight: 69
        });
        this.load.spritesheet('level4ebrow', 'assets/level1/doll4/ebrow.png', {
            frameWidth: 177,
            frameHeight: 33
        });
        this.load.spritesheet('level4hair', 'assets/level1/doll4/hair.png', {
            frameWidth: 394,
            frameHeight: 752
        });
        this.load.image('endbackground', 'assets/endscreen/background.jpg');
        this.load.image('stand1', 'assets/endscreen/stand1.png');
        this.load.image('light', 'assets/endscreen/light.png');
        this.load.image('stand2', 'assets/endscreen/stand2.png');
        this.load.image('bar', 'assets/endscreen/bar.png');
        if (brandingMaterials.thumbnails[0].display == true && brandingMaterials.thumbnails[0].image != undefined) {
            this.load.image('thumbnails1', thumbnails1);
        }
        if (brandingMaterials.thumbnails[1].display == true && brandingMaterials.thumbnails[1].image != undefined) {
            this.load.image('thumbnails2', thumbnails2);
        }
        if (brandingMaterials.moreGamesButton.display == true && brandingMaterials.moreGamesButton.image != undefined) {
            this.load.image('moreGamesButton1', moreGamesButton1);
        }
        this.load.start();
    }
});
var startgame1 = false;
var marr = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
var darr = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
var levelEnter = false
var i = 0;
var t1;
var titleScreen = new Phaser.Class({
    Extends: Phaser.Scene,
    initialize: function titleScreen() {
        Phaser.Scene.call(this, {
            key: 'titleScreen'
        });
    },
    preload: function () {
        levelEnter = false
        pageNo = 1;
        startgame1 = false;
        setcount = false
        level = 1
        max = 0;
        update_interval = 4 * 60;
        i = 0;
        marr = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        marr1 = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        marr2 = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        marr3 = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        marr4 = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        darr = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        darr1 = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        darr2 = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        darr3 = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        darr4 = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        dollcount = 0
        loadFinish = false
    },
    create: function () {
        WitchSDK.recordEvent(WitchSDK.Events.MAIN_MENU);
        titlescreenbg = this.add.image(400, 300, 'titlescreenbg');
        titlescreenbg.setOrigin(0.5);
        rays1 = this.add.image(140, 120, 'rays1');
        rays2 = this.add.image(700, 120, 'rays1');
        rays2.setScale(-1, 1)
        rays3 = this.add.image(405, -105, 'rays3');
        rays3.setOrigin(0.5, 0)
        rays3.angle = -4
        this.tweens.add({
            targets: rays3,
            angle: 4,
            ease: 'Linear',
            duration: 2000,
            repeat: -1,
            yoyo: true,
            callbackScope: this
        });
        this.tweens.add({
            targets: rays1,
            angle: 14,
            ease: 'Linear',
            duration: 700,
            repeat: -1,
            yoyo: true,
            callbackScope: this
        });
        this.tweens.add({
            targets: rays2,
            angle: 14,
            ease: 'Linear',
            duration: 700,
            repeat: -1,
            yoyo: true,
            callbackScope: this
        });
        elsabody = this.add.image(101, 37, 'elsabody').setOrigin(0, 0)
        elsalips = this.add.image(193, 113, 'elsalips').setOrigin(0, 0)
        elsaeye1 = this.add.image(192, 90, 'elsaeye1').setOrigin(0, 0)
        elsaeye2 = this.add.image(192, 90, 'elsaeye2').setOrigin(0, 0)
        elsatoplayer = this.add.image(185, 85, 'elsatoplayer').setOrigin(0, 0)
        elsaeshade = this.add.image(184, 87, 'elsaeshade').setOrigin(0, 0)
        elsaelash = this.add.image(185, 86, 'elsaelash').setOrigin(0, 0)
        elsablush = this.add.image(188, 101, 'elsablush').setOrigin(0, 0)
        elsaebrow = this.add.image(185, 76, 'elsaebrow').setOrigin(0, 0)
        elsasegment1dress1 = this.add.image(181, 147, 'elsasegment1dress1').setOrigin(0, 0)
        elsasegment2dress2 = this.add.image(188, 215, 'elsasegment2dress2').setOrigin(0, 0)
        elsasegment3dress3 = this.add.image(143, 233, 'elsasegment3dress3').setOrigin(0, 0)
        elsasegment4dress4 = this.add.image(144, 352, 'elsasegment4dress4').setOrigin(0, 0)
        elsasegment5dress5 = this.add.image(131, 443, 'elsasegment5dress5').setOrigin(0, 0)
        elsasegment6dress6 = this.add.image(121, 264, 'elsasegment6dress6').setOrigin(0, 0)
        elsasegment7dress7 = this.add.image(95, 341, 'elsasegment7dress7').setOrigin(0, 0)
        elsasegment8dress8 = this.add.image(74, 427, 'elsasegment8dress8').setOrigin(0, 0)
        elsasegment9dress9 = this.add.image(245, 269, 'elsasegment9dress9').setOrigin(0, 0)
        elsasegment10dress10 = this.add.image(254, 343, 'elsasegment10dress10').setOrigin(0, 0)
        elsasegment11dress11 = this.add.image(272, 431, 'elsasegment11dress11').setOrigin(0, 0)
        elsachain = this.add.image(190, 143, 'elsachain').setOrigin(0, 0)
        elsahair = this.add.image(139, 12, 'elsahair').setOrigin(0, 0)
        elsaelash.setFrame(marr1[0])
        elsablush.setFrame(marr1[1])
        elsalips.setFrame(marr1[2] * 3)
        elsaeye1.setFrame(marr1[3])
        elsaeye2.setFrame(marr1[3])
        elsaeshade.setFrame(marr1[4] * 2)
        elsaebrow.setFrame(marr1[5])
        elsahair.setFrame(5)
        var elsadollgroup = this.add.container(0, 0);
        elsadollgroup.add(elsasegment11dress11)
        elsadollgroup.add(elsasegment10dress10)
        elsadollgroup.add(elsasegment9dress9)
        elsadollgroup.add(elsasegment8dress8)
        elsadollgroup.add(elsasegment7dress7)
        elsadollgroup.add(elsasegment6dress6)
        elsadollgroup.add(elsabody)
        elsadollgroup.add(elsalips)
        elsadollgroup.add(elsaeye1)
        elsadollgroup.add(elsaeye2)
        elsadollgroup.add(elsatoplayer)
        elsadollgroup.add(elsaeshade)
        elsadollgroup.add(elsaelash)
        elsadollgroup.add(elsablush)
        elsadollgroup.add(elsaebrow)
        elsadollgroup.add(elsasegment1dress1)
        elsadollgroup.add(elsasegment5dress5)
        elsadollgroup.add(elsasegment4dress4)
        elsadollgroup.add(elsasegment3dress3)
        elsadollgroup.add(elsasegment2dress2)
        elsadollgroup.add(elsachain)
        elsadollgroup.add(elsahair)
        elsadollgroup.x = 160
        elsadollgroup.y = 20
        var tween1 = this.tweens.add({
            targets: elsaeye1,
            x: elsaeye1.x + 2,
            ease: 'Linear',
            duration: 300,
            delay: 2400,
            callbackScope: this,
        });
        var tween2 = this.tweens.add({
            targets: elsaeye2,
            x: elsaeye2.x + 2,
            ease: 'Linear',
            duration: 300,
            delay: 2400,
            onComplete: elsaeyestartani1,
            callbackScope: this
        });

        function elsaeyestartani1() {
            this.tweens.add({
                targets: elsaeye1,
                x: elsaeye1.x - 2,
                ease: 'Linear',
                duration: 300,
                delay: 3000,
                callbackScope: this
            });
            this.tweens.add({
                targets: elsaeye2,
                x: elsaeye2.x - 2,
                ease: 'Linear',
                duration: 300,
                delay: 3000,
                onComplete: elsaeyestartani2,
                callbackScope: this,
            })
        }

        function elsaeyestartani2() {
            this.tweens.add({
                targets: elsaeye1,
                x: elsaeye1.x + 2,
                ease: 'Linear',
                duration: 300,
                delay: 2400,
                callbackScope: this
            });
            this.tweens.add({
                targets: elsaeye2,
                x: elsaeye2.x + 2,
                ease: 'Linear',
                duration: 300,
                delay: 2400,
                onComplete: elsaeyestartani1,
                callbackScope: this,
            })
        }
        elsaeshad = this.time.addEvent({
            delay: 1000,
            callback: elsaeblinkstart1,
            callbackScope: this
        });

        function elsaeblinkstart1() {
            elsaeshade.setFrame(marr1[4] * 2 + 1)
            elsaelash.visible = false
            elsaeshad = this.time.addEvent({
                delay: 200,
                callback: elsaeblinkstart2,
                callbackScope: this
            });
        }

        function elsaeblinkstart2() {
            elsaeshade.setFrame(marr1[4] * 2)
            elsaelash.visible = true
            elsaeshad = this.time.addEvent({
                delay: 4000,
                callback: elsaeblinkstart1,
                callbackScope: this
            });
        }
        elsalip = this.time.addEvent({
            delay: 1000,
            callback: elsalipsstart1,
            callbackScope: this
        });

        function elsalipsstart1() {
            elsalips.setFrame(marr1[2] * 3 + 1)
            elsalip = this.time.addEvent({
                delay: 100,
                callback: elsalipsstart2,
                callbackScope: this
            });
        }

        function elsalipsstart2() {
            elsalips.setFrame(marr1[2] * 3 + 2)
            elsalip = this.time.addEvent({
                delay: 2500,
                callback: elsalipsstart3,
                callbackScope: this
            });
        }

        function elsalipsstart3() {
            elsalips.setFrame(marr1[2] * 3 + 1)
            elsalip = this.time.addEvent({
                delay: 100,
                callback: elsalipsstart4,
                callbackScope: this
            });
        }

        function elsalipsstart4() {
            elsalips.setFrame(marr1[2] * 3)
            elsalip = this.time.addEvent({
                delay: 3000,
                callback: elsalipsstart1,
                callbackScope: this
            });
        }
        arielbhair = this.add.image(140, 112, 'arielbhair').setOrigin(0, 0)
        arielbody = this.add.image(73, 43, 'arielbody').setOrigin(0, 0)
        ariellips = this.add.image(161, 105, 'ariellips').setOrigin(0, 0)
        arieleye1 = this.add.image(161, 83, 'arieleye1').setOrigin(0, 0)
        arieleye2 = this.add.image(161, 83, 'arieleye2').setOrigin(0, 0)
        arielblush = this.add.image(156, 96, 'arielblush').setOrigin(0, 0)
        arieltoplayer = this.add.image(158, 79, 'arieltoplayer').setOrigin(0, 0)
        arieleshade = this.add.image(154, 80, 'arieleshade').setOrigin(0, 0)
        arielelash = this.add.image(153, 80, 'arielelash').setOrigin(0, 0)
        arielebrow = this.add.image(157, 72, 'arielebrow').setOrigin(0, 0)
        arielhair = this.add.image(124, 28, 'arielhair').setOrigin(0, 0)
        arielcrown = this.add.image(149, 25, 'arielcrown').setOrigin(0, 0)
        arielsegment1dress1 = this.add.image(130, 145, 'arielsegment1dress1').setOrigin(0, 0)
        arielsegment2dress2 = this.add.image(165, 198, 'arielsegment2dress2').setOrigin(0, 0)
        arielsegment3dress3 = this.add.image(121, 224, 'arielsegment3dress3').setOrigin(0, 0)
        arielsegment4dress4 = this.add.image(96, 333, 'arielsegment4dress4').setOrigin(0, 0)
        arielsegment5dress5 = this.add.image(99, 424, 'arielsegment5dress5').setOrigin(0, 0)
        arielsegment6dress6 = this.add.image(79, 257, 'arielsegment6dress6').setOrigin(0, 0)
        arielsegment7dress7 = this.add.image(78, 330, 'arielsegment7dress7').setOrigin(0, 0)
        arielsegment8dress8 = this.add.image(61, 429, 'arielsegment8dress8').setOrigin(0, 0)
        arielsegment9dress9 = this.add.image(211, 254, 'arielsegment9dress9').setOrigin(0, 0)
        arielsegment10dress10 = this.add.image(224, 325, 'arielsegment10dress10').setOrigin(0, 0)
        arielsegment11dress11 = this.add.image(247, 414, 'arielsegment11dress11').setOrigin(0, 0)
        arielgilter = this.add.image(106, 162, 'arielgilter').setOrigin(0, 0)
        arielbhair.setFrame(1)
        arielelash.setFrame(marr2[0])
        arielblush.setFrame(marr2[1])
        ariellips.setFrame(marr2[2] * 3)
        arieleye1.setFrame(marr2[3])
        arieleye2.setFrame(marr2[3])
        arieleshade.setFrame(marr2[4] * 2)
        arielebrow.setFrame(marr2[5])
        arielhair.setFrame(5)
        var arieldollgroup = this.add.container(0, 0);
        arieldollgroup.add(arielsegment11dress11)
        arieldollgroup.add(arielsegment10dress10)
        arieldollgroup.add(arielsegment9dress9)
        arieldollgroup.add(arielsegment8dress8)
        arieldollgroup.add(arielsegment7dress7)
        arieldollgroup.add(arielsegment6dress6)
        arieldollgroup.add(arielbhair)
        arieldollgroup.add(arielbody)
        arieldollgroup.add(ariellips)
        arieldollgroup.add(arieleye1)
        arieldollgroup.add(arieleye2)
        arieldollgroup.add(arieltoplayer)
        arieldollgroup.add(arieleshade)
        arieldollgroup.add(arielelash)
        arieldollgroup.add(arielblush)
        arieldollgroup.add(arielebrow)
        arieldollgroup.add(arielsegment1dress1)
        arieldollgroup.add(arielsegment4dress4)
        arieldollgroup.add(arielsegment5dress5)
        arieldollgroup.add(arielsegment3dress3)
        arieldollgroup.add(arielsegment2dress2)
        arieldollgroup.add(arielgilter)
        arieldollgroup.add(arielhair)
        arieldollgroup.add(arielcrown)
        arieldollgroup.x = 170
        arieldollgroup.y = 20
        var tween1 = this.tweens.add({
            targets: arieleye1,
            x: arieleye1.x + 2,
            ease: 'Linear',
            duration: 300,
            delay: 2400,
            callbackScope: this,
        });
        var tween2 = this.tweens.add({
            targets: arieleye2,
            x: arieleye2.x + 2,
            ease: 'Linear',
            duration: 300,
            delay: 2400,
            onComplete: arieleyestartani1,
            callbackScope: this
        });

        function arieleyestartani1() {
            this.tweens.add({
                targets: arieleye1,
                x: arieleye1.x - 2,
                ease: 'Linear',
                duration: 300,
                delay: 3000,
                callbackScope: this
            });
            this.tweens.add({
                targets: arieleye2,
                x: arieleye2.x - 2,
                ease: 'Linear',
                duration: 300,
                delay: 3000,
                onComplete: arieleyestartani2,
                callbackScope: this,
            })
        }

        function arieleyestartani2() {
            this.tweens.add({
                targets: arieleye1,
                x: arieleye1.x + 2,
                ease: 'Linear',
                duration: 300,
                delay: 2400,
                callbackScope: this
            });
            this.tweens.add({
                targets: arieleye2,
                x: arieleye2.x + 2,
                ease: 'Linear',
                duration: 300,
                delay: 2400,
                onComplete: arieleyestartani1,
                callbackScope: this,
            })
        }
        arieleshad = this.time.addEvent({
            delay: 1000,
            callback: arieleblinkstart1,
            callbackScope: this
        });

        function arieleblinkstart1() {
            arieleshade.setFrame(marr1[4] * 2 + 1)
            arielelash.visible = false
            arieleshad = this.time.addEvent({
                delay: 200,
                callback: arieleblinkstart2,
                callbackScope: this
            });
        }

        function arieleblinkstart2() {
            arieleshade.setFrame(marr1[4] * 2)
            arielelash.visible = true
            arieleshad = this.time.addEvent({
                delay: 4000,
                callback: arieleblinkstart1,
                callbackScope: this
            });
        }
        ariellip = this.time.addEvent({
            delay: 1000,
            callback: ariellipsstart1,
            callbackScope: this
        });

        function ariellipsstart1() {
            ariellips.setFrame(marr1[2] * 3 + 1)
            ariellip = this.time.addEvent({
                delay: 100,
                callback: ariellipsstart2,
                callbackScope: this
            });
        }

        function ariellipsstart2() {
            ariellips.setFrame(marr1[2] * 3 + 2)
            ariellip = this.time.addEvent({
                delay: 2500,
                callback: ariellipsstart3,
                callbackScope: this
            });
        }

        function ariellipsstart3() {
            ariellips.setFrame(marr1[2] * 3 + 1)
            ariellip = this.time.addEvent({
                delay: 100,
                callback: ariellipsstart4,
                callbackScope: this
            });
        }

        function ariellipsstart4() {
            ariellips.setFrame(marr1[2] * 3)
            ariellip = this.time.addEvent({
                delay: 3000,
                callback: ariellipsstart1,
                callbackScope: this
            });
        }
        annabody = this.add.image(54, 42, 'annabody').setOrigin(0, 0)
        annalips = this.add.image(154, 102, 'annalips').setOrigin(0, 0)
        annaeye1 = this.add.image(154, 79, 'annaeye1').setOrigin(0, 0)
        annaeye2 = this.add.image(154, 79, 'annaeye2').setOrigin(0, 0)
        annablush = this.add.image(145, 88, 'annablush').setOrigin(0, 0)
        annatoplayer = this.add.image(150, 74, 'annatoplayer').setOrigin(0, 0)
        annaeshade = this.add.image(147, 75, 'annaeshade').setOrigin(0, 0)
        annaelash = this.add.image(147, 74, 'annaelash').setOrigin(0, 0)
        annaebrow = this.add.image(150, 69, 'annaebrow').setOrigin(0, 0)
        annachain = this.add.image(157, 133, 'annachain').setOrigin(0, 0)
        annahair = this.add.image(117, 20, 'annahair').setOrigin(0, 0)
        annahand = this.add.image(221, 146, 'annahand').setOrigin(0, 0)
        annahand1 = this.add.image(223, 146, 'annahand1').setOrigin(0, 0)
        annasegment1dress1 = this.add.image(111, 125, 'annasegment1dress1').setOrigin(0, 0)
        annasegment2dress2 = this.add.image(153, 207, 'annasegment2dress2').setOrigin(0, 0)
        annasegment3dress3 = this.add.image(100, 226, 'annasegment3dress3').setOrigin(0, 0)
        annasegment4dress4 = this.add.image(97, 346, 'annasegment4dress4').setOrigin(0, 0)
        annasegment5dress5 = this.add.image(98, 399, 'annasegment5dress5').setOrigin(0, 0)
        annasegment6dress6 = this.add.image(71, 351, 'annasegment6dress6').setOrigin(0, 0)
        annasegment7dress7 = this.add.image(41, 414, 'annasegment7dress7').setOrigin(0, 0)
        annasegment8dress8 = this.add.image(213, 335, 'annasegment8dress8').setOrigin(0, 0)
        annasegment9dress9 = this.add.image(224, 419, 'annasegment9dress9').setOrigin(0, 0)
        annaelash.setFrame(marr2[0])
        annablush.setFrame(marr2[1])
        annalips.setFrame(marr2[2] * 3)
        annaeye1.setFrame(marr2[3])
        annaeye2.setFrame(marr2[3])
        annaeshade.setFrame(marr2[4] * 2)
        annaebrow.setFrame(marr2[5])
        annahair.setFrame(3)
        var annadollgroup = this.add.container(0, 0);
        annadollgroup.add(annasegment9dress9)
        annadollgroup.add(annasegment8dress8)
        annadollgroup.add(annasegment7dress7)
        annadollgroup.add(annasegment6dress6)
        annadollgroup.add(annabody)
        annadollgroup.add(annalips)
        annadollgroup.add(annaeye1)
        annadollgroup.add(annaeye2)
        annadollgroup.add(annatoplayer)
        annadollgroup.add(annaeshade)
        annadollgroup.add(annaelash)
        annadollgroup.add(annablush)
        annadollgroup.add(annaebrow)
        annadollgroup.add(annasegment1dress1)
        annadollgroup.add(annasegment5dress5)
        annadollgroup.add(annasegment2dress2)
        annadollgroup.add(annasegment4dress4)
        annadollgroup.add(annasegment3dress3)
        annadollgroup.add(annahand)
        annadollgroup.add(annachain)
        annadollgroup.add(annahair)
        annadollgroup.add(annahand1)
        annadollgroup.x = 170
        annadollgroup.y = 30
        var tween1 = this.tweens.add({
            targets: annaeye1,
            x: annaeye1.x + 2,
            ease: 'Linear',
            duration: 300,
            delay: 2400,
            callbackScope: this,
        });
        var tween2 = this.tweens.add({
            targets: annaeye2,
            x: annaeye2.x + 2,
            ease: 'Linear',
            duration: 300,
            delay: 2400,
            onComplete: annaeyestartani1,
            callbackScope: this
        });

        function annaeyestartani1() {
            this.tweens.add({
                targets: annaeye1,
                x: annaeye1.x - 2,
                ease: 'Linear',
                duration: 300,
                delay: 3000,
                callbackScope: this
            });
            this.tweens.add({
                targets: annaeye2,
                x: annaeye2.x - 2,
                ease: 'Linear',
                duration: 300,
                delay: 3000,
                onComplete: annaeyestartani2,
                callbackScope: this,
            })
        }

        function annaeyestartani2() {
            this.tweens.add({
                targets: annaeye1,
                x: annaeye1.x + 2,
                ease: 'Linear',
                duration: 300,
                delay: 2400,
                callbackScope: this
            });
            this.tweens.add({
                targets: annaeye2,
                x: annaeye2.x + 2,
                ease: 'Linear',
                duration: 300,
                delay: 2400,
                onComplete: annaeyestartani1,
                callbackScope: this,
            })
        }
        annaeshad = this.time.addEvent({
            delay: 1000,
            callback: annaeblinkstart1,
            callbackScope: this
        });

        function annaeblinkstart1() {
            annaeshade.setFrame(marr1[4] * 2 + 1)
            annaelash.visible = false
            annaeshad = this.time.addEvent({
                delay: 200,
                callback: annaeblinkstart2,
                callbackScope: this
            });
        }

        function annaeblinkstart2() {
            annaeshade.setFrame(marr1[4] * 2)
            annaelash.visible = true
            annaeshad = this.time.addEvent({
                delay: 4000,
                callback: annaeblinkstart1,
                callbackScope: this
            });
        }
        annalip = this.time.addEvent({
            delay: 1000,
            callback: annalipsstart1,
            callbackScope: this
        });

        function annalipsstart1() {
            annalips.setFrame(marr1[2] * 3 + 1)
            annalip = this.time.addEvent({
                delay: 100,
                callback: annalipsstart2,
                callbackScope: this
            });
        }

        function annalipsstart2() {
            annalips.setFrame(marr1[2] * 3 + 2)
            annalip = this.time.addEvent({
                delay: 2500,
                callback: annalipsstart3,
                callbackScope: this
            });
        }

        function annalipsstart3() {
            annalips.setFrame(marr1[2] * 3 + 1)
            annalip = this.time.addEvent({
                delay: 100,
                callback: annalipsstart4,
                callbackScope: this
            });
        }

        function annalipsstart4() {
            annalips.setFrame(marr1[2] * 3)
            annalip = this.time.addEvent({
                delay: 3000,
                callback: annalipsstart1,
                callbackScope: this
            });
        }
        rapunbhair = this.add.sprite(151, 98, 'rapunbhair').setOrigin(0, 0)
        rapunbody = this.add.sprite(99, 46, 'rapunbody').setOrigin(0, 0)
        rapunlips = this.add.sprite(198, 115, 'rapunlips').setOrigin(0, 0)
        rapuneye1 = this.add.sprite(200, 98, 'rapuneye1').setOrigin(0, 0)
        rapuneye2 = this.add.sprite(200, 98, 'rapuneye2').setOrigin(0, 0)
        rapunblush = this.add.sprite(196, 111, 'rapunblush').setOrigin(0, 0)
        rapuntoplayer = this.add.sprite(191, 86, 'rapuntoplayer').setOrigin(0, 0)
        rapuneshade = this.add.sprite(191, 91, 'rapuneshade').setOrigin(0, 0)
        rapunelash = this.add.sprite(193, 94, 'rapunelash').setOrigin(0, 0)
        rapunebrow = this.add.sprite(190, 83, 'rapunebrow').setOrigin(0, 0)
        rapunhair = this.add.sprite(146, 34, 'rapunhair').setOrigin(0, 0)
        rapuncrown = this.add.sprite(181, 29, 'rapuncrown').setOrigin(0, 0)
        rapunsegment1dress1 = this.add.sprite(164, 154, 'rapunsegment1dress1').setOrigin(0, 0)
        rapunsegment2dress2 = this.add.sprite(188, 227, 'rapunsegment2dress2').setOrigin(0, 0)
        rapunsegment3dress3 = this.add.sprite(139, 247, 'rapunsegment3dress3').setOrigin(0, 0)
        rapunsegment4dress4 = this.add.sprite(132, 349, 'rapunsegment4dress4').setOrigin(0, 0)
        rapunsegment5dress5 = this.add.sprite(107, 429, 'rapunsegment5dress5').setOrigin(0, 0)
        rapunsegment6dress6 = this.add.sprite(90, 329, 'rapunsegment6dress6').setOrigin(0, 0)
        rapunsegment7dress7 = this.add.sprite(256, 326, 'rapunsegment7dress7').setOrigin(0, 0)
        rapunelash.setFrame(marr2[0])
        rapunblush.setFrame(marr2[1])
        rapunlips.setFrame(marr2[2] * 3)
        rapuneye1.setFrame(marr2[3])
        rapuneye2.setFrame(marr2[3])
        rapuneshade.setFrame(marr2[4] * 2)
        rapunebrow.setFrame(marr2[5])
        rapunhair.setFrame(1)
        rapunbhair.setFrame(1)
        var rapundollgroup = this.add.container(0, 0);
        rapundollgroup.add(rapunsegment7dress7)
        rapundollgroup.add(rapunsegment6dress6)
        rapundollgroup.add(rapunbhair)
        rapundollgroup.add(rapunbody)
        rapundollgroup.add(rapunlips)
        rapundollgroup.add(rapuneye1)
        rapundollgroup.add(rapuneye2)
        rapundollgroup.add(rapuntoplayer)
        rapundollgroup.add(rapuneshade)
        rapundollgroup.add(rapunelash)
        rapundollgroup.add(rapunblush)
        rapundollgroup.add(rapunebrow)
        rapundollgroup.add(rapunsegment1dress1)
        rapundollgroup.add(rapunsegment5dress5)
        rapundollgroup.add(rapunsegment2dress2)
        rapundollgroup.add(rapunsegment4dress4)
        rapundollgroup.add(rapunsegment3dress3)
        rapundollgroup.add(rapunhair)
        rapundollgroup.add(rapuncrown)
        rapundollgroup.x = 170
        rapundollgroup.y = 10
        elsasegment11dress11.setFrame(2)
        elsasegment10dress10.setFrame(2)
        elsasegment9dress9.setFrame(2)
        elsasegment8dress8.setFrame(2)
        elsasegment7dress7.setFrame(2)
        elsasegment6dress6.setFrame(2)
        elsasegment1dress1.setFrame(2)
        elsasegment5dress5.setFrame(2)
        elsasegment4dress4.setFrame(0)
        elsasegment3dress3.setFrame(2)
        elsasegment2dress2.setFrame(2)
        arielsegment1dress1.setFrame(6)
        arielsegment5dress5.setFrame(6)
        arielsegment4dress4.setFrame(6)
        arielsegment3dress3.setFrame(6)
        arielsegment2dress2.setFrame(6)
        annasegment1dress1.setFrame(2)
        annasegment5dress5.setFrame(2)
        annasegment4dress4.setFrame(2)
        annasegment3dress3.setFrame(2)
        annasegment2dress2.setFrame(2)
        rapunsegment1dress1.setFrame(1)
        rapunsegment5dress5.setFrame(1)
        rapunsegment4dress4.setFrame(1)
        rapunsegment3dress3.setFrame(1)
        rapunsegment2dress2.setFrame(1)
        elsasegment11dress11.tint = '0x3bdcff'
        elsasegment10dress10.tint = '0x3bdcff'
        elsasegment9dress9.tint = '0x3bdcff'
        elsasegment8dress8.tint = '0x3bdcff'
        elsasegment7dress7.tint = '0x3bdcff'
        elsasegment6dress6.tint = '0x3bdcff'
        elsasegment1dress1.tint = '0x3bdcff'
        elsasegment5dress5.tint = '0x3bdcff'
        elsasegment4dress4.tint = '0x3bdcff'
        elsasegment3dress3.tint = '0x3bdcff'
        elsasegment2dress2.tint = '0x3bdcff'
        rapunsegment1dress1.tint = ' 0x3ba13b'
        rapunsegment5dress5.tint = ' 0x3ba13b'
        rapunsegment4dress4.tint = ' 0x3ba13b'
        rapunsegment3dress3.tint = ' 0x3ba13b'
        rapunsegment2dress2.tint = ' 0x3ba13b'
        arielsegment1dress1.tint = ' 0x9cdc3b'
        arielsegment5dress5.tint = ' 0x9cdc3b'
        arielsegment4dress4.tint = ' 0x9cdc3b'
        arielsegment3dress3.tint = ' 0x9cdc3b'
        arielsegment2dress2.tint = ' 0x9cdc3b'
        annasegment1dress1.tint = ' 0xb04ab1'
        annasegment5dress5.tint = ' 0xb04ab1'
        annasegment4dress4.tint = ' 0xb04ab1'
        annasegment3dress3.tint = ' 0xb04ab1'
        annasegment2dress2.tint = ' 0xb04ab1'
        var colorcode = [, ' 0xff3838', ' 0xfe7d3b', ' 0xffe33b', ' 0x9cdc3b', ' 0x3ba13b', ' 0x3bd8c8', ' 0x3bdcff', ' 0x3b91ed', ' 0x3f3ce6', ' 0x7443f6', ' 0xfeb463', ' 0x9b9b9b', ' 0xb04ab1', ' 0xff8be6', '0xed3b8a', ' 0xfff2f2', ' 0x3b2e2e']
        var tween1 = this.tweens.add({
            targets: rapuneye1,
            x: rapuneye1.x + 2,
            ease: 'Linear',
            duration: 300,
            delay: 2400,
            callbackScope: this,
        });
        var tween2 = this.tweens.add({
            targets: rapuneye2,
            x: rapuneye2.x + 2,
            ease: 'Linear',
            duration: 300,
            delay: 2400,
            onComplete: rapuneyestartani1,
            callbackScope: this
        });

        function rapuneyestartani1() {
            this.tweens.add({
                targets: rapuneye1,
                x: rapuneye1.x - 2,
                ease: 'Linear',
                duration: 300,
                delay: 3000,
                callbackScope: this
            });
            this.tweens.add({
                targets: rapuneye2,
                x: rapuneye2.x - 2,
                ease: 'Linear',
                duration: 300,
                delay: 3000,
                onComplete: rapuneyestartani2,
                callbackScope: this,
            })
        }

        function rapuneyestartani2() {
            this.tweens.add({
                targets: rapuneye1,
                x: rapuneye1.x + 2,
                ease: 'Linear',
                duration: 300,
                delay: 2400,
                callbackScope: this
            });
            this.tweens.add({
                targets: rapuneye2,
                x: rapuneye2.x + 2,
                ease: 'Linear',
                duration: 300,
                delay: 2400,
                onComplete: rapuneyestartani1,
                callbackScope: this,
            })
        }
        rapuneshad = this.time.addEvent({
            delay: 1000,
            callback: rapuneblinkstart1,
            callbackScope: this
        });

        function rapuneblinkstart1() {
            rapuneshade.setFrame(marr1[4] * 2 + 1)
            rapunelash.visible = false
            rapuneshad = this.time.addEvent({
                delay: 200,
                callback: rapuneblinkstart2,
                callbackScope: this
            });
        }

        function rapuneblinkstart2() {
            rapuneshade.setFrame(marr1[4] * 2)
            rapunelash.visible = true
            rapuneshad = this.time.addEvent({
                delay: 4000,
                callback: rapuneblinkstart1,
                callbackScope: this
            });
        }
        rapunlip = this.time.addEvent({
            delay: 1000,
            callback: rapunlipsstart1,
            callbackScope: this
        });

        function rapunlipsstart1() {
            rapunlips.setFrame(marr1[2] * 3 + 1)
            rapunlip = this.time.addEvent({
                delay: 100,
                callback: rapunlipsstart2,
                callbackScope: this
            });
        }

        function rapunlipsstart2() {
            rapunlips.setFrame(marr1[2] * 3 + 2)
            rapunlip = this.time.addEvent({
                delay: 2500,
                callback: rapunlipsstart3,
                callbackScope: this
            });
        }

        function rapunlipsstart3() {
            rapunlips.setFrame(marr1[2] * 3 + 1)
            rapunlip = this.time.addEvent({
                delay: 100,
                callback: rapunlipsstart4,
                callbackScope: this
            });
        }

        function rapunlipsstart4() {
            rapunlips.setFrame(marr1[2] * 3)
            rapunlip = this.time.addEvent({
                delay: 3000,
                callback: rapunlipsstart1,
                callbackScope: this
            });
        }
        elsadollgroup.x = -400
        arieldollgroup.x = -400
        annadollgroup.x = -400
        rapundollgroup.x = -400
        var dollContainer = this.add.container(0, 0);
        dollContainer.add([elsadollgroup, annadollgroup, arieldollgroup, rapundollgroup])
        dollContainerani = this.time.addEvent({
            delay: 1500,
            callback: dollContaineranistart1,
            callbackScope: this
        });

        function dollContaineranistart1() {
            this.tweens.add({
                targets: elsadollgroup,
                x: 120,
                ease: 'Back',
                duration: 300,
            });
            this.tweens.add({
                targets: arieldollgroup,
                x: -30,
                ease: 'Back',
                duration: 300,
                delay: 200,
                onComplete: dollContaineranistart2,
                callbackScope: this
            });
        }

        function dollContaineranistart2() {
            dollContainerani = this.time.addEvent({
                delay: 3500,
                callback: dollContaineranistart3,
                callbackScope: this
            });
        }

        function dollContaineranistart3() {
            this.tweens.add({
                targets: arieldollgroup,
                x: -400,
                ease: 'Back.easeIn',
                duration: 300,
            });
            this.tweens.add({
                targets: elsadollgroup,
                x: -400,
                ease: 'Back.easeIn',
                duration: 300,
                delay: 200,
                onComplete: dollContaineranistart4,
                callbackScope: this
            });
        }

        function dollContaineranistart4() {
            this.tweens.add({
                targets: annadollgroup,
                x: 150,
                ease: 'Back',
                delay: 200,
                duration: 300,
            });
            this.tweens.add({
                targets: rapundollgroup,
                x: -60,
                ease: 'Back',
                duration: 300,
                delay: 400,
                onComplete: dollContaineranistart5,
                callbackScope: this
            });
        }

        function dollContaineranistart5() {
            dollContainerani = this.time.addEvent({
                delay: 3500,
                callback: dollContaineranistart6,
                callbackScope: this
            });
        }

        function dollContaineranistart6() {
            this.tweens.add({
                targets: rapundollgroup,
                x: -400,
                ease: 'Back.easeIn',
                duration: 300,
            });
            this.tweens.add({
                targets: annadollgroup,
                x: -400,
                ease: 'Back.easeIn',
                duration: 300,
                delay: 200,
                onComplete: dollContaineranistart1,
                callbackScope: this
            });
        }
        titleobj = this.add.image(605, 163, 'titleobj');
        titleobj.setOrigin(0.5);
        title1 = this.add.image(572, 149, 'title1');
        title1.setOrigin(0.5);
        title2 = this.add.image(671, 225.5, 'title2');
        title2.setOrigin(0.5);
        titleobj1 = this.add.image(579.5, 190.5, 'titleobj1');
        titleobj1.setOrigin(0.5);
        title3 = this.add.image(480, 285, 'title3');
        title3.setOrigin(0.5);
        title1.setScale(0)
        title2.visible = false
        title3.visible = false
        titleobj.visible = false
        titleobj1.visible = false
        titleobj1.alpha = 0
        this.tweens.add({
            targets: title1,
            scaleX: 1,
            scaleY: 1,
            ease: 'Back.easeOut',
            duration: 700,
            delay: 500,
            onComplete: titlestart1,
            callbackScope: this
        });

        function titlestart1() {
            title2.visible = true
            title3.visible = true
            this.tweens.add({
                targets: title2,
                x: title2.x - 100,
                ease: 'Back.easeOut',
                duration: 700,
                onComplete: titlestart2,
                callbackScope: this
            });
            this.tweens.add({
                targets: title3,
                x: title3.x + 100,
                ease: 'Back.easeOut',
                duration: 700,
            });
        }

        function titlestart2() {
            titleobj.visible = true
            titleobj1.visible = true
            this.tweens.add({
                targets: titleobj,
                scaleX: 1.05,
                scaleY: 1.05,
                alpha: 1,
                ease: 'Linear',
                duration: 1000,
                repeat: -1,
                yoyo: true
            });
            this.tweens.add({
                targets: titleobj1,
                alpha: 1,
                ease: 'Linear',
                duration: 1000,
            });
            this.tweens.add({
                targets: title1,
                scaleX: 0.9,
                scaleY: 0.9,
                ease: 'Linear',
                duration: 1000,
                repeat: -1,
                yoyo: true
            });
            this.tweens.add({
                targets: title2,
                scaleY: 0.9,
                ease: 'Linear',
                duration: 1000,
                repeat: -1,
                yoyo: true
            });
            this.tweens.add({
                targets: title3,
                scaleX: 1.05,
                ease: 'Linear',
                duration: 1000,
                repeat: -1,
                yoyo: true
            });
        }
        if (brandingMaterials.html5GamesButton.display == true && brandingMaterials.html5GamesButton.image != undefined) {
            html5GamesButton1 = this.add.image(10, 10, 'html5GamesButton1').setInteractive({
                pixelPerfect: true,
                useHandCursor: true
            })
            html5GamesButton1.setOrigin(0, 0);
            html5GamesButton1.on('pointerdown', brandingMaterials.html5GamesButton.clickURL);
        }
        if (brandingMaterials.partnerLogo.display == true && brandingMaterials.partnerLogo.image != undefined) {
            partnerLogo1 = this.add.image(10, 590, 'partnerLogo1').setInteractive({
                pixelPerfect: true,
                useHandCursor: true
            })
            partnerLogo1.setOrigin(0, 1);
            partnerLogo1.on('pointerdown', brandingMaterials.partnerLogo.clickURL);
        }
        if (brandingMaterials.coverLogo.display == true && brandingMaterials.coverLogo.image != undefined) {
            coverLogo1 = this.add.image(562, 530, 'coverLogo1').setInteractive({
                pixelPerfect: true,
                useHandCursor: true
            })
            coverLogo1.setOrigin(0.5, 0.5);
            coverLogo1.on('pointerdown', brandingMaterials.coverLogo.clickURL);
        }
        play = this.add.image(560, 396.00, 'uiButton').setOrigin(0.5, 0.5).setInteractive({
            pixelPerfect: true,
            useHandCursor: true
        })
        play.setFrame('button_play')
        play.setScale(0)
        var playEvent = this.time.addEvent({
            delay: 2800,
            callback: onplaytweeEvent,
            callbackScope: this
        });

        function onplaytweeEvent() {
            var tween2 = this.tweens.add({
                targets: play,
                scaleX: 1,
                scaleY: 1,
                ease: 'Linear',
                duration: 700
            });
        }
        play.on('pointerover', function () {
            play.setScale(1.05)
        }, this);
        play.on('pointerout', function () {
            play.setScale(1)
        }, this);
        play.on('pointerdown', function () {
            if (loadFinish && !startgame1) {
                if (music.isPlaying == false) {
                    clicksound.stop()
                } else {
                    clicksound.play()
                }
                clearTimeout(t1)
                play.setScale(1)
                startgame1 = true
                transitionbackground1.alpha = 1
                transitionbackground2.alpha = 1
                transitionbackground3.alpha = 1
                transitionpanel.alpha = 1
                transitiondress.alpha = 1
                transitiontitle1.alpha = 1
                transitiontitle2.alpha = 1
                transitiontitle3.alpha = 1
                transitionstar1.alpha = 1
                transitionstar2.alpha = 1
                transitionstar3.alpha = 1
                transitionstar4.alpha = 1
                transitionstar5.alpha = 1
                transitionbackground1.visible = true
                transitionbackground2.visible = true
                transitionbackground3.visible = true
                transitiondress.visible = false
                transitionpanel.visible = false
                transitiontitle1.visible = false
                transitiontitle2.visible = false
                transitiontitle3.visible = false
                transitionbackground1.alpha = 0
                transitionbackground2.alpha = 0
                transitionbackground3.alpha = 0
                this.tweens.add({
                    targets: transitionbackground1,
                    alpha: 1,
                    ease: 'Linear',
                    duration: 800,
                    onComplete: transitionstart1,
                    callbackScope: this
                })

                function transitionstart1() {
                    transitiondress.visible = true
                    transitiondress.alpha = 0
                    this.tweens.add({
                        targets: transitiondress,
                        alpha: 1,
                        ease: 'Linear',
                        duration: 400,
                        onComplete: transitionstart2,
                        callbackScope: this
                    })
                    this.tweens.add({
                        targets: transitionbackground2,
                        alpha: 1,
                        ease: 'Linear',
                        duration: 800,
                    })
                }

                function transitionstart2() {
                    transitionpanel.visible = true
                    transitionpanel.setScale(0)
                    this.tweens.add({
                        targets: transitionpanel,
                        scaleX: 1,
                        scaleY: 1,
                        ease: 'Linear',
                        duration: 500,
                        onComplete: transitionstart3,
                        callbackScope: this
                    })
                }

                function transitionstart3() {
                    this.tweens.add({
                        targets: transitionbackground3,
                        alpha: 1,
                        ease: 'Linear',
                        duration: 800,
                        onComplete: transitionstart4,
                        callbackScope: this
                    })
                }

                function transitionstart4() {
                    transitiontitle1.visible = true
                    transitiontitle2.visible = true
                    transitiontitle3.visible = true
                    transitiontitle1.alpha = 0
                    transitiontitle2.alpha = 0
                    transitiontitle3.alpha = 0
                    transitiontitle2.x = transitiontitle2.x + 100
                    transitiontitle3.x = transitiontitle3.x - 100
                    this.tweens.add({
                        targets: transitiontitle1,
                        alpha: 1,
                        ease: 'Linear',
                        duration: 800,
                        onComplete: transitionstart5,
                        callbackScope: this
                    })
                    this.tweens.add({
                        targets: transitiontitle2,
                        alpha: 1,
                        x: transitiontitle2.x - 100,
                        ease: 'Linear',
                        duration: 800,
                    })
                    this.tweens.add({
                        targets: transitiontitle3,
                        alpha: 1,
                        x: transitiontitle3.x + 100,
                        ease: 'Linear',
                        duration: 800,
                    })
                }

                function transitionstart5() {
                    transitionstar1.setScale(0)
                    transitionstar2.setScale(0)
                    transitionstar3.setScale(0)
                    transitionstar4.setScale(0)
                    transitionstar5.setScale(0)
                    transitionstar1.visible = true
                    transitionstar2.visible = true
                    transitionstar3.visible = true
                    transitionstar4.visible = true
                    transitionstar5.visible = true
                    this.tweens.add({
                        targets: transitionstar1,
                        scaleX: 1,
                        scaleY: 1,
                        ease: 'Linear',
                        duration: 300,
                    })
                    this.tweens.add({
                        targets: transitionstar2,
                        scaleX: 0.9,
                        scaleY: 0.9,
                        ease: 'Linear',
                        duration: 300,
                    })
                    this.tweens.add({
                        targets: transitionstar3,
                        scaleX: 0.9,
                        scaleY: 0.9,
                        ease: 'Linear',
                        duration: 300,
                    })
                    this.tweens.add({
                        targets: transitionstar4,
                        scaleX: 0.8,
                        scaleY: 0.8,
                        ease: 'Linear',
                        duration: 300,
                    })
                    this.tweens.add({
                        targets: transitionstar5,
                        scaleX: 0.65,
                        scaleY: 0.65,
                        ease: 'Linear',
                        duration: 300,
                        onComplete: transitionstar6,
                        callbackScope: this
                    })
                }

                function transitionstar6() {
                    this.scene.stop('titleScreen')
                    game.scene.start('levelSelect');
                }
            }
        }, this);
        play.on('pointerup', function () {
            if (loadFinish && startgame1) {
                play.setScale(1.05)
            }
        }, this);
        mute = this.add.sprite(790, 10, 'uiButton').setFrame("button_music_on").setInteractive({
            pixelPerfect: true,
            useHandCursor: true
        })
        mute.setOrigin(1, 0)
        mute.on('pointerdown', function () {
            if (!isMuted) {
                isMuted = true
                music.pause()
                mute.setFrame("button_music_off")
            } else {
                isMuted = false
                music.resume()
                mute.setFrame("button_music_on")
            }
            var tween2 = this.tweens.add({
                targets: mute,
                scaleX: 0.9,
                scaleY: 0.9,
                ease: 'Linear',
                duration: 80,
                yoyo: true,
                repeat: 1,
            });
            if (music.isPlaying == false) {
                clicksound.stop()
            } else {
                clicksound.play()
            }
        }, this);
        if (firstTime) {
            firstTime = false;
            music = this.sound.add('boden');
            music.play({
                loop: true
            });
        } else {
            mute.setFrame("button_music_on")
            if (isMuted) {
                isMuted = false;
                music.resume();
            }
        }
        if (music.isPlaying == false) {
            mute.setFrame("button_music_off")
        } else {
            mute.setFrame("button_music_on")
        }
        clicksound = this.sound.add('clickss');
        transitionbackground1 = this.add.image(400, 300, 'transitionbackground1').setOrigin(0.5, 0.5).setInteractive({
            pixelPerfect: true,
            useHandCursor: true
        })
        transitionbackground2 = this.add.image(400, 300, 'transitionbackground2').setOrigin(0.5, 0.5)
        transitionbackground3 = this.add.image(400, 300, 'transitionbackground3').setOrigin(0.5, 0.5)
        transitionpanel = this.add.image(393.5, 314, 'transitionpanel').setOrigin(0.5)
        transitiondress = this.add.image(267, 141, 'transitiondress').setOrigin(0, 0)
        transitiontitle1 = this.add.image(400, 329, 'title1');
        transitiontitle1.setOrigin(0.5);
        transitiontitle2 = this.add.image(380, 399, 'title2');
        transitiontitle2.setOrigin(0.5);
        transitiontitle3 = this.add.image(380, 459, 'title3');
        transitiontitle3.setOrigin(0.5);
        transitionstar1 = this.add.image(616.5, 168, 'transitionstar');
        transitionstar1.setOrigin(0.5);
        transitionstar2 = this.add.image(156.5, 168, 'transitionstar');
        transitionstar2.setOrigin(0.5);
        transitionstar2.setScale(0.9)
        transitionstar3 = this.add.image(596.5, 468, 'transitionstar');
        transitionstar3.setOrigin(0.5);
        transitionstar3.setScale(0.9)
        transitionstar4 = this.add.image(116.5, 468, 'transitionstar');
        transitionstar4.setOrigin(0.5);
        transitionstar4.setScale(0.8)
        transitionstar5 = this.add.image(400, 558, 'transitionstar');
        transitionstar5.setOrigin(0.5);
        transitionstar5.setScale(0.65)
        transitionbackground1.visible = false
        transitionbackground2.visible = false
        transitionbackground3.visible = false
        transitiondress.visible = false
        transitionpanel.visible = false
        transitiontitle1.visible = false
        transitiontitle2.visible = false
        transitiontitle3.visible = false
        transitionstar1.visible = false
        transitionstar2.visible = false
        transitionstar3.visible = false
        transitionstar4.visible = false
        transitionstar5.visible = false
        hitobject = this.add.image(400, 300, 'hitobject').setOrigin(0.5, 0.5).setInteractive({
            useHandCursor: true
        })
        hitobject.visible = false
        hitobject.on('pointerdown', hitobjectdown)

        function hitobjectdown() {}
        this.load.on('complete', function () {
            loadFinish = true
        });
        this.load.start();
    }
});
var startgame2 = false
var levelSelect = new Phaser.Class({
    Extends: Phaser.Scene,
    initialize: function levelSelect() {
        Phaser.Scene.call(this, {
            key: 'levelSelect'
        });
    },
    preload: function () {
        startgame2 = false
        loadFinish = false
    },
    create: function () {
        clicksound = this.sound.add('clickss');
        levelbackground = this.add.image(400, 300, 'levelbackground').setOrigin(0.5, 0.5)
        var lxarr = [, 215, 392, 31, 211, 391, 576, 210, 390]
        var lyarr = [, 43, 43, 227, 227, 227, 218, 419, 415]
        for (i = 1; i <= 8; i++) {
            game['level' + i] = this.add.sprite(lxarr[i], lyarr[i], 'level' + i).setOrigin(0.5, 0.5).setInteractive({
                pixelPerfect: true,
                useHandCursor: true
            })
            game['level' + i].x += parseFloat(game['level' + i].width / 2)
            game['level' + i].y += parseFloat(game['level' + i].height / 2)
        }
        var txarr = [, 215, 392, 31, 211, 391, 576, 210, 390]
        var tyarr = [, 43, 43, 227, 227, 227, 218, 419, 415]
        for (i = 1; i <= 8; i++) {
            game['leveltick' + i] = this.add.sprite(txarr[i] + 120, tyarr[i] + 100, 'leveltick').setOrigin(0.5, 0.5)
            game['leveltick' + i].visible = false
        }
        var lxarr = [, 215, 392, 31, 211, 391, 576, 210, 390]
        var lyarr = [, 43, 43, 227, 227, 227, 218, 419, 415]
        levellock1 = this.add.sprite(lxarr[1] + 100, lyarr[1] + 100, 'levellock').setOrigin(0.5, 0.5)
        levellock2 = this.add.sprite(lxarr[2] + 100, lyarr[2] + 100, 'levellock').setOrigin(0.5, 0.5)
        levellock3 = this.add.sprite(lxarr[3] + 100, lyarr[3] + 100, 'levellock').setOrigin(0.5, 0.5)
        levellock4 = this.add.sprite(lxarr[4] + 100, lyarr[4] + 100, 'levellock').setOrigin(0.5, 0.5)
        levellock5 = this.add.sprite(lxarr[5] + 100, lyarr[5] + 100, 'levellock').setOrigin(0.5, 0.5)
        levellock6 = this.add.sprite(lxarr[6] + 100, lyarr[6] + 100, 'levellock').setOrigin(0.5, 0.5)
        levellock7 = this.add.sprite(lxarr[7] + 100, lyarr[7] + 100, 'levellock').setOrigin(0.5, 0.5)
        levellock8 = this.add.sprite(lxarr[8] + 100, lyarr[8] + 100, 'levellock').setOrigin(0.5, 0.5)
        glittersound = this.sound.add('glittersound');
        glitter = this.add.sprite(game['level' + 1].x, game['level' + 1].y + 20, 'glitter').setOrigin(0.5, 0.5)
        anim = this.anims.create({
            key: 'glitter',
            frames: this.anims.generateFrameNumbers('glitter', {
                start: 0,
                end: 41
            }),
            frameRate: 24,
        });
        glitter.anims.load('glitter');
        if (level == 2) {
            glitter.x = game['level' + 2].x
            glitter.y = game['level' + 2].y
            game['level' + 1].setFrame(1)
            levellock1.visible = false
            for (i = 1; i <= 1; i++) {
                game['leveltick' + i].visible = true
            }
        } else if (level == 3) {
            glitter.x = game['level' + 3].x
            glitter.y = game['level' + 3].y
            game['level' + 1].setFrame(1)
            game['level' + 2].setFrame(1)
            levellock1.visible = false
            levellock2.visible = false
            for (i = 1; i <= 2; i++) {
                game['leveltick' + i].visible = true
            }
        } else if (level == 4) {
            glitter.x = game['level' + 4].x
            glitter.y = game['level' + 4].y
            game['level' + 1].setFrame(1)
            game['level' + 2].setFrame(1)
            game['level' + 3].setFrame(1)
            levellock1.visible = false
            levellock2.visible = false
            levellock3.visible = false
            for (i = 1; i <= 3; i++) {
                game['leveltick' + i].visible = true
            }
        } else if (level == 5) {
            glitter.x = game['level' + 5].x
            glitter.y = game['level' + 5].y
            game['level' + 1].setFrame(1)
            game['level' + 2].setFrame(1)
            game['level' + 3].setFrame(1)
            game['level' + 4].setFrame(1)
            levellock1.visible = false
            levellock2.visible = false
            levellock3.visible = false
            levellock4.visible = false
            for (i = 1; i <= 4; i++) {
                game['leveltick' + i].visible = true
            }
        } else if (level == 6) {
            glitter.x = game['level' + 6].x
            glitter.y = game['level' + 6].y
            game['level' + 1].setFrame(1)
            game['level' + 2].setFrame(1)
            game['level' + 3].setFrame(1)
            game['level' + 4].setFrame(1)
            game['level' + 5].setFrame(1)
            levellock1.visible = false
            levellock2.visible = false
            levellock3.visible = false
            levellock4.visible = false
            levellock5.visible = false
            for (i = 1; i <= 5; i++) {
                game['leveltick' + i].visible = true
            }
        } else if (level == 7) {
            glitter.x = game['level' + 7].x
            glitter.y = game['level' + 7].y
            game['level' + 1].setFrame(1)
            game['level' + 2].setFrame(1)
            game['level' + 3].setFrame(1)
            game['level' + 4].setFrame(1)
            game['level' + 5].setFrame(1)
            game['level' + 6].setFrame(1)
            levellock1.visible = false
            levellock2.visible = false
            levellock3.visible = false
            levellock4.visible = false
            levellock5.visible = false
            levellock6.visible = false
            for (i = 1; i <= 6; i++) {
                game['leveltick' + i].visible = true
            }
        } else if (level == 8) {
            glitter.x = game['level' + 8].x
            glitter.y = game['level' + 8].y
            game['level' + 1].setFrame(1)
            game['level' + 2].setFrame(1)
            game['level' + 3].setFrame(1)
            game['level' + 4].setFrame(1)
            game['level' + 5].setFrame(1)
            game['level' + 6].setFrame(1)
            game['level' + 7].setFrame(1)
            levellock1.visible = false
            levellock2.visible = false
            levellock3.visible = false
            levellock4.visible = false
            levellock5.visible = false
            levellock6.visible = false
            levellock7.visible = false
            for (i = 1; i <= 7; i++) {
                game['leveltick' + i].visible = true
            }
        }
        if (level == 1) {
            this.tweens.add({
                targets: levellock1,
                angle: 14,
                ease: 'Linear',
                duration: 100,
                delay: 2000,
                repeat: 3,
                yoyo: true,
            });
            this.tweens.add({
                targets: levellock1,
                scaleX: 0,
                scaleY: 0,
                ease: 'Linear',
                duration: 500,
                delay: 3500,
                onComplete: level1start,
                callbackScope: this
            });

            function level1start() {
                if (music.isPlaying == false) {
                    glittersound.stop()
                } else {
                    glittersound.play()
                }
                glitter.anims.play('glitter')
                game['level' + 1].setFrame(1)
                game['level' + 1].setInteractive({
                    pixelPerfect: true,
                    useHandCursor: true
                })
            }
        } else if (level == 2) {
            this.tweens.add({
                targets: levellock2,
                angle: 14,
                ease: 'Linear',
                duration: 100,
                delay: 2000,
                repeat: 3,
                yoyo: true,
            });
            this.tweens.add({
                targets: levellock2,
                scaleX: 0,
                scaleY: 0,
                ease: 'Linear',
                duration: 500,
                delay: 3500,
                onComplete: level1start,
                callbackScope: this
            });

            function level1start() {
                if (music.isPlaying == false) {
                    glittersound.stop()
                } else {
                    glittersound.play()
                }
                glitter.anims.play('glitter')
                game['level' + 2].setFrame(1)
                game['level' + 2].setInteractive({
                    pixelPerfect: true,
                    useHandCursor: true
                })
            }
        } else if (level == 3) {
            this.tweens.add({
                targets: levellock3,
                angle: 14,
                ease: 'Linear',
                duration: 100,
                delay: 2000,
                repeat: 3,
                yoyo: true,
            });
            this.tweens.add({
                targets: levellock3,
                scaleX: 0,
                scaleY: 0,
                ease: 'Linear',
                duration: 500,
                delay: 3500,
                onComplete: level1start,
                callbackScope: this
            });

            function level1start() {
                if (music.isPlaying == false) {
                    glittersound.stop()
                } else {
                    glittersound.play()
                }
                glitter.anims.play('glitter')
                game['level' + 3].setFrame(1)
                game['level' + 3].setInteractive({
                    pixelPerfect: true,
                    useHandCursor: true
                })
            }
        } else if (level == 4) {
            this.tweens.add({
                targets: levellock4,
                angle: 14,
                ease: 'Linear',
                duration: 100,
                delay: 2000,
                repeat: 3,
                yoyo: true,
            });
            this.tweens.add({
                targets: levellock4,
                scaleX: 0,
                scaleY: 0,
                ease: 'Linear',
                duration: 500,
                delay: 3500,
                onComplete: level1start,
                callbackScope: this
            });

            function level1start() {
                if (music.isPlaying == false) {
                    glittersound.stop()
                } else {
                    glittersound.play()
                }
                glitter.anims.play('glitter')
                game['level' + 4].setFrame(1)
                game['level' + 4].setInteractive({
                    pixelPerfect: true,
                    useHandCursor: true
                })
            }
        } else if (level == 5) {
            this.tweens.add({
                targets: levellock5,
                angle: 14,
                ease: 'Linear',
                duration: 100,
                delay: 2000,
                repeat: 3,
                yoyo: true,
            });
            this.tweens.add({
                targets: levellock5,
                scaleX: 0,
                scaleY: 0,
                ease: 'Linear',
                duration: 500,
                delay: 3500,
                onComplete: level1start,
                callbackScope: this
            });

            function level1start() {
                if (music.isPlaying == false) {
                    glittersound.stop()
                } else {
                    glittersound.play()
                }
                glitter.anims.play('glitter')
                game['level' + 5].setFrame(1)
                game['level' + 5].setInteractive({
                    pixelPerfect: true,
                    useHandCursor: true
                })
            }
        } else if (level == 6) {
            this.tweens.add({
                targets: levellock6,
                angle: 14,
                ease: 'Linear',
                duration: 100,
                delay: 2000,
                repeat: 3,
                yoyo: true,
            });
            this.tweens.add({
                targets: levellock6,
                scaleX: 0,
                scaleY: 0,
                ease: 'Linear',
                duration: 500,
                delay: 3500,
                onComplete: level1start,
                callbackScope: this
            });

            function level1start() {
                if (music.isPlaying == false) {
                    glittersound.stop()
                } else {
                    glittersound.play()
                }
                glitter.anims.play('glitter')
                game['level' + 6].setFrame(1)
                game['level' + 6].setInteractive({
                    pixelPerfect: true,
                    useHandCursor: true
                })
            }
        } else if (level == 7) {
            this.tweens.add({
                targets: levellock7,
                angle: 14,
                ease: 'Linear',
                duration: 100,
                delay: 2000,
                repeat: 3,
                yoyo: true,
            });
            this.tweens.add({
                targets: levellock7,
                scaleX: 0,
                scaleY: 0,
                ease: 'Linear',
                duration: 500,
                delay: 3500,
                onComplete: level1start,
                callbackScope: this
            });

            function level1start() {
                if (music.isPlaying == false) {
                    glittersound.stop()
                } else {
                    glittersound.play()
                }
                glitter.anims.play('glitter')
                game['level' + 7].setFrame(1)
                game['level' + 7].setInteractive({
                    pixelPerfect: true,
                    useHandCursor: true
                })
            }
        } else if (level == 8) {
            this.tweens.add({
                targets: levellock8,
                angle: 14,
                ease: 'Linear',
                duration: 100,
                delay: 2000,
                repeat: 3,
                yoyo: true,
            });
            this.tweens.add({
                targets: levellock8,
                scaleX: 0,
                scaleY: 0,
                ease: 'Linear',
                duration: 500,
                delay: 3500,
                onComplete: level1start,
                callbackScope: this
            });

            function level1start() {
                if (music.isPlaying == false) {
                    glittersound.stop()
                } else {
                    glittersound.play()
                }
                glitter.anims.play('glitter')
                game['level' + 8].setFrame(1)
                game['level' + 8].setInteractive({
                    pixelPerfect: true,
                    useHandCursor: true
                })
            }
        }
        for (i = 1; i <= 8; i++) {
            game['level' + i].on('pointerover', levelselectoverclick1);
            game['level' + i].on('pointerout', levelselectoutclick1);
            game['level' + i].on('pointerdown', levelselectdownclick1);
        }

        function levelselectoverclick1() {
            if (game['level' + this.texture.key.substr(5)].frame.name == 1 && !game['leveltick' + this.texture.key.substr(5)].visible) {
                game['level' + this.texture.key.substr(5)].setScale(1.05)
            }
        }

        function levelselectoutclick1() {
            if (game['level' + this.texture.key.substr(5)].frame.name == 1 && !game['leveltick' + this.texture.key.substr(5)].visible) {
                game['level' + this.texture.key.substr(5)].setScale(1)
            }
        }

        function levelselectdownclick1() {
            if (game['level' + this.texture.key.substr(5)].frame.name == 1 && !game['leveltick' + this.texture.key.substr(5)].visible && loadFinish) {
                WitchSDK.recordEvent(WitchSDK.Events.START_STAGE);
                if (music.isPlaying == false) {
                    clicksound.stop()
                } else {
                    clicksound.play()
                }
                game['level' + this.texture.key.substr(5)].setScale(1)
                transitionbackground1.alpha = 1
                transitionbackground2.alpha = 1
                transitionbackground3.alpha = 1
                transitionpanel.alpha = 1
                transitiondress.alpha = 1
                transitiontitle1.alpha = 1
                transitiontitle2.alpha = 1
                transitiontitle3.alpha = 1
                transitionstar1.alpha = 1
                transitionstar2.alpha = 1
                transitionstar3.alpha = 1
                transitionstar4.alpha = 1
                transitionstar5.alpha = 1
                transitionstar1.setScale(0)
                transitionstar2.setScale(0)
                transitionstar3.setScale(0)
                transitionstar4.setScale(0)
                transitionstar5.setScale(0)
                transitionbackground1.visible = true
                transitionbackground2.visible = true
                transitionbackground3.visible = true
                transitiondress.visible = false
                transitionpanel.visible = false
                transitiontitle1.visible = false
                transitiontitle2.visible = false
                transitiontitle3.visible = false
                transitionstar1.visible = false
                transitionstar2.visible = false
                transitionstar3.visible = false
                transitionstar4.visible = false
                transitionstar5.visible = false
                transitionbackground1.alpha = 0
                transitionbackground2.alpha = 0
                transitionbackground3.alpha = 0
                this.scene.tweens.add({
                    targets: transitionbackground1,
                    alpha: 1,
                    ease: 'Linear',
                    duration: 800,
                    onComplete: transitionstart1,
                    callbackScope: this
                })

                function transitionstart1() {
                    transitiondress.visible = true
                    transitiondress.alpha = 0
                    this.scene.tweens.add({
                        targets: transitiondress,
                        alpha: 1,
                        ease: 'Linear',
                        duration: 400,
                        onComplete: transitionstart2,
                        callbackScope: this
                    })
                    this.scene.tweens.add({
                        targets: transitionbackground2,
                        alpha: 1,
                        ease: 'Linear',
                        duration: 800,
                    })
                }

                function transitionstart2() {
                    transitionpanel.visible = true
                    transitionpanel.setScale(0)
                    this.scene.tweens.add({
                        targets: transitionpanel,
                        scaleX: 1,
                        scaleY: 1,
                        ease: 'Linear',
                        duration: 500,
                        onComplete: transitionstart3,
                        callbackScope: this
                    })
                }

                function transitionstart3() {
                    this.scene.tweens.add({
                        targets: transitionbackground3,
                        alpha: 1,
                        ease: 'Linear',
                        duration: 800,
                        onComplete: transitionstart4,
                        callbackScope: this
                    })
                }

                function transitionstart4() {
                    transitiontitle1.visible = true
                    transitiontitle2.visible = true
                    transitiontitle3.visible = true
                    transitiontitle1.alpha = 0
                    transitiontitle2.alpha = 0
                    transitiontitle3.alpha = 0
                    transitiontitle2.x = transitiontitle2.x + 100
                    transitiontitle3.x = transitiontitle3.x - 100
                    this.scene.tweens.add({
                        targets: transitiontitle1,
                        alpha: 1,
                        ease: 'Linear',
                        duration: 800,
                        onComplete: transitionstart5,
                        callbackScope: this
                    })
                    this.scene.tweens.add({
                        targets: transitiontitle2,
                        alpha: 1,
                        x: transitiontitle2.x - 100,
                        ease: 'Linear',
                        duration: 800,
                    })
                    this.scene.tweens.add({
                        targets: transitiontitle3,
                        alpha: 1,
                        x: transitiontitle3.x + 100,
                        ease: 'Linear',
                        duration: 800,
                    })
                }

                function transitionstart5() {
                    transitionstar1.setScale(0)
                    transitionstar2.setScale(0)
                    transitionstar3.setScale(0)
                    transitionstar4.setScale(0)
                    transitionstar5.setScale(0)
                    transitionstar1.visible = true
                    transitionstar2.visible = true
                    transitionstar3.visible = true
                    transitionstar4.visible = true
                    transitionstar5.visible = true
                    this.scene.tweens.add({
                        targets: transitionstar1,
                        scaleX: 1,
                        scaleY: 1,
                        ease: 'Linear',
                        duration: 300,
                    })
                    this.scene.tweens.add({
                        targets: transitionstar2,
                        scaleX: 0.9,
                        scaleY: 0.9,
                        ease: 'Linear',
                        duration: 300,
                    })
                    this.scene.tweens.add({
                        targets: transitionstar3,
                        scaleX: 0.9,
                        scaleY: 0.9,
                        ease: 'Linear',
                        duration: 300,
                    })
                    this.scene.tweens.add({
                        targets: transitionstar4,
                        scaleX: 0.8,
                        scaleY: 0.8,
                        ease: 'Linear',
                        duration: 300,
                    })
                    this.scene.tweens.add({
                        targets: transitionstar5,
                        scaleX: 0.65,
                        scaleY: 0.65,
                        ease: 'Linear',
                        duration: 300,
                        onComplete: transitionstar6,
                        callbackScope: this
                    })
                }

                function transitionstar6() {
                    if (level == 1) {
                        this.scene.scene.stop('titleScreen')
                        game.scene.run('Level1');
                    } else if (level == 2) {
                        this.scene.scene.stop('levelSelect')
                        game.scene.run('Level5');
                    } else if (level == 3) {
                        this.scene.scene.stop('levelSelect')
                        game.scene.run('Level2');
                    } else if (level == 4) {
                        this.scene.scene.stop('levelSelect')
                        game.scene.run('Level6');
                    } else if (level == 5) {
                        this.scene.scene.stop('levelSelect')
                        game.scene.run('Level3');
                    } else if (level == 6) {
                        this.scene.scene.stop('levelSelect')
                        game.scene.run('Level7');
                    } else if (level == 7) {
                        this.scene.scene.stop('levelSelect')
                        game.scene.run('Level4');
                    } else if (level == 8) {
                        this.scene.scene.stop('levelSelect')
                        game.scene.run('Level8');
                    }
                }
            }
        }

        function levelselectupclick1() {
            if (game['level' + this.texture.key.substr(5)].frame.name == 1 && !game['leveltick' + this.texture.key.substr(5)].visible) {
                game['level' + this.texture.key.substr(5)].setScale(1.05)
            }
        }
        mute = this.add.sprite(790, 10, 'uiButton').setFrame("button_music_on").setInteractive({
            pixelPerfect: true,
            useHandCursor: true
        })
        mute.setOrigin(1, 0)
        mute.on('pointerdown', function () {
            if (!isMuted) {
                isMuted = true
                music.pause()
                mute.setFrame("button_music_off")
            } else {
                isMuted = false
                music.resume()
                mute.setFrame("button_music_on")
            }
            var tween2 = this.tweens.add({
                targets: mute,
                scaleX: 0.9,
                scaleY: 0.9,
                ease: 'Linear',
                duration: 80,
                yoyo: true,
                repeat: 1,
            });
            if (music.isPlaying == false) {
                clicksound.stop()
            } else {
                clicksound.play()
            }
        }, this);
        if (music.isPlaying == false) {
            mute.setFrame("button_music_off")
        } else {
            mute.setFrame("button_music_on")
        }
        if (brandingMaterials.inGameLogo.display == true && brandingMaterials.inGameLogo.image != undefined) {
            inGameLogo1 = this.add.image(10, 590, 'inGameLogo1').setOrigin(0, 1).setInteractive({
                pixelPerfect: true,
                useHandCursor: true
            })
            inGameLogo1.on('pointerdown', brandingMaterials.inGameLogo.clickURL);
            inGameLogoani = this.time.addEvent({
                delay: 2000,
                callback: inGameLogoani,
                callbackScope: this
            });

            function inGameLogoani() {
                this.tweens.add({
                    targets: inGameLogo1,
                    scaleX: 1.05,
                    scaleY: 1.05,
                    ease: 'Linear',
                    duration: 100,
                    repeat: 1,
                    yoyo: true,
                    delay: 3000,
                    onComplete: inGameLogoani1,
                    callbackScope: this
                })
            }

            function inGameLogoani1() {
                this.tweens.add({
                    targets: inGameLogo1,
                    scaleX: 1.05,
                    scaleY: 1.05,
                    ease: 'Linear',
                    duration: 100,
                    repeat: 1,
                    yoyo: true,
                    delay: 3000,
                    onComplete: inGameLogoani1,
                    callbackScope: this
                })
            }
        }
        transitionbackground1 = this.add.image(400, 300, 'transitionbackground1').setOrigin(0.5, 0.5).setInteractive({
            pixelPerfect: true,
            useHandCursor: true
        })
        transitionbackground2 = this.add.image(400, 300, 'transitionbackground2').setOrigin(0.5, 0.5)
        transitionbackground3 = this.add.image(400, 300, 'transitionbackground3').setOrigin(0.5, 0.5)
        transitionpanel = this.add.image(393.5, 314, 'transitionpanel').setOrigin(0.5)
        transitiondress = this.add.image(267, 141, 'transitiondress').setOrigin(0, 0)
        transitiontitle1 = this.add.image(400, 329, 'title1');
        transitiontitle1.setOrigin(0.5);
        transitiontitle2 = this.add.image(380, 399, 'title2');
        transitiontitle2.setOrigin(0.5);
        transitiontitle3 = this.add.image(380, 459, 'title3');
        transitiontitle3.setOrigin(0.5);
        transitionstar1 = this.add.image(616.5, 168, 'transitionstar');
        transitionstar1.setOrigin(0.5);
        transitionstar2 = this.add.image(156.5, 168, 'transitionstar');
        transitionstar2.setOrigin(0.5);
        transitionstar2.setScale(0.9)
        transitionstar3 = this.add.image(596.5, 468, 'transitionstar');
        transitionstar3.setOrigin(0.5);
        transitionstar3.setScale(0.9)
        transitionstar4 = this.add.image(116.5, 468, 'transitionstar');
        transitionstar4.setOrigin(0.5);
        transitionstar4.setScale(0.8)
        transitionstar5 = this.add.image(400, 558, 'transitionstar');
        transitionstar5.setOrigin(0.5);
        transitionstar5.setScale(0.65)
        this.tweens.add({
            targets: transitionstar1,
            angle: 4,
            ease: 'Linear',
            duration: 300,
            repeat: -1,
            yoyo: true
        })
        this.tweens.add({
            targets: transitionstar2,
            angle: 4,
            ease: 'Linear',
            duration: 300,
            repeat: -1,
            yoyo: true
        })
        this.tweens.add({
            targets: transitionstar3,
            angle: 4,
            ease: 'Linear',
            duration: 300,
            repeat: -1,
            yoyo: true
        })
        this.tweens.add({
            targets: transitionstar4,
            angle: 4,
            ease: 'Linear',
            duration: 300,
            repeat: -1,
            yoyo: true
        })
        this.tweens.add({
            targets: transitionstar5,
            angle: 4,
            ease: 'Linear',
            duration: 300,
            repeat: -1,
            yoyo: true
        })
        this.time.addEvent({
            delay: 1000,
            callback: transitionstart11,
            callbackScope: this
        });

        function transitionstart11() {
            this.tweens.add({
                targets: transitionbackground1,
                alpha: 0,
                ease: 'Linear',
                duration: 700
            })
            this.tweens.add({
                targets: transitionbackground2,
                alpha: 0,
                ease: 'Linear',
                duration: 700
            })
            this.tweens.add({
                targets: transitionbackground3,
                alpha: 0,
                ease: 'Linear',
                duration: 700
            })
            this.tweens.add({
                targets: transitionpanel,
                alpha: 0,
                ease: 'Linear',
                duration: 700
            })
            this.tweens.add({
                targets: transitiondress,
                alpha: 0,
                ease: 'Linear',
                duration: 700
            })
            this.tweens.add({
                targets: transitiontitle1,
                alpha: 0,
                ease: 'Linear',
                duration: 700
            })
            this.tweens.add({
                targets: transitiontitle2,
                alpha: 0,
                ease: 'Linear',
                duration: 700
            })
            this.tweens.add({
                targets: transitiontitle3,
                alpha: 0,
                ease: 'Linear',
                duration: 700
            })
            this.tweens.add({
                targets: transitionstar1,
                alpha: 0,
                ease: 'Linear',
                duration: 700
            })
            this.tweens.add({
                targets: transitionstar2,
                alpha: 0,
                ease: 'Linear',
                duration: 700
            })
            this.tweens.add({
                targets: transitionstar3,
                alpha: 0,
                ease: 'Linear',
                duration: 700
            })
            this.tweens.add({
                targets: transitionstar4,
                alpha: 0,
                ease: 'Linear',
                duration: 700
            })
            this.tweens.add({
                targets: transitionstar5,
                alpha: 0,
                ease: 'Linear',
                duration: 700
            })
        }
        hitobject = this.add.image(400, 300, 'hitobject').setOrigin(0.5, 0.5).setInteractive({
            useHandCursor: true
        })
        hitobject.visible = false
        hitobject.on('pointerdown', hitobjectdown)

        function hitobjectdown() {}
        this.load.on('complete', function () {
            loadFinish = true
        });
        this.load.start();
    }
});
var ecount = 0
var ecount1 = 0
var level1lip;
var makestart1 = false
var startgame3 = false
var marr1 = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
var Level1 = new Phaser.Class({
    Extends: Phaser.Scene,
    initialize: function Level1() {
        Phaser.Scene.call(this, {
            key: 'Level1'
        });
    },
    preload: function () {
        marr1 = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        ecount = 0
        ecount1 = 0
        loadFinish = false
        startgame3 = false
        makestart1 = false
    },
    create: function () {
        level1background = this.add.image(400, 300, 'elsabackground1').setOrigin(0.5, 0.5).setScale(1.2)
        level1cupboard = this.add.image(0, 0, 'level1cupboard').setOrigin(0, 0)
        level1cupboard.y=15;
        var elsaelxarr = [, 109.50 , 107.50 , 206.50 , 202.50 , 216.50 , 310.50 , 322.50]
        var elsaelyarr = [,159.00 , 324.00 , 115.00 , 237.00 , 361.00 , 173.00 , 325.00 ]
        for (i = 1; i <= 7; i++) {
            game['elash' + i] = this.add.image(elsaelxarr[i], elsaelyarr[i], 'elash' + i).setOrigin(0.5, 0.5).setInteractive({
                useHandCursor: true
            })
            // game['elash' + i].x += parseFloat(game['elash' + i].width / 2)
            // game['elash' + i].y += parseFloat(game['elash' + i].height / 2)
            game['elash' + i].visible = false
        }
        var elsablxarr = [, 99.50, 100.50, 100.50, 214.50, 216.50, 217.50, 216.50, 324.50, 326.50, 327.50]
        var elsablyarr = [, 130.50, 241.50, 350.50, 114.50, 195.50, 284.50, 373.50, 128.50, 237.50, 351.50]
        for (i = 1; i <= 10; i++) {
            game['blush' + i] = this.add.image(elsablxarr[i], elsablyarr[i], 'blush' + i).setOrigin(0.5, 0.5).setInteractive({
                pixelPerfect: true,
                useHandCursor: true
            })
            // game['blush' + i].x += parseFloat(game['blush' + i].width / 2)
            // game['blush' + i].y += parseFloat(game['blush' + i].height / 2)
            game['blush' + i].visible = false
        }
        var elsalipxarr = [,99.00, 99.00, 98.00, 215.00, 215.00, 215.00, 213.00, 321.00, 321.00, 322.00]
        var elsalipyarr = [, 140.50, 235.50, 338.50, 115.50, 202.00, 286.00, 370.00, 135.00, 235.00, 333.00]
        for (i = 1; i <= 10; i++) {
            game['lip' + i] = this.add.image(elsalipxarr[i], elsalipyarr[i], 'lip' + i).setOrigin(0.5, 0.5).setInteractive({
                pixelPerfect: true,
                useHandCursor: true
            })
            // game['lip' + i].x += parseFloat(game['lip' + i].width / 2)
            // game['lip' + i].y += parseFloat(game['lip' + i].height / 2)
            game['lip' + i].visible = false
        }
        var elsaeyxarr = [, 99.50, 100.50, 100.50, 214.50, 216.50, 217.50, 216.50, 324.50, 326.50, 327.50]
        var elsaeyyarr = [,130.50, 241.50, 350.50, 114.50, 195.50, 284.50, 373.50, 128.50, 237.50, 351.50]
        for (i = 1; i <= 10; i++) {
            game['eye' + i] = this.add.image(elsaeyxarr[i], elsaeyyarr[i], 'eye' + i).setOrigin(0.5, 0.5).setInteractive({
                pixelPerfect: true,
                useHandCursor: true
            })
            // game['eye' + i].x += parseFloat(game['eye' + i].width / 2)
            // game['eye' + i].y += parseFloat(game['eye' + i].height / 2)
            game['eye' + i].visible = false
        }
        var elsaesxarr = [,57,57,57,170,170,170,170,280,280,280]
        var elsaesyarr = [, 92,192,292,72,155,239,330,91,191,291]
        for (i = 1; i <= 10; i++) {
            game['eshade' + i] = this.add.image(elsaesxarr[i], elsaesyarr[i], 'eshade' + i).setOrigin(0, 0).setInteractive({
                pixelPerfect: true,
                useHandCursor: true
            })
            // game['eshade' + i].x += parseFloat(game['eshade' + i].width / 2)
            // game['eshade' + i].y += parseFloat(game['eshade' + i].height / 2)
            game['eshade' + i].visible = false
        }
        var elsaebxarr = [,105.50, 108.50, 99.50, 215.50, 217.50, 211.50, 198.50, 326.50, 321.50, 321.50]
        var elsaebyarr = [,  142.50, 229.50, 321.50, 104.50, 195.50, 284.50, 367.50, 139.50, 227.50, 321.50]
        for (i = 1; i <= 10; i++) {
            game['ebrow' + i] = this.add.image(elsaebxarr[i], elsaebyarr[i], 'ebrow' + i).setOrigin(0.5, 0.5).setInteractive({
                pixelPerfect: true,
                useHandCursor: true
            })
            // game['ebrow' + i].x += parseFloat(game['ebrow' + i].width / 2)
            // game['ebrow' + i].y += parseFloat(game['ebrow' + i].height / 2)
            game['ebrow' + i].visible = false
        }
        var elsahrxarr = [, 36, 20, 218, 115, 230]
        var elsahryarr = [, 59, 229, 61, 160, 230]
        for (i = 1; i <= 5; i++) {
            game['elsahair' + i] = this.add.image(elsahrxarr[i], elsahryarr[i], 'elsahair' + i).setOrigin(0.5, 0.5).setInteractive({
                pixelPerfect: true,
                useHandCursor: true
            })
            game['elsahair' + i].x += parseFloat(game['elsahair' + i].width / 2)
            game['elsahair' + i].y += parseFloat(game['elsahair' + i].height / 2)
            game['elsahair' + i].visible = false
        }
        level1panelgroup = this.add.container(0, 0);
        level1panelgroup.add(level1cupboard)
        for (i = 1; i <= 7; i++) {
            level1panelgroup.add(game['elash' + i])
        }
        for (i = 1; i <= 10; i++) {
            level1panelgroup.add(game['blush' + i])
        }
        for (i = 1; i <= 10; i++) {
            level1panelgroup.add(game['lip' + i])
        }
        for (i = 1; i <= 10; i++) {
            level1panelgroup.add(game['eye' + i])
        }
        for (i = 1; i <= 10; i++) {
            level1panelgroup.add(game['eshade' + i])
        }
        for (i = 1; i <= 10; i++) {
            level1panelgroup.add(game['ebrow' + i])
        }
        for (i = 1; i <= 5; i++) {
            level1panelgroup.add(game['elsahair' + i])
        }
        level1panelgroup.x = -500

        l1back_hair = this.add.image(513  ,395  , 'l1b_hair').setOrigin(0, 0)

        level1body = this.add.image(699.00, 993.50, 'level1body').setOrigin(0.5, 0.5)
        l1dress = this.add.image(463  ,464 , 'l1dress').setOrigin(0, 0)
        level1lips = this.add.sprite(580.00, 410.00, 'level1lips').setOrigin(0, 0)
        level1lips.setFrame(ecount1)
        l1eye_white= this.add.image(676.50 ,391.00  , 'l1eye_white').setOrigin(0.5, 0.5)
        level1eye1 = this.add.sprite(574 ,327 , 'level1eye1').setOrigin(0, 0)
        // level1eye2 = this.add.sprite(466, 210, 'level1eye2').setOrigin(0, 0)
        level1toplayer = this.add.image(577, 315, 'level1toplayer').setOrigin(0, 0)
        level1eshade = this.add.sprite(577, 315, 'level1eshade').setOrigin(0, 0)
        level1eshade.setFrame(ecount)
        // level1elash = this.add.sprite(571 ,321  , 'level1elash').setOrigin(0, 0)
        level1elash = this.add.sprite(566 ,326  , 'level1elash').setOrigin(0, 0)
        level1blush = this.add.sprite(571,361 , 'level1blush').setOrigin(0, 0)
        level1ebrow = this.add.sprite(550 ,299, 'level1ebrow').setOrigin(0, 0)
        level1hair = this.add.sprite(432  ,169 , 'level1hair').setOrigin(0, 0)
        var level1dollgroup = this.add.container(0, 0);
        level1dollgroup.add([l1back_hair,level1body, l1dress,level1lips, l1eye_white,level1eye1, level1toplayer, level1eshade, level1elash, level1blush, level1ebrow, level1hair]);
        level1dollgroup.x = -50
        level1dollgroup.y = -150

        table = this.add.image(0, 485, 'table').setOrigin(0, 0)
        shadow = this.add.image(62, 491, 'shadow').setOrigin(0, 0)
        shadow1 = this.add.image(553, 550, 'shadow1').setOrigin(0, 0)
        var catxarr = [, 67, 140, 250, 295, 418, 515]
        var catyarr = [, 413, 442, 427, 503, 439, 433]

        
        for (i = 2; i <= 6; i++) {
            game['category' + i] = this.add.image(catxarr[i], catyarr[i], 'category' + i).setOrigin(0.5, 0.5).setInteractive({
                pixelPerfect: true,
                useHandCursor: true
            })
            game['category' + i].x += parseFloat(game['category' + i].width / 2)
            game['category' + i].y += parseFloat(game['category' + i].height / 2)
        }
        for (i = 1; i <= 1; i++) {
            game['category' + i] = this.add.image(catxarr[i], catyarr[i], 'category' + i).setOrigin(0.5, 0.5).setInteractive({
                useHandCursor: true
            })
            game['category' + i].x += parseFloat(game['category' + i].width / 2)
            game['category' + i].y += parseFloat(game['category' + i].height / 2)
        }
        // elsahairbtn = this.add.image(563, 408, 'elsahairbtn').setOrigin(0.5, 0.5).setInteractive({
        //     pixelPerfect: true,
        //     useHandCursor: true
        // })
        // elsahairbtn.x += parseFloat(elsahairbtn.width / 2)
        // elsahairbtn.y += parseFloat(elsahairbtn.height / 2)
        makeupcategory1 = this.add.container(0, 0);
        makeupcategory1.add([shadow, shadow1, game['category' + 1], game['category' + 2], game['category' + 3], game['category' + 4], game['category' + 5], game['category' + 6]]);
        makeupcategory1.x = 40
        glitter = this.add.sprite(589.1, 270.55, 'glitter').setOrigin(0.5, 0.5)
        anim = this.anims.create({
            key: 'glitter',
            frames: this.anims.generateFrameNumbers('glitter', {
                start: 0,
                end: 41
            }),
            frameRate: 24,
        });
        glitter.anims.load('glitter');
        clicksound = this.sound.add('clickss');
        done = this.add.image(790, 590, 'uiButton').setOrigin(1, 1).setInteractive({
            pixelPerfect: true,
            useHandCursor: true
        })
        done.setFrame("button_next")
        done.visible = false
        done.on('pointerover', function () {
            done.setScale(1.05, 1.05)
        });
        done.on('pointerout', function () {
            done.setScale(1, 1)
        });
        done.on('pointerup', function () {
            if (startgame3) {
                done.setScale(1.05, 1.05)
            }
        });
        done.on('pointerdown', function () {
            if (!startgame3 && loadFinish) {
                marr1[0] = level1elash.frame.name
                marr1[1] = level1blush.frame.name
                marr1[2] = ecount1
                marr1[3] = level1eye1.frame.name
                marr1[4] = ecount
                marr1[5] = level1ebrow.frame.name
                // marr1[6] = level1hair.frame.name
                startgame3 = true
                level = 2
                if (music.isPlaying == false) {
                    clicksound.stop()
                } else {
                    clicksound.play()
                }
                done.setScale(1, 1)
                transitionbackground1.alpha = 1
                transitionbackground2.alpha = 1
                transitionbackground3.alpha = 1
                transitionpanel.alpha = 1
                transitiondress.alpha = 1
                transitiontitle1.alpha = 1
                transitiontitle2.alpha = 1
                transitiontitle3.alpha = 1
                transitionstar1.alpha = 1
                transitionstar2.alpha = 1
                transitionstar3.alpha = 1
                transitionstar4.alpha = 1
                transitionstar5.alpha = 1
                transitionstar1.setScale(0)
                transitionstar2.setScale(0)
                transitionstar3.setScale(0)
                transitionstar4.setScale(0)
                transitionstar5.setScale(0)
                transitionbackground1.visible = true
                transitionbackground2.visible = true
                transitionbackground3.visible = true
                transitiondress.visible = false
                transitionpanel.visible = false
                transitiontitle1.visible = false
                transitiontitle2.visible = false
                transitiontitle3.visible = false
                transitionstar1.visible = false
                transitionstar2.visible = false
                transitionstar3.visible = false
                transitionstar4.visible = false
                transitionstar5.visible = false
                transitionbackground1.alpha = 0
                transitionbackground2.alpha = 0
                transitionbackground3.alpha = 0
                this.scene.tweens.add({
                    targets: transitionbackground1,
                    alpha: 1,
                    ease: 'Linear',
                    duration: 800,
                    onComplete: transitionstart1,
                    callbackScope: this
                })

                function transitionstart1() {
                    transitiondress.visible = true
                    transitiondress.alpha = 0
                    this.scene.tweens.add({
                        targets: transitiondress,
                        alpha: 1,
                        ease: 'Linear',
                        duration: 400,
                        onComplete: transitionstart2,
                        callbackScope: this
                    })
                    this.scene.tweens.add({
                        targets: transitionbackground2,
                        alpha: 1,
                        ease: 'Linear',
                        duration: 800,
                    })
                }

                function transitionstart2() {
                    transitionpanel.visible = true
                    transitionpanel.setScale(0)
                    this.scene.tweens.add({
                        targets: transitionpanel,
                        scaleX: 1,
                        scaleY: 1,
                        ease: 'Linear',
                        duration: 500,
                        onComplete: transitionstart3,
                        callbackScope: this
                    })
                }

                function transitionstart3() {
                    this.scene.tweens.add({
                        targets: transitionbackground3,
                        alpha: 1,
                        ease: 'Linear',
                        duration: 800,
                        onComplete: transitionstart4,
                        callbackScope: this
                    })
                }

                function transitionstart4() {
                    transitiontitle1.visible = true
                    transitiontitle2.visible = true
                    transitiontitle3.visible = true
                    transitiontitle1.alpha = 0
                    transitiontitle2.alpha = 0
                    transitiontitle3.alpha = 0
                    transitiontitle2.x = transitiontitle2.x + 100
                    transitiontitle3.x = transitiontitle3.x - 100
                    this.scene.tweens.add({
                        targets: transitiontitle1,
                        alpha: 1,
                        ease: 'Linear',
                        duration: 800,
                        onComplete: transitionstart5,
                        callbackScope: this
                    })
                    this.scene.tweens.add({
                        targets: transitiontitle2,
                        alpha: 1,
                        x: transitiontitle2.x - 100,
                        ease: 'Linear',
                        duration: 800,
                    })
                    this.scene.tweens.add({
                        targets: transitiontitle3,
                        alpha: 1,
                        x: transitiontitle3.x + 100,
                        ease: 'Linear',
                        duration: 800,
                    })
                }

                function transitionstart5() {
                    transitionstar1.setScale(0)
                    transitionstar2.setScale(0)
                    transitionstar3.setScale(0)
                    transitionstar4.setScale(0)
                    transitionstar5.setScale(0)
                    transitionstar1.visible = true
                    transitionstar2.visible = true
                    transitionstar3.visible = true
                    transitionstar4.visible = true
                    transitionstar5.visible = true
                    this.scene.tweens.add({
                        targets: transitionstar1,
                        scaleX: 1,
                        scaleY: 1,
                        ease: 'Linear',
                        duration: 300,
                    })
                    this.scene.tweens.add({
                        targets: transitionstar2,
                        scaleX: 0.9,
                        scaleY: 0.9,
                        ease: 'Linear',
                        duration: 300,
                    })
                    this.scene.tweens.add({
                        targets: transitionstar3,
                        scaleX: 0.9,
                        scaleY: 0.9,
                        ease: 'Linear',
                        duration: 300,
                    })
                    this.scene.tweens.add({
                        targets: transitionstar4,
                        scaleX: 0.8,
                        scaleY: 0.8,
                        ease: 'Linear',
                        duration: 300,
                    })
                    this.scene.tweens.add({
                        targets: transitionstar5,
                        scaleX: 0.65,
                        scaleY: 0.65,
                        ease: 'Linear',
                        duration: 300,
                        onComplete: transitionstar6,
                        callbackScope: this
                    })
                }

                function transitionstar6() {
                    this.scene.scene.stop('Level1')
                    game.scene.run('levelSelect');
                }
            }
        });
        var tween1 = this.tweens.add({
            targets: level1eye1,
            x: level1eye1.x - 3,
            ease: 'Linear',
            duration: 300,
            delay: 2400,
            callbackScope: this,
        });
        // var tween2 = this.tweens.add({
        //     targets: level1eye2,
        //     x: level1eye2.x - 3,
        //     ease: 'Linear',
        //     duration: 300,
        //     delay: 2400,
        //     onComplete: annaeyestartani1,
        //     callbackScope: this
        // });

        function annaeyestartani1() {
            this.tweens.add({
                targets: level1eye1,
                x: level1eye1.x + 3,
                ease: 'Linear',
                duration: 300,
                delay: 3000,
                callbackScope: this
            });
            // this.tweens.add({
            //     targets: level1eye2,
            //     x: level1eye2.x + 3,
            //     ease: 'Linear',
            //     duration: 300,
            //     delay: 3000,
            //     onComplete: annaeyestartani2,
            //     callbackScope: this,
            // })
        }

        function annaeyestartani2() {
            this.tweens.add({
                targets: level1eye1,
                x: level1eye1.x - 3,
                ease: 'Linear',
                duration: 300,
                delay: 2400,
                callbackScope: this
            });
            // this.tweens.add({
            //     targets: level1eye2,
            //     x: level1eye2.x - 3,
            //     ease: 'Linear',
            //     duration: 300,
            //     delay: 2400,
            //     onComplete: annaeyestartani1,
            //     callbackScope: this,
            // })
        }
        level1eshad = this.time.addEvent({
            delay: 1000,
            callback: level1eblinkstart1,
            callbackScope: this
        });

        function level1eblinkstart1() {
            level1eshade.setFrame(ecount + 1)
            level1elash.visible = false
            level1eshad = this.time.addEvent({
                delay: 200,
                callback: level1eblinkstart2,
                callbackScope: this
            });
        }

        function level1eblinkstart2() {
            level1eshade.setFrame(ecount)
            level1elash.visible = true
            level1eshad = this.time.addEvent({
                delay: 4000,
                callback: level1eblinkstart1,
                callbackScope: this
            });
        }
        level1lip = this.time.addEvent({
            delay: 1000,
            callback: level1lipsstart1,
            callbackScope: this
        });

        function level1lipsstart1() {
            level1lips.setFrame(ecount1 + 1)
            level1lip = this.time.addEvent({
                delay: 100,
                callback: level1lipsstart2,
                callbackScope: this
            });
        }

        function level1lipsstart2() {
            level1lips.setFrame(ecount1 + 2)
            level1lip = this.time.addEvent({
                delay: 2500,
                callback: level1lipsstart3,
                callbackScope: this
            });
        }

        function level1lipsstart3() {
            level1lips.setFrame(ecount1 + 1)
            level1lip = this.time.addEvent({
                delay: 100,
                callback: level1lipsstart4,
                callbackScope: this
            });
        }

        function level1lipsstart4() {
            level1lips.setFrame(ecount1)
            level1lip = this.time.addEvent({
                delay: 3000,
                callback: level1lipsstart1,
                callbackScope: this
            });
        }
        mute = this.add.sprite(790, 10, 'uiButton').setFrame("button_music_on").setInteractive({
            pixelPerfect: true,
            useHandCursor: true
        })
        mute.setOrigin(1, 0)
        mute.on('pointerdown', function () {
            if (!isMuted) {
                isMuted = true
                music.pause()
                mute.setFrame("button_music_off")
            } else {
                isMuted = false
                music.resume()
                mute.setFrame("button_music_on")
            }
            var tween2 = this.tweens.add({
                targets: mute,
                scaleX: 0.9,
                scaleY: 0.9,
                ease: 'Linear',
                duration: 80,
                yoyo: true,
                repeat: 1,
            });
            if (music.isPlaying == false) {
                clicksound.stop()
            } else {
                clicksound.play()
            }
        }, this);
        if (music.isPlaying == false) {
            mute.setFrame("button_music_off")
        } else {
            mute.setFrame("button_music_on")
        }
        if (brandingMaterials.inGameLogo.display == true && brandingMaterials.inGameLogo.image != undefined) {
            inGameLogo1 = this.add.image(10, 590, 'inGameLogo1').setOrigin(0, 1).setInteractive({
                pixelPerfect: true,
                useHandCursor: true
            })
            inGameLogo1.on('pointerdown', brandingMaterials.inGameLogo.clickURL);
            inGameLogoani = this.time.addEvent({
                delay: 2000,
                callback: inGameLogoani,
                callbackScope: this
            });

            function inGameLogoani() {
                this.tweens.add({
                    targets: inGameLogo1,
                    scaleX: 1.05,
                    scaleY: 1.05,
                    ease: 'Linear',
                    duration: 100,
                    repeat: 1,
                    yoyo: true,
                    delay: 3000,
                    onComplete: inGameLogoani1,
                    callbackScope: this
                })
            }

            function inGameLogoani1() {
                this.tweens.add({
                    targets: inGameLogo1,
                    scaleX: 1.05,
                    scaleY: 1.05,
                    ease: 'Linear',
                    duration: 100,
                    repeat: 1,
                    yoyo: true,
                    delay: 3000,
                    onComplete: inGameLogoani1,
                    callbackScope: this
                })
            }
        }
        transitionbackground1 = this.add.image(400, 300, 'transitionbackground1').setOrigin(0.5, 0.5).setInteractive({
            pixelPerfect: true,
            useHandCursor: true
        })
        transitionbackground2 = this.add.image(400, 300, 'transitionbackground2').setOrigin(0.5, 0.5)
        transitionbackground3 = this.add.image(400, 300, 'transitionbackground3').setOrigin(0.5, 0.5)
        transitionpanel = this.add.image(393.5, 314, 'transitionpanel').setOrigin(0.5)
        transitiondress = this.add.image(267, 141, 'transitiondress').setOrigin(0, 0)
        transitiontitle1 = this.add.image(400, 329, 'title1');
        transitiontitle1.setOrigin(0.5);
        transitiontitle2 = this.add.image(380, 399, 'title2');
        transitiontitle2.setOrigin(0.5);
        transitiontitle3 = this.add.image(380, 459, 'title3');
        transitiontitle3.setOrigin(0.5);
        transitionstar1 = this.add.image(616.5, 168, 'transitionstar');
        transitionstar1.setOrigin(0.5);
        transitionstar2 = this.add.image(156.5, 168, 'transitionstar');
        transitionstar2.setOrigin(0.5);
        transitionstar2.setScale(0.9)
        transitionstar3 = this.add.image(596.5, 468, 'transitionstar');
        transitionstar3.setOrigin(0.5);
        transitionstar3.setScale(0.9)
        transitionstar4 = this.add.image(116.5, 468, 'transitionstar');
        transitionstar4.setOrigin(0.5);
        transitionstar4.setScale(0.8)
        transitionstar5 = this.add.image(400, 558, 'transitionstar');
        transitionstar5.setOrigin(0.5);
        transitionstar5.setScale(0.65)
        this.tweens.add({
            targets: transitionstar1,
            angle: 4,
            ease: 'Linear',
            duration: 300,
            repeat: -1,
            yoyo: true
        })
        this.tweens.add({
            targets: transitionstar2,
            angle: 4,
            ease: 'Linear',
            duration: 300,
            repeat: -1,
            yoyo: true
        })
        this.tweens.add({
            targets: transitionstar3,
            angle: 4,
            ease: 'Linear',
            duration: 300,
            repeat: -1,
            yoyo: true
        })
        this.tweens.add({
            targets: transitionstar4,
            angle: 4,
            ease: 'Linear',
            duration: 300,
            repeat: -1,
            yoyo: true
        })
        this.tweens.add({
            targets: transitionstar5,
            angle: 4,
            ease: 'Linear',
            duration: 300,
            repeat: -1,
            yoyo: true
        })
        this.time.addEvent({
            delay: 1000,
            callback: transitionstart11,
            callbackScope: this
        });

        function transitionstart11() {
            this.tweens.add({
                targets: transitionbackground1,
                alpha: 0,
                ease: 'Linear',
                duration: 700
            })
            this.tweens.add({
                targets: transitionbackground2,
                alpha: 0,
                ease: 'Linear',
                duration: 700
            })
            this.tweens.add({
                targets: transitionbackground3,
                alpha: 0,
                ease: 'Linear',
                duration: 700
            })
            this.tweens.add({
                targets: transitionpanel,
                alpha: 0,
                ease: 'Linear',
                duration: 700
            })
            this.tweens.add({
                targets: transitiondress,
                alpha: 0,
                ease: 'Linear',
                duration: 700
            })
            this.tweens.add({
                targets: transitiontitle1,
                alpha: 0,
                ease: 'Linear',
                duration: 700
            })
            this.tweens.add({
                targets: transitiontitle2,
                alpha: 0,
                ease: 'Linear',
                duration: 700
            })
            this.tweens.add({
                targets: transitiontitle3,
                alpha: 0,
                ease: 'Linear',
                duration: 700
            })
            this.tweens.add({
                targets: transitionstar1,
                alpha: 0,
                ease: 'Linear',
                duration: 700
            })
            this.tweens.add({
                targets: transitionstar2,
                alpha: 0,
                ease: 'Linear',
                duration: 700
            })
            this.tweens.add({
                targets: transitionstar3,
                alpha: 0,
                ease: 'Linear',
                duration: 700
            })
            this.tweens.add({
                targets: transitionstar4,
                alpha: 0,
                ease: 'Linear',
                duration: 700
            })
            this.tweens.add({
                targets: transitionstar5,
                alpha: 0,
                ease: 'Linear',
                duration: 700
            })
        }
        hitobject = this.add.image(400, 300, 'hitobject').setOrigin(0.5, 0.5).setInteractive({
            useHandCursor: true
        })
        hitobject.visible = false
        hitobject.on('pointerdown', hitobjectdown)

        function hitobjectdown() {}
        this.load.on('complete', function () {
            loadFinish = true
        });
        this.load.start();
        level1clickstart()
    }
});

function level1clickstart() {
    for (i = 1; i <= 7; i++) {
        game['elash' + i].on('pointerover', elashoverclick1);
        game['elash' + i].on('pointerout', elashoutclick1);
        game['elash' + i].on('pointerdown', elashdownclick1);
        game['elash' + i].on('pointerup', elashupclick1);
    }

    function elashoverclick1() {
        game['elash' + this.texture.key.substr(5)].setScale(1.05, 1.05)
    }

    function elashoutclick1() {
        game['elash' + this.texture.key.substr(5)].setScale(1, 1)
    }

    function elashdownclick1() {
        if (music.isPlaying == false) {
            clicksound.stop()
        } else {
            clicksound.play()
        }
        done.visible = true
        game['elash' + this.texture.key.substr(5)].setScale(1, 1)
        done.visible = true
        sno = this.texture.key.substr(5)
        glitter.anims.play('glitter')
        if (marr1[0] == parseInt(sno)) {
            level1elash.setFrame(0)
            marr1[0] = 0
        } else {
            marr1[0] = parseInt(sno)
            level1elash.setFrame(parseInt(sno))
        }
    }

    function elashupclick1() {
        game['elash' + this.texture.key.substr(5)].setScale(1.05, 1.05)
    }
    for (i = 1; i <= 10; i++) {
        game['blush' + i].on('pointerover', blushoverclick1);
        game['blush' + i].on('pointerout', blushoutclick1);
        game['blush' + i].on('pointerdown', blushdownclick1);
        game['blush' + i].on('pointerup', blushupclick1);
    }

    function blushoverclick1() {
        game['blush' + this.texture.key.substr(5)].setScale(1.05, 1.05)
    }

    function blushoutclick1() {
        game['blush' + this.texture.key.substr(5)].setScale(1, 1)
    }

    function blushdownclick1() {
        if (music.isPlaying == false) {
            clicksound.stop()
        } else {
            clicksound.play()
        }
        done.visible = true
        game['blush' + this.texture.key.substr(5)].setScale(1, 1)
        done.visible = true
        sno = this.texture.key.substr(5)
        glitter.anims.play('glitter')
        if (marr1[1] == parseInt(sno)) {
            level1blush.setFrame(0)
            marr1[1] = 0
        } else {
            marr1[1] = parseInt(sno)
            level1blush.setFrame(parseInt(sno))
        }
    }

    function blushupclick1() {
        game['blush' + this.texture.key.substr(5)].setScale(1.05, 1.05)
    }
    for (i = 1; i <= 10; i++) {
        game['lip' + i].on('pointerover', lipoverclick1);
        game['lip' + i].on('pointerout', lipoutclick1);
        game['lip' + i].on('pointerdown', lipdownclick1);
        game['lip' + i].on('pointerup', lipupclick1);
    }

    function lipoverclick1() {
        game['lip' + this.texture.key.substr(3)].setScale(1.05, 1.05)
    }

    function lipoutclick1() {
        game['lip' + this.texture.key.substr(3)].setScale(1, 1)
    }

    function lipdownclick1() {
        if (music.isPlaying == false) {
            clicksound.stop()
        } else {
            clicksound.play()
        }
        done.visible = true
        game['lip' + this.texture.key.substr(3)].setScale(1, 1)
        done.visible = true
        sno = this.texture.key.substr(3)
        glitter.anims.play('glitter')
        if (marr1[2] == parseInt(sno)) {
            level1lips.setFrame(0)
            marr1[2] = 0
            ecount1 = 0
        } else {
            marr1[2] = parseInt(sno)
            level1lips.setFrame(parseInt(sno) * 3)
            ecount1 = marr1[2] * 3
        }
        level1lip.remove()
        level1lip = this.scene.time.addEvent({
            delay: 1000,
            callback: level1lipsstart1,
            callbackScope: this
        });

        function level1lipsstart1() {
            level1lips.setFrame(ecount1 + 1)
            level1lip = this.scene.time.addEvent({
                delay: 100,
                callback: level1lipsstart2,
                callbackScope: this
            });
        }

        function level1lipsstart2() {
            level1lips.setFrame(ecount1 + 2)
            level1lip = this.scene.time.addEvent({
                delay: 2500,
                callback: level1lipsstart3,
                callbackScope: this
            });
        }

        function level1lipsstart3() {
            level1lips.setFrame(ecount1 + 1)
            level1lip = this.scene.time.addEvent({
                delay: 100,
                callback: level1lipsstart4,
                callbackScope: this
            });
        }

        function level1lipsstart4() {
            level1lips.setFrame(ecount1)
            level1lip = this.scene.time.addEvent({
                delay: 3000,
                callback: level1lipsstart1,
                callbackScope: this
            });
        }
    }

    function lipupclick1() {
        game['lip' + this.texture.key.substr(3)].setScale(1.05, 1.05)
    }
    for (i = 1; i <= 10; i++) {
        game['eye' + i].on('pointerover', eyeoverclick1);
        game['eye' + i].on('pointerout', eyeoutclick1);
        game['eye' + i].on('pointerdown', eyedownclick1);
        game['eye' + i].on('pointerup', eyeupclick1);
    }

    function eyeoverclick1() {
        game['eye' + this.texture.key.substr(3)].setScale(1.05, 1.05)
    }

    function eyeoutclick1() {
        game['eye' + this.texture.key.substr(3)].setScale(1, 1)
    }

    function eyedownclick1() {
        if (music.isPlaying == false) {
            clicksound.stop()
        } else {
            clicksound.play()
        }
        done.visible = true
        game['eye' + this.texture.key.substr(3)].setScale(1, 1)
        done.visible = true
        sno = this.texture.key.substr(3)
        glitter.anims.play('glitter')
        if (marr1[3] == parseInt(sno)) {
            level1eye1.setFrame(0)
            // level1eye2.setFrame(0)
            marr1[3] = 0
        } else {
            marr1[3] = parseInt(sno)
            level1eye1.setFrame(parseInt(sno))
            // level1eye2.setFrame(parseInt(sno))
        }
    }

    function eyeupclick1() {
        game['eye' + this.texture.key.substr(3)].setScale(1.05, 1.05)
    }
    for (i = 1; i <= 10; i++) {
        game['eshade' + i].on('pointerover', eshadeoverclick1);
        game['eshade' + i].on('pointerout', eshadeoutclick1);
        game['eshade' + i].on('pointerdown', eshadedownclick1);
        game['eshade' + i].on('pointerup', eshadeupclick1);
    }

    function eshadeoverclick1() {
        game['eshade' + this.texture.key.substr(6)].setScale(1.05, 1.05)
    }

    function eshadeoutclick1() {
        game['eshade' + this.texture.key.substr(6)].setScale(1, 1)
    }

    function eshadedownclick1() {
        if (music.isPlaying == false) {
            clicksound.stop()
        } else {
            clicksound.play()
        }
        done.visible = true
        game['eshade' + this.texture.key.substr(6)].setScale(1, 1)
        done.visible = true
        sno = this.texture.key.substr(6)
        glitter.anims.play('glitter')
        if (marr1[4] == parseInt(sno)) {
            level1eshade.setFrame(0)
            marr1[4] = 0
            ecount = 0
        } else {
            marr1[4] = parseInt(sno)
            level1eshade.setFrame(parseInt(sno) * 2)
            ecount = marr1[4] * 2
        }
    }

    function eshadeupclick1() {
        game['eshade' + this.texture.key.substr(6)].setScale(1.05, 1.05)
    }
    for (i = 1; i <= 10; i++) {
        game['ebrow' + i].on('pointerover', ebrowoverclick1);
        game['ebrow' + i].on('pointerout', ebrowoutclick1);
        game['ebrow' + i].on('pointerdown', ebrowdownclick1);
        game['ebrow' + i].on('pointerup', ebrowupclick1);
    }

    function ebrowoverclick1() {
        game['ebrow' + this.texture.key.substr(5)].setScale(1.05, 1.05)
    }

    function ebrowoutclick1() {
        game['ebrow' + this.texture.key.substr(5)].setScale(1, 1)
    }

    function ebrowdownclick1() {
        if (music.isPlaying == false) {
            clicksound.stop()
        } else {
            clicksound.play()
        }
        done.visible = true
        game['ebrow' + this.texture.key.substr(5)].setScale(1, 1)
        done.visible = true
        sno = this.texture.key.substr(5)
        glitter.anims.play('glitter')
        if (marr1[5] == parseInt(sno)) {
            level1ebrow.setFrame(0)
            marr1[5] = 0
        } else {
            marr1[5] = parseInt(sno)
            level1ebrow.setFrame(parseInt(sno))
        }
    }

    function ebrowupclick1() {
        game['ebrow' + this.texture.key.substr(5)].setScale(1.05, 1.05)
    }
    // for (i = 1; i <= 5; i++) {
    //     game['elsahair' + i].on('pointerover', hairoverclick1);
    //     game['elsahair' + i].on('pointerout', hairoutclick1);
    //     game['elsahair' + i].on('pointerdown', hairdownclick1);
    //     game['elsahair' + i].on('pointerup', hairupclick1);
    // }

    // function hairoverclick1() {
    //     game['elsahair' + this.texture.key.substr(8)].setScale(1.05, 1.05)
    // }

    // function hairoutclick1() {
    //     game['elsahair' + this.texture.key.substr(8)].setScale(1, 1)
    // }

    function hairdownclick1() {
        // if (music.isPlaying == false) {
        //     clicksound.stop()
        // } else {
        //     clicksound.play()
        // }
        // done.visible = true
        // game['elsahair' + this.texture.key.substr(8)].setScale(1, 1)
        // done.visible = true
        // sno = this.texture.key.substr(8)
        // glitter.anims.play('glitter')
        // if (marr1[6] == parseInt(sno)) {
        //     // level1hair.setFrame(0)
        //     marr1[6] = 0
        // } else {
        //     marr1[6] = parseInt(sno)
        //     level1hair.setFrame(parseInt(sno))
        // }
    }

    // function hairupclick1() {
    //     game['elsahair' + this.texture.key.substr(8)].setScale(1.05, 1.05)
    // }
    // elsahairbtn.on('pointerover', elsahairbtnoverclick1);
    // elsahairbtn.on('pointerout', elsahairbtnoutclick1);
    // elsahairbtn.on('pointerdown', elsahairbtndownclick1);
    // elsahairbtn.on('pointerup', elsahairbtnupclick1);

    // function elsahairbtnoverclick1() {
    //     elsahairbtn.setScale(1.05, 1.05)
    // }

    // function elsahairbtnoutclick1() {
    //     elsahairbtn.setScale(1, 1)
    // }

    // function elsahairbtndownclick1() {
    //     if (music.isPlaying == false) {
    //         clicksound.stop()
    //     } else {
    //         clicksound.play()
    //     }
    //     elsahairbtn.setScale(1, 1)
    //     if (!makestart1) {
    //         makestart1 = true
    //         this.scene.tweens.add({
    //             targets: level1panelgroup,
    //             x: 0,
    //             ease: 'Linear',
    //             duration: 300,
    //         });
    //         for (i = 1; i <= 7; i++) {
    //             game['elash' + i].visible = false
    //         }
    //         for (i = 1; i <= 10; i++) {
    //             game['blush' + i].visible = false
    //             game['lip' + i].visible = false
    //             game['eye' + i].visible = false
    //             game['eshade' + i].visible = false
    //             game['ebrow' + i].visible = false
    //         }
    //         for (i = 1; i <= 5; i++) {
    //             game['elsahair' + i].visible = true
    //         }
    //     } else {
    //         if (!game['elsahair' + 1].visible) {
    //             this.scene.tweens.add({
    //                 targets: level1panelgroup,
    //                 x: -500,
    //                 ease: 'Sine.easeIn',
    //                 duration: 300,
    //                 onComplete: levelpanel1move
    //             });

    //             function levelpanel1move() {
    //                 for (i = 1; i <= 7; i++) {
    //                     game['elash' + i].visible = false
    //                 }
    //                 for (i = 1; i <= 10; i++) {
    //                     game['blush' + i].visible = false
    //                     game['lip' + i].visible = false
    //                     game['eye' + i].visible = false
    //                     game['eshade' + i].visible = false
    //                     game['ebrow' + i].visible = false
    //                 }
    //                 this.parent.scene.tweens.add({
    //                     targets: level1panelgroup,
    //                     x: 0,
    //                     ease: 'Sine.easeOut',
    //                     duration: 300,
    //                 });
    //                 for (i = 1; i <= 5; i++) {
    //                     game['elsahair' + i].visible = true
    //                 }
    //             }
    //         }
    //     }
    // }

    // function elsahairbtnupclick1() {
    //     elsahairbtn.setScale(1.05, 1.05)
    // }
    for (i = 1; i <= 6; i++) {
        game['category' + i].on('pointerover', categoryoverclick1);
        game['category' + i].on('pointerout', categoryoutclick1);
        game['category' + i].on('pointerdown', categorydownclick1);
        game['category' + i].on('pointerup', categoryupclick1);
    }

    function categoryoverclick1() {
        game['category' + this.texture.key.substr(8)].setScale(1.05, 1.05)
    }

    function categoryoutclick1() {
        game['category' + this.texture.key.substr(8)].setScale(1, 1)
    }

    function categoryupclick1() {
        game['category' + this.texture.key.substr(8)].setScale(1.05, 1.05)
    }

    function categorydownclick1() {
        if (music.isPlaying == false) {
            clicksound.stop()
        } else {
            clicksound.play()
        }
        sno = this.texture.key.substr(8)
        game['category' + parseInt(sno)].setScale(1, 1)
        if (!makestart1) {
            makestart1 = true
            this.scene.tweens.add({
                targets: level1panelgroup,
                x: 0,
                ease: 'Linear',
                duration: 300,
            });
            for (i = 1; i <= 7; i++) {
                game['elash' + i].visible = false
            }
            for (i = 1; i <= 10; i++) {
                game['blush' + i].visible = false
                game['lip' + i].visible = false
                game['eye' + i].visible = false
                game['eshade' + i].visible = false
                game['ebrow' + i].visible = false
            }
            for (i = 1; i <= 5; i++) {
                game['elsahair' + i].visible = false
            }
            if (parseInt(sno) == 1) {
                for (i = 1; i <= 7; i++) {
                    game['elash' + i].visible = true
                }
            } else if (parseInt(sno) == 2) {
                for (i = 1; i <= 10; i++) {
                    game['blush' + i].visible = true
                }
            } else if (parseInt(sno) == 3) {
                for (i = 1; i <= 10; i++) {
                    game['lip' + i].visible = true
                }
            } else if (parseInt(sno) == 4) {
                for (i = 1; i <= 10; i++) {
                    game['eye' + i].visible = true
                }
            } else if (parseInt(sno) == 5) {
                for (i = 1; i <= 10; i++) {
                    game['eshade' + i].visible = true
                }
            } else if (parseInt(sno) == 6) {
                for (i = 1; i <= 10; i++) {
                    game['ebrow' + i].visible = true
                }
            }
        } else {
            if (parseInt(sno) == 1 && !game['elash' + 1].visible) {
                this.scene.tweens.add({
                    targets: level1panelgroup,
                    x: -500,
                    ease: 'Linear',
                    duration: 300,
                    onComplete: levelpanel1move
                });

                function levelpanel1move() {
                    for (i = 1; i <= 7; i++) {
                        game['elash' + i].visible = false
                    }
                    for (i = 1; i <= 10; i++) {
                        game['blush' + i].visible = false
                        game['lip' + i].visible = false
                        game['eye' + i].visible = false
                        game['eshade' + i].visible = false
                        game['ebrow' + i].visible = false
                    }
                    for (i = 1; i <= 5; i++) {
                        game['elsahair' + i].visible = false
                    }
                    this.parent.scene.tweens.add({
                        targets: level1panelgroup,
                        x: 0,
                        ease: 'Linear',
                        duration: 300,
                    });
                    for (i = 1; i <= 7; i++) {
                        game['elash' + i].visible = true
                    }
                }
            } else if (parseInt(sno) == 2 && !game['blush' + 1].visible) {
                this.scene.tweens.add({
                    targets: level1panelgroup,
                    x: -500,
                    ease: 'Linear',
                    duration: 300,
                    onComplete: levelpanel1move
                });

                function levelpanel1move() {
                    for (i = 1; i <= 7; i++) {
                        game['elash' + i].visible = false
                    }
                    for (i = 1; i <= 10; i++) {
                        game['blush' + i].visible = false
                        game['lip' + i].visible = false
                        game['eye' + i].visible = false
                        game['eshade' + i].visible = false
                        game['ebrow' + i].visible = false
                    }
                    for (i = 1; i <= 5; i++) {
                        game['elsahair' + i].visible = false
                    }
                    this.parent.scene.tweens.add({
                        targets: level1panelgroup,
                        x: 0,
                        ease: 'Linear',
                        duration: 300,
                    });
                    for (i = 1; i <= 10; i++) {
                        game['blush' + i].visible = true
                    }
                }
            } else if (parseInt(sno) == 3 && !game['lip' + 1].visible) {
                this.scene.tweens.add({
                    targets: level1panelgroup,
                    x: -500,
                    ease: 'Linear',
                    duration: 300,
                    onComplete: levelpanel1move
                });

                function levelpanel1move() {
                    for (i = 1; i <= 7; i++) {
                        game['elash' + i].visible = false
                    }
                    for (i = 1; i <= 10; i++) {
                        game['blush' + i].visible = false
                        game['lip' + i].visible = false
                        game['eye' + i].visible = false
                        game['eshade' + i].visible = false
                        game['ebrow' + i].visible = false
                    }
                    for (i = 1; i <= 5; i++) {
                        game['elsahair' + i].visible = false
                    }
                    this.parent.scene.tweens.add({
                        targets: level1panelgroup,
                        x: 0,
                        ease: 'Linear',
                        duration: 300,
                    });
                    for (i = 1; i <= 10; i++) {
                        game['lip' + i].visible = true
                    }
                }
            } else if (parseInt(sno) == 4 && !game['eye' + 1].visible) {
                this.scene.tweens.add({
                    targets: level1panelgroup,
                    x: -500,
                    ease: 'Linear',
                    duration: 300,
                    onComplete: levelpanel1move
                });

                function levelpanel1move() {
                    for (i = 1; i <= 7; i++) {
                        game['elash' + i].visible = false
                    }
                    for (i = 1; i <= 10; i++) {
                        game['blush' + i].visible = false
                        game['lip' + i].visible = false
                        game['eye' + i].visible = false
                        game['eshade' + i].visible = false
                        game['ebrow' + i].visible = false
                    }
                    for (i = 1; i <= 5; i++) {
                        game['elsahair' + i].visible = false
                    }
                    this.parent.scene.tweens.add({
                        targets: level1panelgroup,
                        x: 0,
                        ease: 'Linear',
                        duration: 300,
                    });
                    for (i = 1; i <= 10; i++) {
                        game['eye' + i].visible = true
                    }
                }
            } else if (parseInt(sno) == 5 && !game['eshade' + 1].visible) {
                this.scene.tweens.add({
                    targets: level1panelgroup,
                    x: -500,
                    ease: 'Linear',
                    duration: 300,
                    onComplete: levelpanel1move
                });

                function levelpanel1move() {
                    for (i = 1; i <= 7; i++) {
                        game['elash' + i].visible = false
                    }
                    for (i = 1; i <= 10; i++) {
                        game['blush' + i].visible = false
                        game['lip' + i].visible = false
                        game['eye' + i].visible = false
                        game['eshade' + i].visible = false
                        game['ebrow' + i].visible = false
                    }
                    for (i = 1; i <= 5; i++) {
                        game['elsahair' + i].visible = false
                    }
                    this.parent.scene.tweens.add({
                        targets: level1panelgroup,
                        x: 0,
                        ease: 'Linear',
                        duration: 300,
                    });
                    for (i = 1; i <= 10; i++) {
                        game['eshade' + i].visible = true
                    }
                }
            } else if (parseInt(sno) == 6 && !game['ebrow' + 1].visible) {
                this.scene.tweens.add({
                    targets: level1panelgroup,
                    x: -500,
                    ease: 'Linear',
                    duration: 300,
                    onComplete: levelpanel1move
                });

                function levelpanel1move() {
                    for (i = 1; i <= 7; i++) {
                        game['elash' + i].visible = false
                    }
                    for (i = 1; i <= 10; i++) {
                        game['blush' + i].visible = false
                        game['lip' + i].visible = false
                        game['eye' + i].visible = false
                        game['eshade' + i].visible = false
                        game['ebrow' + i].visible = false
                    }
                    for (i = 1; i <= 5; i++) {
                        game['elsahair' + i].visible = false
                    }
                    this.parent.scene.tweens.add({
                        targets: level1panelgroup,
                        x: 0,
                        ease: 'Linear',
                        duration: 300,
                    });
                    for (i = 1; i <= 10; i++) {
                        game['ebrow' + i].visible = true
                    }
                }
            }
        }
    }
}
var startgame4 = false
var makestart1 = false
var acount = 0
var acount1 = 0
var marr2 = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
var Level2 = new Phaser.Class({
    Extends: Phaser.Scene,
    initialize: function Level2() {
        Phaser.Scene.call(this, {
            key: 'Level2'
        });
    },
    preload: function () {
        marr2 = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        acount = 0
        acount1 = 0
        startgame4 = false
        loadFinish = false
        makestart1 = false
    },
    create: function () {
        level2background = this.add.image(400, 300, 'arielbackground1').setOrigin(0.5, 0.5).setScale(1.2)
        level1cupboard = this.add.image(0, 0, 'level1cupboard').setOrigin(0, 0)
        level1cupboard.y=15;
        var elsaelxarr = [, 109.50 , 107.50 , 206.50 , 202.50 , 216.50 , 310.50 , 322.50]
        var elsaelyarr = [,159.00 , 324.00 , 115.00 , 237.00 , 361.00 , 173.00 , 325.00 ]

        for (i = 1; i <= 7; i++) {
            game['elash' + i] = this.add.image(elsaelxarr[i], elsaelyarr[i], 'elash' + i).setOrigin(0.5, 0.5).setInteractive({
                useHandCursor: true
            })
            // game['elash' + i].x += parseFloat(game['elash' + i].width / 2)
            // game['elash' + i].y += parseFloat(game['elash' + i].height / 2)
            game['elash' + i].visible = false
        }
        var elsablxarr =  [, 99.50, 100.50, 100.50, 214.50, 216.50, 217.50, 216.50, 324.50, 326.50, 327.50]
        var elsablyarr = [, 130.50, 241.50, 350.50, 114.50, 195.50, 284.50, 373.50, 128.50, 237.50, 351.50]

        for (i = 1; i <= 10; i++) {
            game['blush' + i] = this.add.image(elsablxarr[i], elsablyarr[i], 'blush' + i).setOrigin(0.5, 0.5).setInteractive({
                pixelPerfect: true,
                useHandCursor: true
            })
            // game['blush' + i].x += parseFloat(game['blush' + i].width / 2)
            // game['blush' + i].y += parseFloat(game['blush' + i].height / 2)
            game['blush' + i].visible = false
        }
        var elsalipxarr = [,99.00, 99.00, 98.00, 215.00, 215.00, 215.00, 213.00, 321.00, 321.00, 322.00]
        var elsalipyarr = [, 140.50, 235.50, 338.50, 115.50, 202.00, 286.00, 370.00, 135.00, 235.00, 333.00]

        for (i = 1; i <= 10; i++) {
            game['lip' + i] = this.add.image(elsalipxarr[i], elsalipyarr[i], 'lip' + i).setOrigin(0.5, 0.5).setInteractive({
                pixelPerfect: true,
                useHandCursor: true
            })
            // game['lip' + i].x += parseFloat(game['lip' + i].width / 2)
            // game['lip' + i].y += parseFloat(game['lip' + i].height / 2)
            game['lip' + i].visible = false
        }
        var elsaeyxarr = [, 99.50, 100.50, 100.50, 214.50, 216.50, 217.50, 216.50, 324.50, 326.50, 327.50]
        var elsaeyyarr = [,130.50, 241.50, 350.50, 114.50, 195.50, 284.50, 373.50, 128.50, 237.50, 351.50]
    
        for (i = 1; i <= 10; i++) {
            game['eye' + i] = this.add.image(elsaeyxarr[i], elsaeyyarr[i], 'eye' + i).setOrigin(0.5, 0.5).setInteractive({
                pixelPerfect: true,
                useHandCursor: true
            })
            // game['eye' + i].x += parseFloat(game['eye' + i].width / 2)
            // game['eye' + i].y += parseFloat(game['eye' + i].height / 2)
            game['eye' + i].visible = false
        }
        var elsaesxarr = [,57,57,57,170,170,170,170,280,280,280]
        var elsaesyarr =[, 92,192,292,72,155,239,330,91,191,291]

        for (i = 1; i <= 10; i++) {
            game['eshade' + i] = this.add.image(elsaesxarr[i], elsaesyarr[i], 'eshade' + i).setOrigin(0, 0).setInteractive({
                pixelPerfect: true,
                useHandCursor: true
            })
            // game['eshade' + i].x += parseFloat(game['eshade' + i].width / 2)
            // game['eshade' + i].y += parseFloat(game['eshade' + i].height / 2)
            game['eshade' + i].visible = false
        }
        var elsaebxarr = [,105.50, 108.50, 99.50, 215.50, 217.50, 211.50, 198.50, 326.50, 321.50, 321.50]
        var elsaebyarr = [,  142.50, 229.50, 321.50, 104.50, 195.50, 284.50, 367.50, 139.50, 227.50, 321.50]

        for (i = 1; i <= 10; i++) {
            game['ebrow' + i] = this.add.image(elsaebxarr[i], elsaebyarr[i], 'ebrow' + i).setOrigin(0.5, 0.5).setInteractive({
                pixelPerfect: true,
                useHandCursor: true
            })
            // game['ebrow' + i].x += parseFloat(game['ebrow' + i].width / 2)
            // game['ebrow' + i].y += parseFloat(game['ebrow' + i].height / 2)
            game['ebrow' + i].visible = false
        }
        var arielhrxarr = [, 31, 28, 117, 218, 220]
        var arielhryarr = [, 68, 236, 172, 242, 60]
        for (i = 1; i <= 5; i++) {
            game['arielhair' + i] = this.add.image(arielhrxarr[i], arielhryarr[i], 'arielhair' + i).setOrigin(0.5, 0.5).setInteractive({
                pixelPerfect: true,
                useHandCursor: true
            })
            game['arielhair' + i].x += parseFloat(game['arielhair' + i].width / 2)
            game['arielhair' + i].y += parseFloat(game['arielhair' + i].height / 2)
            game['arielhair' + i].visible = false
        }
        level1panelgroup = this.add.container(0, 0);
        level1panelgroup.add(level1cupboard)
        for (i = 1; i <= 7; i++) {
            level1panelgroup.add(game['elash' + i])
        }
        for (i = 1; i <= 10; i++) {
            level1panelgroup.add(game['blush' + i])
        }
        for (i = 1; i <= 10; i++) {
            level1panelgroup.add(game['lip' + i])
        }
        for (i = 1; i <= 10; i++) {
            level1panelgroup.add(game['eye' + i])
        }
        for (i = 1; i <= 10; i++) {
            level1panelgroup.add(game['eshade' + i])
        }
        for (i = 1; i <= 10; i++) {
            level1panelgroup.add(game['ebrow' + i])
        }
        for (i = 1; i <= 5; i++) {
            level1panelgroup.add(game['arielhair' + i])
        }
        level1panelgroup.x = -500
        level2bhair = this.add.image(524,413, 'level2bhair').setOrigin(0, 0)
        level2body = this.add.image(485,197, 'level2body').setOrigin(0, 0)
        raqdress = this.add.image(538,498,'raqdress').setOrigin(0,0);
        level2lips = this.add.image(662,422, 'level2lips').setOrigin(0, 0)
        level2lips.setFrame(acount1)
        level2eye1 = this.add.image(650,374, 'level2eye1').setOrigin(0, 0)
        // level2eye2 = this.add.image(499, 217, 'level2eye2').setOrigin(0, 0)
        level2blush = this.add.image(644,401, 'level2blush').setOrigin(0, 0)
        level2toplayer = this.add.image(623,332, 'level2toplayer').setOrigin(0, 0)
        level2eshade = this.add.image(623,332, 'level2eshade').setOrigin(0, 0)
        level2eshade.setFrame(acount)
        level2elash = this.add.image(659,358, 'level2elash').setOrigin(0, 0)
        level2ebrow = this.add.image(638,332, 'level2ebrow').setOrigin(0, 0)
        level2hair = this.add.image(518,185, 'level2hair').setOrigin(0, 0)
        var level2dollgroup = this.add.container(0, 0);
        level2dollgroup.add([level2bhair,level2body, raqdress,level2lips, level2eye1, level2blush, level2toplayer, level2eshade, level2elash, level2ebrow, level2hair]);
        
        level2dollgroup.x=-120;
        level2dollgroup.y =-180;

        clicksound = this.sound.add('clickss');
        var tween1 = this.tweens.add({
            targets: level2eye1,
            x: level2eye1.x + 3,
            ease: 'Linear',
            duration: 300,
            delay: 2400,
            callbackScope: this,
        });
        // var tween2 = this.tweens.add({
        //     targets: level2eye2,
        //     x: level2eye2.x + 3,
        //     ease: 'Linear',
        //     duration: 300,
        //     delay: 2400,
        //     onComplete: arieleyestartani1,
        //     callbackScope: this
        // });

        function arieleyestartani1() {
            this.tweens.add({
                targets: level2eye1,
                x: level2eye1.x - 3,
                ease: 'Linear',
                duration: 300,
                delay: 3000,
                callbackScope: this
            });
            // this.tweens.add({
            //     targets: level2eye2,
            //     x: level2eye2.x - 3,
            //     ease: 'Linear',
            //     duration: 300,
            //     delay: 3000,
            //     onComplete: arieleyestartani2,
            //     callbackScope: this,
            // })
        }

        function arieleyestartani2() {
            this.tweens.add({
                targets: level2eye1,
                x: level2eye1.x + 3,
                ease: 'Linear',
                duration: 300,
                delay: 2400,
                callbackScope: this
            });
            // this.tweens.add({
            //     targets: level2eye2,
            //     x: level2eye2.x + 3,
            //     ease: 'Linear',
            //     duration: 300,
            //     delay: 2400,
            //     onComplete: arieleyestartani1,
            //     callbackScope: this,
            // })
        }
        table = this.add.image(0, 485, 'table').setOrigin(0, 0)
        shadow = this.add.image(62, 491, 'shadow').setOrigin(0, 0)
        shadow1 = this.add.image(535, 528, 'shadow1').setOrigin(0, 0)
        var catxarr = [, 67, 140, 250, 295, 418, 515]
        var catyarr = [, 413, 442, 427, 503, 439, 433]
        for (i = 1; i <= 1; i++) {
            game['category' + i] = this.add.image(catxarr[i], catyarr[i], 'category' + i).setOrigin(0.5, 0.5).setInteractive({
                useHandCursor: true
            })
            game['category' + i].x += parseFloat(game['category' + i].width / 2)
            game['category' + i].y += parseFloat(game['category' + i].height / 2)
        }
        for (i = 2; i <= 6; i++) {
            game['category' + i] = this.add.image(catxarr[i], catyarr[i], 'category' + i).setOrigin(0.5, 0.5).setInteractive({
                pixelPerfect: true,
                useHandCursor: true
            })
            game['category' + i].x += parseFloat(game['category' + i].width / 2)
            game['category' + i].y += parseFloat(game['category' + i].height / 2)
        }
        // arielhairbtn = this.add.image(542, 412, 'arielhairbtn').setOrigin(0.5, 0.5).setInteractive({
        //     pixelPerfect: true,
        //     useHandCursor: true
        // })
        // arielhairbtn.x += parseFloat(arielhairbtn.width / 2)
        // arielhairbtn.y += parseFloat(arielhairbtn.height / 2)
        makeupcategory1 = this.add.container(0, 0);
        makeupcategory1.add([shadow, shadow1, game['category' + 1], game['category' + 2], game['category' + 3], game['category' + 4], game['category' + 5], game['category' + 6]]);
        makeupcategory1.x = 40
        glitter = this.add.sprite(589.1, 270.55, 'glitter').setOrigin(0.5, 0.5)
        anim = this.anims.create({
            key: 'glitter',
            frames: this.anims.generateFrameNumbers('glitter', {
                start: 0,
                end: 41
            }),
            frameRate: 24,
        });
        glitter.anims.load('glitter');
        done = this.add.image(790, 590, 'uiButton').setOrigin(1, 1).setInteractive({
            pixelPerfect: true,
            useHandCursor: true
        })
        done.setFrame("button_next")
        done.visible = false
        done.on('pointerover', doneoverclick1);
        done.on('pointerout', doneoutclick1);
        done.on('pointerdown', donedownclick1);
        done.on('pointerup', doneupclick1);

        function doneoverclick1() {
            done.setScale(1.05, 1.05)
        }

        function doneoutclick1() {
            done.setScale(1, 1)
        }

        function doneupclick1() {
            if (startgame4) {
                done.setScale(1.05, 1.05)
            }
        }

        function donedownclick1() {
            if (!startgame4 && loadFinish) {
                startgame4 = true
                level = 4
                done.setScale(1, 1)
                marr2[1] = marr2[0]
                if (music.isPlaying == false) {
                    clicksound.stop()
                } else {
                    clicksound.play()
                }
                marr2[0] = level2elash.frame.name
                marr2[1] = level2blush.frame.name
                marr2[2] = acount1
                marr2[3] = level2eye1.frame.name
                marr2[4] = acount
                marr2[5] = level2ebrow.frame.name
                marr2[6] = level2hair.frame.name
                transitionbackground1.alpha = 1
                transitionbackground2.alpha = 1
                transitionbackground3.alpha = 1
                transitionpanel.alpha = 1
                transitiondress.alpha = 1
                transitiontitle1.alpha = 1
                transitiontitle2.alpha = 1
                transitiontitle3.alpha = 1
                transitionstar1.alpha = 1
                transitionstar2.alpha = 1
                transitionstar3.alpha = 1
                transitionstar4.alpha = 1
                transitionstar5.alpha = 1
                transitionstar1.setScale(0)
                transitionstar2.setScale(0)
                transitionstar3.setScale(0)
                transitionstar4.setScale(0)
                transitionstar5.setScale(0)
                transitionbackground1.visible = true
                transitionbackground2.visible = true
                transitionbackground3.visible = true
                transitiondress.visible = false
                transitionpanel.visible = false
                transitiontitle1.visible = false
                transitiontitle2.visible = false
                transitiontitle3.visible = false
                transitionstar1.visible = false
                transitionstar2.visible = false
                transitionstar3.visible = false
                transitionstar4.visible = false
                transitionstar5.visible = false
                transitionbackground1.alpha = 0
                transitionbackground2.alpha = 0
                transitionbackground3.alpha = 0
                this.scene.tweens.add({
                    targets: transitionbackground1,
                    alpha: 1,
                    ease: 'Linear',
                    duration: 800,
                    onComplete: transitionstart1,
                    callbackScope: this
                })

                function transitionstart1() {
                    transitiondress.visible = true
                    transitiondress.alpha = 0
                    this.scene.tweens.add({
                        targets: transitiondress,
                        alpha: 1,
                        ease: 'Linear',
                        duration: 400,
                        onComplete: transitionstart2,
                        callbackScope: this
                    })
                    this.scene.tweens.add({
                        targets: transitionbackground2,
                        alpha: 1,
                        ease: 'Linear',
                        duration: 800,
                    })
                }

                function transitionstart2() {
                    transitionpanel.visible = true
                    transitionpanel.setScale(0)
                    this.scene.tweens.add({
                        targets: transitionpanel,
                        scaleX: 1,
                        scaleY: 1,
                        ease: 'Linear',
                        duration: 500,
                        onComplete: transitionstart3,
                        callbackScope: this
                    })
                }

                function transitionstart3() {
                    this.scene.tweens.add({
                        targets: transitionbackground3,
                        alpha: 1,
                        ease: 'Linear',
                        duration: 800,
                        onComplete: transitionstart4,
                        callbackScope: this
                    })
                }

                function transitionstart4() {
                    transitiontitle1.visible = true
                    transitiontitle2.visible = true
                    transitiontitle3.visible = true
                    transitiontitle1.alpha = 0
                    transitiontitle2.alpha = 0
                    transitiontitle3.alpha = 0
                    transitiontitle2.x = transitiontitle2.x + 100
                    transitiontitle3.x = transitiontitle3.x - 100
                    this.scene.tweens.add({
                        targets: transitiontitle1,
                        alpha: 1,
                        ease: 'Linear',
                        duration: 800,
                        onComplete: transitionstart5,
                        callbackScope: this
                    })
                    this.scene.tweens.add({
                        targets: transitiontitle2,
                        alpha: 1,
                        x: transitiontitle2.x - 100,
                        ease: 'Linear',
                        duration: 800,
                    })
                    this.scene.tweens.add({
                        targets: transitiontitle3,
                        alpha: 1,
                        x: transitiontitle3.x + 100,
                        ease: 'Linear',
                        duration: 800,
                    })
                }

                function transitionstart5() {
                    transitionstar1.setScale(0)
                    transitionstar2.setScale(0)
                    transitionstar3.setScale(0)
                    transitionstar4.setScale(0)
                    transitionstar5.setScale(0)
                    transitionstar1.visible = true
                    transitionstar2.visible = true
                    transitionstar3.visible = true
                    transitionstar4.visible = true
                    transitionstar5.visible = true
                    this.scene.tweens.add({
                        targets: transitionstar1,
                        scaleX: 1,
                        scaleY: 1,
                        ease: 'Linear',
                        duration: 300,
                    })
                    this.scene.tweens.add({
                        targets: transitionstar2,
                        scaleX: 0.9,
                        scaleY: 0.9,
                        ease: 'Linear',
                        duration: 300,
                    })
                    this.scene.tweens.add({
                        targets: transitionstar3,
                        scaleX: 0.9,
                        scaleY: 0.9,
                        ease: 'Linear',
                        duration: 300,
                    })
                    this.scene.tweens.add({
                        targets: transitionstar4,
                        scaleX: 0.8,
                        scaleY: 0.8,
                        ease: 'Linear',
                        duration: 300,
                    })
                    this.scene.tweens.add({
                        targets: transitionstar5,
                        scaleX: 0.65,
                        scaleY: 0.65,
                        ease: 'Linear',
                        duration: 300,
                        onComplete: transitionstar6,
                        callbackScope: this
                    })
                }

                function transitionstar6() {
                    this.scene.scene.stop('Level2')
                    game.scene.run('levelSelect');
                }
            }
        }
        level2eshad = this.time.addEvent({
            delay: 1000,
            callback: level2eblinkstart1,
            callbackScope: this
        });

        function level2eblinkstart1() {
            level2eshade.setFrame(acount + 1)
            level2elash.visible = false
            level2eshad = this.time.addEvent({
                delay: 200,
                callback: level2eblinkstart2,
                callbackScope: this
            });
        }

        function level2eblinkstart2() {
            level2eshade.setFrame(acount)
            level2elash.visible = true
            level2eshad = this.time.addEvent({
                delay: 4000,
                callback: level2eblinkstart1,
                callbackScope: this
            });
        }
        level2lip = this.time.addEvent({
            delay: 1000,
            callback: level2lipsstart1,
            callbackScope: this
        });

        function level2lipsstart1() {
            level2lips.setFrame(acount1 + 1)
            level2lip = this.time.addEvent({
                delay: 100,
                callback: level2lipsstart2,
                callbackScope: this
            });
        }

        function level2lipsstart2() {
            level2lips.setFrame(acount1 + 2)
            level2lip = this.time.addEvent({
                delay: 2500,
                callback: level2lipsstart3,
                callbackScope: this
            });
        }

        function level2lipsstart3() {
            level2lips.setFrame(acount1 + 1)
            level2lip = this.time.addEvent({
                delay: 100,
                callback: level2lipsstart4,
                callbackScope: this
            });
        }

        function level2lipsstart4() {
            level2lips.setFrame(acount1)
            level2lip = this.time.addEvent({
                delay: 3000,
                callback: level2lipsstart1,
                callbackScope: this
            });
        }
        mute = this.add.sprite(790, 10, 'uiButton').setFrame("button_music_on").setInteractive({
            pixelPerfect: true,
            useHandCursor: true
        })
        mute.setOrigin(1, 0)
        mute.on('pointerdown', function () {
            if (!isMuted) {
                isMuted = true
                music.pause()
                mute.setFrame("button_music_off")
            } else {
                isMuted = false
                music.resume()
                mute.setFrame("button_music_on")
            }
            var tween2 = this.tweens.add({
                targets: mute,
                scaleX: 0.9,
                scaleY: 0.9,
                ease: 'Linear',
                duration: 80,
                yoyo: true,
                repeat: 1,
            });
            if (music.isPlaying == false) {
                clicksound.stop()
            } else {
                clicksound.play()
            }
        }, this);
        if (music.isPlaying == false) {
            mute.setFrame("button_music_off")
        } else {
            mute.setFrame("button_music_on")
        }
        if (brandingMaterials.inGameLogo.display == true && brandingMaterials.inGameLogo.image != undefined) {
            inGameLogo1 = this.add.image(10, 590, 'inGameLogo1').setOrigin(0, 1).setInteractive({
                pixelPerfect: true,
                useHandCursor: true
            })
            inGameLogo1.on('pointerdown', brandingMaterials.inGameLogo.clickURL);
            inGameLogoani = this.time.addEvent({
                delay: 2000,
                callback: inGameLogoani,
                callbackScope: this
            });

            function inGameLogoani() {
                this.tweens.add({
                    targets: inGameLogo1,
                    scaleX: 1.05,
                    scaleY: 1.05,
                    ease: 'Linear',
                    duration: 100,
                    repeat: 1,
                    yoyo: true,
                    delay: 3000,
                    onComplete: inGameLogoani1,
                    callbackScope: this
                })
            }

            function inGameLogoani1() {
                this.tweens.add({
                    targets: inGameLogo1,
                    scaleX: 1.05,
                    scaleY: 1.05,
                    ease: 'Linear',
                    duration: 100,
                    repeat: 1,
                    yoyo: true,
                    delay: 3000,
                    onComplete: inGameLogoani1,
                    callbackScope: this
                })
            }
        }
        transitionbackground1 = this.add.image(400, 300, 'transitionbackground1').setOrigin(0.5, 0.5).setInteractive({
            pixelPerfect: true,
            useHandCursor: true
        })
        transitionbackground2 = this.add.image(400, 300, 'transitionbackground2').setOrigin(0.5, 0.5)
        transitionbackground3 = this.add.image(400, 300, 'transitionbackground3').setOrigin(0.5, 0.5)
        transitionpanel = this.add.image(393.5, 314, 'transitionpanel').setOrigin(0.5)
        transitiondress = this.add.image(267, 141, 'transitiondress').setOrigin(0, 0)
        transitiontitle1 = this.add.image(400, 329, 'title1');
        transitiontitle1.setOrigin(0.5);
        transitiontitle2 = this.add.image(380, 399, 'title2');
        transitiontitle2.setOrigin(0.5);
        transitiontitle3 = this.add.image(380, 459, 'title3');
        transitiontitle3.setOrigin(0.5);
        transitionstar1 = this.add.image(616.5, 168, 'transitionstar');
        transitionstar1.setOrigin(0.5);
        transitionstar2 = this.add.image(156.5, 168, 'transitionstar');
        transitionstar2.setOrigin(0.5);
        transitionstar2.setScale(0.9)
        transitionstar3 = this.add.image(596.5, 468, 'transitionstar');
        transitionstar3.setOrigin(0.5);
        transitionstar3.setScale(0.9)
        transitionstar4 = this.add.image(116.5, 468, 'transitionstar');
        transitionstar4.setOrigin(0.5);
        transitionstar4.setScale(0.8)
        transitionstar5 = this.add.image(400, 558, 'transitionstar');
        transitionstar5.setOrigin(0.5);
        transitionstar5.setScale(0.65)
        this.tweens.add({
            targets: transitionstar1,
            angle: 4,
            ease: 'Linear',
            duration: 300,
            repeat: -1,
            yoyo: true
        })
        this.tweens.add({
            targets: transitionstar2,
            angle: 4,
            ease: 'Linear',
            duration: 300,
            repeat: -1,
            yoyo: true
        })
        this.tweens.add({
            targets: transitionstar3,
            angle: 4,
            ease: 'Linear',
            duration: 300,
            repeat: -1,
            yoyo: true
        })
        this.tweens.add({
            targets: transitionstar4,
            angle: 4,
            ease: 'Linear',
            duration: 300,
            repeat: -1,
            yoyo: true
        })
        this.tweens.add({
            targets: transitionstar5,
            angle: 4,
            ease: 'Linear',
            duration: 300,
            repeat: -1,
            yoyo: true
        })
        this.time.addEvent({
            delay: 1000,
            callback: transitionstart11,
            callbackScope: this
        });

        function transitionstart11() {
            this.tweens.add({
                targets: transitionbackground1,
                alpha: 0,
                ease: 'Linear',
                duration: 700
            })
            this.tweens.add({
                targets: transitionbackground2,
                alpha: 0,
                ease: 'Linear',
                duration: 700
            })
            this.tweens.add({
                targets: transitionbackground3,
                alpha: 0,
                ease: 'Linear',
                duration: 700
            })
            this.tweens.add({
                targets: transitionpanel,
                alpha: 0,
                ease: 'Linear',
                duration: 700
            })
            this.tweens.add({
                targets: transitiondress,
                alpha: 0,
                ease: 'Linear',
                duration: 700
            })
            this.tweens.add({
                targets: transitiontitle1,
                alpha: 0,
                ease: 'Linear',
                duration: 700
            })
            this.tweens.add({
                targets: transitiontitle2,
                alpha: 0,
                ease: 'Linear',
                duration: 700
            })
            this.tweens.add({
                targets: transitiontitle3,
                alpha: 0,
                ease: 'Linear',
                duration: 700
            })
            this.tweens.add({
                targets: transitionstar1,
                alpha: 0,
                ease: 'Linear',
                duration: 700
            })
            this.tweens.add({
                targets: transitionstar2,
                alpha: 0,
                ease: 'Linear',
                duration: 700
            })
            this.tweens.add({
                targets: transitionstar3,
                alpha: 0,
                ease: 'Linear',
                duration: 700
            })
            this.tweens.add({
                targets: transitionstar4,
                alpha: 0,
                ease: 'Linear',
                duration: 700
            })
            this.tweens.add({
                targets: transitionstar5,
                alpha: 0,
                ease: 'Linear',
                duration: 700
            })
        }
        level2clickstart()
        hitobject = this.add.image(400, 300, 'hitobject').setOrigin(0.5, 0.5).setInteractive({
            useHandCursor: true
        })
        hitobject.visible = false
        hitobject.on('pointerdown', hitobjectdown)

        function hitobjectdown() {}
        this.load.on('complete', function () {
            loadFinish = true
        });
        this.load.start();
    }
});

function level2clickstart() {
    for (i = 1; i <= 7; i++) {
        game['elash' + i].on('pointerover', elashoverclick1);
        game['elash' + i].on('pointerout', elashoutclick1);
        game['elash' + i].on('pointerdown', elashdownclick1);
        game['elash' + i].on('pointerup', elashupclick1);
    }

    function elashoverclick1() {
        game['elash' + this.texture.key.substr(5)].setScale(1.05, 1.05)
    }

    function elashoutclick1() {
        game['elash' + this.texture.key.substr(5)].setScale(1, 1)
    }

    function elashdownclick1() {
        if (music.isPlaying == false) {
            clicksound.stop()
        } else {
            clicksound.play()
        }
        game['elash' + this.texture.key.substr(5)].setScale(1, 1)
        done.visible = true
        sno = this.texture.key.substr(5)
        glitter.anims.play('glitter')
        if (marr2[0] == parseInt(sno)) {
            level2elash.setFrame(0)
            marr2[0] = 0
        } else {
            marr2[0] = parseInt(sno)
            level2elash.setFrame(parseInt(sno))
        }
    }

    function elashupclick1() {
        game['elash' + this.texture.key.substr(5)].setScale(1.05, 1.05)
    }
    for (i = 1; i <= 10; i++) {
        game['blush' + i].on('pointerover', blushoverclick1);
        game['blush' + i].on('pointerout', blushoutclick1);
        game['blush' + i].on('pointerdown', blushdownclick1);
        game['blush' + i].on('pointerup', blushupclick1);
    }

    function blushoverclick1() {
        game['blush' + this.texture.key.substr(5)].setScale(1.05, 1.05)
    }

    function blushoutclick1() {
        game['blush' + this.texture.key.substr(5)].setScale(1, 1)
    }

    function blushdownclick1() {
        if (music.isPlaying == false) {
            clicksound.stop()
        } else {
            clicksound.play()
        }
        game['blush' + this.texture.key.substr(5)].setScale(1, 1)
        done.visible = true
        sno = this.texture.key.substr(5)
        glitter.anims.play('glitter')
        if (marr2[1] == parseInt(sno)) {
            level2blush.setFrame(0)
            marr2[1] = 0
        } else {
            marr2[1] = parseInt(sno)
            level2blush.setFrame(parseInt(sno))
        }
    }

    function blushupclick1() {
        game['blush' + this.texture.key.substr(5)].setScale(1.05, 1.05)
    }
    for (i = 1; i <= 10; i++) {
        game['lip' + i].on('pointerover', lipoverclick1);
        game['lip' + i].on('pointerout', lipoutclick1);
        game['lip' + i].on('pointerdown', lipdownclick1);
        game['lip' + i].on('pointerup', lipupclick1);
    }

    function lipoverclick1() {
        game['lip' + this.texture.key.substr(3)].setScale(1.05, 1.05)
    }

    function lipoutclick1() {
        game['lip' + this.texture.key.substr(3)].setScale(1, 1)
    }

    function lipdownclick1() {
        if (music.isPlaying == false) {
            clicksound.stop()
        } else {
            clicksound.play()
        }
        game['lip' + this.texture.key.substr(3)].setScale(1, 1)
        done.visible = true
        sno = this.texture.key.substr(3)
        glitter.anims.play('glitter')
        if (marr2[2] == parseInt(sno)) {
            level2lips.setFrame(0)
            marr2[2] = 0
            acount1 = 0
        } else {
            marr2[2] = parseInt(sno)
            level2lips.setFrame(parseInt(sno) * 3)
            acount1 = marr2[2] * 3
        }
        level2lip.remove()
        level2lip = this.scene.time.addEvent({
            delay: 1000,
            callback: level2lipsstart1,
            callbackScope: this
        });

        function level2lipsstart1() {
            level2lips.setFrame(acount1 + 1)
            level2lip = this.scene.time.addEvent({
                delay: 100,
                callback: level2lipsstart2,
                callbackScope: this
            });
        }

        function level2lipsstart2() {
            level2lips.setFrame(acount1 + 2)
            level2lip = this.scene.time.addEvent({
                delay: 2500,
                callback: level2lipsstart3,
                callbackScope: this
            });
        }

        function level2lipsstart3() {
            level2lips.setFrame(acount1 + 1)
            level2lip = this.scene.time.addEvent({
                delay: 100,
                callback: level2lipsstart4,
                callbackScope: this
            });
        }

        function level2lipsstart4() {
            level2lips.setFrame(acount1)
            level2lip = this.scene.time.addEvent({
                delay: 3000,
                callback: level2lipsstart1,
                callbackScope: this
            });
        }
    }

    function lipupclick1() {
        game['lip' + this.texture.key.substr(3)].setScale(1.05, 1.05)
    }
    for (i = 1; i <= 10; i++) {
        game['eye' + i].on('pointerover', eyeoverclick1);
        game['eye' + i].on('pointerout', eyeoutclick1);
        game['eye' + i].on('pointerdown', eyedownclick1);
        game['eye' + i].on('pointerup', eyeupclick1);
    }

    function eyeoverclick1() {
        game['eye' + this.texture.key.substr(3)].setScale(1.05, 1.05)
    }

    function eyeoutclick1() {
        game['eye' + this.texture.key.substr(3)].setScale(1, 1)
    }

    function eyedownclick1() {
        if (music.isPlaying == false) {
            clicksound.stop()
        } else {
            clicksound.play()
        }
        game['eye' + this.texture.key.substr(3)].setScale(1, 1)
        done.visible = true
        sno = this.texture.key.substr(3)
        glitter.anims.play('glitter')
        if (marr2[3] == parseInt(sno)) {
            level2eye1.setFrame(0)
            // level2eye2.setFrame(0)
            marr2[3] = 0
        } else {
            marr2[3] = parseInt(sno)
            level2eye1.setFrame(parseInt(sno))
            // level2eye2.setFrame(parseInt(sno))
        }
    }

    function eyeupclick1() {
        game['eye' + this.texture.key.substr(3)].setScale(1.05, 1.05)
    }
    for (i = 1; i <= 10; i++) {
        game['eshade' + i].on('pointerover', eshadeoverclick1);
        game['eshade' + i].on('pointerout', eshadeoutclick1);
        game['eshade' + i].on('pointerdown', eshadedownclick1);
        game['eshade' + i].on('pointerup', eshadeupclick1);
    }

    function eshadeoverclick1() {
        game['eshade' + this.texture.key.substr(6)].setScale(1.05, 1.05)
    }

    function eshadeoutclick1() {
        game['eshade' + this.texture.key.substr(6)].setScale(1, 1)
    }

    function eshadedownclick1() {
        if (music.isPlaying == false) {
            clicksound.stop()
        } else {
            clicksound.play()
        }
        game['eshade' + this.texture.key.substr(6)].setScale(1, 1)
        done.visible = true
        sno = this.texture.key.substr(6)
        glitter.anims.play('glitter')
        if (marr2[4] == parseInt(sno)) {
            level2eshade.setFrame(0)
            marr2[4] = 0
            acount = 0
        } else {
            marr2[4] = parseInt(sno)
            level2eshade.setFrame(parseInt(sno) * 2)
            acount = marr2[4] * 2
        }
    }

    function eshadeupclick1() {
        game['eshade' + this.texture.key.substr(6)].setScale(1.05, 1.05)
    }
    for (i = 1; i <= 10; i++) {
        game['ebrow' + i].on('pointerover', ebrowoverclick1);
        game['ebrow' + i].on('pointerout', ebrowoutclick1);
        game['ebrow' + i].on('pointerdown', ebrowdownclick1);
        game['ebrow' + i].on('pointerup', ebrowupclick1);
    }

    function ebrowoverclick1() {
        game['ebrow' + this.texture.key.substr(5)].setScale(1.05, 1.05)
    }

    function ebrowoutclick1() {
        game['ebrow' + this.texture.key.substr(5)].setScale(1, 1)
    }

    function ebrowdownclick1() {
        if (music.isPlaying == false) {
            clicksound.stop()
        } else {
            clicksound.play()
        }
        game['ebrow' + this.texture.key.substr(5)].setScale(1, 1)
        done.visible = true
        sno = this.texture.key.substr(5)
        glitter.anims.play('glitter')
        if (marr2[5] == parseInt(sno)) {
            level2ebrow.setFrame(0)
            marr2[5] = 0
        } else {
            marr2[5] = parseInt(sno)
            level2ebrow.setFrame(parseInt(sno))
        }
    }

    function ebrowupclick1() {
        game['ebrow' + this.texture.key.substr(5)].setScale(1.05, 1.05)
    }
    // for (i = 1; i <= 5; i++) {
    //     game['arielhair' + i].on('pointerover', hairoverclick1);
    //     game['arielhair' + i].on('pointerout', hairoutclick1);
    //     game['arielhair' + i].on('pointerdown', hairdownclick1);
    //     game['arielhair' + i].on('pointerup', hairupclick1);
    // }

    // function hairoverclick1() {
    //     game['arielhair' + this.texture.key.substr(9)].setScale(1.05, 1.05)
    // }

    // function hairoutclick1() {
    //     game['arielhair' + this.texture.key.substr(9)].setScale(1, 1)
    // }

    // function hairdownclick1() {
    //     if (music.isPlaying == false) {
    //         clicksound.stop()
    //     } else {
    //         clicksound.play()
    //     }
    //     game['arielhair' + this.texture.key.substr(9)].setScale(1, 1)
    //     done.visible = true
    //     sno = this.texture.key.substr(9)
    //     glitter.anims.play('glitter')
    //     if (marr2[6] == parseInt(sno)) {
    //         level2hair.setFrame(0)
    //         marr2[6] = 0
    //         level2bhair.setFrame(0)
    //     } else {
    //         marr2[6] = parseInt(sno)
    //         level2hair.setFrame(parseInt(sno))
    //         level2bhair.setFrame(1)
    //     }
    // }

    // function hairupclick1() {
    //     game['arielhair' + this.texture.key.substr(9)].setScale(1.05, 1.05)
    // }
    // arielhairbtn.on('pointerover', arielhairbtnoverclick1);
    // arielhairbtn.on('pointerout', arielhairbtnoutclick1);
    // arielhairbtn.on('pointerdown', arielhairbtndownclick1);
    // arielhairbtn.on('pointerup', arielhairbtnupclick1);

    // function arielhairbtnoverclick1() {
    //     arielhairbtn.setScale(1.05, 1.05)
    // }

    // function arielhairbtnoutclick1() {
    //     arielhairbtn.setScale(1, 1)
    // }

    // function arielhairbtndownclick1() {
    //     if (music.isPlaying == false) {
    //         clicksound.stop()
    //     } else {
    //         clicksound.play()
    //     }
    //     arielhairbtn.setScale(1, 1)
    //     if (!makestart1) {
    //         makestart1 = true
    //         this.scene.tweens.add({
    //             targets: level1panelgroup,
    //             x: 0,
    //             ease: 'Linear',
    //             duration: 300,
    //         });
    //         for (i = 1; i <= 7; i++) {
    //             game['elash' + i].visible = false
    //         }
    //         for (i = 1; i <= 10; i++) {
    //             game['blush' + i].visible = false
    //             game['lip' + i].visible = false
    //             game['eye' + i].visible = false
    //             game['eshade' + i].visible = false
    //             game['ebrow' + i].visible = false
    //         }
    //         for (i = 1; i <= 5; i++) {
    //             game['arielhair' + i].visible = true
    //         }
    //     } else {
    //         if (!game['arielhair' + 1].visible) {
    //             this.scene.tweens.add({
    //                 targets: level1panelgroup,
    //                 x: -500,
    //                 ease: 'Sine.easeIn',
    //                 duration: 300,
    //                 onComplete: levelpanel1move
    //             });

    //             function levelpanel1move() {
    //                 for (i = 1; i <= 7; i++) {
    //                     game['elash' + i].visible = false
    //                 }
    //                 for (i = 1; i <= 10; i++) {
    //                     game['blush' + i].visible = false
    //                     game['lip' + i].visible = false
    //                     game['eye' + i].visible = false
    //                     game['eshade' + i].visible = false
    //                     game['ebrow' + i].visible = false
    //                 }
    //                 this.parent.scene.tweens.add({
    //                     targets: level1panelgroup,
    //                     x: 0,
    //                     ease: 'Sine.easeOut',
    //                     duration: 300,
    //                 });
    //                 for (i = 1; i <= 5; i++) {
    //                     game['arielhair' + i].visible = true
    //                 }
    //             }
    //         }
    //     }
    // }

    // function arielhairbtnupclick1() {
    //     arielhairbtn.setScale(1.05, 1.05)
    // }
    for (i = 1; i <= 6; i++) {
        game['category' + i].on('pointerover', categoryoverclick1);
        game['category' + i].on('pointerout', categoryoutclick1);
        game['category' + i].on('pointerdown', categorydownclick1);
        game['category' + i].on('pointerup', categoryupclick1);
    }

    function categoryoverclick1() {
        game['category' + this.texture.key.substr(8)].setScale(1.05, 1.05)
    }

    function categoryoutclick1() {
        game['category' + this.texture.key.substr(8)].setScale(1, 1)
    }

    function categoryupclick1() {
        game['category' + this.texture.key.substr(8)].setScale(1.05, 1.05)
    }

    function categorydownclick1() {
        if (music.isPlaying == false) {
            clicksound.stop()
        } else {
            clicksound.play()
        }
        sno = this.texture.key.substr(8)
        game['category' + parseInt(sno)].setScale(1, 1)
        if (!makestart1) {
            makestart1 = true
            this.scene.tweens.add({
                targets: level1panelgroup,
                x: 0,
                ease: 'Linear',
                duration: 300,
            });
            for (i = 1; i <= 7; i++) {
                game['elash' + i].visible = false
            }
            for (i = 1; i <= 10; i++) {
                game['blush' + i].visible = false
                game['lip' + i].visible = false
                game['eye' + i].visible = false
                game['eshade' + i].visible = false
                game['ebrow' + i].visible = false
            }
            for (i = 1; i <= 5; i++) {
                game['arielhair' + i].visible = false
            }
            if (parseInt(sno) == 1) {
                for (i = 1; i <= 7; i++) {
                    game['elash' + i].visible = true
                }
            } else if (parseInt(sno) == 2) {
                for (i = 1; i <= 10; i++) {
                    game['blush' + i].visible = true
                }
            } else if (parseInt(sno) == 3) {
                for (i = 1; i <= 10; i++) {
                    game['lip' + i].visible = true
                }
            } else if (parseInt(sno) == 4) {
                for (i = 1; i <= 10; i++) {
                    game['eye' + i].visible = true
                }
            } else if (parseInt(sno) == 5) {
                for (i = 1; i <= 10; i++) {
                    game['eshade' + i].visible = true
                }
            } else if (parseInt(sno) == 6) {
                for (i = 1; i <= 10; i++) {
                    game['ebrow' + i].visible = true
                }
            }
        } else {
            if (parseInt(sno) == 1 && !game['elash' + 1].visible) {
                this.scene.tweens.add({
                    targets: level1panelgroup,
                    x: -500,
                    ease: 'Linear',
                    duration: 300,
                    onComplete: levelpanel1move
                });

                function levelpanel1move() {
                    for (i = 1; i <= 7; i++) {
                        game['elash' + i].visible = false
                    }
                    for (i = 1; i <= 10; i++) {
                        game['blush' + i].visible = false
                        game['lip' + i].visible = false
                        game['eye' + i].visible = false
                        game['eshade' + i].visible = false
                        game['ebrow' + i].visible = false
                    }
                    for (i = 1; i <= 5; i++) {
                        game['arielhair' + i].visible = false
                    }
                    this.parent.scene.tweens.add({
                        targets: level1panelgroup,
                        x: 0,
                        ease: 'Linear',
                        duration: 300,
                    });
                    for (i = 1; i <= 7; i++) {
                        game['elash' + i].visible = true
                    }
                }
            } else if (parseInt(sno) == 2 && !game['blush' + 1].visible) {
                this.scene.tweens.add({
                    targets: level1panelgroup,
                    x: -500,
                    ease: 'Linear',
                    duration: 300,
                    onComplete: levelpanel1move
                });

                function levelpanel1move() {
                    for (i = 1; i <= 7; i++) {
                        game['elash' + i].visible = false
                    }
                    for (i = 1; i <= 10; i++) {
                        game['blush' + i].visible = false
                        game['lip' + i].visible = false
                        game['eye' + i].visible = false
                        game['eshade' + i].visible = false
                        game['ebrow' + i].visible = false
                    }
                    for (i = 1; i <= 5; i++) {
                        game['arielhair' + i].visible = false
                    }
                    this.parent.scene.tweens.add({
                        targets: level1panelgroup,
                        x: 0,
                        ease: 'Linear',
                        duration: 300,
                    });
                    for (i = 1; i <= 10; i++) {
                        game['blush' + i].visible = true
                    }
                }
            } else if (parseInt(sno) == 3 && !game['lip' + 1].visible) {
                this.scene.tweens.add({
                    targets: level1panelgroup,
                    x: -500,
                    ease: 'Linear',
                    duration: 300,
                    onComplete: levelpanel1move
                });

                function levelpanel1move() {
                    for (i = 1; i <= 7; i++) {
                        game['elash' + i].visible = false
                    }
                    for (i = 1; i <= 10; i++) {
                        game['blush' + i].visible = false
                        game['lip' + i].visible = false
                        game['eye' + i].visible = false
                        game['eshade' + i].visible = false
                        game['ebrow' + i].visible = false
                    }
                    for (i = 1; i <= 5; i++) {
                        game['arielhair' + i].visible = false
                    }
                    this.parent.scene.tweens.add({
                        targets: level1panelgroup,
                        x: 0,
                        ease: 'Linear',
                        duration: 300,
                    });
                    for (i = 1; i <= 10; i++) {
                        game['lip' + i].visible = true
                    }
                }
            } else if (parseInt(sno) == 4 && !game['eye' + 1].visible) {
                this.scene.tweens.add({
                    targets: level1panelgroup,
                    x: -500,
                    ease: 'Linear',
                    duration: 300,
                    onComplete: levelpanel1move
                });

                function levelpanel1move() {
                    for (i = 1; i <= 7; i++) {
                        game['elash' + i].visible = false
                    }
                    for (i = 1; i <= 10; i++) {
                        game['blush' + i].visible = false
                        game['lip' + i].visible = false
                        game['eye' + i].visible = false
                        game['eshade' + i].visible = false
                        game['ebrow' + i].visible = false
                    }
                    for (i = 1; i <= 5; i++) {
                        game['arielhair' + i].visible = false
                    }
                    this.parent.scene.tweens.add({
                        targets: level1panelgroup,
                        x: 0,
                        ease: 'Linear',
                        duration: 300,
                    });
                    for (i = 1; i <= 10; i++) {
                        game['eye' + i].visible = true
                    }
                }
            } else if (parseInt(sno) == 5 && !game['eshade' + 1].visible) {
                this.scene.tweens.add({
                    targets: level1panelgroup,
                    x: -500,
                    ease: 'Linear',
                    duration: 300,
                    onComplete: levelpanel1move
                });

                function levelpanel1move() {
                    for (i = 1; i <= 7; i++) {
                        game['elash' + i].visible = false
                    }
                    for (i = 1; i <= 10; i++) {
                        game['blush' + i].visible = false
                        game['lip' + i].visible = false
                        game['eye' + i].visible = false
                        game['eshade' + i].visible = false
                        game['ebrow' + i].visible = false
                    }
                    for (i = 1; i <= 5; i++) {
                        game['arielhair' + i].visible = false
                    }
                    this.parent.scene.tweens.add({
                        targets: level1panelgroup,
                        x: 0,
                        ease: 'Linear',
                        duration: 300,
                    });
                    for (i = 1; i <= 10; i++) {
                        game['eshade' + i].visible = true
                    }
                }
            } else if (parseInt(sno) == 6 && !game['ebrow' + 1].visible) {
                this.scene.tweens.add({
                    targets: level1panelgroup,
                    x: -500,
                    ease: 'Linear',
                    duration: 300,
                    onComplete: levelpanel1move
                });

                function levelpanel1move() {
                    for (i = 1; i <= 7; i++) {
                        game['elash' + i].visible = false
                    }
                    for (i = 1; i <= 10; i++) {
                        game['blush' + i].visible = false
                        game['lip' + i].visible = false
                        game['eye' + i].visible = false
                        game['eshade' + i].visible = false
                        game['ebrow' + i].visible = false
                    }
                    for (i = 1; i <= 5; i++) {
                        game['arielhair' + i].visible = false
                    }
                    this.parent.scene.tweens.add({
                        targets: level1panelgroup,
                        x: 0,
                        ease: 'Linear',
                        duration: 300,
                    });
                    for (i = 1; i <= 10; i++) {
                        game['ebrow' + i].visible = true
                    }
                }
            }
        }
    }
}
var startgame5 = false
var bcount = 0
var bcount1 = 0
var marr3 = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
var Level3 = new Phaser.Class({
    Extends: Phaser.Scene,
    initialize: function Level3() {
        Phaser.Scene.call(this, {
            key: 'Level3'
        });
    },
    preload: function () {
        marr3 = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        bcount = 0
        bcount1 = 0
        startgame5 = false
        loadFinish = false
        makestart1 = false
    },
    create: function () {
        level3background = this.add.image(400, 300, 'annabackground1').setOrigin(0.5, 0.5).setScale(1.2)
        level1cupboard = this.add.image(0, 0, 'level1cupboard').setOrigin(0, 0)
        var elsaelxarr = [, 27, 27, 139, 139, 145, 245, 250]
        var elsaelyarr = [, 90, 230, 47, 177, 311, 86, 232]
        for (i = 1; i <= 7; i++) {
            game['elash' + i] = this.add.image(elsaelxarr[i], elsaelyarr[i], 'elash' + i).setOrigin(0.5, 0.5).setInteractive({
                useHandCursor: true
            })
            game['elash' + i].x += parseFloat(game['elash' + i].width / 2)
            game['elash' + i].y += parseFloat(game['elash' + i].height / 2)
            game['elash' + i].visible = false
        }
        var elsablxarr = [, 42, 0, 40, 143, 143, 143, 143, 258, 298, 257]
        var elsablyarr = [, 88, 173, 263, 42, 132, 232, 322, 81, 171, 260]
        for (i = 1; i <= 10; i++) {
            game['blush' + i] = this.add.image(elsablxarr[i], elsablyarr[i], 'blush' + i).setOrigin(0.5, 0.5).setInteractive({
                pixelPerfect: true,
                useHandCursor: true
            })
            game['blush' + i].x += parseFloat(game['blush' + i].width / 2)
            game['blush' + i].y += parseFloat(game['blush' + i].height / 2)
            game['blush' + i].visible = false
        }
        var elsalipxarr = [, 27, 0, 28, 136, 136, 136, 136, 247, 277, 250]
        var elsalipyarr = [, 120, 207, 297, 55, 153, 248, 348, 116, 207, 301]
        for (i = 1; i <= 10; i++) {
            game['lip' + i] = this.add.image(elsalipxarr[i], elsalipyarr[i], 'lip' + i).setOrigin(0.5, 0.5).setInteractive({
                pixelPerfect: true,
                useHandCursor: true
            })
            game['lip' + i].x += parseFloat(game['lip' + i].width / 2)
            game['lip' + i].y += parseFloat(game['lip' + i].height / 2)
            game['lip' + i].visible = false
        }
        var elsaeyxarr = [, 39, 9, 42, 149, 149, 149, 149, 258, 291, 258]
        var elsaeyyarr = [, 91, 193, 295, 43, 145, 246, 346, 93, 193, 293]
        for (i = 1; i <= 10; i++) {
            game['eye' + i] = this.add.image(elsaeyxarr[i], elsaeyyarr[i], 'eye' + i).setOrigin(0.5, 0.5).setInteractive({
                pixelPerfect: true,
                useHandCursor: true
            })
            game['eye' + i].x += parseFloat(game['eye' + i].width / 2)
            game['eye' + i].y += parseFloat(game['eye' + i].height / 2)
            game['eye' + i].visible = false
        }
        var elsaesxarr = [, 43, 0, 39, 138, 140, 140, 140, 244, 285, 245]
        var elsaesyarr = [, 99, 175, 270, 49, 145, 234, 325, 93, 173, 268]
        for (i = 1; i <= 10; i++) {
            game['eshade' + i] = this.add.image(elsaesxarr[i], elsaesyarr[i], 'eshade' + i).setOrigin(0.5, 0.5).setInteractive({
                pixelPerfect: true,
                useHandCursor: true
            })
            game['eshade' + i].x += parseFloat(game['eshade' + i].width / 2)
            game['eshade' + i].y += parseFloat(game['eshade' + i].height / 2)
            game['eshade' + i].visible = false
        }
        var elsaebxarr = [, 9, 9, 91, 82, 83, 181, 181, 181, 261, 261]
        var elsaebyarr = [, 130, 240, 63, 189, 297, 63, 177, 297, 122, 222]
        for (i = 1; i <= 10; i++) {
            game['ebrow' + i] = this.add.image(elsaebxarr[i], elsaebyarr[i], 'ebrow' + i).setOrigin(0.5, 0.5).setInteractive({
                pixelPerfect: true,
                useHandCursor: true
            })
            game['ebrow' + i].x += parseFloat(game['ebrow' + i].width / 2)
            game['ebrow' + i].y += parseFloat(game['ebrow' + i].height / 2)
            game['ebrow' + i].visible = false
        }
        var annahrxarr = [, 31, 28, 117, 218, 220]
        var annahryarr = [, 68, 236, 172, 242, 60]
        for (i = 1; i <= 5; i++) {
            game['annahair' + i] = this.add.image(annahrxarr[i], annahryarr[i], 'annahair' + i).setOrigin(0.5, 0.5).setInteractive({
                pixelPerfect: true,
                useHandCursor: true
            })
            game['annahair' + i].x += parseFloat(game['annahair' + i].width / 2)
            game['annahair' + i].y += parseFloat(game['annahair' + i].height / 2)
            game['annahair' + i].visible = false
        }
        level1panelgroup = this.add.container(0, 0);
        level1panelgroup.add(level1cupboard)
        for (i = 1; i <= 7; i++) {
            level1panelgroup.add(game['elash' + i])
        }
        for (i = 1; i <= 10; i++) {
            level1panelgroup.add(game['blush' + i])
        }
        for (i = 1; i <= 10; i++) {
            level1panelgroup.add(game['lip' + i])
        }
        for (i = 1; i <= 10; i++) {
            level1panelgroup.add(game['eye' + i])
        }
        for (i = 1; i <= 10; i++) {
            level1panelgroup.add(game['eshade' + i])
        }
        for (i = 1; i <= 10; i++) {
            level1panelgroup.add(game['ebrow' + i])
        }
        for (i = 1; i <= 5; i++) {
            level1panelgroup.add(game['annahair' + i])
        }
        level1panelgroup.x = -500
        level3body = this.add.image(485,197, 'level3body').setOrigin(0, 0)
        // raqdress = this.add.image(538,498,'raqdress').setOrigin(0,0);
        level3hand = this.add.image(704, 316, 'level3hand').setOrigin(0, 0)
        level3eye1 = this.add.image(537, 152, 'level3eye1').setOrigin(0, 0)
        level3eye2 = this.add.image(537, 152, 'level3eye2').setOrigin(0, 0)
        level3eshade = this.add.image(517, 139, 'level3eshade').setOrigin(0, 0)
        level3toplayer = this.add.image(527, 141, 'level3toplayer').setOrigin(0, 0)
        level3lips = this.add.image(533, 206, 'level3lips').setOrigin(0, 0)
        level3ebrow = this.add.image(527, 124, 'level3ebrow').setOrigin(0, 0)
        level3elash = this.add.image(520, 140, 'level3elash').setOrigin(0, 0)
        level3blush = this.add.image(511, 162, 'level3blush').setOrigin(0, 0)
        level3hair = this.add.image(431, 0, 'level3hair').setOrigin(0, 0)
        var level3dollgroup = this.add.container(0, 0);
        level3dollgroup.add([level3body, level3eye1, level3eye2, level3toplayer, level3eshade, level3lips, level3ebrow, level3elash, level3blush, level3hair, level3hand]);
        clicksound = this.sound.add('clickss');
        var tween1 = this.tweens.add({
            targets: level3eye1,
            x: level3eye1.x + 3,
            ease: 'Linear',
            duration: 300,
            delay: 2400,
            callbackScope: this,
        });
        var tween2 = this.tweens.add({
            targets: level3eye2,
            x: level3eye2.x + 3,
            ease: 'Linear',
            duration: 300,
            delay: 2400,
            onComplete: elsaeyestartani1,
            callbackScope: this
        });

        function elsaeyestartani1() {
            this.tweens.add({
                targets: level3eye1,
                x: level3eye1.x - 3,
                ease: 'Linear',
                duration: 300,
                delay: 3000,
                callbackScope: this
            });
            this.tweens.add({
                targets: level3eye2,
                x: level3eye2.x - 3,
                ease: 'Linear',
                duration: 300,
                delay: 3000,
                onComplete: elsaeyestartani2,
                callbackScope: this,
            })
        }

        function elsaeyestartani2() {
            this.tweens.add({
                targets: level3eye1,
                x: level3eye1.x + 3,
                ease: 'Linear',
                duration: 300,
                delay: 2400,
                callbackScope: this
            });
            this.tweens.add({
                targets: level3eye2,
                x: level3eye2.x + 3,
                ease: 'Linear',
                duration: 300,
                delay: 2400,
                onComplete: elsaeyestartani1,
                callbackScope: this,
            })
        }
        level3eshad = this.time.addEvent({
            delay: 1000,
            callback: level3eblinkstart1,
            callbackScope: this
        });

        function level3eblinkstart1() {
            level3eshade.setFrame(bcount + 1)
            level3elash.visible = false
            level3eshad = this.time.addEvent({
                delay: 200,
                callback: level3eblinkstart2,
                callbackScope: this
            });
        }

        function level3eblinkstart2() {
            level3eshade.setFrame(bcount)
            level3elash.visible = true
            level3eshad = this.time.addEvent({
                delay: 4000,
                callback: level3eblinkstart1,
                callbackScope: this
            });
        }
        level3lip = this.time.addEvent({
            delay: 1000,
            callback: level3lipsstart1,
            callbackScope: this
        });

        function level3lipsstart1() {
            level3lips.setFrame(bcount1 + 1)
            level3lip = this.time.addEvent({
                delay: 100,
                callback: level3lipsstart2,
                callbackScope: this
            });
        }

        function level3lipsstart2() {
            level3lips.setFrame(bcount1 + 2)
            level3lip = this.time.addEvent({
                delay: 2500,
                callback: level3lipsstart3,
                callbackScope: this
            });
        }

        function level3lipsstart3() {
            level3lips.setFrame(bcount1 + 1)
            level3lip = this.time.addEvent({
                delay: 100,
                callback: level3lipsstart4,
                callbackScope: this
            });
        }

        function level3lipsstart4() {
            level3lips.setFrame(bcount1)
            level3lip = this.time.addEvent({
                delay: 3000,
                callback: level3lipsstart1,
                callbackScope: this
            });
        }
        table = this.add.image(0, 485, 'table').setOrigin(0, 0)
        shadow = this.add.image(62, 491, 'shadow').setOrigin(0, 0)
        shadow3 = this.add.image(555, 553, 'shadow3').setOrigin(0, 0)
        var catxarr = [, 67, 140, 250, 295, 418, 515]
        var catyarr = [, 413, 442, 427, 503, 439, 433]
        for (i = 1; i <= 1; i++) {
            game['category' + i] = this.add.image(catxarr[i], catyarr[i], 'category' + i).setOrigin(0.5, 0.5).setInteractive({
                useHandCursor: true
            })
            game['category' + i].x += parseFloat(game['category' + i].width / 2)
            game['category' + i].y += parseFloat(game['category' + i].height / 2)
        }
        for (i = 2; i <= 6; i++) {
            game['category' + i] = this.add.image(catxarr[i], catyarr[i], 'category' + i).setOrigin(0.5, 0.5).setInteractive({
                pixelPerfect: true,
                useHandCursor: true
            })
            game['category' + i].x += parseFloat(game['category' + i].width / 2)
            game['category' + i].y += parseFloat(game['category' + i].height / 2)
        }
        // annahairbtn = this.add.image(560, 413, 'annahairbtn').setOrigin(0.5, 0.5).setInteractive({
        //     pixelPerfect: true,
        //     useHandCursor: true
        // })
        // annahairbtn.x += parseFloat(annahairbtn.width / 2)
        // annahairbtn.y += parseFloat(annahairbtn.height / 2)
        makeupcategory1 = this.add.container(0, 0);
        makeupcategory1.add([shadow, shadow3, game['category' + 1], game['category' + 2], game['category' + 3], game['category' + 4], game['category' + 5], game['category' + 6]]);
        makeupcategory1.x = 40
        glitter = this.add.sprite(589.1, 190.55, 'glitter').setOrigin(0.5, 0.5)
        anim = this.anims.create({
            key: 'glitter',
            frames: this.anims.generateFrameNumbers('glitter', {
                start: 0,
                end: 41
            }),
            frameRate: 24,
        });
        glitter.anims.load('glitter');
        mute = this.add.sprite(790, 10, 'uiButton').setFrame("button_music_on").setInteractive({
            pixelPerfect: true,
            useHandCursor: true
        })
        mute.setOrigin(1, 0)
        mute.on('pointerdown', function () {
            if (!isMuted) {
                isMuted = true
                music.pause()
                mute.setFrame("button_music_off")
            } else {
                isMuted = false
                music.resume()
                mute.setFrame("button_music_on")
            }
            var tween2 = this.tweens.add({
                targets: mute,
                scaleX: 0.9,
                scaleY: 0.9,
                ease: 'Linear',
                duration: 80,
                yoyo: true,
                repeat: 1,
            });
            if (music.isPlaying == false) {
                clicksound.stop()
            } else {
                clicksound.play()
            }
        }, this);
        if (music.isPlaying == false) {
            mute.setFrame("button_music_off")
        } else {
            mute.setFrame("button_music_on")
        }
        if (brandingMaterials.inGameLogo.display == true && brandingMaterials.inGameLogo.image != undefined) {
            inGameLogo1 = this.add.image(10, 590, 'inGameLogo1').setOrigin(0, 1).setInteractive({
                pixelPerfect: true,
                useHandCursor: true
            })
            inGameLogo1.on('pointerdown', brandingMaterials.inGameLogo.clickURL);
            inGameLogoani = this.time.addEvent({
                delay: 2000,
                callback: inGameLogoani,
                callbackScope: this
            });

            function inGameLogoani() {
                this.tweens.add({
                    targets: inGameLogo1,
                    scaleX: 1.05,
                    scaleY: 1.05,
                    ease: 'Linear',
                    duration: 100,
                    repeat: 1,
                    yoyo: true,
                    delay: 3000,
                    onComplete: inGameLogoani1,
                    callbackScope: this
                })
            }

            function inGameLogoani1() {
                this.tweens.add({
                    targets: inGameLogo1,
                    scaleX: 1.05,
                    scaleY: 1.05,
                    ease: 'Linear',
                    duration: 100,
                    repeat: 1,
                    yoyo: true,
                    delay: 3000,
                    onComplete: inGameLogoani1,
                    callbackScope: this
                })
            }
        }
        done = this.add.image(790, 590, 'uiButton').setOrigin(1, 1).setInteractive({
            pixelPerfect: true,
            useHandCursor: true
        })
        done.setFrame("button_next")
        done.visible = false
        done.on('pointerover', doneoverclick1);
        done.on('pointerout', doneoutclick1);
        done.on('pointerdown', donedownclick1);
        done.on('pointerup', doneupclick1);

        function doneoverclick1() {
            done.setScale(1.05, 1.05)
        }

        function doneoutclick1() {
            done.setScale(1, 1)
        }

        function doneupclick1() {
            if (startgame5) {
                done.setScale(1.05, 1.05)
            }
        }

        function donedownclick1() {
            if (!startgame5 && loadFinish) {
                startgame5 = true
                level = 6
                done.setScale(1, 1)
                marr3[1] = marr3[0]
                if (music.isPlaying == false) {
                    clicksound.stop()
                } else {
                    clicksound.play()
                }
                marr3[0] = level3elash.frame.name
                marr3[1] = level3blush.frame.name
                marr3[2] = bcount1
                marr3[3] = level3eye1.frame.name
                marr3[4] = bcount
                marr3[5] = level3ebrow.frame.name
                marr3[6] = level3hair.frame.name
                transitionbackground1.alpha = 1
                transitionbackground2.alpha = 1
                transitionbackground3.alpha = 1
                transitionpanel.alpha = 1
                transitiondress.alpha = 1
                transitiontitle1.alpha = 1
                transitiontitle2.alpha = 1
                transitiontitle3.alpha = 1
                transitionstar1.alpha = 1
                transitionstar2.alpha = 1
                transitionstar3.alpha = 1
                transitionstar4.alpha = 1
                transitionstar5.alpha = 1
                transitionstar1.setScale(0)
                transitionstar2.setScale(0)
                transitionstar3.setScale(0)
                transitionstar4.setScale(0)
                transitionstar5.setScale(0)
                transitionbackground1.visible = true
                transitionbackground2.visible = true
                transitionbackground3.visible = true
                transitiondress.visible = false
                transitionpanel.visible = false
                transitiontitle1.visible = false
                transitiontitle2.visible = false
                transitiontitle3.visible = false
                transitionstar1.visible = false
                transitionstar2.visible = false
                transitionstar3.visible = false
                transitionstar4.visible = false
                transitionstar5.visible = false
                transitionbackground1.alpha = 0
                transitionbackground2.alpha = 0
                transitionbackground3.alpha = 0
                this.scene.tweens.add({
                    targets: transitionbackground1,
                    alpha: 1,
                    ease: 'Linear',
                    duration: 800,
                    onComplete: transitionstart1,
                    callbackScope: this
                })

                function transitionstart1() {
                    transitiondress.visible = true
                    transitiondress.alpha = 0
                    this.scene.tweens.add({
                        targets: transitiondress,
                        alpha: 1,
                        ease: 'Linear',
                        duration: 400,
                        onComplete: transitionstart2,
                        callbackScope: this
                    })
                    this.scene.tweens.add({
                        targets: transitionbackground2,
                        alpha: 1,
                        ease: 'Linear',
                        duration: 800,
                    })
                }

                function transitionstart2() {
                    transitionpanel.visible = true
                    transitionpanel.setScale(0)
                    this.scene.tweens.add({
                        targets: transitionpanel,
                        scaleX: 1,
                        scaleY: 1,
                        ease: 'Linear',
                        duration: 500,
                        onComplete: transitionstart3,
                        callbackScope: this
                    })
                }

                function transitionstart3() {
                    this.scene.tweens.add({
                        targets: transitionbackground3,
                        alpha: 1,
                        ease: 'Linear',
                        duration: 800,
                        onComplete: transitionstart4,
                        callbackScope: this
                    })
                }

                function transitionstart4() {
                    transitiontitle1.visible = true
                    transitiontitle2.visible = true
                    transitiontitle3.visible = true
                    transitiontitle1.alpha = 0
                    transitiontitle2.alpha = 0
                    transitiontitle3.alpha = 0
                    transitiontitle2.x = transitiontitle2.x + 100
                    transitiontitle3.x = transitiontitle3.x - 100
                    this.scene.tweens.add({
                        targets: transitiontitle1,
                        alpha: 1,
                        ease: 'Linear',
                        duration: 800,
                        onComplete: transitionstart5,
                        callbackScope: this
                    })
                    this.scene.tweens.add({
                        targets: transitiontitle2,
                        alpha: 1,
                        x: transitiontitle2.x - 100,
                        ease: 'Linear',
                        duration: 800,
                    })
                    this.scene.tweens.add({
                        targets: transitiontitle3,
                        alpha: 1,
                        x: transitiontitle3.x + 100,
                        ease: 'Linear',
                        duration: 800,
                    })
                }

                function transitionstart5() {
                    transitionstar1.setScale(0)
                    transitionstar2.setScale(0)
                    transitionstar3.setScale(0)
                    transitionstar4.setScale(0)
                    transitionstar5.setScale(0)
                    transitionstar1.visible = true
                    transitionstar2.visible = true
                    transitionstar3.visible = true
                    transitionstar4.visible = true
                    transitionstar5.visible = true
                    this.scene.tweens.add({
                        targets: transitionstar1,
                        scaleX: 1,
                        scaleY: 1,
                        ease: 'Linear',
                        duration: 300,
                    })
                    this.scene.tweens.add({
                        targets: transitionstar2,
                        scaleX: 0.9,
                        scaleY: 0.9,
                        ease: 'Linear',
                        duration: 300,
                    })
                    this.scene.tweens.add({
                        targets: transitionstar3,
                        scaleX: 0.9,
                        scaleY: 0.9,
                        ease: 'Linear',
                        duration: 300,
                    })
                    this.scene.tweens.add({
                        targets: transitionstar4,
                        scaleX: 0.8,
                        scaleY: 0.8,
                        ease: 'Linear',
                        duration: 300,
                    })
                    this.scene.tweens.add({
                        targets: transitionstar5,
                        scaleX: 0.65,
                        scaleY: 0.65,
                        ease: 'Linear',
                        duration: 300,
                        onComplete: transitionstar6,
                        callbackScope: this
                    })
                }

                function transitionstar6() {
                    this.scene.scene.stop('Level3')
                    game.scene.run('levelSelect');
                }
            }
        }
        transitionbackground1 = this.add.image(400, 300, 'transitionbackground1').setOrigin(0.5, 0.5).setInteractive({
            pixelPerfect: true,
            useHandCursor: true
        })
        transitionbackground2 = this.add.image(400, 300, 'transitionbackground2').setOrigin(0.5, 0.5)
        transitionbackground3 = this.add.image(400, 300, 'transitionbackground3').setOrigin(0.5, 0.5)
        transitionpanel = this.add.image(393.5, 314, 'transitionpanel').setOrigin(0.5)
        transitiondress = this.add.image(267, 141, 'transitiondress').setOrigin(0, 0)
        transitiontitle1 = this.add.image(400, 329, 'title1');
        transitiontitle1.setOrigin(0.5);
        transitiontitle2 = this.add.image(380, 399, 'title2');
        transitiontitle2.setOrigin(0.5);
        transitiontitle3 = this.add.image(380, 459, 'title3');
        transitiontitle3.setOrigin(0.5);
        transitionstar1 = this.add.image(616.5, 168, 'transitionstar');
        transitionstar1.setOrigin(0.5);
        transitionstar2 = this.add.image(156.5, 168, 'transitionstar');
        transitionstar2.setOrigin(0.5);
        transitionstar2.setScale(0.9)
        transitionstar3 = this.add.image(596.5, 468, 'transitionstar');
        transitionstar3.setOrigin(0.5);
        transitionstar3.setScale(0.9)
        transitionstar4 = this.add.image(116.5, 468, 'transitionstar');
        transitionstar4.setOrigin(0.5);
        transitionstar4.setScale(0.8)
        transitionstar5 = this.add.image(400, 558, 'transitionstar');
        transitionstar5.setOrigin(0.5);
        transitionstar5.setScale(0.65)
        this.tweens.add({
            targets: transitionstar1,
            angle: 4,
            ease: 'Linear',
            duration: 300,
            repeat: -1,
            yoyo: true
        })
        this.tweens.add({
            targets: transitionstar2,
            angle: 4,
            ease: 'Linear',
            duration: 300,
            repeat: -1,
            yoyo: true
        })
        this.tweens.add({
            targets: transitionstar3,
            angle: 4,
            ease: 'Linear',
            duration: 300,
            repeat: -1,
            yoyo: true
        })
        this.tweens.add({
            targets: transitionstar4,
            angle: 4,
            ease: 'Linear',
            duration: 300,
            repeat: -1,
            yoyo: true
        })
        this.tweens.add({
            targets: transitionstar5,
            angle: 4,
            ease: 'Linear',
            duration: 300,
            repeat: -1,
            yoyo: true
        })
        this.time.addEvent({
            delay: 1000,
            callback: transitionstart11,
            callbackScope: this
        });

        function transitionstart11() {
            this.tweens.add({
                targets: transitionbackground1,
                alpha: 0,
                ease: 'Linear',
                duration: 700
            })
            this.tweens.add({
                targets: transitionbackground2,
                alpha: 0,
                ease: 'Linear',
                duration: 700
            })
            this.tweens.add({
                targets: transitionbackground3,
                alpha: 0,
                ease: 'Linear',
                duration: 700
            })
            this.tweens.add({
                targets: transitionpanel,
                alpha: 0,
                ease: 'Linear',
                duration: 700
            })
            this.tweens.add({
                targets: transitiondress,
                alpha: 0,
                ease: 'Linear',
                duration: 700
            })
            this.tweens.add({
                targets: transitiontitle1,
                alpha: 0,
                ease: 'Linear',
                duration: 700
            })
            this.tweens.add({
                targets: transitiontitle2,
                alpha: 0,
                ease: 'Linear',
                duration: 700
            })
            this.tweens.add({
                targets: transitiontitle3,
                alpha: 0,
                ease: 'Linear',
                duration: 700
            })
            this.tweens.add({
                targets: transitionstar1,
                alpha: 0,
                ease: 'Linear',
                duration: 700
            })
            this.tweens.add({
                targets: transitionstar2,
                alpha: 0,
                ease: 'Linear',
                duration: 700
            })
            this.tweens.add({
                targets: transitionstar3,
                alpha: 0,
                ease: 'Linear',
                duration: 700
            })
            this.tweens.add({
                targets: transitionstar4,
                alpha: 0,
                ease: 'Linear',
                duration: 700
            })
            this.tweens.add({
                targets: transitionstar5,
                alpha: 0,
                ease: 'Linear',
                duration: 700
            })
        }
        level3clickstart()
        hitobject = this.add.image(400, 300, 'hitobject').setOrigin(0.5, 0.5).setInteractive({
            useHandCursor: true
        })
        hitobject.visible = false
        hitobject.on('pointerdown', hitobjectdown)

        function hitobjectdown() {}
        this.load.on('complete', function () {
            loadFinish = true
        });
        this.load.start();
    }
});

function level3clickstart() {
    for (i = 1; i <= 7; i++) {
        game['elash' + i].on('pointerover', elashoverclick1);
        game['elash' + i].on('pointerout', elashoutclick1);
        game['elash' + i].on('pointerdown', elashdownclick1);
        game['elash' + i].on('pointerup', elashupclick1);
    }

    function elashoverclick1() {
        game['elash' + this.texture.key.substr(5)].setScale(1.05, 1.05)
    }

    function elashoutclick1() {
        game['elash' + this.texture.key.substr(5)].setScale(1, 1)
    }

    function elashdownclick1() {
        if (music.isPlaying == false) {
            clicksound.stop()
        } else {
            clicksound.play()
        }
        game['elash' + this.texture.key.substr(5)].setScale(1, 1)
        done.visible = true
        sno = this.texture.key.substr(5)
        glitter.anims.play('glitter')
        if (marr3[0] == parseInt(sno)) {
            level3elash.setFrame(0)
            marr3[0] = 0
        } else {
            marr3[0] = parseInt(sno)
            level3elash.setFrame(parseInt(sno))
        }
    }

    function elashupclick1() {
        game['elash' + this.texture.key.substr(5)].setScale(1.05, 1.05)
    }
    for (i = 1; i <= 10; i++) {
        game['blush' + i].on('pointerover', blushoverclick1);
        game['blush' + i].on('pointerout', blushoutclick1);
        game['blush' + i].on('pointerdown', blushdownclick1);
        game['blush' + i].on('pointerup', blushupclick1);
    }

    function blushoverclick1() {
        game['blush' + this.texture.key.substr(5)].setScale(1.05, 1.05)
    }

    function blushoutclick1() {
        game['blush' + this.texture.key.substr(5)].setScale(1, 1)
    }

    function blushdownclick1() {
        if (music.isPlaying == false) {
            clicksound.stop()
        } else {
            clicksound.play()
        }
        game['blush' + this.texture.key.substr(5)].setScale(1, 1)
        done.visible = true
        sno = this.texture.key.substr(5)
        glitter.anims.play('glitter')
        if (marr3[1] == parseInt(sno)) {
            level3blush.setFrame(0)
            marr3[1] = 0
        } else {
            marr3[1] = parseInt(sno)
            level3blush.setFrame(parseInt(sno))
        }
    }

    function blushupclick1() {
        game['blush' + this.texture.key.substr(5)].setScale(1.05, 1.05)
    }
    for (i = 1; i <= 10; i++) {
        game['lip' + i].on('pointerover', lipoverclick1);
        game['lip' + i].on('pointerout', lipoutclick1);
        game['lip' + i].on('pointerdown', lipdownclick1);
        game['lip' + i].on('pointerup', lipupclick1);
    }

    function lipoverclick1() {
        game['lip' + this.texture.key.substr(3)].setScale(1.05, 1.05)
    }

    function lipoutclick1() {
        game['lip' + this.texture.key.substr(3)].setScale(1, 1)
    }

    function lipdownclick1() {
        if (music.isPlaying == false) {
            clicksound.stop()
        } else {
            clicksound.play()
        }
        game['lip' + this.texture.key.substr(3)].setScale(1, 1)
        done.visible = true
        sno = this.texture.key.substr(3)
        glitter.anims.play('glitter')
        if (marr3[2] == parseInt(sno)) {
            level3lips.setFrame(0)
            marr3[2] = 0
            bcount1 = 0
        } else {
            marr3[2] = parseInt(sno)
            level3lips.setFrame(parseInt(sno) * 3)
            bcount1 = marr3[2] * 3
        }
        level3lip.remove()
        level3lip = this.scene.time.addEvent({
            delay: 1000,
            callback: level3lipsstart1,
            callbackScope: this
        });

        function level3lipsstart1() {
            level3lips.setFrame(bcount1 + 1)
            level3lip = this.scene.time.addEvent({
                delay: 100,
                callback: level3lipsstart2,
                callbackScope: this
            });
        }

        function level3lipsstart2() {
            level3lips.setFrame(bcount1 + 2)
            level3lip = this.scene.time.addEvent({
                delay: 2500,
                callback: level3lipsstart3,
                callbackScope: this
            });
        }

        function level3lipsstart3() {
            level3lips.setFrame(bcount1 + 1)
            level3lip = this.scene.time.addEvent({
                delay: 100,
                callback: level3lipsstart4,
                callbackScope: this
            });
        }

        function level3lipsstart4() {
            level3lips.setFrame(bcount1)
            level3lip = this.scene.time.addEvent({
                delay: 3000,
                callback: level3lipsstart1,
                callbackScope: this
            });
        }
    }

    function lipupclick1() {
        game['lip' + this.texture.key.substr(3)].setScale(1.05, 1.05)
    }
    for (i = 1; i <= 10; i++) {
        game['eye' + i].on('pointerover', eyeoverclick1);
        game['eye' + i].on('pointerout', eyeoutclick1);
        game['eye' + i].on('pointerdown', eyedownclick1);
        game['eye' + i].on('pointerup', eyeupclick1);
    }

    function eyeoverclick1() {
        game['eye' + this.texture.key.substr(3)].setScale(1.05, 1.05)
    }

    function eyeoutclick1() {
        game['eye' + this.texture.key.substr(3)].setScale(1, 1)
    }

    function eyedownclick1() {
        if (music.isPlaying == false) {
            clicksound.stop()
        } else {
            clicksound.play()
        }
        game['eye' + this.texture.key.substr(3)].setScale(1, 1)
        done.visible = true
        sno = this.texture.key.substr(3)
        glitter.anims.play('glitter')
        if (marr3[3] == parseInt(sno)) {
            level3eye1.setFrame(0)
            level3eye2.setFrame(0)
            marr3[3] = 0
        } else {
            marr3[3] = parseInt(sno)
            level3eye1.setFrame(parseInt(sno))
            level3eye2.setFrame(parseInt(sno))
        }
    }

    function eyeupclick1() {
        game['eye' + this.texture.key.substr(3)].setScale(1.05, 1.05)
    }
    for (i = 1; i <= 10; i++) {
        game['eshade' + i].on('pointerover', eshadeoverclick1);
        game['eshade' + i].on('pointerout', eshadeoutclick1);
        game['eshade' + i].on('pointerdown', eshadedownclick1);
        game['eshade' + i].on('pointerup', eshadeupclick1);
    }

    function eshadeoverclick1() {
        game['eshade' + this.texture.key.substr(6)].setScale(1.05, 1.05)
    }

    function eshadeoutclick1() {
        game['eshade' + this.texture.key.substr(6)].setScale(1, 1)
    }

    function eshadedownclick1() {
        if (music.isPlaying == false) {
            clicksound.stop()
        } else {
            clicksound.play()
        }
        game['eshade' + this.texture.key.substr(6)].setScale(1, 1)
        done.visible = true
        sno = this.texture.key.substr(6)
        glitter.anims.play('glitter')
        if (marr3[4] == parseInt(sno)) {
            level3eshade.setFrame(0)
            marr3[4] = 0
            bcount = 0
        } else {
            marr3[4] = parseInt(sno)
            level3eshade.setFrame(parseInt(sno) * 2)
            bcount = marr3[4] * 2
        }
    }

    function eshadeupclick1() {
        game['eshade' + this.texture.key.substr(6)].setScale(1.05, 1.05)
    }
    for (i = 1; i <= 10; i++) {
        game['ebrow' + i].on('pointerover', ebrowoverclick1);
        game['ebrow' + i].on('pointerout', ebrowoutclick1);
        game['ebrow' + i].on('pointerdown', ebrowdownclick1);
        game['ebrow' + i].on('pointerup', ebrowupclick1);
    }

    function ebrowoverclick1() {
        game['ebrow' + this.texture.key.substr(5)].setScale(1.05, 1.05)
    }

    function ebrowoutclick1() {
        game['ebrow' + this.texture.key.substr(5)].setScale(1, 1)
    }

    function ebrowdownclick1() {
        if (music.isPlaying == false) {
            clicksound.stop()
        } else {
            clicksound.play()
        }
        game['ebrow' + this.texture.key.substr(5)].setScale(1, 1)
        done.visible = true
        sno = this.texture.key.substr(5)
        glitter.anims.play('glitter')
        if (marr3[5] == parseInt(sno)) {
            level3ebrow.setFrame(0)
            marr3[5] = 0
        } else {
            marr3[5] = parseInt(sno)
            level3ebrow.setFrame(parseInt(sno))
        }
    }

    function ebrowupclick1() {
        game['ebrow' + this.texture.key.substr(5)].setScale(1.05, 1.05)
    }
    // for (i = 1; i <= 5; i++) {
    //     game['annahair' + i].on('pointerover', hairoverclick1);
    //     game['annahair' + i].on('pointerout', hairoutclick1);
    //     game['annahair' + i].on('pointerdown', hairdownclick1);
    //     game['annahair' + i].on('pointerup', hairupclick1);
    // }

    // function hairoverclick1() {
    //     game['annahair' + this.texture.key.substr(8)].setScale(1.05, 1.05)
    // }

    // function hairoutclick1() {
    //     game['annahair' + this.texture.key.substr(8)].setScale(1, 1)
    // }

    // function hairdownclick1() {
    //     if (music.isPlaying == false) {
    //         clicksound.stop()
    //     } else {
    //         clicksound.play()
    //     }
    //     game['annahair' + this.texture.key.substr(8)].setScale(1, 1)
    //     done.visible = true
    //     sno = this.texture.key.substr(8)
    //     glitter.anims.play('glitter')
    //     if (marr3[6] == parseInt(sno)) {
    //         level3hair.setFrame(0)
    //         marr3[6] = 0
    //     } else {
    //         marr3[6] = parseInt(sno)
    //         level3hair.setFrame(parseInt(sno))
    //     }
    // }

    // function hairupclick1() {
    //     game['annahair' + this.texture.key.substr(8)].setScale(1.05, 1.05)
    // }
    // annahairbtn.on('pointerover', annahairbtnoverclick1);
    // annahairbtn.on('pointerout', annahairbtnoutclick1);
    // annahairbtn.on('pointerdown', annahairbtndownclick1);
    // annahairbtn.on('pointerup', annahairbtnupclick1);

    // function annahairbtnoverclick1() {
    //     annahairbtn.setScale(1.05, 1.05)
    // }

    // function annahairbtnoutclick1() {
    //     annahairbtn.setScale(1, 1)
    // }

    // function annahairbtndownclick1() {
    //     if (music.isPlaying == false) {
    //         clicksound.stop()
    //     } else {
    //         clicksound.play()
    //     }
    //     annahairbtn.setScale(1, 1)
    //     if (!makestart1) {
    //         makestart1 = true
    //         this.scene.tweens.add({
    //             targets: level1panelgroup,
    //             x: 0,
    //             ease: 'Linear',
    //             duration: 300,
    //         });
    //         for (i = 1; i <= 7; i++) {
    //             game['elash' + i].visible = false
    //         }
    //         for (i = 1; i <= 10; i++) {
    //             game['blush' + i].visible = false
    //             game['lip' + i].visible = false
    //             game['eye' + i].visible = false
    //             game['eshade' + i].visible = false
    //             game['ebrow' + i].visible = false
    //         }
    //         for (i = 1; i <= 5; i++) {
    //             game['annahair' + i].visible = true
    //         }
    //     } else {
    //         if (!game['annahair' + 1].visible) {
    //             this.scene.tweens.add({
    //                 targets: level1panelgroup,
    //                 x: -500,
    //                 ease: 'Sine.easeIn',
    //                 duration: 300,
    //                 onComplete: levelpanel1move
    //             });

    //             function levelpanel1move() {
    //                 for (i = 1; i <= 7; i++) {
    //                     game['elash' + i].visible = false
    //                 }
    //                 for (i = 1; i <= 10; i++) {
    //                     game['blush' + i].visible = false
    //                     game['lip' + i].visible = false
    //                     game['eye' + i].visible = false
    //                     game['eshade' + i].visible = false
    //                     game['ebrow' + i].visible = false
    //                 }
    //                 this.parent.scene.tweens.add({
    //                     targets: level1panelgroup,
    //                     x: 0,
    //                     ease: 'Sine.easeOut',
    //                     duration: 300,
    //                 });
    //                 for (i = 1; i <= 5; i++) {
    //                     game['annahair' + i].visible = true
    //                 }
    //             }
    //         }
    //     }
    // }

    // function annahairbtnupclick1() {
    //     annahairbtn.setScale(1.05, 1.05)
    // }
    for (i = 1; i <= 6; i++) {
        game['category' + i].on('pointerover', categoryoverclick1);
        game['category' + i].on('pointerout', categoryoutclick1);
        game['category' + i].on('pointerdown', categorydownclick1);
        game['category' + i].on('pointerup', categoryupclick1);
    }

    function categoryoverclick1() {
        game['category' + this.texture.key.substr(8)].setScale(1.05, 1.05)
    }

    function categoryoutclick1() {
        game['category' + this.texture.key.substr(8)].setScale(1, 1)
    }

    function categoryupclick1() {
        game['category' + this.texture.key.substr(8)].setScale(1.05, 1.05)
    }

    function categorydownclick1() {
        if (music.isPlaying == false) {
            clicksound.stop()
        } else {
            clicksound.play()
        }
        sno = this.texture.key.substr(8)
        game['category' + parseInt(sno)].setScale(1, 1)
        if (!makestart1) {
            makestart1 = true
            this.scene.tweens.add({
                targets: level1panelgroup,
                x: 0,
                ease: 'Linear',
                duration: 300,
            });
            for (i = 1; i <= 7; i++) {
                game['elash' + i].visible = false
            }
            for (i = 1; i <= 10; i++) {
                game['blush' + i].visible = false
                game['lip' + i].visible = false
                game['eye' + i].visible = false
                game['eshade' + i].visible = false
                game['ebrow' + i].visible = false
            }
            for (i = 1; i <= 5; i++) {
                game['annahair' + i].visible = false
            }
            if (parseInt(sno) == 1) {
                for (i = 1; i <= 7; i++) {
                    game['elash' + i].visible = true
                }
            } else if (parseInt(sno) == 2) {
                for (i = 1; i <= 10; i++) {
                    game['blush' + i].visible = true
                }
            } else if (parseInt(sno) == 3) {
                for (i = 1; i <= 10; i++) {
                    game['lip' + i].visible = true
                }
            } else if (parseInt(sno) == 4) {
                for (i = 1; i <= 10; i++) {
                    game['eye' + i].visible = true
                }
            } else if (parseInt(sno) == 5) {
                for (i = 1; i <= 10; i++) {
                    game['eshade' + i].visible = true
                }
            } else if (parseInt(sno) == 6) {
                for (i = 1; i <= 10; i++) {
                    game['ebrow' + i].visible = true
                }
            }
        } else {
            if (parseInt(sno) == 1 && !game['elash' + 1].visible) {
                this.scene.tweens.add({
                    targets: level1panelgroup,
                    x: -500,
                    ease: 'Linear',
                    duration: 300,
                    onComplete: levelpanel1move
                });

                function levelpanel1move() {
                    for (i = 1; i <= 7; i++) {
                        game['elash' + i].visible = false
                    }
                    for (i = 1; i <= 10; i++) {
                        game['blush' + i].visible = false
                        game['lip' + i].visible = false
                        game['eye' + i].visible = false
                        game['eshade' + i].visible = false
                        game['ebrow' + i].visible = false
                    }
                    for (i = 1; i <= 5; i++) {
                        game['annahair' + i].visible = false
                    }
                    this.parent.scene.tweens.add({
                        targets: level1panelgroup,
                        x: 0,
                        ease: 'Linear',
                        duration: 300,
                    });
                    for (i = 1; i <= 7; i++) {
                        game['elash' + i].visible = true
                    }
                }
            } else if (parseInt(sno) == 2 && !game['blush' + 1].visible) {
                this.scene.tweens.add({
                    targets: level1panelgroup,
                    x: -500,
                    ease: 'Linear',
                    duration: 300,
                    onComplete: levelpanel1move
                });

                function levelpanel1move() {
                    for (i = 1; i <= 7; i++) {
                        game['elash' + i].visible = false
                    }
                    for (i = 1; i <= 10; i++) {
                        game['blush' + i].visible = false
                        game['lip' + i].visible = false
                        game['eye' + i].visible = false
                        game['eshade' + i].visible = false
                        game['ebrow' + i].visible = false
                    }
                    for (i = 1; i <= 5; i++) {
                        game['annahair' + i].visible = false
                    }
                    this.parent.scene.tweens.add({
                        targets: level1panelgroup,
                        x: 0,
                        ease: 'Linear',
                        duration: 300,
                    });
                    for (i = 1; i <= 10; i++) {
                        game['blush' + i].visible = true
                    }
                }
            } else if (parseInt(sno) == 3 && !game['lip' + 1].visible) {
                this.scene.tweens.add({
                    targets: level1panelgroup,
                    x: -500,
                    ease: 'Linear',
                    duration: 300,
                    onComplete: levelpanel1move
                });

                function levelpanel1move() {
                    for (i = 1; i <= 7; i++) {
                        game['elash' + i].visible = false
                    }
                    for (i = 1; i <= 10; i++) {
                        game['blush' + i].visible = false
                        game['lip' + i].visible = false
                        game['eye' + i].visible = false
                        game['eshade' + i].visible = false
                        game['ebrow' + i].visible = false
                    }
                    for (i = 1; i <= 5; i++) {
                        game['annahair' + i].visible = false
                    }
                    this.parent.scene.tweens.add({
                        targets: level1panelgroup,
                        x: 0,
                        ease: 'Linear',
                        duration: 300,
                    });
                    for (i = 1; i <= 10; i++) {
                        game['lip' + i].visible = true
                    }
                }
            } else if (parseInt(sno) == 4 && !game['eye' + 1].visible) {
                this.scene.tweens.add({
                    targets: level1panelgroup,
                    x: -500,
                    ease: 'Linear',
                    duration: 300,
                    onComplete: levelpanel1move
                });

                function levelpanel1move() {
                    for (i = 1; i <= 7; i++) {
                        game['elash' + i].visible = false
                    }
                    for (i = 1; i <= 10; i++) {
                        game['blush' + i].visible = false
                        game['lip' + i].visible = false
                        game['eye' + i].visible = false
                        game['eshade' + i].visible = false
                        game['ebrow' + i].visible = false
                    }
                    for (i = 1; i <= 5; i++) {
                        game['annahair' + i].visible = false
                    }
                    this.parent.scene.tweens.add({
                        targets: level1panelgroup,
                        x: 0,
                        ease: 'Linear',
                        duration: 300,
                    });
                    for (i = 1; i <= 10; i++) {
                        game['eye' + i].visible = true
                    }
                }
            } else if (parseInt(sno) == 5 && !game['eshade' + 1].visible) {
                this.scene.tweens.add({
                    targets: level1panelgroup,
                    x: -500,
                    ease: 'Linear',
                    duration: 300,
                    onComplete: levelpanel1move
                });

                function levelpanel1move() {
                    for (i = 1; i <= 7; i++) {
                        game['elash' + i].visible = false
                    }
                    for (i = 1; i <= 10; i++) {
                        game['blush' + i].visible = false
                        game['lip' + i].visible = false
                        game['eye' + i].visible = false
                        game['eshade' + i].visible = false
                        game['ebrow' + i].visible = false
                    }
                    for (i = 1; i <= 5; i++) {
                        game['annahair' + i].visible = false
                    }
                    this.parent.scene.tweens.add({
                        targets: level1panelgroup,
                        x: 0,
                        ease: 'Linear',
                        duration: 300,
                    });
                    for (i = 1; i <= 10; i++) {
                        game['eshade' + i].visible = true
                    }
                }
            } else if (parseInt(sno) == 6 && !game['ebrow' + 1].visible) {
                this.scene.tweens.add({
                    targets: level1panelgroup,
                    x: -500,
                    ease: 'Linear',
                    duration: 300,
                    onComplete: levelpanel1move
                });

                function levelpanel1move() {
                    for (i = 1; i <= 7; i++) {
                        game['elash' + i].visible = false
                    }
                    for (i = 1; i <= 10; i++) {
                        game['blush' + i].visible = false
                        game['lip' + i].visible = false
                        game['eye' + i].visible = false
                        game['eshade' + i].visible = false
                        game['ebrow' + i].visible = false
                    }
                    for (i = 1; i <= 5; i++) {
                        game['annahair' + i].visible = false
                    }
                    this.parent.scene.tweens.add({
                        targets: level1panelgroup,
                        x: 0,
                        ease: 'Linear',
                        duration: 300,
                    });
                    for (i = 1; i <= 10; i++) {
                        game['ebrow' + i].visible = true
                    }
                }
            }
        }
    }
}
var ccount = 0
var ccount1 = 0
var startgame6 = false
var marr4 = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
var Level4 = new Phaser.Class({
    Extends: Phaser.Scene,
    initialize: function Level4() {
        Phaser.Scene.call(this, {
            key: 'Level4'
        });
    },
    preload: function () {
        marr4 = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        ccount = 0
        ccount1 = 0
        startgame6 = false
        loadFinish = false
        makestart1 = false
    },
    create: function () {
        level4background = this.add.image(400, 300, 'rapunbackground1').setOrigin(0.5, 0.5).setScale(1.2)
        level1cupboard = this.add.image(0, 0, 'level1cupboard').setOrigin(0, 0)
        
        var elsaelxarr = [, 27, 27, 139, 139, 145, 245, 250]
        var elsaelyarr = [, 90, 230, 47, 177, 311, 86, 232]
        for (i = 1; i <= 7; i++) {
            game['elash' + i] = this.add.image(elsaelxarr[i], elsaelyarr[i], 'elash' + i).setOrigin(0.5, 0.5).setInteractive({
                useHandCursor: true
            })
            game['elash' + i].x += parseFloat(game['elash' + i].width / 2)
            game['elash' + i].y += parseFloat(game['elash' + i].height / 2)
            game['elash' + i].visible = false
        }
        var elsablxarr = [, 42, 0, 40, 143, 143, 143, 143, 258, 298, 257]
        var elsablyarr = [, 88, 173, 263, 42, 132, 232, 322, 81, 171, 260]
        for (i = 1; i <= 10; i++) {
            game['blush' + i] = this.add.image(elsablxarr[i], elsablyarr[i], 'blush' + i).setOrigin(0.5, 0.5).setInteractive({
                pixelPerfect: true,
                useHandCursor: true
            })
            game['blush' + i].x += parseFloat(game['blush' + i].width / 2)
            game['blush' + i].y += parseFloat(game['blush' + i].height / 2)
            game['blush' + i].visible = false
        }
        var elsalipxarr = [, 27, 0, 28, 136, 136, 136, 136, 247, 277, 250]
        var elsalipyarr = [, 120, 207, 297, 55, 153, 248, 348, 116, 207, 301]
        for (i = 1; i <= 10; i++) {
            game['lip' + i] = this.add.image(elsalipxarr[i], elsalipyarr[i], 'lip' + i).setOrigin(0.5, 0.5).setInteractive({
                pixelPerfect: true,
                useHandCursor: true
            })
            game['lip' + i].x += parseFloat(game['lip' + i].width / 2)
            game['lip' + i].y += parseFloat(game['lip' + i].height / 2)
            game['lip' + i].visible = false
        }
        var elsaeyxarr = [, 39, 9, 42, 149, 149, 149, 149, 258, 291, 258]
        var elsaeyyarr = [, 91, 193, 295, 43, 145, 246, 346, 93, 193, 293]
        for (i = 1; i <= 10; i++) {
            game['eye' + i] = this.add.image(elsaeyxarr[i], elsaeyyarr[i], 'eye' + i).setOrigin(0.5, 0.5).setInteractive({
                pixelPerfect: true,
                useHandCursor: true
            })
            game['eye' + i].x += parseFloat(game['eye' + i].width / 2)
            game['eye' + i].y += parseFloat(game['eye' + i].height / 2)
            game['eye' + i].visible = false
        }
        var elsaesxarr = [, 43, 0, 39, 138, 140, 140, 140, 244, 285, 245]
        var elsaesyarr = [, 99, 175, 270, 49, 145, 234, 325, 93, 173, 268]
        for (i = 1; i <= 10; i++) {
            game['eshade' + i] = this.add.image(elsaesxarr[i], elsaesyarr[i], 'eshade' + i).setOrigin(0.5, 0.5).setInteractive({
                pixelPerfect: true,
                useHandCursor: true
            })
            game['eshade' + i].x += parseFloat(game['eshade' + i].width / 2)
            game['eshade' + i].y += parseFloat(game['eshade' + i].height / 2)
            game['eshade' + i].visible = false
        }
        var elsaebxarr = [, 9, 9, 91, 82, 83, 181, 181, 181, 261, 261]
        var elsaebyarr = [, 130, 240, 63, 189, 297, 63, 177, 297, 122, 222]
        for (i = 1; i <= 10; i++) {
            game['ebrow' + i] = this.add.image(elsaebxarr[i], elsaebyarr[i], 'ebrow' + i).setOrigin(0.5, 0.5).setInteractive({
                pixelPerfect: true,
                useHandCursor: true
            })
            game['ebrow' + i].x += parseFloat(game['ebrow' + i].width / 2)
            game['ebrow' + i].y += parseFloat(game['ebrow' + i].height / 2)
            game['ebrow' + i].visible = false
        }
        var rapunhrxarr = [, 15, 30, 213, 242, 132]
        var rapunhryarr = [, 65, 241, 60, 247, 162]
        for (i = 1; i <= 5; i++) {
            game['rapunhair' + i] = this.add.image(rapunhrxarr[i], rapunhryarr[i], 'rapunhair' + i).setOrigin(0.5, 0.5).setInteractive({
                pixelPerfect: true,
                useHandCursor: true
            })
            game['rapunhair' + i].x += parseFloat(game['rapunhair' + i].width / 2)
            game['rapunhair' + i].y += parseFloat(game['rapunhair' + i].height / 2)
            game['rapunhair' + i].visible = false
        }
        level1panelgroup = this.add.container(0, 0);
        level1panelgroup.add(level1cupboard)
        for (i = 1; i <= 7; i++) {
            level1panelgroup.add(game['elash' + i])
        }
        for (i = 1; i <= 10; i++) {
            level1panelgroup.add(game['blush' + i])
        }
        for (i = 1; i <= 10; i++) {
            level1panelgroup.add(game['lip' + i])
        }
        for (i = 1; i <= 10; i++) {
            level1panelgroup.add(game['eye' + i])
        }
        for (i = 1; i <= 10; i++) {
            level1panelgroup.add(game['eshade' + i])
        }
        for (i = 1; i <= 10; i++) {
            level1panelgroup.add(game['ebrow' + i])
        }
        for (i = 1; i <= 5; i++) {
            level1panelgroup.add(game['rapunhair' + i])
        }
        level1panelgroup.x = -500
        level4bhair = this.add.image(378, 220, 'level4bhair').setOrigin(0, 0)
        level4body = this.add.image(348, 102, 'level4body').setOrigin(0, 0)
        level4eye1 = this.add.image(498, 243, 'level4eye1').setOrigin(0, 0)
        level4eye2 = this.add.image(498, 243, 'level4eye2').setOrigin(0, 0)
        level4toplayer = this.add.image(482, 216, 'level4toplayer').setOrigin(0, 0)
        level4eshade = this.add.image(474, 230, 'level4eshade').setOrigin(0, 0)
        level4lips = this.add.image(500, 291, 'level4lips').setOrigin(0, 0)
        level4blush = this.add.image(497, 280, 'level4blush').setOrigin(0, 0)
        level4elash = this.add.image(484, 237, 'level4elash').setOrigin(0, 0)
        level4ebrow = this.add.image(482, 216, 'level4ebrow').setOrigin(0, 0)
        level4hair = this.add.image(352, 60, 'level4hair').setOrigin(0, 0)
        var level4dollgroup = this.add.container(0, 0);
        level4dollgroup.add([level4bhair, level4body, level4eye1, level4eye2, level4toplayer, level4blush, level4eshade, level4lips, level4blush, level4elash, level4ebrow, level4hair]);
        level4dollgroup.y = -60
        var tween1 = this.tweens.add({
            targets: level4eye1,
            x: level4eye1.x + 3,
            ease: 'Linear',
            duration: 300,
            delay: 2400,
            callbackScope: this,
        });
        var tween2 = this.tweens.add({
            targets: level4eye2,
            x: level4eye2.x + 3,
            ease: 'Linear',
            duration: 300,
            delay: 2400,
            onComplete: rapuneyestartani1,
            callbackScope: this
        });

        function rapuneyestartani1() {
            this.tweens.add({
                targets: level4eye1,
                x: level4eye1.x - 3,
                ease: 'Linear',
                duration: 300,
                delay: 3000,
                callbackScope: this
            });
            this.tweens.add({
                targets: level4eye2,
                x: level4eye2.x - 3,
                ease: 'Linear',
                duration: 300,
                delay: 3000,
                onComplete: rapuneyestartani2,
                callbackScope: this,
            })
        }

        function rapuneyestartani2() {
            this.tweens.add({
                targets: level4eye1,
                x: level4eye1.x + 3,
                ease: 'Linear',
                duration: 300,
                delay: 2400,
                callbackScope: this
            });
            this.tweens.add({
                targets: level4eye2,
                x: level4eye2.x + 3,
                ease: 'Linear',
                duration: 300,
                delay: 2400,
                onComplete: rapuneyestartani1,
                callbackScope: this,
            })
        }
        clicksound = this.sound.add('clickss');
        level4eshad = this.time.addEvent({
            delay: 1000,
            callback: level4eblinkstart1,
            callbackScope: this
        });

        function level4eblinkstart1() {
            level4eshade.setFrame(ccount + 1)
            level4elash.visible = false
            level4eshad = this.time.addEvent({
                delay: 200,
                callback: level4eblinkstart2,
                callbackScope: this
            });
        }

        function level4eblinkstart2() {
            level4eshade.setFrame(ccount)
            level4elash.visible = true
            level4eshad = this.time.addEvent({
                delay: 4000,
                callback: level4eblinkstart1,
                callbackScope: this
            });
        }
        level4lip = this.time.addEvent({
            delay: 1000,
            callback: level4lipsstart1,
            callbackScope: this
        });

        function level4lipsstart1() {
            level4lips.setFrame(ccount1 + 1)
            level4lip = this.time.addEvent({
                delay: 100,
                callback: level4lipsstart2,
                callbackScope: this
            });
        }

        function level4lipsstart2() {
            level4lips.setFrame(ccount1 + 2)
            level4lip = this.time.addEvent({
                delay: 2500,
                callback: level4lipsstart3,
                callbackScope: this
            });
        }

        function level4lipsstart3() {
            level4lips.setFrame(ccount1 + 1)
            level4lip = this.time.addEvent({
                delay: 100,
                callback: level4lipsstart4,
                callbackScope: this
            });
        }

        function level4lipsstart4() {
            level4lips.setFrame(ccount1)
            level4lip = this.time.addEvent({
                delay: 3000,
                callback: level4lipsstart1,
                callbackScope: this
            });
        }
        table = this.add.image(0, 485, 'table').setOrigin(0, 0)
        shadow = this.add.image(62, 491, 'shadow').setOrigin(0, 0)
        shadow4 = this.add.image(544, 546, 'shadow4').setOrigin(0, 0)
        var catxarr = [, 67, 140, 250, 295, 418, 515]
        var catyarr = [, 413, 442, 427, 503, 439, 433]
        for (i = 1; i <= 1; i++) {
            game['category' + i] = this.add.image(catxarr[i], catyarr[i], 'category' + i).setOrigin(0.5, 0.5).setInteractive({
                useHandCursor: true
            })
            game['category' + i].x += parseFloat(game['category' + i].width / 2)
            game['category' + i].y += parseFloat(game['category' + i].height / 2)
        }
        for (i = 2; i <= 6; i++) {
            game['category' + i] = this.add.image(catxarr[i], catyarr[i], 'category' + i).setOrigin(0.5, 0.5).setInteractive({
                pixelPerfect: true,
                useHandCursor: true
            })
            game['category' + i].x += parseFloat(game['category' + i].width / 2)
            game['category' + i].y += parseFloat(game['category' + i].height / 2)
        }
        // rapunhairbtn = this.add.image(543, 412, 'rapunhairbtn').setOrigin(0.5, 0.5).setInteractive({
        //     pixelPerfect: true,
        //     useHandCursor: true
        // })
        // rapunhairbtn.x += parseFloat(rapunhairbtn.width / 2)
        // rapunhairbtn.y += parseFloat(rapunhairbtn.height / 2)
        makeupcategory1 = this.add.container(0, 0);
        makeupcategory1.add([shadow, shadow4, game['category' + 1], game['category' + 2], game['category' + 3], game['category' + 4], game['category' + 5], game['category' + 6]]);
        makeupcategory1.x = 40
        glitter = this.add.sprite(569.1, 210.55, 'glitter').setOrigin(0.5, 0.5)
        anim = this.anims.create({
            key: 'glitter',
            frames: this.anims.generateFrameNumbers('glitter', {
                start: 0,
                end: 41
            }),
            frameRate: 24,
        });
        glitter.anims.load('glitter');
        mute = this.add.sprite(790, 10, 'uiButton').setFrame("button_music_on").setInteractive({
            pixelPerfect: true,
            useHandCursor: true
        })
        mute.setOrigin(1, 0)
        mute.on('pointerdown', function () {
            if (!isMuted) {
                isMuted = true
                music.pause()
                mute.setFrame("button_music_off")
            } else {
                isMuted = false
                music.resume()
                mute.setFrame("button_music_on")
            }
            var tween2 = this.tweens.add({
                targets: mute,
                scaleX: 0.9,
                scaleY: 0.9,
                ease: 'Linear',
                duration: 80,
                yoyo: true,
                repeat: 1,
            });
            if (music.isPlaying == false) {
                clicksound.stop()
            } else {
                clicksound.play()
            }
        }, this);
        if (music.isPlaying == false) {
            mute.setFrame("button_music_off")
        } else {
            mute.setFrame("button_music_on")
        }
        if (brandingMaterials.inGameLogo.display == true && brandingMaterials.inGameLogo.image != undefined) {
            inGameLogo1 = this.add.image(10, 590, 'inGameLogo1').setOrigin(0, 1).setInteractive({
                pixelPerfect: true,
                useHandCursor: true
            })
            inGameLogo1.on('pointerdown', brandingMaterials.inGameLogo.clickURL);
            inGameLogoani = this.time.addEvent({
                delay: 2000,
                callback: inGameLogoani,
                callbackScope: this
            });

            function inGameLogoani() {
                this.tweens.add({
                    targets: inGameLogo1,
                    scaleX: 1.05,
                    scaleY: 1.05,
                    ease: 'Linear',
                    duration: 100,
                    repeat: 1,
                    yoyo: true,
                    delay: 3000,
                    onComplete: inGameLogoani1,
                    callbackScope: this
                })
            }

            function inGameLogoani1() {
                this.tweens.add({
                    targets: inGameLogo1,
                    scaleX: 1.05,
                    scaleY: 1.05,
                    ease: 'Linear',
                    duration: 100,
                    repeat: 1,
                    yoyo: true,
                    delay: 3000,
                    onComplete: inGameLogoani1,
                    callbackScope: this
                })
            }
        }
        done = this.add.image(790, 590, 'uiButton').setOrigin(1, 1).setInteractive({
            pixelPerfect: true,
            useHandCursor: true
        })
        done.setFrame("button_next")
        done.visible = false
        done.on('pointerover', doneoverclick1);
        done.on('pointerout', doneoutclick1);
        done.on('pointerdown', donedownclick1);
        done.on('pointerup', doneupclick1);

        function doneoverclick1() {
            done.setScale(1.05, 1.05)
        }

        function doneoutclick1() {
            done.setScale(1, 1)
        }

        function doneupclick1() {
            if (startgame6) {
                done.setScale(1.05, 1.05)
            }
        }

        function donedownclick1() {
            if (!startgame6 && loadFinish) {
                startgame6 = true
                level = 8
                done.setScale(1, 1)
                marr4[1] = marr4[0]
                if (music.isPlaying == false) {
                    clicksound.stop()
                } else {
                    clicksound.play()
                }
                marr4[0] = level4elash.frame.name
                marr4[1] = level4blush.frame.name
                marr4[2] = ccount1
                marr4[3] = level4eye1.frame.name
                marr4[4] = ccount
                marr4[5] = level4ebrow.frame.name
                marr4[6] = level4hair.frame.name
                transitionbackground1.alpha = 1
                transitionbackground2.alpha = 1
                transitionbackground3.alpha = 1
                transitionpanel.alpha = 1
                transitiondress.alpha = 1
                transitiontitle1.alpha = 1
                transitiontitle2.alpha = 1
                transitiontitle3.alpha = 1
                transitionstar1.alpha = 1
                transitionstar2.alpha = 1
                transitionstar3.alpha = 1
                transitionstar4.alpha = 1
                transitionstar5.alpha = 1
                transitionstar1.setScale(0)
                transitionstar2.setScale(0)
                transitionstar3.setScale(0)
                transitionstar4.setScale(0)
                transitionstar5.setScale(0)
                transitionbackground1.visible = true
                transitionbackground2.visible = true
                transitionbackground3.visible = true
                transitiondress.visible = false
                transitionpanel.visible = false
                transitiontitle1.visible = false
                transitiontitle2.visible = false
                transitiontitle3.visible = false
                transitionstar1.visible = false
                transitionstar2.visible = false
                transitionstar3.visible = false
                transitionstar4.visible = false
                transitionstar5.visible = false
                transitionbackground1.alpha = 0
                transitionbackground2.alpha = 0
                transitionbackground3.alpha = 0
                this.scene.tweens.add({
                    targets: transitionbackground1,
                    alpha: 1,
                    ease: 'Linear',
                    duration: 800,
                    onComplete: transitionstart1,
                    callbackScope: this
                })

                function transitionstart1() {
                    transitiondress.visible = true
                    transitiondress.alpha = 0
                    this.scene.tweens.add({
                        targets: transitiondress,
                        alpha: 1,
                        ease: 'Linear',
                        duration: 400,
                        onComplete: transitionstart2,
                        callbackScope: this
                    })
                    this.scene.tweens.add({
                        targets: transitionbackground2,
                        alpha: 1,
                        ease: 'Linear',
                        duration: 800,
                    })
                }

                function transitionstart2() {
                    transitionpanel.visible = true
                    transitionpanel.setScale(0)
                    this.scene.tweens.add({
                        targets: transitionpanel,
                        scaleX: 1,
                        scaleY: 1,
                        ease: 'Linear',
                        duration: 500,
                        onComplete: transitionstart3,
                        callbackScope: this
                    })
                }

                function transitionstart3() {
                    this.scene.tweens.add({
                        targets: transitionbackground3,
                        alpha: 1,
                        ease: 'Linear',
                        duration: 800,
                        onComplete: transitionstart4,
                        callbackScope: this
                    })
                }

                function transitionstart4() {
                    transitiontitle1.visible = true
                    transitiontitle2.visible = true
                    transitiontitle3.visible = true
                    transitiontitle1.alpha = 0
                    transitiontitle2.alpha = 0
                    transitiontitle3.alpha = 0
                    transitiontitle2.x = transitiontitle2.x + 100
                    transitiontitle3.x = transitiontitle3.x - 100
                    this.scene.tweens.add({
                        targets: transitiontitle1,
                        alpha: 1,
                        ease: 'Linear',
                        duration: 800,
                        onComplete: transitionstart5,
                        callbackScope: this
                    })
                    this.scene.tweens.add({
                        targets: transitiontitle2,
                        alpha: 1,
                        x: transitiontitle2.x - 100,
                        ease: 'Linear',
                        duration: 800,
                    })
                    this.scene.tweens.add({
                        targets: transitiontitle3,
                        alpha: 1,
                        x: transitiontitle3.x + 100,
                        ease: 'Linear',
                        duration: 800,
                    })
                }

                function transitionstart5() {
                    transitionstar1.setScale(0)
                    transitionstar2.setScale(0)
                    transitionstar3.setScale(0)
                    transitionstar4.setScale(0)
                    transitionstar5.setScale(0)
                    transitionstar1.visible = true
                    transitionstar2.visible = true
                    transitionstar3.visible = true
                    transitionstar4.visible = true
                    transitionstar5.visible = true
                    this.scene.tweens.add({
                        targets: transitionstar1,
                        scaleX: 1,
                        scaleY: 1,
                        ease: 'Linear',
                        duration: 300,
                    })
                    this.scene.tweens.add({
                        targets: transitionstar2,
                        scaleX: 0.9,
                        scaleY: 0.9,
                        ease: 'Linear',
                        duration: 300,
                    })
                    this.scene.tweens.add({
                        targets: transitionstar3,
                        scaleX: 0.9,
                        scaleY: 0.9,
                        ease: 'Linear',
                        duration: 300,
                    })
                    this.scene.tweens.add({
                        targets: transitionstar4,
                        scaleX: 0.8,
                        scaleY: 0.8,
                        ease: 'Linear',
                        duration: 300,
                    })
                    this.scene.tweens.add({
                        targets: transitionstar5,
                        scaleX: 0.65,
                        scaleY: 0.65,
                        ease: 'Linear',
                        duration: 300,
                        onComplete: transitionstar6,
                        callbackScope: this
                    })
                }

                function transitionstar6() {
                    this.scene.scene.stop('Level4')
                    game.scene.run('levelSelect');
                }
            }
        }
        transitionbackground1 = this.add.image(400, 300, 'transitionbackground1').setOrigin(0.5, 0.5).setInteractive({
            pixelPerfect: true,
            useHandCursor: true
        })
        transitionbackground2 = this.add.image(400, 300, 'transitionbackground2').setOrigin(0.5, 0.5)
        transitionbackground3 = this.add.image(400, 300, 'transitionbackground3').setOrigin(0.5, 0.5)
        transitionpanel = this.add.image(393.5, 314, 'transitionpanel').setOrigin(0.5)
        transitiondress = this.add.image(267, 141, 'transitiondress').setOrigin(0, 0)
        transitiontitle1 = this.add.image(400, 329, 'title1');
        transitiontitle1.setOrigin(0.5);
        transitiontitle2 = this.add.image(380, 399, 'title2');
        transitiontitle2.setOrigin(0.5);
        transitiontitle3 = this.add.image(380, 459, 'title3');
        transitiontitle3.setOrigin(0.5);
        transitionstar1 = this.add.image(616.5, 168, 'transitionstar');
        transitionstar1.setOrigin(0.5);
        transitionstar2 = this.add.image(156.5, 168, 'transitionstar');
        transitionstar2.setOrigin(0.5);
        transitionstar2.setScale(0.9)
        transitionstar3 = this.add.image(596.5, 468, 'transitionstar');
        transitionstar3.setOrigin(0.5);
        transitionstar3.setScale(0.9)
        transitionstar4 = this.add.image(116.5, 468, 'transitionstar');
        transitionstar4.setOrigin(0.5);
        transitionstar4.setScale(0.8)
        transitionstar5 = this.add.image(400, 558, 'transitionstar');
        transitionstar5.setOrigin(0.5);
        transitionstar5.setScale(0.65)
        this.tweens.add({
            targets: transitionstar1,
            angle: 4,
            ease: 'Linear',
            duration: 300,
            repeat: -1,
            yoyo: true
        })
        this.tweens.add({
            targets: transitionstar2,
            angle: 4,
            ease: 'Linear',
            duration: 300,
            repeat: -1,
            yoyo: true
        })
        this.tweens.add({
            targets: transitionstar3,
            angle: 4,
            ease: 'Linear',
            duration: 300,
            repeat: -1,
            yoyo: true
        })
        this.tweens.add({
            targets: transitionstar4,
            angle: 4,
            ease: 'Linear',
            duration: 300,
            repeat: -1,
            yoyo: true
        })
        this.tweens.add({
            targets: transitionstar5,
            angle: 4,
            ease: 'Linear',
            duration: 300,
            repeat: -1,
            yoyo: true
        })
        this.time.addEvent({
            delay: 1000,
            callback: transitionstart11,
            callbackScope: this
        });

        function transitionstart11() {
            this.tweens.add({
                targets: transitionbackground1,
                alpha: 0,
                ease: 'Linear',
                duration: 700
            })
            this.tweens.add({
                targets: transitionbackground2,
                alpha: 0,
                ease: 'Linear',
                duration: 700
            })
            this.tweens.add({
                targets: transitionbackground3,
                alpha: 0,
                ease: 'Linear',
                duration: 700
            })
            this.tweens.add({
                targets: transitionpanel,
                alpha: 0,
                ease: 'Linear',
                duration: 700
            })
            this.tweens.add({
                targets: transitiondress,
                alpha: 0,
                ease: 'Linear',
                duration: 700
            })
            this.tweens.add({
                targets: transitiontitle1,
                alpha: 0,
                ease: 'Linear',
                duration: 700
            })
            this.tweens.add({
                targets: transitiontitle2,
                alpha: 0,
                ease: 'Linear',
                duration: 700
            })
            this.tweens.add({
                targets: transitiontitle3,
                alpha: 0,
                ease: 'Linear',
                duration: 700
            })
            this.tweens.add({
                targets: transitionstar1,
                alpha: 0,
                ease: 'Linear',
                duration: 700
            })
            this.tweens.add({
                targets: transitionstar2,
                alpha: 0,
                ease: 'Linear',
                duration: 700
            })
            this.tweens.add({
                targets: transitionstar3,
                alpha: 0,
                ease: 'Linear',
                duration: 700
            })
            this.tweens.add({
                targets: transitionstar4,
                alpha: 0,
                ease: 'Linear',
                duration: 700
            })
            this.tweens.add({
                targets: transitionstar5,
                alpha: 0,
                ease: 'Linear',
                duration: 700
            })
        }
        level4clickstart()
        hitobject = this.add.image(400, 300, 'hitobject').setOrigin(0.5, 0.5).setInteractive({
            useHandCursor: true
        })
        hitobject.visible = false
        hitobject.on('pointerdown', hitobjectdown)

        function hitobjectdown() {}
        this.load.on('complete', function () {
            loadFinish = true
        });
        this.load.start();
    }
});

function level4clickstart() {
    for (i = 1; i <= 7; i++) {
        game['elash' + i].on('pointerover', elashoverclick1);
        game['elash' + i].on('pointerout', elashoutclick1);
        game['elash' + i].on('pointerdown', elashdownclick1);
        game['elash' + i].on('pointerup', elashupclick1);
    }

    function elashoverclick1() {
        game['elash' + this.texture.key.substr(5)].setScale(1.05, 1.05)
    }

    function elashoutclick1() {
        game['elash' + this.texture.key.substr(5)].setScale(1, 1)
    }

    function elashdownclick1() {
        if (music.isPlaying == false) {
            clicksound.stop()
        } else {
            clicksound.play()
        }
        game['elash' + this.texture.key.substr(5)].setScale(1, 1)
        done.visible = true
        sno = this.texture.key.substr(5)
        glitter.anims.play('glitter')
        if (marr4[0] == parseInt(sno)) {
            level4elash.setFrame(0)
            marr4[0] = 0
        } else {
            marr4[0] = parseInt(sno)
            level4elash.setFrame(parseInt(sno))
        }
    }

    function elashupclick1() {
        game['elash' + this.texture.key.substr(5)].setScale(1.05, 1.05)
    }
    for (i = 1; i <= 10; i++) {
        game['blush' + i].on('pointerover', blushoverclick1);
        game['blush' + i].on('pointerout', blushoutclick1);
        game['blush' + i].on('pointerdown', blushdownclick1);
        game['blush' + i].on('pointerup', blushupclick1);
    }

    function blushoverclick1() {
        game['blush' + this.texture.key.substr(5)].setScale(1.05, 1.05)
    }

    function blushoutclick1() {
        game['blush' + this.texture.key.substr(5)].setScale(1, 1)
    }

    function blushdownclick1() {
        if (music.isPlaying == false) {
            clicksound.stop()
        } else {
            clicksound.play()
        }
        game['blush' + this.texture.key.substr(5)].setScale(1, 1)
        done.visible = true
        sno = this.texture.key.substr(5)
        glitter.anims.play('glitter')
        if (marr4[1] == parseInt(sno)) {
            level4blush.setFrame(0)
            marr4[1] = 0
        } else {
            marr4[1] = parseInt(sno)
            level4blush.setFrame(parseInt(sno))
        }
    }

    function blushupclick1() {
        game['blush' + this.texture.key.substr(5)].setScale(1.05, 1.05)
    }
    for (i = 1; i <= 10; i++) {
        game['lip' + i].on('pointerover', lipoverclick1);
        game['lip' + i].on('pointerout', lipoutclick1);
        game['lip' + i].on('pointerdown', lipdownclick1);
        game['lip' + i].on('pointerup', lipupclick1);
    }

    function lipoverclick1() {
        game['lip' + this.texture.key.substr(3)].setScale(1.05, 1.05)
    }

    function lipoutclick1() {
        game['lip' + this.texture.key.substr(3)].setScale(1, 1)
    }

    function lipdownclick1() {
        if (music.isPlaying == false) {
            clicksound.stop()
        } else {
            clicksound.play()
        }
        game['lip' + this.texture.key.substr(3)].setScale(1, 1)
        done.visible = true
        sno = this.texture.key.substr(3)
        glitter.anims.play('glitter')
        if (marr4[2] == parseInt(sno)) {
            level4lips.setFrame(0)
            marr4[2] = 0
            ccount1 = 0
        } else {
            marr4[2] = parseInt(sno)
            level4lips.setFrame(parseInt(sno) * 3)
            ccount1 = marr4[2] * 3
        }
        level4lip.remove()
        level4lip = this.scene.time.addEvent({
            delay: 1000,
            callback: level4lipsstart1,
            callbackScope: this
        });

        function level4lipsstart1() {
            level4lips.setFrame(ccount1 + 1)
            level4lip = this.scene.time.addEvent({
                delay: 100,
                callback: level4lipsstart2,
                callbackScope: this
            });
        }

        function level4lipsstart2() {
            level4lips.setFrame(ccount1 + 2)
            level4lip = this.scene.time.addEvent({
                delay: 2500,
                callback: level4lipsstart3,
                callbackScope: this
            });
        }

        function level4lipsstart3() {
            level4lips.setFrame(ccount1 + 1)
            level4lip = this.scene.time.addEvent({
                delay: 100,
                callback: level4lipsstart4,
                callbackScope: this
            });
        }

        function level4lipsstart4() {
            level4lips.setFrame(ccount1)
            level4lip = this.scene.time.addEvent({
                delay: 3000,
                callback: level4lipsstart1,
                callbackScope: this
            });
        }
    }

    function lipupclick1() {
        game['lip' + this.texture.key.substr(3)].setScale(1.05, 1.05)
    }
    for (i = 1; i <= 10; i++) {
        game['eye' + i].on('pointerover', eyeoverclick1);
        game['eye' + i].on('pointerout', eyeoutclick1);
        game['eye' + i].on('pointerdown', eyedownclick1);
        game['eye' + i].on('pointerup', eyeupclick1);
    }

    function eyeoverclick1() {
        game['eye' + this.texture.key.substr(3)].setScale(1.05, 1.05)
    }

    function eyeoutclick1() {
        game['eye' + this.texture.key.substr(3)].setScale(1, 1)
    }

    function eyedownclick1() {
        if (music.isPlaying == false) {
            clicksound.stop()
        } else {
            clicksound.play()
        }
        game['eye' + this.texture.key.substr(3)].setScale(1, 1)
        done.visible = true
        sno = this.texture.key.substr(3)
        glitter.anims.play('glitter')
        if (marr4[3] == parseInt(sno)) {
            level4eye1.setFrame(0)
            level4eye2.setFrame(0)
            marr4[3] = 0
        } else {
            marr4[3] = parseInt(sno)
            level4eye1.setFrame(parseInt(sno))
            level4eye2.setFrame(parseInt(sno))
        }
    }

    function eyeupclick1() {
        game['eye' + this.texture.key.substr(3)].setScale(1.05, 1.05)
    }
    for (i = 1; i <= 10; i++) {
        game['eshade' + i].on('pointerover', eshadeoverclick1);
        game['eshade' + i].on('pointerout', eshadeoutclick1);
        game['eshade' + i].on('pointerdown', eshadedownclick1);
        game['eshade' + i].on('pointerup', eshadeupclick1);
    }

    function eshadeoverclick1() {
        game['eshade' + this.texture.key.substr(6)].setScale(1.05, 1.05)
    }

    function eshadeoutclick1() {
        game['eshade' + this.texture.key.substr(6)].setScale(1, 1)
    }

    function eshadedownclick1() {
        if (music.isPlaying == false) {
            clicksound.stop()
        } else {
            clicksound.play()
        }
        game['eshade' + this.texture.key.substr(6)].setScale(1, 1)
        done.visible = true
        sno = this.texture.key.substr(6)
        glitter.anims.play('glitter')
        if (marr4[4] == parseInt(sno)) {
            level4eshade.setFrame(0)
            marr4[4] = 0
            ccount = 0
        } else {
            marr4[4] = parseInt(sno)
            level4eshade.setFrame(parseInt(sno) * 2)
            ccount = marr4[4] * 2
        }
    }

    function eshadeupclick1() {
        game['eshade' + this.texture.key.substr(6)].setScale(1.05, 1.05)
    }
    for (i = 1; i <= 10; i++) {
        game['ebrow' + i].on('pointerover', ebrowoverclick1);
        game['ebrow' + i].on('pointerout', ebrowoutclick1);
        game['ebrow' + i].on('pointerdown', ebrowdownclick1);
        game['ebrow' + i].on('pointerup', ebrowupclick1);
    }

    function ebrowoverclick1() {
        game['ebrow' + this.texture.key.substr(5)].setScale(1.05, 1.05)
    }

    function ebrowoutclick1() {
        game['ebrow' + this.texture.key.substr(5)].setScale(1, 1)
    }

    function ebrowdownclick1() {
        if (music.isPlaying == false) {
            clicksound.stop()
        } else {
            clicksound.play()
        }
        game['ebrow' + this.texture.key.substr(5)].setScale(1, 1)
        done.visible = true
        sno = this.texture.key.substr(5)
        glitter.anims.play('glitter')
        if (marr4[5] == parseInt(sno)) {
            level4ebrow.setFrame(0)
            marr4[5] = 0
        } else {
            marr4[5] = parseInt(sno)
            level4ebrow.setFrame(parseInt(sno))
        }
    }

    function ebrowupclick1() {
        game['ebrow' + this.texture.key.substr(5)].setScale(1.05, 1.05)
    }
    // for (i = 1; i <= 5; i++) {
    //     game['rapunhair' + i].on('pointerover', hairoverclick1);
    //     game['rapunhair' + i].on('pointerout', hairoutclick1);
    //     game['rapunhair' + i].on('pointerdown', hairdownclick1);
    //     game['rapunhair' + i].on('pointerup', hairupclick1);
    // }

    // function hairoverclick1() {
    //     game['rapunhair' + this.texture.key.substr(9)].setScale(1.05, 1.05)
    // }

    // function hairoutclick1() {
    //     game['rapunhair' + this.texture.key.substr(9)].setScale(1, 1)
    // }

    // function hairdownclick1() {
    //     if (music.isPlaying == false) {
    //         clicksound.stop()
    //     } else {
    //         clicksound.play()
    //     }
    //     game['rapunhair' + this.texture.key.substr(9)].setScale(1, 1)
    //     done.visible = true
    //     sno = this.texture.key.substr(9)
    //     glitter.anims.play('glitter')
    //     if (marr4[6] == parseInt(sno)) {
    //         level4hair.setFrame(0)
    //         level4bhair.setFrame(0)
    //         marr4[6] = 0
    //     } else {
    //         marr4[6] = parseInt(sno)
    //         level4hair.setFrame(parseInt(sno))
    //         level4bhair.setFrame(parseInt(sno))
    //     }
    // }

    // function hairupclick1() {
    //     game['rapunhair' + this.texture.key.substr(9)].setScale(1.05, 1.05)
    // }
    // rapunhairbtn.on('pointerover', rapunhairbtnoverclick1);
    // rapunhairbtn.on('pointerout', rapunhairbtnoutclick1);
    // rapunhairbtn.on('pointerdown', rapunhairbtndownclick1);
    // rapunhairbtn.on('pointerup', rapunhairbtnupclick1);

    // function rapunhairbtnoverclick1() {
    //     rapunhairbtn.setScale(1.05, 1.05)
    // }

    // function rapunhairbtnoutclick1() {
    //     rapunhairbtn.setScale(1, 1)
    // }

    // function rapunhairbtndownclick1() {
    //     if (music.isPlaying == false) {
    //         clicksound.stop()
    //     } else {
    //         clicksound.play()
    //     }
    //     rapunhairbtn.setScale(1, 1)
    //     if (!makestart1) {
    //         makestart1 = true
    //         this.scene.tweens.add({
    //             targets: level1panelgroup,
    //             x: 0,
    //             ease: 'Linear',
    //             duration: 300,
    //         });
    //         for (i = 1; i <= 7; i++) {
    //             game['elash' + i].visible = false
    //         }
    //         for (i = 1; i <= 10; i++) {
    //             game['blush' + i].visible = false
    //             game['lip' + i].visible = false
    //             game['eye' + i].visible = false
    //             game['eshade' + i].visible = false
    //             game['ebrow' + i].visible = false
    //         }
    //         for (i = 1; i <= 5; i++) {
    //             game['rapunhair' + i].visible = true
    //         }
    //     } else {
    //         if (!game['rapunhair' + 1].visible) {
    //             this.scene.tweens.add({
    //                 targets: level1panelgroup,
    //                 x: -500,
    //                 ease: 'Sine.easeIn',
    //                 duration: 300,
    //                 onComplete: levelpanel1move
    //             });

    //             function levelpanel1move() {
    //                 for (i = 1; i <= 7; i++) {
    //                     game['elash' + i].visible = false
    //                 }
    //                 for (i = 1; i <= 10; i++) {
    //                     game['blush' + i].visible = false
    //                     game['lip' + i].visible = false
    //                     game['eye' + i].visible = false
    //                     game['eshade' + i].visible = false
    //                     game['ebrow' + i].visible = false
    //                 }
    //                 this.parent.scene.tweens.add({
    //                     targets: level1panelgroup,
    //                     x: 0,
    //                     ease: 'Sine.easeOut',
    //                     duration: 300,
    //                 });
    //                 for (i = 1; i <= 5; i++) {
    //                     game['rapunhair' + i].visible = true
    //                 }
    //             }
    //         }
    //     }
    // }

    // function rapunhairbtnupclick1() {
    //     rapunhairbtn.setScale(1.05, 1.05)
    // }
    for (i = 1; i <= 6; i++) {
        game['category' + i].on('pointerover', categoryoverclick1);
        game['category' + i].on('pointerout', categoryoutclick1);
        game['category' + i].on('pointerdown', categorydownclick1);
        game['category' + i].on('pointerup', categoryupclick1);
    }

    function categoryoverclick1() {
        game['category' + this.texture.key.substr(8)].setScale(1.05, 1.05)
    }

    function categoryoutclick1() {
        game['category' + this.texture.key.substr(8)].setScale(1, 1)
    }

    function categoryupclick1() {
        game['category' + this.texture.key.substr(8)].setScale(1.05, 1.05)
    }

    function categorydownclick1() {
        if (music.isPlaying == false) {
            clicksound.stop()
        } else {
            clicksound.play()
        }
        sno = this.texture.key.substr(8)
        game['category' + parseInt(sno)].setScale(1, 1)
        if (!makestart1) {
            makestart1 = true
            this.scene.tweens.add({
                targets: level1panelgroup,
                x: 0,
                ease: 'Linear',
                duration: 300,
            });
            for (i = 1; i <= 7; i++) {
                game['elash' + i].visible = false
            }
            for (i = 1; i <= 10; i++) {
                game['blush' + i].visible = false
                game['lip' + i].visible = false
                game['eye' + i].visible = false
                game['eshade' + i].visible = false
                game['ebrow' + i].visible = false
            }
            for (i = 1; i <= 5; i++) {
                game['rapunhair' + i].visible = false
            }
            if (parseInt(sno) == 1) {
                for (i = 1; i <= 7; i++) {
                    game['elash' + i].visible = true
                }
            } else if (parseInt(sno) == 2) {
                for (i = 1; i <= 10; i++) {
                    game['blush' + i].visible = true
                }
            } else if (parseInt(sno) == 3) {
                for (i = 1; i <= 10; i++) {
                    game['lip' + i].visible = true
                }
            } else if (parseInt(sno) == 4) {
                for (i = 1; i <= 10; i++) {
                    game['eye' + i].visible = true
                }
            } else if (parseInt(sno) == 5) {
                for (i = 1; i <= 10; i++) {
                    game['eshade' + i].visible = true
                }
            } else if (parseInt(sno) == 6) {
                for (i = 1; i <= 10; i++) {
                    game['ebrow' + i].visible = true
                }
            }
        } else {
            if (parseInt(sno) == 1 && !game['elash' + 1].visible) {
                this.scene.tweens.add({
                    targets: level1panelgroup,
                    x: -500,
                    ease: 'Linear',
                    duration: 300,
                    onComplete: levelpanel1move
                });

                function levelpanel1move() {
                    for (i = 1; i <= 7; i++) {
                        game['elash' + i].visible = false
                    }
                    for (i = 1; i <= 10; i++) {
                        game['blush' + i].visible = false
                        game['lip' + i].visible = false
                        game['eye' + i].visible = false
                        game['eshade' + i].visible = false
                        game['ebrow' + i].visible = false
                    }
                    for (i = 1; i <= 5; i++) {
                        game['rapunhair' + i].visible = false
                    }
                    this.parent.scene.tweens.add({
                        targets: level1panelgroup,
                        x: 0,
                        ease: 'Linear',
                        duration: 300,
                    });
                    for (i = 1; i <= 7; i++) {
                        game['elash' + i].visible = true
                    }
                }
            } else if (parseInt(sno) == 2 && !game['blush' + 1].visible) {
                this.scene.tweens.add({
                    targets: level1panelgroup,
                    x: -500,
                    ease: 'Linear',
                    duration: 300,
                    onComplete: levelpanel1move
                });

                function levelpanel1move() {
                    for (i = 1; i <= 7; i++) {
                        game['elash' + i].visible = false
                    }
                    for (i = 1; i <= 10; i++) {
                        game['blush' + i].visible = false
                        game['lip' + i].visible = false
                        game['eye' + i].visible = false
                        game['eshade' + i].visible = false
                        game['ebrow' + i].visible = false
                    }
                    for (i = 1; i <= 5; i++) {
                        game['rapunhair' + i].visible = false
                    }
                    this.parent.scene.tweens.add({
                        targets: level1panelgroup,
                        x: 0,
                        ease: 'Linear',
                        duration: 300,
                    });
                    for (i = 1; i <= 10; i++) {
                        game['blush' + i].visible = true
                    }
                }
            } else if (parseInt(sno) == 3 && !game['lip' + 1].visible) {
                this.scene.tweens.add({
                    targets: level1panelgroup,
                    x: -500,
                    ease: 'Linear',
                    duration: 300,
                    onComplete: levelpanel1move
                });

                function levelpanel1move() {
                    for (i = 1; i <= 7; i++) {
                        game['elash' + i].visible = false
                    }
                    for (i = 1; i <= 10; i++) {
                        game['blush' + i].visible = false
                        game['lip' + i].visible = false
                        game['eye' + i].visible = false
                        game['eshade' + i].visible = false
                        game['ebrow' + i].visible = false
                    }
                    for (i = 1; i <= 5; i++) {
                        game['rapunhair' + i].visible = false
                    }
                    this.parent.scene.tweens.add({
                        targets: level1panelgroup,
                        x: 0,
                        ease: 'Linear',
                        duration: 300,
                    });
                    for (i = 1; i <= 10; i++) {
                        game['lip' + i].visible = true
                    }
                }
            } else if (parseInt(sno) == 4 && !game['eye' + 1].visible) {
                this.scene.tweens.add({
                    targets: level1panelgroup,
                    x: -500,
                    ease: 'Linear',
                    duration: 300,
                    onComplete: levelpanel1move
                });

                function levelpanel1move() {
                    for (i = 1; i <= 7; i++) {
                        game['elash' + i].visible = false
                    }
                    for (i = 1; i <= 10; i++) {
                        game['blush' + i].visible = false
                        game['lip' + i].visible = false
                        game['eye' + i].visible = false
                        game['eshade' + i].visible = false
                        game['ebrow' + i].visible = false
                    }
                    for (i = 1; i <= 5; i++) {
                        game['rapunhair' + i].visible = false
                    }
                    this.parent.scene.tweens.add({
                        targets: level1panelgroup,
                        x: 0,
                        ease: 'Linear',
                        duration: 300,
                    });
                    for (i = 1; i <= 10; i++) {
                        game['eye' + i].visible = true
                    }
                }
            } else if (parseInt(sno) == 5 && !game['eshade' + 1].visible) {
                this.scene.tweens.add({
                    targets: level1panelgroup,
                    x: -500,
                    ease: 'Linear',
                    duration: 300,
                    onComplete: levelpanel1move
                });

                function levelpanel1move() {
                    for (i = 1; i <= 7; i++) {
                        game['elash' + i].visible = false
                    }
                    for (i = 1; i <= 10; i++) {
                        game['blush' + i].visible = false
                        game['lip' + i].visible = false
                        game['eye' + i].visible = false
                        game['eshade' + i].visible = false
                        game['ebrow' + i].visible = false
                    }
                    for (i = 1; i <= 5; i++) {
                        game['rapunhair' + i].visible = false
                    }
                    this.parent.scene.tweens.add({
                        targets: level1panelgroup,
                        x: 0,
                        ease: 'Linear',
                        duration: 300,
                    });
                    for (i = 1; i <= 10; i++) {
                        game['eshade' + i].visible = true
                    }
                }
            } else if (parseInt(sno) == 6 && !game['ebrow' + 1].visible) {
                this.scene.tweens.add({
                    targets: level1panelgroup,
                    x: -500,
                    ease: 'Linear',
                    duration: 300,
                    onComplete: levelpanel1move
                });

                function levelpanel1move() {
                    for (i = 1; i <= 7; i++) {
                        game['elash' + i].visible = false
                    }
                    for (i = 1; i <= 10; i++) {
                        game['blush' + i].visible = false
                        game['lip' + i].visible = false
                        game['eye' + i].visible = false
                        game['eshade' + i].visible = false
                        game['ebrow' + i].visible = false
                    }
                    for (i = 1; i <= 5; i++) {
                        game['rapunhair' + i].visible = false
                    }
                    this.parent.scene.tweens.add({
                        targets: level1panelgroup,
                        x: 0,
                        ease: 'Linear',
                        duration: 300,
                    });
                    for (i = 1; i <= 10; i++) {
                        game['ebrow' + i].visible = true
                    }
                }
            }
        }
    }
}
var startgame7 = false
var handindication = false
var colorindication = false
var elsaclickstart = false
var elsastorearr = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ]
var elsaclrarr1 = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
var elsaclrarr2 = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
var elsaovrarr = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
var elsadresarr1 = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
var handstart = false
var elsacolor = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
var darr1 = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
var colorcode = [, ' 0xff3838', ' 0xfe7d3b', ' 0xffe33b', ' 0x9cdc3b', ' 0x3ba13b', ' 0x3bd8c8', ' 0x3bdcff', ' 0x3b91ed', ' 0x3f3ce6', ' 0x7443f6', ' 0xfeb463', ' 0x9b9b9b', ' 0xb04ab1', ' 0xff8be6', '0xed3b8a', ' 0xfff2f2', ' 0x3b2e2e']
var Level5 = new Phaser.Class({
    Extends: Phaser.Scene,
    initialize: function Level5() {
        Phaser.Scene.call(this, {
            key: 'Level5'
        });
    },
    preload: function () {
        colorcode = [, ' 0xff3838', ' 0xfe7d3b', ' 0xffe33b', ' 0x9cdc3b', ' 0x3ba13b', ' 0x3bd8c8', ' 0x3bdcff', ' 0x3b91ed', ' 0x3f3ce6', ' 0x7443f6', ' 0xfeb463', ' 0x9b9b9b', ' 0xb04ab1', ' 0xff8be6', '0xed3b8a', ' 0xfff2f2', ' 0x3b2e2e']
        darr1 = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        elsacolor = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        startgame7 = false
        loadFinish = false
        elsaclickstart = false
        handstart = false
        clrarr = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        elsaclrarr1 = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        elsaclrarr2 = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        elsaovrarr = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        elsadresarr1 = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        elsastorearr = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ]
        handindication = false
        colorindication = false
    },
    create: function () {
        elsabackground1 = this.add.image(400, 300, 'elsabackground1').setOrigin(0.5, 0.5)
        dressuppanel = this.add.image(26, 40, 'dressuppanel').setOrigin(0, 0)
        colortext = this.add.image(83, 302, 'colortext').setOrigin(0, 0)
        tailorbtn = this.add.image(81, 136, 'tailorbtn').setOrigin(0.5, 0.5).setInteractive({
            pixelPerfect: true,
            useHandCursor: true,
        })
        tailorbtn.x += parseFloat(tailorbtn.width / 2)
        tailorbtn.y += parseFloat(tailorbtn.height / 2)
        cutbtn = this.add.image(150, 221, 'cutbtn').setOrigin(0.5, 0.5).setInteractive({
            pixelPerfect: true,
            useHandCursor: true,
        })
        cutbtn.x += parseFloat(cutbtn.width / 2)
        cutbtn.y += parseFloat(cutbtn.height / 2)
        var coloxarr = [, 80, 130, 178, 228, 277, 328, 79, 129, 178, 228, 276, 326, 128, 179, 229, 279, 327]
        var coloyarr = [, 369, 370, 370, 370, 370, 370, 424, 424, 424, 424, 424, 424, 477, 477, 477, 477, 477]
        for (i = 1; i <= 17; i++) {
            game['color' + i] = this.add.image(coloxarr[i], coloyarr[i], 'color' + i).setOrigin(0.5, 0.5).setInteractive({
                pixelPerfect: true,
                useHandCursor: true,
            })
            game['color' + i].x += parseFloat(game['color' + i].width / 2)
            game['color' + i].y += parseFloat(game['color' + i].height / 2)
        }
        dressphotoframe = this.add.image(251, 89, 'dressphotoframe').setOrigin(0, 0)
        rarrow = this.add.image(412, 143, 'arrow').setOrigin(0, 0).setInteractive({
            pixelPerfect: true,
            useHandCursor: true,
        })
        larrow = this.add.image(262, 147, 'arrow').setOrigin(0, 0).setInteractive({
            pixelPerfect: true,
            useHandCursor: true,
        })
        larrow.setScale(-1, 1)
        larrow.angle = -7
        this.tweens.add({
            targets: rarrow,
            x: 416,
            ease: 'Linear',
            duration: 400,
            repeat: -1,
            yoyo: true,
        })
        this.tweens.add({
            targets: larrow,
            x: 258,
            ease: 'Linear',
            duration: 400,
            repeat: -1,
            yoyo: true,
        })
        doll = this.add.image(275, 72, 'doll').setOrigin(0, 0)
        elsabody = this.add.image(101, 37, 'elsabody').setOrigin(0, 0)
        elsalips = this.add.image(193, 113, 'elsalips').setOrigin(0, 0)
        elsaeye1 = this.add.image(192, 90, 'elsaeye1').setOrigin(0, 0)
        elsaeye2 = this.add.image(192, 90, 'elsaeye2').setOrigin(0, 0)
        elsatoplayer = this.add.image(185, 85, 'elsatoplayer').setOrigin(0, 0)
        elsaeshade = this.add.image(184, 87, 'elsaeshade').setOrigin(0, 0)
        elsaelash = this.add.image(185, 86, 'elsaelash').setOrigin(0, 0)
        elsablush = this.add.image(188, 101, 'elsablush').setOrigin(0, 0)
        elsaebrow = this.add.image(185, 76, 'elsaebrow').setOrigin(0, 0)
        elsasegment1dress1 = this.add.image(181, 147, 'elsasegment1dress1').setOrigin(0, 0)
        elsasegment2dress2 = this.add.image(188, 215, 'elsasegment2dress2').setOrigin(0, 0)
        elsasegment3dress3 = this.add.image(143, 233, 'elsasegment3dress3').setOrigin(0, 0)
        elsasegment4dress4 = this.add.image(144, 352, 'elsasegment4dress4').setOrigin(0, 0)
        elsasegment5dress5 = this.add.image(131, 443, 'elsasegment5dress5').setOrigin(0, 0)
        elsasegment6dress6 = this.add.image(121, 264, 'elsasegment6dress6').setOrigin(0, 0)
        elsasegment7dress7 = this.add.image(95, 341, 'elsasegment7dress7').setOrigin(0, 0)
        elsasegment8dress8 = this.add.image(74, 427, 'elsasegment8dress8').setOrigin(0, 0)
        elsasegment9dress9 = this.add.image(245, 269, 'elsasegment9dress9').setOrigin(0, 0)
        elsasegment10dress10 = this.add.image(254, 343, 'elsasegment10dress10').setOrigin(0, 0)
        elsasegment11dress11 = this.add.image(272, 431, 'elsasegment11dress11').setOrigin(0, 0)
        elsahair = this.add.image(139, 12, 'elsahair').setOrigin(0, 0)
        elsaline = this.add.image(45, 160, 'elsaline').setOrigin(0, 0)
        elsaline.visible = false
        elsaelash.setFrame(marr1[0])
        elsablush.setFrame(marr1[1])
        elsalips.setFrame(marr1[2])
        elsaeye1.setFrame(marr1[3])
        elsaeye2.setFrame(marr1[3])
        elsaeshade.setFrame(marr1[4])
        elsaebrow.setFrame(marr1[5])
        elsahair.setFrame(marr1[6])
        var elsadollgroup = this.add.container(0, 0);
        elsadollgroup.add(elsasegment11dress11)
        elsadollgroup.add(elsasegment10dress10)
        elsadollgroup.add(elsasegment9dress9)
        elsadollgroup.add(elsasegment8dress8)
        elsadollgroup.add(elsasegment7dress7)
        elsadollgroup.add(elsasegment6dress6)
        elsadollgroup.add(elsabody)
        elsadollgroup.add(elsalips)
        elsadollgroup.add(elsaeye1)
        elsadollgroup.add(elsaeye2)
        elsadollgroup.add(elsatoplayer)
        elsadollgroup.add(elsaeshade)
        elsadollgroup.add(elsaelash)
        elsadollgroup.add(elsablush)
        elsadollgroup.add(elsaebrow)
        elsadollgroup.add(elsasegment2dress2)
        elsadollgroup.add(elsasegment1dress1)
        elsadollgroup.add(elsasegment5dress5)
        elsadollgroup.add(elsasegment4dress4)
        elsadollgroup.add(elsasegment3dress3)
        elsadollgroup.add(elsahair)
        elsadollgroup.add(elsaline)
        elsadollgroup.x = 400
        elsadollgroup.y = 20
        elsasegment1dress1.setFrame(1)
        elsasegment1dress1.alpha = 0
        elsahit1 = this.add.image(624.95, 220, 'elsahit1').setOrigin(0.5, 0.5).setInteractive({
            pixelperfect: true,
            useHandCursor: true
        })
        elsahit2 = this.add.image(625.95, 258, 'elsahit1').setOrigin(0.5, 0.5).setInteractive({
            pixelperfect: true,
            useHandCursor: true
        })
        elsahit2.setScale(0.64)
        elsahit3 = this.add.image(617.95, 337, 'elsahit1').setOrigin(0.5, 0.5).setInteractive({
            pixelperfect: true,
            useHandCursor: true
        })
        elsahit3.setScale(2.02)
        elsahit3.angle = 90
        elsahit4 = this.add.image(624.95, 453, 'elsahit1').setOrigin(0.5, 0.5).setInteractive({
            pixelperfect: true,
            useHandCursor: true
        })
        elsahit4.setScale(1.86)
        elsahit5 = this.add.image(627.95, 552, 'elsahit1').setOrigin(0.5, 0.5).setInteractive({
            pixelperfect: true,
            useHandCursor: true
        })
        elsahit5.setScale(2.25)
        elsahit6 = this.add.image(550.15, 342, 'elsahit1').setOrigin(0.5, 0.5).setInteractive({
            pixelperfect: true,
            useHandCursor: true
        })
        elsahit6.setScale(0.87)
        elsahit6.angle = 108
        elsahit7 = this.add.image(528.05, 425, 'elsahit1').setOrigin(0.5, 0.5).setInteractive({
            pixelperfect: true,
            useHandCursor: true
        })
        elsahit7.setScale(1.2)
        elsahit7.angle = 108
        elsahit8 = this.add.image(506.95, 519.7, 'elsahit1').setOrigin(0.5, 0.5).setInteractive({
            pixelperfect: true,
            useHandCursor: true
        })
        elsahit8.setScale(1.29)
        elsahit8.angle = 101
        elsahit9 = this.add.image(694.05, 345, 'elsahit1').setOrigin(0.5, 0.5).setInteractive({
            pixelperfect: true,
            useHandCursor: true
        })
        elsahit9.setScale(1.02)
        elsahit9.angle = 80
        elsahit10 = this.add.image(712.05, 425, 'elsahit1').setOrigin(0.5, 0.5).setInteractive({
            pixelperfect: true,
            useHandCursor: true
        })
        elsahit10.setScale(1.2)
        elsahit10.angle = 73
        elsahit11 = this.add.image(748.95, 510.7, 'elsahit1').setOrigin(0.5, 0.5).setInteractive({
            pixelperfect: true,
            useHandCursor: true
        })
        elsahit11.setScale(1.29)
        elsahit11.angle = 63
        var tween1 = this.tweens.add({
            targets: elsaline,
            alpha: 0.3,
            ease: 'Linear',
            duration: 1000,
            repeat: -1,
            yoyo: true,
        });
        var tween1 = this.tweens.add({
            targets: elsaeye1,
            x: elsaeye1.x + 2,
            ease: 'Linear',
            duration: 300,
            delay: 2400,
            callbackScope: this,
        });
        var tween2 = this.tweens.add({
            targets: elsaeye2,
            x: elsaeye2.x + 2,
            ease: 'Linear',
            duration: 300,
            delay: 2400,
            onComplete: elsaeyestartani1,
            callbackScope: this
        });

        function elsaeyestartani1() {
            this.tweens.add({
                targets: elsaeye1,
                x: elsaeye1.x - 2,
                ease: 'Linear',
                duration: 300,
                delay: 3000,
                callbackScope: this
            });
            this.tweens.add({
                targets: elsaeye2,
                x: elsaeye2.x - 2,
                ease: 'Linear',
                duration: 300,
                delay: 3000,
                onComplete: elsaeyestartani2,
                callbackScope: this,
            })
        }

        function elsaeyestartani2() {
            this.tweens.add({
                targets: elsaeye1,
                x: elsaeye1.x + 2,
                ease: 'Linear',
                duration: 300,
                delay: 2400,
                callbackScope: this
            });
            this.tweens.add({
                targets: elsaeye2,
                x: elsaeye2.x + 2,
                ease: 'Linear',
                duration: 300,
                delay: 2400,
                onComplete: elsaeyestartani1,
                callbackScope: this,
            })
        }
        elsaeshad = this.time.addEvent({
            delay: 1000,
            callback: elsaeblinkstart1,
            callbackScope: this
        });

        function elsaeblinkstart1() {
            elsaeshade.setFrame(marr1[4] + 1)
            elsaelash.visible = false
            elsaeshad = this.time.addEvent({
                delay: 200,
                callback: elsaeblinkstart2,
                callbackScope: this
            });
        }

        function elsaeblinkstart2() {
            elsaeshade.setFrame(marr1[4])
            elsaelash.visible = true
            elsaeshad = this.time.addEvent({
                delay: 4000,
                callback: elsaeblinkstart1,
                callbackScope: this
            });
        }
        elsalip = this.time.addEvent({
            delay: 1000,
            callback: elsalipsstart1,
            callbackScope: this
        });

        function elsalipsstart1() {
            elsalips.setFrame(marr1[2] + 1)
            elsalip = this.time.addEvent({
                delay: 100,
                callback: elsalipsstart2,
                callbackScope: this
            });
        }

        function elsalipsstart2() {
            elsalips.setFrame(marr1[2] + 2)
            elsalip = this.time.addEvent({
                delay: 2500,
                callback: elsalipsstart3,
                callbackScope: this
            });
        }

        function elsalipsstart3() {
            elsalips.setFrame(marr1[2] + 1)
            elsalip = this.time.addEvent({
                delay: 100,
                callback: elsalipsstart4,
                callbackScope: this
            });
        }

        function elsalipsstart4() {
            elsalips.setFrame(marr1[2])
            elsalip = this.time.addEvent({
                delay: 3000,
                callback: elsalipsstart1,
                callbackScope: this
            });
        }
        handindication1 = this.add.sprite(139, 110, 'uiButton').setFrame("hand")
        handindication1.setScale(0.8)
        handindication1.angle = 180
        handindication2 = this.add.sprite(239, 310, 'uiButton').setFrame("hand")
        handindication2.setScale(0.8)
        handindication2.angle = 180
        handindication3 = this.add.sprite(639, 510, 'uiButton').setFrame("hand")
        handindication3.setScale(0.8)
        handindication4 = this.add.sprite(139, 290, 'uiButton').setFrame("hand")
        handindication4.setScale(0.8)
        handindication4.angle = 90
        this.tweens.add({
            targets: handindication1,
            y: handindication1.y + 10,
            ease: 'Linear',
            duration: 300,
            repeat: -1,
            yoyo: true
        })
        this.tweens.add({
            targets: handindication2,
            y: handindication2.y + 10,
            ease: 'Linear',
            duration: 300,
            repeat: -1,
            yoyo: true
        })
        handindication1.visible = false
        handindication2.visible = false
        this.tweens.add({
            targets: handindication3,
            y: handindication3.y + 10,
            ease: 'Linear',
            duration: 300,
            repeat: -1,
            yoyo: true
        })
        handindication3.visible = false
        this.tweens.add({
            targets: handindication4,
            x: handindication4.x + 10,
            ease: 'Linear',
            duration: 300,
            repeat: -1,
            yoyo: true
        })
        handindication4.visible = false
        clicksound = this.sound.add('clickss');
        done = this.add.image(790, 590, 'uiButton').setOrigin(1, 1).setInteractive({
            pixelPerfect: true,
            useHandCursor: true
        })
        done.setFrame("button_next")
        done.visible = false
        done.on('pointerover', doneoverclick1);
        done.on('pointerout', doneoutclick1);
        done.on('pointerdown', donedownclick1);
        done.on('pointerup', doneupclick1);

        function doneoverclick1() {
            done.setScale(1.05, 1.05)
        }

        function doneoutclick1() {
            done.setScale(1, 1)
        }

        function doneupclick1() {
            if (startgame7) {
                done.setScale(1.05, 1.05)
            }
        }

        function donedownclick1() {
            if (!startgame7 && loadFinish) {
                startgame7 = true
                level = 3
                if (music.isPlaying == false) {
                    clicksound.stop()
                } else {
                    clicksound.play()
                }
                done.setScale(1, 1)
                elsastorearr[0] = elsasegment11dress11.frame.name
                elsastorearr[1] = elsasegment10dress10.frame.name
                elsastorearr[2] = elsasegment9dress9.frame.name
                elsastorearr[3] = elsasegment8dress8.frame.name
                elsastorearr[4] = elsasegment7dress7.frame.name
                elsastorearr[5] = elsasegment6dress6.frame.name
                elsastorearr[6] = elsasegment1dress1.frame.name
                elsastorearr[7] = elsasegment5dress5.frame.name
                elsastorearr[8] = elsasegment4dress4.frame.name
                elsastorearr[9] = elsasegment3dress3.frame.name
                elsastorearr[10] = elsasegment2dress2.frame.name
                elsastorearr[11] = elsasegment11dress11.alpha
                elsastorearr[12] = elsasegment10dress10.alpha
                elsastorearr[13] = elsasegment9dress9.alpha
                elsastorearr[14] = elsasegment8dress8.alpha
                elsastorearr[15] = elsasegment7dress7.alpha
                elsastorearr[16] = elsasegment6dress6.alpha
                elsastorearr[17] = elsasegment1dress1.alpha
                elsastorearr[18] = elsasegment5dress5.alpha
                elsastorearr[19] = elsasegment4dress4.alpha
                elsastorearr[20] = elsasegment3dress3.alpha
                elsastorearr[21] = elsasegment2dress2.alpha
                transitionbackground1.alpha = 1
                transitionbackground2.alpha = 1
                transitionbackground3.alpha = 1
                transitionpanel.alpha = 1
                transitiondress.alpha = 1
                transitiontitle1.alpha = 1
                transitiontitle2.alpha = 1
                transitiontitle3.alpha = 1
                transitionstar1.alpha = 1
                transitionstar2.alpha = 1
                transitionstar3.alpha = 1
                transitionstar4.alpha = 1
                transitionstar5.alpha = 1
                transitionstar1.setScale(0)
                transitionstar2.setScale(0)
                transitionstar3.setScale(0)
                transitionstar4.setScale(0)
                transitionstar5.setScale(0)
                transitionbackground1.visible = true
                transitionbackground2.visible = true
                transitionbackground3.visible = true
                transitiondress.visible = false
                transitionpanel.visible = false
                transitiontitle1.visible = false
                transitiontitle2.visible = false
                transitiontitle3.visible = false
                transitionstar1.visible = false
                transitionstar2.visible = false
                transitionstar3.visible = false
                transitionstar4.visible = false
                transitionstar5.visible = false
                transitionbackground1.alpha = 0
                transitionbackground2.alpha = 0
                transitionbackground3.alpha = 0
                this.scene.tweens.add({
                    targets: transitionbackground1,
                    alpha: 1,
                    ease: 'Linear',
                    duration: 800,
                    onComplete: transitionstart1,
                    callbackScope: this
                })

                function transitionstart1() {
                    transitiondress.visible = true
                    transitiondress.alpha = 0
                    this.scene.tweens.add({
                        targets: transitiondress,
                        alpha: 1,
                        ease: 'Linear',
                        duration: 400,
                        onComplete: transitionstart2,
                        callbackScope: this
                    })
                    this.scene.tweens.add({
                        targets: transitionbackground2,
                        alpha: 1,
                        ease: 'Linear',
                        duration: 800,
                    })
                }

                function transitionstart2() {
                    transitionpanel.visible = true
                    transitionpanel.setScale(0)
                    this.scene.tweens.add({
                        targets: transitionpanel,
                        scaleX: 1,
                        scaleY: 1,
                        ease: 'Linear',
                        duration: 500,
                        onComplete: transitionstart3,
                        callbackScope: this
                    })
                }

                function transitionstart3() {
                    this.scene.tweens.add({
                        targets: transitionbackground3,
                        alpha: 1,
                        ease: 'Linear',
                        duration: 800,
                        onComplete: transitionstart4,
                        callbackScope: this
                    })
                }

                function transitionstart4() {
                    transitiontitle1.visible = true
                    transitiontitle2.visible = true
                    transitiontitle3.visible = true
                    transitiontitle1.alpha = 0
                    transitiontitle2.alpha = 0
                    transitiontitle3.alpha = 0
                    transitiontitle2.x = transitiontitle2.x + 100
                    transitiontitle3.x = transitiontitle3.x - 100
                    this.scene.tweens.add({
                        targets: transitiontitle1,
                        alpha: 1,
                        ease: 'Linear',
                        duration: 800,
                        onComplete: transitionstart5,
                        callbackScope: this
                    })
                    this.scene.tweens.add({
                        targets: transitiontitle2,
                        alpha: 1,
                        x: transitiontitle2.x - 100,
                        ease: 'Linear',
                        duration: 800,
                    })
                    this.scene.tweens.add({
                        targets: transitiontitle3,
                        alpha: 1,
                        x: transitiontitle3.x + 100,
                        ease: 'Linear',
                        duration: 800,
                    })
                }

                function transitionstart5() {
                    transitionstar1.setScale(0)
                    transitionstar2.setScale(0)
                    transitionstar3.setScale(0)
                    transitionstar4.setScale(0)
                    transitionstar5.setScale(0)
                    transitionstar1.visible = true
                    transitionstar2.visible = true
                    transitionstar3.visible = true
                    transitionstar4.visible = true
                    transitionstar5.visible = true
                    this.scene.tweens.add({
                        targets: transitionstar1,
                        scaleX: 1,
                        scaleY: 1,
                        ease: 'Linear',
                        duration: 300,
                    })
                    this.scene.tweens.add({
                        targets: transitionstar2,
                        scaleX: 0.9,
                        scaleY: 0.9,
                        ease: 'Linear',
                        duration: 300,
                    })
                    this.scene.tweens.add({
                        targets: transitionstar3,
                        scaleX: 0.9,
                        scaleY: 0.9,
                        ease: 'Linear',
                        duration: 300,
                    })
                    this.scene.tweens.add({
                        targets: transitionstar4,
                        scaleX: 0.8,
                        scaleY: 0.8,
                        ease: 'Linear',
                        duration: 300,
                    })
                    this.scene.tweens.add({
                        targets: transitionstar5,
                        scaleX: 0.65,
                        scaleY: 0.65,
                        ease: 'Linear',
                        duration: 300,
                        onComplete: transitionstar6,
                        callbackScope: this
                    })
                }

                function transitionstar6() {
                    this.scene.scene.stop('Level5')
                    game.scene.run('levelSelect');
                }
            }
        }
        mute = this.add.sprite(790, 10, 'uiButton').setFrame("button_music_on").setInteractive({
            pixelPerfect: true,
            useHandCursor: true
        })
        mute.setOrigin(1, 0)
        mute.on('pointerdown', function () {
            if (!isMuted) {
                isMuted = true
                music.pause()
                mute.setFrame("button_music_off")
            } else {
                isMuted = false
                music.resume()
                mute.setFrame("button_music_on")
            }
            var tween2 = this.tweens.add({
                targets: mute,
                scaleX: 0.9,
                scaleY: 0.9,
                ease: 'Linear',
                duration: 80,
                yoyo: true,
                repeat: 1,
            });
            if (music.isPlaying == false) {
                clicksound.stop()
            } else {
                clicksound.play()
            }
        }, this);
        if (music.isPlaying == false) {
            mute.setFrame("button_music_off")
        } else {
            mute.setFrame("button_music_on")
        }
        if (brandingMaterials.inGameLogo.display == true && brandingMaterials.inGameLogo.image != undefined) {
            inGameLogo1 = this.add.image(10, 590, 'inGameLogo1').setOrigin(0, 1).setInteractive({
                pixelPerfect: true,
                useHandCursor: true
            })
            inGameLogo1.on('pointerdown', brandingMaterials.inGameLogo.clickURL);
            inGameLogoani = this.time.addEvent({
                delay: 2000,
                callback: inGameLogoani,
                callbackScope: this
            });

            function inGameLogoani() {
                this.tweens.add({
                    targets: inGameLogo1,
                    scaleX: 1.05,
                    scaleY: 1.05,
                    ease: 'Linear',
                    duration: 100,
                    repeat: 1,
                    yoyo: true,
                    delay: 3000,
                    onComplete: inGameLogoani1,
                    callbackScope: this
                })
            }

            function inGameLogoani1() {
                this.tweens.add({
                    targets: inGameLogo1,
                    scaleX: 1.05,
                    scaleY: 1.05,
                    ease: 'Linear',
                    duration: 100,
                    repeat: 1,
                    yoyo: true,
                    delay: 3000,
                    onComplete: inGameLogoani1,
                    callbackScope: this
                })
            }
        }
        transitionbackground1 = this.add.image(400, 300, 'transitionbackground1').setOrigin(0.5, 0.5).setInteractive({
            pixelPerfect: true,
            useHandCursor: true
        })
        transitionbackground2 = this.add.image(400, 300, 'transitionbackground2').setOrigin(0.5, 0.5)
        transitionbackground3 = this.add.image(400, 300, 'transitionbackground3').setOrigin(0.5, 0.5)
        transitionpanel = this.add.image(393.5, 314, 'transitionpanel').setOrigin(0.5)
        transitiondress = this.add.image(267, 141, 'transitiondress').setOrigin(0, 0)
        transitiontitle1 = this.add.image(400, 329, 'title1');
        transitiontitle1.setOrigin(0.5);
        transitiontitle2 = this.add.image(380, 399, 'title2');
        transitiontitle2.setOrigin(0.5);
        transitiontitle3 = this.add.image(380, 459, 'title3');
        transitiontitle3.setOrigin(0.5);
        transitionstar1 = this.add.image(616.5, 168, 'transitionstar');
        transitionstar1.setOrigin(0.5);
        transitionstar2 = this.add.image(156.5, 168, 'transitionstar');
        transitionstar2.setOrigin(0.5);
        transitionstar2.setScale(0.9)
        transitionstar3 = this.add.image(596.5, 468, 'transitionstar');
        transitionstar3.setOrigin(0.5);
        transitionstar3.setScale(0.9)
        transitionstar4 = this.add.image(116.5, 468, 'transitionstar');
        transitionstar4.setOrigin(0.5);
        transitionstar4.setScale(0.8)
        transitionstar5 = this.add.image(400, 558, 'transitionstar');
        transitionstar5.setOrigin(0.5);
        transitionstar5.setScale(0.65)
        this.tweens.add({
            targets: transitionstar1,
            angle: 4,
            ease: 'Linear',
            duration: 300,
            repeat: -1,
            yoyo: true
        })
        this.tweens.add({
            targets: transitionstar2,
            angle: 4,
            ease: 'Linear',
            duration: 300,
            repeat: -1,
            yoyo: true
        })
        this.tweens.add({
            targets: transitionstar3,
            angle: 4,
            ease: 'Linear',
            duration: 300,
            repeat: -1,
            yoyo: true
        })
        this.tweens.add({
            targets: transitionstar4,
            angle: 4,
            ease: 'Linear',
            duration: 300,
            repeat: -1,
            yoyo: true
        })
        this.tweens.add({
            targets: transitionstar5,
            angle: 4,
            ease: 'Linear',
            duration: 300,
            repeat: -1,
            yoyo: true
        })
        this.time.addEvent({
            delay: 1000,
            callback: transitionstart11,
            callbackScope: this
        });

        function transitionstart11() {
            this.tweens.add({
                targets: transitionbackground1,
                alpha: 0,
                ease: 'Linear',
                duration: 700
            })
            this.tweens.add({
                targets: transitionbackground2,
                alpha: 0,
                ease: 'Linear',
                duration: 700
            })
            this.tweens.add({
                targets: transitionbackground3,
                alpha: 0,
                ease: 'Linear',
                duration: 700
            })
            this.tweens.add({
                targets: transitionpanel,
                alpha: 0,
                ease: 'Linear',
                duration: 700
            })
            this.tweens.add({
                targets: transitiondress,
                alpha: 0,
                ease: 'Linear',
                duration: 700
            })
            this.tweens.add({
                targets: transitiontitle1,
                alpha: 0,
                ease: 'Linear',
                duration: 700
            })
            this.tweens.add({
                targets: transitiontitle2,
                alpha: 0,
                ease: 'Linear',
                duration: 700
            })
            this.tweens.add({
                targets: transitiontitle3,
                alpha: 0,
                ease: 'Linear',
                duration: 700
            })
            this.tweens.add({
                targets: transitionstar1,
                alpha: 0,
                ease: 'Linear',
                duration: 700
            })
            this.tweens.add({
                targets: transitionstar2,
                alpha: 0,
                ease: 'Linear',
                duration: 700
            })
            this.tweens.add({
                targets: transitionstar3,
                alpha: 0,
                ease: 'Linear',
                duration: 700
            })
            this.tweens.add({
                targets: transitionstar4,
                alpha: 0,
                ease: 'Linear',
                duration: 700
            })
            this.tweens.add({
                targets: transitionstar5,
                alpha: 0,
                ease: 'Linear',
                duration: 700
            })
        }
        dressup1start()
        hitobject = this.add.image(400, 300, 'hitobject').setOrigin(0.5, 0.5).setInteractive({
            useHandCursor: true
        })
        hitobject.visible = false
        hitobject.on('pointerdown', hitobjectdown)

        function hitobjectdown() {}
        this.load.on('complete', function () {
            loadFinish = true
        });
        this.load.start();
    }
});

function dressup1start() {
    elsahit1.on('pointerover', elsasegment1overstartclick)
    elsahit1.on('pointerout', elsasegment1outstartclick)
    elsahit1.on('pointerdown', elsasegment1startclick)

    function elsasegment1overstartclick() {
        if (elsaclrarr1[1] > 0 && (elsaclrarr1[1] != elsaclrarr2[1] || elsasegment1dress1.frame.name != doll.frame.name + 1)) {
            elsasegment1dress1.alpha = 0.6
            if (!colorindication) {
                elsasegment1dress1.tint = colorcode[elsaclrarr1[1]]
            } else {
                elsasegment1dress1.tint = colorcode[elsaclrarr2[1]]
            }
            if (colorindication) {} else {
                elsasegment1dress1.setFrame(doll.frame.name + 1)
            }
            if (elsaovrarr[1] == 0) {
                elsasegment1dress1.alpha = 0.6
                if (!colorindication) {
                    elsasegment1dress1.tint = colorcode[elsaclrarr1[1]]
                } else if (elsaclrarr2[1] == 0) {
                    elsasegment1dress1.alpha = 0
                    elsasegment1dress1.tint = colorcode[elsaclrarr2[1]]
                }
            }
        } else if (colorindication && elsaclrarr2[1] > 0) {
            elsasegment1dress1.alpha = 0.6
        }
    }

    function elsasegment1outstartclick() {
        if (elsaclrarr1[1] > 0 && (elsaclrarr1[1] != elsaclrarr2[1] || elsasegment1dress1.frame.name == doll.frame.name + 1)) {
            if (elsaovrarr[1] == 0 && elsaclrarr2[1] == 0) {
                elsasegment1dress1.alpha = 0
            } else {
                elsasegment1dress1.alpha = 1
                elsasegment1dress1.tint = colorcode[elsaclrarr2[1]]
                elsasegment1dress1.setFrame(elsadresarr1[1])
            }
        } else if (colorindication && elsaclrarr2[1] > 0) {
            elsasegment1dress1.alpha = 1
        } else if (colorindication) {
            elsasegment1dress1.alpha = 0
        }
    }

    function elsasegment1startclick() {
        if (music.isPlaying == false) {
            clicksound.stop()
        } else {
            clicksound.play()
        }
        if (colorindication) {
            elsaovrarr[1] = 0
            elsaclrarr1[1] = 0
            elsaclrarr2[1] = 0
            elsasegment1dress1.alpha = 0
        }
        if (elsaclrarr1[1] > 0) {
            done.visible = true
            elsasegment1dress1.alpha = 1
            elsadresarr1[1] = 1
            elsasegment1dress1.tint = colorcode[elsaclrarr1[1]]
            elsasegment1dress1.setFrame(doll.frame.name + 1)
            elsadresarr1[1] = elsasegment1dress1.frame.name
            elsaovrarr[1] = 1
            elsacolor[1] = colorcode[elsaclrarr1[1]]
            elsaclrarr2[1] = elsaclrarr1[1]
            if (handindication3.visible) {
                handindication4.visible = true
                handindication3.visible = false
            }
        } else {
            elsasegment1dress1.alpha = 0
            if (handindication3.visible) {
                handindication3.visible = false
            }
        }
    }
    elsahit2.on('pointerover', elsasegment2overstartclick)
    elsahit2.on('pointerout', elsasegment2outstartclick)
    elsahit2.on('pointerdown', elsasegment2startclick)

    function elsasegment2overstartclick() {
        if (elsaclrarr1[2] > 0 && (doll.frame.name + 1 != 2 && doll.frame.name + 1 != 4) && (colorindication || elsaclrarr1[2] != elsaclrarr2[2] || elsasegment2dress2.frame.name != doll.frame.name + 1)) {
            elsasegment2dress2.alpha = 0.6
            if (!colorindication) {
                elsasegment2dress2.tint = colorcode[elsaclrarr1[2]]
            } else {
                elsasegment2dress2.tint = colorcode[elsaclrarr2[2]]
            }
            if (colorindication) {} else {
                elsasegment2dress2.setFrame(doll.frame.name + 1)
            }
            if (elsaovrarr[2] == 0) {
                elsasegment2dress2.alpha = 0.6
                if (!colorindication) {
                    elsasegment2dress2.tint = colorcode[elsaclrarr1[2]]
                } else if (elsaclrarr2[2] == 0) {
                    elsasegment2dress2.alpha = 0
                    elsasegment2dress2.tint = colorcode[elsaclrarr2[2]]
                }
            }
        } else if (colorindication && elsaclrarr2[2] > 0) {
            elsasegment2dress2.alpha = 0.6
        }
    }

    function elsasegment2outstartclick() {
        if (elsaclrarr1[2] > 0 && (doll.frame.name + 1 != 2 && doll.frame.name + 1 != 4) && (colorindication || elsaclrarr1[2] != elsaclrarr2[2] || elsasegment2dress2.frame.name == doll.frame.name + 1)) {
            if (elsaovrarr[2] == 0 && (elsaclrarr2[2] == 0)) {
                elsasegment2dress2.alpha = 0
            } else {
                elsasegment2dress2.alpha = 1
                elsasegment2dress2.tint = colorcode[elsaclrarr2[2]]
                elsasegment2dress2.setFrame(elsadresarr1[2])
            }
        } else if (colorindication && elsaclrarr2[2] > 0) {
            elsasegment2dress2.alpha = 1
        } else if (colorindication) {
            elsasegment2dress2.alpha = 0
        }
    }

    function elsasegment2startclick() {
        if (music.isPlaying == false) {
            clicksound.stop()
        } else {
            clicksound.play()
        }
        if (colorindication) {
            elsaovrarr[2] = 0
            elsaclrarr1[2] = 0
            elsaclrarr2[2] = 0
            elsasegment2dress2.alpha = 0
        }
        if (doll.frame.name + 1 != 2 && doll.frame.name + 1 != 4) {
            if (elsaclrarr1[2] > 0) {
                done.visible = true
                elsasegment2dress2.alpha = 1
                elsadresarr1[2] = 1
                elsasegment2dress2.setFrame(doll.frame.name + 1)
                elsasegment2dress2.tint = colorcode[elsaclrarr1[2]]
                elsadresarr1[2] = elsasegment2dress2.frame.name
                elsaovrarr[2] = 1
                elsacolor[2] = colorcode[elsaclrarr1[2]]
                elsaclrarr2[2] = elsaclrarr1[2]
                if (handindication3.visible) {
                    handindication4.visible = true
                    handindication3.visible = false
                }
            } else {
                elsasegment2dress2.alpha = 0
                if (handindication3.visible) {
                    handindication3.visible = false
                }
            }
        }
    }
    elsahit3.on('pointerover', elsasegment3overstartclick)
    elsahit3.on('pointerout', elsasegment3outstartclick)
    elsahit3.on('pointerdown', elsasegment3startclick)

    function elsasegment3overstartclick() {
        if (elsaclrarr1[3] > 0 && (colorindication || elsaclrarr1[3] != elsaclrarr2[3] || (elsasegment3dress3.frame.name != doll.frame.name + 1))) {
            elsasegment3dress3.alpha = 0.6
            if (!colorindication) {
                elsasegment3dress3.tint = colorcode[elsaclrarr1[3]]
            } else {
                elsasegment3dress3.tint = colorcode[elsaclrarr2[3]]
            }
            if (colorindication) {} else {
                elsasegment3dress3.setFrame(doll.frame.name + 1)
            }
            if (elsaovrarr[3] == 0) {
                elsasegment3dress3.alpha = 0.6
                if (!colorindication) {
                    elsasegment3dress3.tint = colorcode[elsaclrarr1[3]]
                } else if (elsaclrarr2[3] == 0) {
                    elsasegment3dress3.alpha = 0
                    elsasegment3dress3.tint = colorcode[elsaclrarr2[3]]
                }
            }
        } else if (colorindication && elsaclrarr2[3] > 0) {
            elsasegment3dress3.alpha = 0.6
        }
    }

    function elsasegment3outstartclick() {
        if (elsaclrarr1[3] > 0 && (colorindication || elsaclrarr1[3] != elsaclrarr2[3] || (elsasegment3dress3.frame.name == doll.frame.name + 1))) {
            if (elsaovrarr[3] == 0 && elsaclrarr2[3] == 0) {
                elsasegment3dress3.alpha = 0
            } else {
                elsasegment3dress3.alpha = 1
                elsasegment3dress3.tint = colorcode[elsaclrarr2[3]]
                elsasegment3dress3.setFrame(elsadresarr1[3])
            }
        } else if (colorindication && elsaclrarr2[3] > 0) {
            elsasegment3dress3.alpha = 1
        } else if (colorindication) {
            elsasegment3dress3.alpha = 0
        }
    }

    function elsasegment3startclick() {
        if (music.isPlaying == false) {
            clicksound.stop()
        } else {
            clicksound.play()
        }
        if (colorindication) {
            elsaovrarr[3] = 0
            elsaclrarr1[3] = 0
            elsaclrarr2[3] = 0
            elsasegment3dress3.alpha = 0
        }
        if (elsaclrarr1[3] > 0) {
            done.visible = true
            elsasegment3dress3.alpha = 1
            elsadresarr1[3] = 1
            elsasegment3dress3.setFrame(doll.frame.name + 1)
            elsasegment3dress3.tint = colorcode[elsaclrarr1[3]]
            elsadresarr1[3] = elsasegment3dress3.frame.name
            elsaovrarr[3] = 1
            elsacolor[3] = colorcode[elsaclrarr1[3]]
            elsaclrarr2[3] = elsaclrarr1[3]
            if (handindication3.visible) {
                handindication4.visible = true
                handindication3.visible = false
            }
        } else {
            elsasegment3dress3.alpha = 0
            if (handindication3.visible) {
                handindication3.visible = false
            }
        }
    }
    elsahit4.on('pointerover', elsasegment4overstartclick)
    elsahit4.on('pointerout', elsasegment4outstartclick)
    elsahit4.on('pointerdown', elsasegment4startclick)

    function elsasegment4overstartclick() {
        if (elsaclrarr1[4] > 0 && (colorindication || elsaclrarr1[4] != elsaclrarr2[4] || elsasegment4dress4.frame.name != doll.frame.name + 1)) {
            elsasegment4dress4.alpha = 0.6
            if (!colorindication) {
                elsasegment4dress4.tint = colorcode[elsaclrarr1[4]]
            } else {
                elsasegment4dress4.tint = colorcode[elsaclrarr2[4]]
            }
            if (colorindication) {} else {
                elsasegment4dress4.setFrame(doll.frame.name + 1)
            }
            if (elsaovrarr[4] == 0) {
                elsasegment4dress4.alpha = 0.6
                if (!colorindication) {
                    elsasegment4dress4.tint = colorcode[elsaclrarr1[4]]
                } else if (elsaclrarr2[4] == 0) {
                    elsasegment4dress4.alpha = 0
                    elsasegment4dress4.tint = colorcode[elsaclrarr2[4]]
                }
            }
        } else if (colorindication && elsaclrarr2[4] > 0) {
            elsasegment4dress4.alpha = 0.6
        }
    }

    function elsasegment4outstartclick() {
        if (elsaclrarr1[4] > 0 && (colorindication || elsaclrarr1[4] != elsaclrarr2[4] || elsasegment4dress4.frame.name == doll.frame.name + 1)) {
            if (elsaovrarr[4] == 0 && elsaclrarr2[4] == 0) {
                elsasegment4dress4.alpha = 0
            } else {
                elsasegment4dress4.alpha = 1
                elsasegment4dress4.tint = colorcode[elsaclrarr2[4]]
                elsasegment4dress4.setFrame(elsadresarr1[4])
            }
        } else if (colorindication && elsaclrarr2[4] > 0) {
            elsasegment4dress4.alpha = 1
        } else if (colorindication) {
            elsasegment4dress4.alpha = 0
        }
    }

    function elsasegment4startclick() {
        if (music.isPlaying == false) {
            clicksound.stop()
        } else {
            clicksound.play()
        }
        if (colorindication) {
            elsaovrarr[4] = 0
            elsaclrarr1[4] = 0
            elsaclrarr2[4] = 0
            elsasegment4dress4.alpha = 0
        }
        if (elsaclrarr1[4] > 0) {
            done.visible = true
            elsasegment4dress4.alpha = 1
            elsadresarr1[4] = 1
            elsasegment4dress4.setFrame(doll.frame.name + 1)
            elsasegment4dress4.tint = colorcode[elsaclrarr1[4]]
            elsadresarr1[4] = elsasegment4dress4.frame.name
            elsaovrarr[4] = 1
            elsacolor[4] = colorcode[elsaclrarr1[4]]
            elsaclrarr2[4] = elsaclrarr1[4]
            if (handindication3.visible) {
                handindication4.visible = true
                handindication3.visible = false
            }
        } else {
            elsasegment4dress4.alpha = 0
            if (handindication3.visible) {
                handindication3.visible = false
            }
        }
    }
    elsahit5.on('pointerover', elsasegment5overstartclick)
    elsahit5.on('pointerout', elsasegment5outstartclick)
    elsahit5.on('pointerdown', elsasegment5startclick)

    function elsasegment5overstartclick() {
        if (elsaclrarr1[5] > 0 && (doll.frame.name + 1 != 2 && doll.frame.name + 1 != 4 && doll.frame.name + 1 != 6) && (colorindication || elsaclrarr1[5] != elsaclrarr2[5] || elsasegment5dress5.frame.name != doll.frame.name + 1)) {
            elsasegment5dress5.alpha = 0.6
            if (!colorindication) {
                elsasegment5dress5.tint = colorcode[elsaclrarr1[5]]
            } else {
                elsasegment5dress5.tint = colorcode[elsaclrarr2[5]]
            }
            if (colorindication) {} else {
                elsasegment5dress5.setFrame(doll.frame.name + 1)
            }
            if (elsaovrarr[5] == 0) {
                elsasegment5dress5.alpha = 0.6
                if (!colorindication) {
                    elsasegment5dress5.tint = colorcode[elsaclrarr1[5]]
                } else if (elsaclrarr2[5] == 0) {
                    elsasegment5dress5.alpha = 0
                    elsasegment5dress5.tint = colorcode[elsaclrarr2[5]]
                }
            }
        } else if (colorindication && elsaclrarr2[5] > 0) {
            elsasegment5dress5.alpha = 0.6
        }
    }

    function elsasegment5outstartclick() {
        if (elsaclrarr1[5] > 0 && (colorindication || elsaclrarr1[5] != elsaclrarr2[5] || elsasegment5dress5.frame.name == doll.frame.name + 1)) {
            if (elsaovrarr[5] == 0 && elsaclrarr2[5] == 0) {
                elsasegment5dress5.alpha = 0
            } else {
                elsasegment5dress5.alpha = 1
                elsasegment5dress5.tint = colorcode[elsaclrarr2[5]]
                elsasegment5dress5.setFrame(elsadresarr1[5])
            }
        } else if (colorindication && elsaclrarr2[5] > 0) {
            elsasegment5dress5.alpha = 1
        } else if (colorindication) {
            elsasegment5dress5.alpha = 0
        }
    }

    function elsasegment5startclick() {
        if (music.isPlaying == false) {
            clicksound.stop()
        } else {
            clicksound.play()
        }
        if (colorindication) {
            elsaovrarr[5] = 0
            elsaclrarr1[5] = 0
            elsaclrarr2[5] = 0
            elsasegment5dress5.alpha = 0
        }
        if (doll.frame.name + 1 != 2 && doll.frame.name + 1 != 4 && doll.frame.name + 1 != 6) {
            if (elsaclrarr1[5] > 0) {
                done.visible = true
                elsasegment5dress5.alpha = 1
                elsadresarr1[5] = 1
                elsasegment5dress5.setFrame(doll.frame.name + 1)
                elsasegment5dress5.tint = colorcode[elsaclrarr1[5]]
                elsadresarr1[5] = elsasegment5dress5.frame.name
                elsaovrarr[5] = 1
                elsacolor[5] = colorcode[elsaclrarr1[5]]
                elsaclrarr2[5] = elsaclrarr1[5]
                if (handindication3.visible) {
                    handindication4.visible = true
                    handindication3.visible = false
                }
            } else {
                elsasegment5dress5.alpha = 0
                if (handindication3.visible) {
                    handindication3.visible = false
                }
            }
        }
    }
    elsahit6.on('pointerover', elsasegment6overstartclick)
    elsahit6.on('pointerout', elsasegment6outstartclick)
    elsahit6.on('pointerdown', elsasegment6startclick)

    function elsasegment6overstartclick() {
        if (elsaclrarr1[6] > 0 && (doll.frame.name + 1 == 3) && (colorindication || elsaclrarr1[6] != elsaclrarr2[6] || elsasegment6dress6.frame.name != doll.frame.name + 1)) {
            elsasegment6dress6.alpha = 0.6
            if (!colorindication) {
                elsasegment6dress6.tint = colorcode[elsaclrarr1[6]]
            } else {
                elsasegment6dress6.tint = colorcode[elsaclrarr2[6]]
            }
            if (colorindication) {} else {
                elsasegment6dress6.setFrame(doll.frame.name + 1)
            }
            if (elsaovrarr[6] == 0) {
                elsasegment6dress6.alpha = 0.6
                if (!colorindication) {
                    elsasegment6dress6.tint = colorcode[elsaclrarr1[6]]
                } else if (elsaclrarr2[6] == 0) {
                    elsasegment6dress6.alpha = 0
                    elsasegment6dress6.tint = colorcode[elsaclrarr2[6]]
                }
                elsasegment6dress6.setFrame(doll.frame.name + 1)
            }
        } else if (colorindication && elsaclrarr2[6] > 0) {
            elsasegment6dress6.alpha = 0.6
        }
    }

    function elsasegment6outstartclick() {
        if (elsaclrarr1[6] > 0 && (doll.frame.name + 1 == 3) && (colorindication || elsaclrarr1[6] != elsaclrarr2[6] || elsasegment6dress6.frame.name == doll.frame.name + 1)) {
            if (elsaovrarr[6] == 0 && (elsaclrarr2[6] == 0)) {
                elsasegment6dress6.alpha = 0
            } else {
                elsasegment6dress6.alpha = 1
                elsasegment6dress6.tint = colorcode[elsaclrarr2[6]]
                elsasegment6dress6.setFrame(elsadresarr1[6])
            }
        } else if (colorindication && elsaclrarr2[6] > 0) {
            elsasegment6dress6.alpha = 1
        } else if (colorindication) {
            elsasegment6dress6.alpha = 0
        }
    }

    function elsasegment6startclick() {
        if (music.isPlaying == false) {
            clicksound.stop()
        } else {
            clicksound.play()
        }
        if (colorindication) {
            elsaovrarr[6] = 0
            elsaclrarr1[6] = 0
            elsaclrarr2[6] = 0
            elsasegment6dress6.alpha = 0
        }
        if (doll.frame.name + 1 == 3) {
            if (elsaclrarr1[6] > 0) {
                done.visible = true
                elsasegment6dress6.alpha = 1
                elsasegment6dress6.setFrame(doll.frame.name + 1)
                elsadresarr1[6] = 1
                elsasegment6dress6.tint = colorcode[elsaclrarr1[6]]
                elsadresarr1[6] = elsasegment6dress6.frame.name
                elsaovrarr[6] = 1
                elsacolor[6] = colorcode[elsaclrarr1[6]]
                elsaclrarr2[6] = elsaclrarr1[6]
                if (handindication3.visible) {
                    handindication4.visible = true
                    handindication3.visible = false
                }
            } else {
                elsasegment6dress6.alpha = 0
                if (handindication3.visible) {
                    handindication3.visible = false
                }
            }
        }
    }
    elsahit7.on('pointerover', elsasegment7overstartclick)
    elsahit7.on('pointerout', elsasegment7outstartclick)
    elsahit7.on('pointerdown', elsasegment7startclick)

    function elsasegment7overstartclick() {
        if (elsaclrarr1[7] > 0 && (doll.frame.name + 1 == 3) && (colorindication || elsaclrarr1[7] != elsaclrarr2[7] || elsasegment7dress7.frame.name != doll.frame.name + 1)) {
            elsasegment7dress7.alpha = 0.6
            if (!colorindication) {
                elsasegment7dress7.tint = colorcode[elsaclrarr1[7]]
            } else {
                elsasegment7dress7.tint = colorcode[elsaclrarr2[7]]
            }
            if (colorindication) {} else {
                elsasegment7dress7.setFrame(doll.frame.name + 1)
            }
            if (elsaovrarr[7] == 0) {
                elsasegment7dress7.alpha = 0.6
                if (!colorindication) {
                    elsasegment7dress7.tint = colorcode[elsaclrarr1[7]]
                } else if (elsaclrarr2[7] == 0) {
                    elsasegment7dress7.alpha = 0
                    elsasegment7dress7.tint = colorcode[elsaclrarr2[7]]
                }
                elsasegment7dress7.setFrame(doll.frame.name + 1)
            }
        } else if (colorindication && elsaclrarr2[7] > 0) {
            elsasegment7dress7.alpha = 0.6
        }
    }

    function elsasegment7outstartclick() {
        if (elsaclrarr1[7] > 0 && (doll.frame.name + 1 == 3) && (colorindication || elsaclrarr1[7] != elsaclrarr2[7] || elsasegment7dress7.frame.name == doll.frame.name + 1)) {
            if (elsaovrarr[7] == 0 && (elsaclrarr2[7] == 0)) {
                elsasegment7dress7.alpha = 0
            } else {
                elsasegment7dress7.alpha = 1
                elsasegment7dress7.tint = colorcode[elsaclrarr2[7]]
                elsasegment7dress7.setFrame(elsadresarr1[7])
            }
        } else if (colorindication && elsaclrarr2[7] > 0) {
            elsasegment7dress7.alpha = 1
        } else if (colorindication) {
            elsasegment7dress7.alpha = 0
        }
    }

    function elsasegment7startclick() {
        if (music.isPlaying == false) {
            clicksound.stop()
        } else {
            clicksound.play()
        }
        if (colorindication) {
            elsaovrarr[7] = 0
            elsaclrarr1[7] = 0
            elsaclrarr2[7] = 0
            elsasegment7dress7.alpha = 0
        }
        if (doll.frame.name + 1 == 3) {
            if (elsaclrarr1[7] > 0) {
                done.visible = true
                elsasegment7dress7.alpha = 1
                elsadresarr1[7] = 1
                elsasegment7dress7.setFrame(doll.frame.name + 1)
                elsasegment7dress7.tint = colorcode[elsaclrarr1[7]]
                elsadresarr1[7] = elsasegment7dress7.frame.name
                elsaovrarr[7] = 1
                elsacolor[7] = colorcode[elsaclrarr1[7]]
                elsaclrarr2[7] = elsaclrarr1[7]
                if (handindication3.visible) {
                    handindication4.visible = true
                    handindication3.visible = false
                }
            } else {
                elsasegment7dress7.alpha = 0
                if (handindication3.visible) {
                    handindication3.visible = false
                }
            }
        }
    }
    elsahit8.on('pointerover', elsasegment8overstartclick)
    elsahit8.on('pointerout', elsasegment8outstartclick)
    elsahit8.on('pointerdown', elsasegment8startclick)

    function elsasegment8overstartclick() {
        if (elsaclrarr1[8] > 0 && (doll.frame.name + 1 == 3) && (colorindication || elsaclrarr1[8] != elsaclrarr2[8] || elsasegment8dress8.frame.name != doll.frame.name + 1)) {
            elsasegment8dress8.alpha = 0.6
            if (!colorindication) {
                elsasegment8dress8.tint = colorcode[elsaclrarr1[8]]
            } else {
                elsasegment8dress8.tint = colorcode[elsaclrarr2[8]]
            }
            if (colorindication) {} else {
                elsasegment8dress8.setFrame(doll.frame.name + 1)
            }
            if (elsaovrarr[8] == 0) {
                elsasegment8dress8.alpha = 0.6
                if (!colorindication) {
                    elsasegment8dress8.tint = colorcode[elsaclrarr1[8]]
                } else if (elsaclrarr2[8] == 0) {
                    elsasegment8dress8.alpha = 0
                    elsasegment8dress8.tint = colorcode[elsaclrarr2[8]]
                }
                elsasegment8dress8.setFrame(doll.frame.name + 1)
            }
        } else if (colorindication && elsaclrarr2[8] > 0) {
            elsasegment8dress8.alpha = 0.6
        }
    }

    function elsasegment8outstartclick() {
        if (elsaclrarr1[8] > 0 && (doll.frame.name + 1 == 3) && (colorindication || elsaclrarr1[8] != elsaclrarr2[8] || elsasegment8dress8.frame.name == doll.frame.name + 1)) {
            if (elsaovrarr[8] == 0 && elsaclrarr2[8] == 0) {
                elsasegment8dress8.alpha = 0
            } else {
                elsasegment8dress8.alpha = 1
                elsasegment8dress8.tint = colorcode[elsaclrarr2[8]]
                elsasegment8dress8.setFrame(elsadresarr1[8])
            }
        } else if (colorindication && elsaclrarr2[8] > 0) {
            elsasegment8dress8.alpha = 1
        } else if (colorindication) {
            elsasegment8dress8.alpha = 0
        }
    }

    function elsasegment8startclick() {
        if (music.isPlaying == false) {
            clicksound.stop()
        } else {
            clicksound.play()
        }
        if (colorindication) {
            elsaovrarr[8] = 0
            elsaclrarr1[8] = 0
            elsaclrarr2[8] = 0
            elsasegment8dress8.alpha = 0
        }
        if (doll.frame.name + 1 == 3) {
            if (elsaclrarr1[8] > 0) {
                done.visible = true
                elsasegment8dress8.alpha = 1
                elsadresarr1[8] = 1
                elsasegment8dress8.setFrame(doll.frame.name + 1)
                elsasegment8dress8.tint = colorcode[elsaclrarr1[8]]
                elsadresarr1[8] = elsasegment8dress8.frame.name
                elsaovrarr[8] = 1
                elsacolor[8] = colorcode[elsaclrarr1[8]]
                elsaclrarr2[8] = elsaclrarr1[8]
                if (handindication3.visible) {
                    handindication4.visible = true
                    handindication3.visible = false
                }
            } else {
                elsasegment8dress8.alpha = 0
                if (handindication3.visible) {
                    handindication3.visible = false
                }
            }
        }
    }
    elsahit9.on('pointerover', elsasegment9overstartclick)
    elsahit9.on('pointerout', elsasegment9outstartclick)
    elsahit9.on('pointerdown', elsasegment9startclick)

    function elsasegment9overstartclick() {
        if (elsaclrarr1[9] > 0 && (doll.frame.name + 1 == 3 || doll.frame.name + 1 == 5) && (colorindication || elsaclrarr1[9] != elsaclrarr2[9] || elsasegment9dress9.frame.name != doll.frame.name + 1)) {
            elsasegment9dress9.alpha = 0.6
            if (!colorindication) {
                elsasegment9dress9.tint = colorcode[elsaclrarr1[9]]
            } else {
                elsasegment9dress9.tint = colorcode[elsaclrarr2[9]]
            }
            if (colorindication) {} else {
                elsasegment9dress9.setFrame(doll.frame.name + 1)
            }
            if (elsaovrarr[9] == 0) {
                elsasegment9dress9.alpha = 0.6
                if (!colorindication) {
                    elsasegment9dress9.tint = colorcode[elsaclrarr1[9]]
                } else if (elsaclrarr2[9] == 0) {
                    elsasegment9dress9.alpha = 0
                    elsasegment9dress9.tint = colorcode[elsaclrarr2[9]]
                }
                elsasegment9dress9.setFrame(doll.frame.name + 1)
            }
        } else if (colorindication && elsaclrarr2[9] > 0) {
            elsasegment9dress9.alpha = 0.6
        }
    }

    function elsasegment9outstartclick() {
        if (elsaclrarr1[9] > 0 && (doll.frame.name + 1 == 3 || doll.frame.name + 1 == 5) && (colorindication || elsaclrarr1[9] != elsaclrarr2[9] || elsasegment9dress9.frame.name == doll.frame.name + 1)) {
            if (elsaovrarr[9] == 0 && (elsaclrarr2[9] == 0)) {
                elsasegment9dress9.alpha = 0
            } else {
                elsasegment9dress9.alpha = 1
                elsasegment9dress9.tint = colorcode[elsaclrarr2[9]]
                elsasegment9dress9.setFrame(elsadresarr1[9])
            }
        } else if (colorindication && elsaclrarr2[9] > 0) {
            elsasegment9dress9.alpha = 1
        } else if (colorindication) {
            elsasegment9dress9.alpha = 0
        }
    }

    function elsasegment9startclick() {
        if (music.isPlaying == false) {
            clicksound.stop()
        } else {
            clicksound.play()
        }
        if (colorindication) {
            elsaovrarr[9] = 0
            elsaclrarr1[9] = 0
            elsaclrarr2[9] = 0
            elsasegment9dress9.alpha = 0
        }
        if (doll.frame.name + 1 == 3 || doll.frame.name + 1 == 5) {
            if (elsaclrarr1[9] > 0) {
                done.visible = true
                elsasegment9dress9.alpha = 1
                elsadresarr1[9] = 1
                elsasegment9dress9.setFrame(doll.frame.name + 1)
                elsasegment9dress9.tint = colorcode[elsaclrarr1[9]]
                elsadresarr1[9] = elsasegment9dress9.frame.name
                elsaovrarr[9] = 1
                elsacolor[9] = colorcode[elsaclrarr1[9]]
                elsaclrarr2[9] = elsaclrarr1[9]
                if (handindication3.visible) {
                    handindication4.visible = true
                    handindication3.visible = false
                }
            } else {
                elsasegment9dress9.alpha = 0
                if (handindication3.visible) {
                    handindication3.visible = false
                }
            }
        }
    }
    elsahit10.on('pointerover', elsasegment10overstartclick)
    elsahit10.on('pointerout', elsasegment10outstartclick)
    elsahit10.on('pointerdown', elsasegment10startclick)

    function elsasegment10overstartclick() {
        if (elsaclrarr1[10] > 0 && (doll.frame.name + 1 == 3 || doll.frame.name + 1 == 5) && (colorindication || elsaclrarr1[10] != elsaclrarr2[10] || elsasegment10dress10.frame.name != doll.frame.name + 1)) {
            elsasegment10dress10.alpha = 0.6
            if (!colorindication) {
                elsasegment10dress10.tint = colorcode[elsaclrarr1[10]]
            } else {
                elsasegment10dress10.tint = colorcode[elsaclrarr2[10]]
            }
            if (colorindication) {} else {
                elsasegment10dress10.setFrame(doll.frame.name + 1)
            }
            if (elsaovrarr[10] == 0) {
                elsasegment10dress10.alpha = 0.6
                if (!colorindication) {
                    elsasegment10dress10.tint = colorcode[elsaclrarr1[10]]
                } else if (elsaclrarr2[10] == 0) {
                    elsasegment10dress10.alpha = 0
                    elsasegment10dress10.tint = colorcode[elsaclrarr2[10]]
                }
                elsasegment10dress10.setFrame(doll.frame.name + 1)
            }
        } else if (colorindication && elsaclrarr2[10] > 0) {
            elsasegment10dress10.alpha = 0.6
        }
    }

    function elsasegment10outstartclick() {
        if (elsaclrarr1[10] > 0 && (doll.frame.name + 1 == 3 || doll.frame.name + 1 == 5) && (colorindication || elsaclrarr1[10] != elsaclrarr2[10] || elsasegment10dress10.frame.name == doll.frame.name + 1)) {
            if (elsaovrarr[10] == 0 && (elsaclrarr2[10] == 0)) {
                elsasegment10dress10.alpha = 0
            } else {
                elsasegment10dress10.alpha = 1
                elsasegment10dress10.tint = colorcode[elsaclrarr2[10]]
                elsasegment10dress10.setFrame(elsadresarr1[10])
            }
        } else if (colorindication && elsaclrarr2[10] > 0) {
            elsasegment10dress10.alpha = 1
        } else if (colorindication) {
            elsasegment10dress10.alpha = 0
        }
    }

    function elsasegment10startclick() {
        if (music.isPlaying == false) {
            clicksound.stop()
        } else {
            clicksound.play()
        }
        if (colorindication) {
            elsaovrarr[10] = 0
            elsaclrarr1[10] = 0
            elsaclrarr2[10] = 0
            elsasegment10dress10.alpha = 0
        }
        if (doll.frame.name + 1 == 3 || doll.frame.name + 1 == 5) {
            if (elsaclrarr1[10] > 0) {
                done.visible = true
                elsasegment10dress10.alpha = 1
                elsadresarr1[10] = 1
                elsasegment10dress10.tint = colorcode[elsaclrarr1[10]]
                elsasegment10dress10.setFrame(doll.frame.name + 1)
                elsadresarr1[10] = elsasegment10dress10.frame.name
                elsaovrarr[10] = 1
                elsacolor[10] = colorcode[elsaclrarr1[10]]
                elsaclrarr2[10] = elsaclrarr1[10]
                if (handindication3.visible) {
                    handindication4.visible = true
                    handindication3.visible = false
                }
            } else {
                elsasegment10dress10.alpha = 0
                if (handindication3.visible) {
                    handindication3.visible = false
                }
            }
        }
    }
    elsahit11.on('pointerover', elsasegment11overstartclick)
    elsahit11.on('pointerout', elsasegment11outstartclick)
    elsahit11.on('pointerdown', elsasegment11startclick)

    function elsasegment11overstartclick() {
        if (elsaclrarr1[11] > 0 && (doll.frame.name + 1 == 3 || doll.frame.name + 1 == 5) && (colorindication || elsaclrarr1[11] != elsaclrarr2[11] || elsasegment11dress11.frame.name != doll.frame.name + 1)) {
            elsasegment11dress11.alpha = 0.6
            if (!colorindication) {
                elsasegment11dress11.tint = colorcode[elsaclrarr1[11]]
            } else {
                elsasegment11dress11.tint = colorcode[elsaclrarr2[11]]
            }
            if (colorindication) {} else {
                elsasegment11dress11.setFrame(doll.frame.name + 1)
            }
            if (elsaovrarr[11] == 0) {
                elsasegment11dress11.alpha = 0.6
                if (!colorindication) {
                    elsasegment11dress11.tint = colorcode[elsaclrarr1[11]]
                } else if (elsaclrarr2[11] == 0) {
                    elsasegment11dress11.alpha = 0
                    elsasegment11dress11.tint = colorcode[elsaclrarr2[11]]
                }
                elsasegment11dress11.setFrame(doll.frame.name + 1)
            }
        } else if (colorindication && elsaclrarr2[11] > 0) {
            elsasegment11dress11.alpha = 0.6
        }
    }

    function elsasegment11outstartclick() {
        if (elsaclrarr1[11] > 0 && (doll.frame.name + 1 == 3 || doll.frame.name + 1 == 5) && (colorindication || elsaclrarr1[11] != elsaclrarr2[11] || elsasegment11dress11.frame.name == doll.frame.name + 1)) {
            if (elsaovrarr[11] == 0 && (elsaclrarr2[11] == 0)) {
                elsasegment11dress11.alpha = 0
            } else {
                elsasegment11dress11.alpha = 1
                elsasegment11dress11.tint = colorcode[elsaclrarr2[11]]
                elsasegment11dress11.setFrame(elsadresarr1[11])
            }
        } else if (colorindication && elsaclrarr2[11] > 0) {
            elsasegment11dress11.alpha = 1
        } else if (colorindication) {
            elsasegment11dress11.alpha = 0
        }
    }

    function elsasegment11startclick() {
        if (music.isPlaying == false) {
            clicksound.stop()
        } else {
            clicksound.play()
        }
        if (colorindication) {
            elsaovrarr[11] = 0
            elsaclrarr1[11] = 0
            elsaclrarr2[11] = 0
            elsasegment11dress11.alpha = 0
        }
        if (doll.frame.name + 1 == 3 || doll.frame.name + 1 == 5) {
            if (elsaclrarr1[11] > 0) {
                done.visible = true
                elsasegment11dress11.alpha = 1
                elsadresarr1[11] = 1
                elsasegment11dress11.tint = colorcode[elsaclrarr1[11]]
                elsasegment11dress11.setFrame(doll.frame.name + 1)
                elsadresarr1[11] = elsasegment11dress11.frame.name
                elsaovrarr[11] = 1
                elsacolor[11] = colorcode[elsaclrarr1[11]]
                elsaclrarr2[11] = elsaclrarr1[11]
                if (handindication3.visible) {
                    handindication4.visible = true
                    handindication3.visible = false
                }
            } else {
                elsasegment11dress11.alpha = 0
                if (handindication3.visible) {
                    handindication3.visible = false
                }
            }
        }
    }
    elsasegment1dress1.on('pointerdown', elsasegment1dress1startclick)

    function elsasegment1dress1startclick() {
        handindication3.visible = false
        handindication3.visible = false
        if (colorindication) {
            if (music.isPlaying == false) {
                clicksound.stop()
            } else {
                clicksound.play()
            }
            if (!handstart) {
                handindication1.visible = true
                handstart = true
            }
            elsasegment1dress1.alpha = 0
            elsaovrarr[1] = 0
            elsaclrarr2[1] = 0
        }
    }
    elsasegment2dress2.on('pointerdown', elsasegment2dress2startclick)

    function elsasegment2dress2startclick() {
        handindication3.visible = false
        if (colorindication) {
            if (music.isPlaying == false) {
                clicksound.stop()
            } else {
                clicksound.play()
            }
            if (!handstart) {
                handindication1.visible = true
                handstart = true
            }
            elsasegment2dress2.alpha = 0
            elsaovrarr[2] = 0
            elsaclrarr2[2] = 0
        }
    }
    elsasegment3dress3.on('pointerdown', elsasegment3dress3startclick)

    function elsasegment3dress3startclick() {
        handindication3.visible = false
        if (colorindication) {
            if (music.isPlaying == false) {
                clicksound.stop()
            } else {
                clicksound.play()
            }
            if (!handstart) {
                handindication1.visible = true
                handstart = true
            }
            elsasegment3dress3.alpha = 0
            elsaovrarr[3] = 0
            elsaclrarr2[3] = 0
        }
    }
    elsasegment4dress4.on('pointerdown', elsasegment4dress4startclick)

    function elsasegment4dress4startclick() {
        handindication3.visible = false
        if (colorindication) {
            if (music.isPlaying == false) {
                clicksound.stop()
            } else {
                clicksound.play()
            }
            if (!handstart) {
                handindication1.visible = true
                handstart = true
            }
            elsasegment4dress4.alpha = 0
            elsaovrarr[4] = 0
            elsaclrarr2[4] = 0
        }
    }
    elsasegment5dress5.on('pointerdown', elsasegment5dress5startclick)

    function elsasegment5dress5startclick() {
        handindication3.visible = false
        if (colorindication) {
            if (music.isPlaying == false) {
                clicksound.stop()
            } else {
                clicksound.play()
            }
            if (!handstart) {
                handindication1.visible = true
                handstart = true
            }
            elsasegment5dress5.alpha = 0
            elsaovrarr[5] = 0
            elsaclrarr2[5] = 0
        }
    }
    elsasegment6dress6.on('pointerdown', elsasegment6dress6startclick)

    function elsasegment6dress6startclick() {
        handindication3.visible = false
        if (colorindication) {
            if (music.isPlaying == false) {
                clicksound.stop()
            } else {
                clicksound.play()
            }
            if (!handstart) {
                handindication1.visible = true
                handstart = true
            }
            elsasegment6dress6.alpha = 0
            elsaovrarr[6] = 0
            elsaclrarr2[6] = 0
        }
    }
    elsasegment7dress7.on('pointerdown', elsasegment7dress7startclick)

    function elsasegment7dress7startclick() {
        handindication3.visible = false
        if (colorindication) {
            if (music.isPlaying == false) {
                clicksound.stop()
            } else {
                clicksound.play()
            }
            if (!handstart) {
                handindication1.visible = true
                handstart = true
            }
            elsasegment7dress7.alpha = 0
            elsaovrarr[7] = 0
            elsaclrarr2[7] = 0
        }
    }
    elsasegment8dress8.on('pointerdown', elsasegment8dress8startclick)

    function elsasegment8dress8startclick() {
        handindication3.visible = false
        if (colorindication) {
            if (music.isPlaying == false) {
                clicksound.stop()
            } else {
                clicksound.play()
            }
            if (!handstart) {
                handindication1.visible = true
                handstart = true
            }
            elsasegment8dress8.alpha = 0
            elsaovrarr[8] = 0
            elsaclrarr2[8] = 0
        }
    }
    elsasegment9dress9.on('pointerdown', elsasegment9dress9startclick)

    function elsasegment9dress9startclick() {
        handindication3.visible = false
        if (colorindication) {
            if (music.isPlaying == false) {
                clicksound.stop()
            } else {
                clicksound.play()
            }
            if (!handstart) {
                handindication1.visible = true
                handstart = true
            }
            elsasegment9dress9.alpha = 0
            elsaovrarr[9] = 0
            elsaclrarr2[9] = 0
        }
    }
    elsasegment10dress10.on('pointerdown', elsasegment10dress10startclick)

    function elsasegment10dress10startclick() {
        handindication3.visible = false
        if (colorindication) {
            if (music.isPlaying == false) {
                clicksound.stop()
            } else {
                clicksound.play()
            }
            if (!handstart) {
                handindication1.visible = true
                handstart = true
            }
            elsasegment10dress10.alpha = 0
            elsaovrarr[10] = 0
            elsaclrarr2[10] = 0
        }
    }
    elsasegment11dress11.on('pointerdown', elsasegment11dress11startclick)

    function elsasegment11dress11startclick() {
        handindication3.visible = false
        if (colorindication) {
            if (music.isPlaying == false) {
                clicksound.stop()
            } else {
                clicksound.play()
            }
            if (!handstart) {
                handindication1.visible = true
                handstart = true
            }
            elsasegment11dress11.alpha = 0
            elsaovrarr[11] = 0
            elsaclrarr2[11] = 0
        }
    }
    for (i = 1; i <= 17; i++) {
        game['color' + i].on('pointerover', elsacoloroverstartclick)
        game['color' + i].on('pointerout', elsacoloroutstartclick)
        game['color' + i].on('pointerdown', elsacolorstartclick)
    }

    function elsacoloroverstartclick() {
        if (elsaclrarr1[7] != this.texture.key.substr(5)) {
            game['color' + this.texture.key.substr(5)].setScale(1.05, 1.05)
        }
    }

    function elsacoloroutstartclick() {
        if (elsaclrarr1[7] != this.texture.key.substr(5)) {
            game['color' + this.texture.key.substr(5)].setScale(1, 1)
        }
    }
    elsaclrarr1[1] = 1
    elsaclrarr1[2] = 1
    elsaclrarr1[3] = 1
    elsaclrarr1[4] = 1
    elsaclrarr1[5] = 1
    elsaclrarr1[6] = 1
    elsaclrarr1[7] = 1
    elsaclrarr1[8] = 1
    elsaclrarr1[9] = 1
    elsaclrarr1[10] = 1
    elsaclrarr1[11] = 1
    tailorbtn.setScale(1.05, 1.05)
    game['color' + 1].setScale(1.05, 1.05)
    tailorbtn.setFrame(1)
    handindication = true
    elsaline.visible = true

    function elsacolorstartclick() {
        if (handindication) {
            if (music.isPlaying == false) {
                clicksound.stop()
            } else {
                clicksound.play()
            }
            sno = this.texture.key.substr(5)
            elsaclrarr1[1] = parseInt(sno)
            elsaclrarr1[2] = parseInt(sno)
            elsaclrarr1[3] = parseInt(sno)
            elsaclrarr1[4] = parseInt(sno)
            elsaclrarr1[5] = parseInt(sno)
            elsaclrarr1[6] = parseInt(sno)
            elsaclrarr1[7] = parseInt(sno)
            elsaclrarr1[8] = parseInt(sno)
            elsaclrarr1[9] = parseInt(sno)
            elsaclrarr1[10] = parseInt(sno)
            elsaclrarr1[11] = parseInt(sno)
            elsaovrarr[1] = 0
            elsaovrarr[2] = 0
            elsaovrarr[3] = 0
            elsaovrarr[4] = 0
            elsaovrarr[5] = 0
            elsaovrarr[6] = 0
            elsaovrarr[7] = 0
            elsaovrarr[7] = 0
            elsaovrarr[8] = 0
            elsaovrarr[9] = 0
            elsaovrarr[10] = 0
            elsaovrarr[11] = 0
            if (handindication2.visible) {
                handindication3.visible = true
                handindication2.visible = false
            }
            for (i = 1; i <= 17; i++) {
                game['color' + i].setScale(1, 1)
            }
            game['color' + elsaclrarr1[7]].setScale(1.05, 1.05)
        }
    }
    tailorbtn.on('pointerdown', tailorbtnstartclick)

    function tailorbtnstartclick() {
        if (music.isPlaying == false) {
            clicksound.stop()
        } else {
            clicksound.play()
        }
        if (!handindication) {
            colorindication = false
            elsaline.visible = true
            handindication = true
            tailorbtn.setScale(1.05, 1.05)
            cutbtn.setScale(1, 1)
            cutbtn.setFrame(0)
            tailorbtn.setFrame(1)
            if (handindication1.visible) {
                handindication1.visible = false
                handindication2.visible = true
            }
            elsahit1.setInteractive({
                pixelPerfect: true,
                useHandCursor: true
            })
            elsahit2.setInteractive({
                pixelPerfect: true,
                useHandCursor: true
            })
            elsahit3.setInteractive({
                pixelPerfect: true,
                useHandCursor: true
            })
            elsahit4.setInteractive({
                pixelPerfect: true,
                useHandCursor: true
            })
            elsahit5.setInteractive({
                pixelPerfect: true,
                useHandCursor: true
            })
            elsahit6.setInteractive({
                pixelPerfect: true,
                useHandCursor: true
            })
            elsahit7.setInteractive({
                pixelPerfect: true,
                useHandCursor: true
            })
            elsahit8.setInteractive({
                pixelPerfect: true,
                useHandCursor: true
            })
            elsahit9.setInteractive({
                pixelPerfect: true,
                useHandCursor: true
            })
            elsahit10.setInteractive({
                pixelPerfect: true,
                useHandCursor: true
            })
            elsahit11.setInteractive({
                pixelPerfect: true,
                useHandCursor: true
            })
            elsasegment1dress1.disableInteractive();
            elsasegment2dress2.disableInteractive();
            elsasegment3dress3.disableInteractive();
            elsasegment4dress4.disableInteractive();
            elsasegment5dress5.disableInteractive();
            elsasegment6dress6.disableInteractive();
            elsasegment7dress7.disableInteractive();
            elsasegment8dress8.disableInteractive();
            elsasegment9dress9.disableInteractive();
            elsasegment10dress10.disableInteractive();
            elsasegment11dress11.disableInteractive();
            elsaclickstart = false
        }
    }
    cutbtn.on('pointerdown', cutbtnstartclick)

    function cutbtnstartclick() {
        if (music.isPlaying == false) {
            clicksound.stop()
        } else {
            clicksound.play()
        }
        elsahit1.disableInteractive();
        elsahit2.disableInteractive();
        elsahit3.disableInteractive();
        elsahit4.disableInteractive();
        elsahit5.disableInteractive();
        elsahit6.disableInteractive();
        elsahit7.disableInteractive();
        elsahit8.disableInteractive();
        elsahit9.disableInteractive();
        elsahit10.disableInteractive();
        elsahit11.disableInteractive();
        elsasegment1dress1.setInteractive({
            pixelPerfect: true,
            useHandCursor: true
        })
        elsasegment2dress2.setInteractive({
            pixelPerfect: true,
            useHandCursor: true
        })
        elsasegment3dress3.setInteractive({
            pixelPerfect: true,
            useHandCursor: true
        })
        elsasegment4dress4.setInteractive({
            pixelPerfect: true,
            useHandCursor: true
        })
        elsasegment5dress5.setInteractive({
            pixelPerfect: true,
            useHandCursor: true
        })
        elsasegment6dress6.setInteractive({
            pixelPerfect: true,
            useHandCursor: true
        })
        elsasegment7dress7.setInteractive({
            pixelPerfect: true,
            useHandCursor: true
        })
        elsasegment8dress8.setInteractive({
            pixelPerfect: true,
            useHandCursor: true
        })
        elsasegment9dress9.setInteractive({
            pixelPerfect: true,
            useHandCursor: true
        })
        elsasegment10dress10.setInteractive({
            pixelPerfect: true,
            useHandCursor: true
        })
        elsasegment11dress11.setInteractive({
            pixelPerfect: true,
            useHandCursor: true
        })
        elsaline.visible = false
        handindication = false
        colorindication = true
        cutbtn.setScale(1.05, 1.05)
        tailorbtn.setScale(1, 1)
        cutbtn.setFrame(1)
        tailorbtn.setFrame(0)
        if (handindication4.visible) {
            handindication4.visible = false
            handindication3.visible = true
        }
    }
    rarrow.on('pointerdown', rarrowstartclick)

    function rarrowstartclick() {
        if (music.isPlaying == false) {
            clicksound.stop()
        } else {
            clicksound.play()
        }
        if (doll.frame.name == 0) {
            doll.setFrame(1)
        } else if (doll.frame.name == 1) {
            doll.setFrame(2)
        } else if (doll.frame.name == 2) {
            doll.setFrame(3)
        } else if (doll.frame.name == 3) {
            doll.setFrame(4)
        } else if (doll.frame.name == 4) {
            doll.setFrame(5)
        } else if (doll.frame.name == 5) {
            doll.setFrame(0)
        }
    }
    larrow.on('pointerdown', larrowstartclick)

    function larrowstartclick() {
        if (music.isPlaying == false) {
            clicksound.stop()
        } else {
            clicksound.play()
        }
        if (doll.frame.name == 0) {
            doll.setFrame(5)
        } else if (doll.frame.name == 5) {
            doll.setFrame(4)
        } else if (doll.frame.name == 4) {
            doll.setFrame(3)
        } else if (doll.frame.name == 3) {
            doll.setFrame(2)
        } else if (doll.frame.name == 2) {
            doll.setFrame(1)
        } else if (doll.frame.name == 1) {
            doll.setFrame(0)
        }
    }
}
var startgame8 = false
var handindication = false
var colorindication = false
var arielstorearr = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
var arielclrarr1 = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
var arielclrarr2 = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
var arielovrarr = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
var arieldresarr1 = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
var darr2 = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
var arielcolor = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
var colorcode = [, ' 0xff3838', ' 0xfe7d3b', ' 0xffe33b', ' 0x9cdc3b', ' 0x3ba13b', ' 0x3bd8c8', ' 0x3bdcff', ' 0x3b91ed', ' 0x3f3ce6', ' 0x7443f6', ' 0xfeb463', ' 0x9b9b9b', ' 0xb04ab1', ' 0xff8be6', '0xed3b8a', ' 0xfff2f2', ' 0x3b2e2e']
var Level6 = new Phaser.Class({
    Extends: Phaser.Scene,
    initialize: function Level6() {
        Phaser.Scene.call(this, {
            key: 'Level6'
        });
    },
    preload: function () {
        colorcode = [, ' 0xff3838', ' 0xfe7d3b', ' 0xffe33b', ' 0x9cdc3b', ' 0x3ba13b', ' 0x3bd8c8', ' 0x3bdcff', ' 0x3b91ed', ' 0x3f3ce6', ' 0x7443f6', ' 0xfeb463', ' 0x9b9b9b', ' 0xb04ab1', ' 0xff8be6', '0xed3b8a', ' 0xfff2f2', ' 0x3b2e2e']
        darr2 = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        arielcolor = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        startgame8 = false
        loadFinish = false
        clrarr = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        arielclrarr1 = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        arielclrarr2 = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        arielovrarr = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        arieldresarr1 = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        arielstorearr = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        handindication = false
        colorindication = false
    },
    create: function () {
        arielbackground1 = this.add.image(400, 300, 'arielbackground1').setOrigin(0.5, 0.5)
        dressuppanel = this.add.image(26, 40, 'dressuppanel').setOrigin(0, 0)
        colortext = this.add.image(83, 302, 'colortext').setOrigin(0, 0)
        tailorbtn = this.add.image(81, 136, 'tailorbtn').setOrigin(0.5, 0.5).setInteractive({
            pixelPerfect: true,
            useHandCursor: true,
        })
        tailorbtn.x += parseFloat(tailorbtn.width / 2)
        tailorbtn.y += parseFloat(tailorbtn.height / 2)
        cutbtn = this.add.image(150, 221, 'cutbtn').setOrigin(0.5, 0.5).setInteractive({
            pixelPerfect: true,
            useHandCursor: true,
        })
        cutbtn.x += parseFloat(cutbtn.width / 2)
        cutbtn.y += parseFloat(cutbtn.height / 2)
        var coloxarr = [, 80, 130, 178, 228, 277, 328, 79, 129, 178, 228, 276, 326, 128, 179, 229, 279, 327]
        var coloyarr = [, 369, 370, 370, 370, 370, 370, 424, 424, 424, 424, 424, 424, 477, 477, 477, 477, 477]
        for (i = 1; i <= 17; i++) {
            game['color' + i] = this.add.image(coloxarr[i], coloyarr[i], 'color' + i).setOrigin(0.5, 0.5).setInteractive({
                pixelPerfect: true,
                useHandCursor: true,
            })
            game['color' + i].x += parseFloat(game['color' + i].width / 2)
            game['color' + i].y += parseFloat(game['color' + i].height / 2)
        }
        dressphotoframe = this.add.image(251, 89, 'dressphotoframe').setOrigin(0, 0)
        rarrow = this.add.image(412, 143, 'arrow').setOrigin(0, 0).setInteractive({
            pixelPerfect: true,
            useHandCursor: true,
        })
        larrow = this.add.image(262, 147, 'arrow').setOrigin(0, 0).setInteractive({
            pixelPerfect: true,
            useHandCursor: true,
        })
        larrow.setScale(-1, 1)
        larrow.angle = -7
        this.tweens.add({
            targets: rarrow,
            x: 416,
            ease: 'Linear',
            duration: 400,
            repeat: -1,
            yoyo: true,
        })
        this.tweens.add({
            targets: larrow,
            x: 258,
            ease: 'Linear',
            duration: 400,
            repeat: -1,
            yoyo: true,
        })
        doll = this.add.image(258, 77, 'arieldoll').setOrigin(0, 0)
        arielbhair = this.add.image(140, 112, 'arielbhair').setOrigin(0, 0)
        arielbody = this.add.image(73, 43, 'arielbody').setOrigin(0, 0)
        ariellips = this.add.image(161, 105, 'ariellips').setOrigin(0, 0)
        arieleye1 = this.add.image(161, 83, 'arieleye1').setOrigin(0, 0)
        arieleye2 = this.add.image(161, 83, 'arieleye2').setOrigin(0, 0)
        arielblush = this.add.image(156, 96, 'arielblush').setOrigin(0, 0)
        arieltoplayer = this.add.image(158, 79, 'arieltoplayer').setOrigin(0, 0)
        arieleshade = this.add.image(154, 80, 'arieleshade').setOrigin(0, 0)
        arielelash = this.add.image(153, 80, 'arielelash').setOrigin(0, 0)
        arielebrow = this.add.image(157, 72, 'arielebrow').setOrigin(0, 0)
        arielhair = this.add.image(124, 28, 'arielhair').setOrigin(0, 0)
        arielsegment1dress1 = this.add.image(130, 145, 'arielsegment1dress1').setOrigin(0, 0)
        arielsegment2dress2 = this.add.image(165, 198, 'arielsegment2dress2').setOrigin(0, 0)
        arielsegment3dress3 = this.add.image(121, 224, 'arielsegment3dress3').setOrigin(0, 0)
        arielsegment4dress4 = this.add.image(96, 333, 'arielsegment4dress4').setOrigin(0, 0)
        arielsegment41dress4 = this.add.image(96, 333, 'arielsegment4dress4').setOrigin(0, 0)
        arielsegment5dress5 = this.add.image(99, 424, 'arielsegment5dress5').setOrigin(0, 0)
        arielsegment6dress6 = this.add.image(79, 257, 'arielsegment6dress6').setOrigin(0, 0)
        arielsegment7dress7 = this.add.image(78, 330, 'arielsegment7dress7').setOrigin(0, 0)
        arielsegment8dress8 = this.add.image(61, 429, 'arielsegment8dress8').setOrigin(0, 0)
        arielsegment9dress9 = this.add.image(211, 254, 'arielsegment9dress9').setOrigin(0, 0)
        arielsegment10dress10 = this.add.image(224, 325, 'arielsegment10dress10').setOrigin(0, 0)
        arielsegment11dress11 = this.add.image(247, 414, 'arielsegment11dress11').setOrigin(0, 0)
        arielelash.setFrame(marr2[0])
        arielblush.setFrame(marr2[1])
        ariellips.setFrame(marr2[2])
        arieleye1.setFrame(marr2[3])
        arieleye2.setFrame(marr2[3])
        arieleshade.setFrame(marr2[4])
        arielebrow.setFrame(marr2[5])
        arielhair.setFrame(marr2[6])
        if (marr2[6] > 0) {
            arielbhair.setFrame(1)
        } else {
            arielbhair.setFrame(0)
        }
        var arieldollgroup = this.add.container(0, 0);
        arieldollgroup.add(arielsegment11dress11)
        arieldollgroup.add(arielsegment10dress10)
        arieldollgroup.add(arielsegment9dress9)
        arieldollgroup.add(arielsegment8dress8)
        arieldollgroup.add(arielsegment7dress7)
        arieldollgroup.add(arielsegment6dress6)
        arieldollgroup.add(arielbhair)
        arieldollgroup.add(arielbody)
        arieldollgroup.add(ariellips)
        arieldollgroup.add(arieleye1)
        arieldollgroup.add(arieleye2)
        arieldollgroup.add(arieltoplayer)
        arieldollgroup.add(arieleshade)
        arieldollgroup.add(arielelash)
        arieldollgroup.add(arielblush)
        arieldollgroup.add(arielebrow)
        arieldollgroup.add(arielsegment1dress1)
        arieldollgroup.add(arielsegment2dress2)
        arieldollgroup.add(arielsegment5dress5)
        arieldollgroup.add(arielsegment41dress4)
        arieldollgroup.add(arielsegment3dress3)
        arieldollgroup.add(arielsegment4dress4)
        arieldollgroup.add(arielhair)
        arieldollgroup.x = 420
        arieldollgroup.y = 20
        arielsegment41dress4.visible = false
        arielline = this.add.image(445, 180, 'elsaline').setOrigin(0, 0)
        arielline.visible = false
        arielhit1 = this.add.image(624.95, 220, 'elsahit1').setOrigin(0.5, 0.5).setInteractive({
            pixelperfect: true,
            useHandCursor: true
        })
        arielhit2 = this.add.image(625.95, 258, 'elsahit1').setOrigin(0.5, 0.5).setInteractive({
            pixelperfect: true,
            useHandCursor: true
        })
        arielhit2.setScale(0.64)
        arielhit3 = this.add.image(617.95, 337, 'elsahit1').setOrigin(0.5, 0.5).setInteractive({
            pixelperfect: true,
            useHandCursor: true
        })
        arielhit3.setScale(2.02)
        arielhit3.angle = 90
        arielhit4 = this.add.image(624.95, 453, 'elsahit1').setOrigin(0.5, 0.5).setInteractive({
            pixelperfect: true,
            useHandCursor: true
        })
        arielhit4.setScale(1.86)
        arielhit5 = this.add.image(627.95, 552, 'elsahit1').setOrigin(0.5, 0.5).setInteractive({
            pixelperfect: true,
            useHandCursor: true
        })
        arielhit5.setScale(2.25)
        arielhit6 = this.add.image(550.15, 342, 'elsahit1').setOrigin(0.5, 0.5).setInteractive({
            pixelperfect: true,
            useHandCursor: true
        })
        arielhit6.setScale(0.87)
        arielhit6.angle = 108
        arielhit7 = this.add.image(528.05, 425, 'elsahit1').setOrigin(0.5, 0.5).setInteractive({
            pixelperfect: true,
            useHandCursor: true
        })
        arielhit7.setScale(1.2)
        arielhit7.angle = 108
        arielhit8 = this.add.image(506.95, 519.7, 'elsahit1').setOrigin(0.5, 0.5).setInteractive({
            pixelperfect: true,
            useHandCursor: true
        })
        arielhit8.setScale(1.29)
        arielhit8.angle = 101
        arielhit9 = this.add.image(694.05, 345, 'elsahit1').setOrigin(0.5, 0.5).setInteractive({
            pixelperfect: true,
            useHandCursor: true
        })
        arielhit9.setScale(1.02)
        arielhit9.angle = 80
        arielhit10 = this.add.image(712.05, 425, 'elsahit1').setOrigin(0.5, 0.5).setInteractive({
            pixelperfect: true,
            useHandCursor: true
        })
        arielhit10.setScale(1.2)
        arielhit10.angle = 73
        arielhit11 = this.add.image(748.95, 510.7, 'elsahit1').setOrigin(0.5, 0.5).setInteractive({
            pixelperfect: true,
            useHandCursor: true
        })
        arielhit11.setScale(1.29)
        arielhit11.angle = 63
        var tween1 = this.tweens.add({
            targets: arielline,
            alpha: 0.3,
            ease: 'Linear',
            duration: 1000,
            repeat: -1,
            yoyo: true,
        });
        var tween1 = this.tweens.add({
            targets: arieleye1,
            x: arieleye1.x + 2,
            ease: 'Linear',
            duration: 300,
            delay: 2400,
            callbackScope: this,
        });
        var tween2 = this.tweens.add({
            targets: arieleye2,
            x: arieleye2.x + 2,
            ease: 'Linear',
            duration: 300,
            delay: 2400,
            onComplete: arieleyestartani1,
            callbackScope: this
        });

        function arieleyestartani1() {
            this.tweens.add({
                targets: arieleye1,
                x: arieleye1.x - 2,
                ease: 'Linear',
                duration: 300,
                delay: 3000,
                callbackScope: this
            });
            this.tweens.add({
                targets: arieleye2,
                x: arieleye2.x - 2,
                ease: 'Linear',
                duration: 300,
                delay: 3000,
                onComplete: arieleyestartani2,
                callbackScope: this,
            })
        }

        function arieleyestartani2() {
            this.tweens.add({
                targets: arieleye1,
                x: arieleye1.x + 2,
                ease: 'Linear',
                duration: 300,
                delay: 2400,
                callbackScope: this
            });
            this.tweens.add({
                targets: arieleye2,
                x: arieleye2.x + 2,
                ease: 'Linear',
                duration: 300,
                delay: 2400,
                onComplete: arieleyestartani1,
                callbackScope: this,
            })
        }
        arieleshad = this.time.addEvent({
            delay: 1000,
            callback: arieleblinkstart1,
            callbackScope: this
        });

        function arieleblinkstart1() {
            arieleshade.setFrame(marr2[4] + 1)
            arielelash.visible = false
            arieleshad = this.time.addEvent({
                delay: 200,
                callback: arieleblinkstart2,
                callbackScope: this
            });
        }

        function arieleblinkstart2() {
            arieleshade.setFrame(marr2[4])
            arielelash.visible = true
            arieleshad = this.time.addEvent({
                delay: 4000,
                callback: arieleblinkstart1,
                callbackScope: this
            });
        }
        ariellip = this.time.addEvent({
            delay: 1000,
            callback: ariellipsstart1,
            callbackScope: this
        });

        function ariellipsstart1() {
            ariellips.setFrame(marr2[2] + 1)
            ariellip = this.time.addEvent({
                delay: 100,
                callback: ariellipsstart2,
                callbackScope: this
            });
        }

        function ariellipsstart2() {
            ariellips.setFrame(marr2[2] + 2)
            ariellip = this.time.addEvent({
                delay: 2500,
                callback: ariellipsstart3,
                callbackScope: this
            });
        }

        function ariellipsstart3() {
            ariellips.setFrame(marr2[2] + 1)
            ariellip = this.time.addEvent({
                delay: 100,
                callback: ariellipsstart4,
                callbackScope: this
            });
        }

        function ariellipsstart4() {
            ariellips.setFrame(marr2[2])
            ariellip = this.time.addEvent({
                delay: 3000,
                callback: ariellipsstart1,
                callbackScope: this
            });
        }
        clicksound = this.sound.add('clickss');
        done = this.add.image(790, 590, 'uiButton').setOrigin(1, 1).setInteractive({
            pixelPerfect: true,
            useHandCursor: true
        })
        done.setFrame("button_next")
        done.visible = false
        done.on('pointerover', doneoverclick1);
        done.on('pointerout', doneoutclick1);
        done.on('pointerdown', donedownclick1);
        done.on('pointerup', doneupclick1);

        function doneoverclick1() {
            done.setScale(1.05, 1.05)
        }

        function doneoutclick1() {
            done.setScale(1, 1)
        }

        function doneupclick1() {
            if (startgame8) {
                done.setScale(1.05, 1.05)
            }
        }

        function donedownclick1() {
            if (!startgame8 && loadFinish) {
                startgame8 = true
                level = 5
                if (music.isPlaying == false) {
                    clicksound.stop()
                } else {
                    clicksound.play()
                }
                done.setScale(1, 1)
                arielstorearr[0] = arielsegment11dress11.frame.name
                arielstorearr[1] = arielsegment10dress10.frame.name
                arielstorearr[2] = arielsegment9dress9.frame.name
                arielstorearr[3] = arielsegment8dress8.frame.name
                arielstorearr[4] = arielsegment7dress7.frame.name
                arielstorearr[5] = arielsegment6dress6.frame.name
                arielstorearr[6] = arielsegment1dress1.frame.name
                arielstorearr[7] = arielsegment5dress5.frame.name
                arielstorearr[8] = arielsegment4dress4.frame.name
                arielstorearr[8] = arielsegment41dress4.frame.name
                arielstorearr[9] = arielsegment3dress3.frame.name
                arielstorearr[10] = arielsegment2dress2.frame.name
                arielstorearr[11] = arielsegment11dress11.alpha
                arielstorearr[12] = arielsegment10dress10.alpha
                arielstorearr[13] = arielsegment9dress9.alpha
                arielstorearr[14] = arielsegment8dress8.alpha
                arielstorearr[15] = arielsegment7dress7.alpha
                arielstorearr[16] = arielsegment6dress6.alpha
                arielstorearr[17] = arielsegment1dress1.alpha
                arielstorearr[18] = arielsegment5dress5.alpha
                arielstorearr[19] = arielsegment4dress4.alpha
                arielstorearr[19] = arielsegment41dress4.alpha
                arielstorearr[20] = arielsegment3dress3.alpha
                arielstorearr[21] = arielsegment2dress2.alpha
                arielstorearr[22] = arielsegment4dress4.visible
                transitionbackground1.alpha = 1
                transitionbackground2.alpha = 1
                transitionbackground3.alpha = 1
                transitionpanel.alpha = 1
                transitiondress.alpha = 1
                transitiontitle1.alpha = 1
                transitiontitle2.alpha = 1
                transitiontitle3.alpha = 1
                transitionstar1.alpha = 1
                transitionstar2.alpha = 1
                transitionstar3.alpha = 1
                transitionstar4.alpha = 1
                transitionstar5.alpha = 1
                transitionstar1.setScale(0)
                transitionstar2.setScale(0)
                transitionstar3.setScale(0)
                transitionstar4.setScale(0)
                transitionstar5.setScale(0)
                transitionbackground1.visible = true
                transitionbackground2.visible = true
                transitionbackground3.visible = true
                transitiondress.visible = false
                transitionpanel.visible = false
                transitiontitle1.visible = false
                transitiontitle2.visible = false
                transitiontitle3.visible = false
                transitionstar1.visible = false
                transitionstar2.visible = false
                transitionstar3.visible = false
                transitionstar4.visible = false
                transitionstar5.visible = false
                transitionbackground1.alpha = 0
                transitionbackground2.alpha = 0
                transitionbackground3.alpha = 0
                this.scene.tweens.add({
                    targets: transitionbackground1,
                    alpha: 1,
                    ease: 'Linear',
                    duration: 800,
                    onComplete: transitionstart1,
                    callbackScope: this
                })

                function transitionstart1() {
                    transitiondress.visible = true
                    transitiondress.alpha = 0
                    this.scene.tweens.add({
                        targets: transitiondress,
                        alpha: 1,
                        ease: 'Linear',
                        duration: 400,
                        onComplete: transitionstart2,
                        callbackScope: this
                    })
                    this.scene.tweens.add({
                        targets: transitionbackground2,
                        alpha: 1,
                        ease: 'Linear',
                        duration: 800,
                    })
                }

                function transitionstart2() {
                    transitionpanel.visible = true
                    transitionpanel.setScale(0)
                    this.scene.tweens.add({
                        targets: transitionpanel,
                        scaleX: 1,
                        scaleY: 1,
                        ease: 'Linear',
                        duration: 500,
                        onComplete: transitionstart3,
                        callbackScope: this
                    })
                }

                function transitionstart3() {
                    this.scene.tweens.add({
                        targets: transitionbackground3,
                        alpha: 1,
                        ease: 'Linear',
                        duration: 800,
                        onComplete: transitionstart4,
                        callbackScope: this
                    })
                }

                function transitionstart4() {
                    transitiontitle1.visible = true
                    transitiontitle2.visible = true
                    transitiontitle3.visible = true
                    transitiontitle1.alpha = 0
                    transitiontitle2.alpha = 0
                    transitiontitle3.alpha = 0
                    transitiontitle2.x = transitiontitle2.x + 100
                    transitiontitle3.x = transitiontitle3.x - 100
                    this.scene.tweens.add({
                        targets: transitiontitle1,
                        alpha: 1,
                        ease: 'Linear',
                        duration: 800,
                        onComplete: transitionstart5,
                        callbackScope: this
                    })
                    this.scene.tweens.add({
                        targets: transitiontitle2,
                        alpha: 1,
                        x: transitiontitle2.x - 100,
                        ease: 'Linear',
                        duration: 800,
                    })
                    this.scene.tweens.add({
                        targets: transitiontitle3,
                        alpha: 1,
                        x: transitiontitle3.x + 100,
                        ease: 'Linear',
                        duration: 800,
                    })
                }

                function transitionstart5() {
                    transitionstar1.setScale(0)
                    transitionstar2.setScale(0)
                    transitionstar3.setScale(0)
                    transitionstar4.setScale(0)
                    transitionstar5.setScale(0)
                    transitionstar1.visible = true
                    transitionstar2.visible = true
                    transitionstar3.visible = true
                    transitionstar4.visible = true
                    transitionstar5.visible = true
                    this.scene.tweens.add({
                        targets: transitionstar1,
                        scaleX: 1,
                        scaleY: 1,
                        ease: 'Linear',
                        duration: 300,
                    })
                    this.scene.tweens.add({
                        targets: transitionstar2,
                        scaleX: 0.9,
                        scaleY: 0.9,
                        ease: 'Linear',
                        duration: 300,
                    })
                    this.scene.tweens.add({
                        targets: transitionstar3,
                        scaleX: 0.9,
                        scaleY: 0.9,
                        ease: 'Linear',
                        duration: 300,
                    })
                    this.scene.tweens.add({
                        targets: transitionstar4,
                        scaleX: 0.8,
                        scaleY: 0.8,
                        ease: 'Linear',
                        duration: 300,
                    })
                    this.scene.tweens.add({
                        targets: transitionstar5,
                        scaleX: 0.65,
                        scaleY: 0.65,
                        ease: 'Linear',
                        duration: 300,
                        onComplete: transitionstar6,
                        callbackScope: this
                    })
                }

                function transitionstar6() {
                    this.scene.scene.stop('Level6')
                    game.scene.run('levelSelect');
                }
            }
        }
        mute = this.add.sprite(790, 10, 'uiButton').setFrame("button_music_on").setInteractive({
            pixelPerfect: true,
            useHandCursor: true
        })
        mute.setOrigin(1, 0)
        mute.on('pointerdown', function () {
            if (!isMuted) {
                isMuted = true
                music.pause()
                mute.setFrame("button_music_off")
            } else {
                isMuted = false
                music.resume()
                mute.setFrame("button_music_on")
            }
            var tween2 = this.tweens.add({
                targets: mute,
                scaleX: 0.9,
                scaleY: 0.9,
                ease: 'Linear',
                duration: 80,
                yoyo: true,
                repeat: 1,
            });
            if (music.isPlaying == false) {
                clicksound.stop()
            } else {
                clicksound.play()
            }
        }, this);
        if (music.isPlaying == false) {
            mute.setFrame("button_music_off")
        } else {
            mute.setFrame("button_music_on")
        }
        if (brandingMaterials.inGameLogo.display == true && brandingMaterials.inGameLogo.image != undefined) {
            inGameLogo1 = this.add.image(10, 590, 'inGameLogo1').setOrigin(0, 1).setInteractive({
                pixelPerfect: true,
                useHandCursor: true
            })
            inGameLogo1.on('pointerdown', brandingMaterials.inGameLogo.clickURL);
            inGameLogoani = this.time.addEvent({
                delay: 2000,
                callback: inGameLogoani,
                callbackScope: this
            });

            function inGameLogoani() {
                this.tweens.add({
                    targets: inGameLogo1,
                    scaleX: 1.05,
                    scaleY: 1.05,
                    ease: 'Linear',
                    duration: 100,
                    repeat: 1,
                    yoyo: true,
                    delay: 3000,
                    onComplete: inGameLogoani1,
                    callbackScope: this
                })
            }

            function inGameLogoani1() {
                this.tweens.add({
                    targets: inGameLogo1,
                    scaleX: 1.05,
                    scaleY: 1.05,
                    ease: 'Linear',
                    duration: 100,
                    repeat: 1,
                    yoyo: true,
                    delay: 3000,
                    onComplete: inGameLogoani1,
                    callbackScope: this
                })
            }
        }
        transitionbackground1 = this.add.image(400, 300, 'transitionbackground1').setOrigin(0.5, 0.5).setInteractive({
            pixelPerfect: true,
            useHandCursor: true
        })
        transitionbackground2 = this.add.image(400, 300, 'transitionbackground2').setOrigin(0.5, 0.5)
        transitionbackground3 = this.add.image(400, 300, 'transitionbackground3').setOrigin(0.5, 0.5)
        transitionpanel = this.add.image(393.5, 314, 'transitionpanel').setOrigin(0.5)
        transitiondress = this.add.image(267, 141, 'transitiondress').setOrigin(0, 0)
        transitiontitle1 = this.add.image(400, 329, 'title1');
        transitiontitle1.setOrigin(0.5);
        transitiontitle2 = this.add.image(380, 399, 'title2');
        transitiontitle2.setOrigin(0.5);
        transitiontitle3 = this.add.image(380, 459, 'title3');
        transitiontitle3.setOrigin(0.5);
        transitionstar1 = this.add.image(616.5, 168, 'transitionstar');
        transitionstar1.setOrigin(0.5);
        transitionstar2 = this.add.image(156.5, 168, 'transitionstar');
        transitionstar2.setOrigin(0.5);
        transitionstar2.setScale(0.9)
        transitionstar3 = this.add.image(596.5, 468, 'transitionstar');
        transitionstar3.setOrigin(0.5);
        transitionstar3.setScale(0.9)
        transitionstar4 = this.add.image(116.5, 468, 'transitionstar');
        transitionstar4.setOrigin(0.5);
        transitionstar4.setScale(0.8)
        transitionstar5 = this.add.image(400, 558, 'transitionstar');
        transitionstar5.setOrigin(0.5);
        transitionstar5.setScale(0.65)
        this.tweens.add({
            targets: transitionstar1,
            angle: 4,
            ease: 'Linear',
            duration: 300,
            repeat: -1,
            yoyo: true
        })
        this.tweens.add({
            targets: transitionstar2,
            angle: 4,
            ease: 'Linear',
            duration: 300,
            repeat: -1,
            yoyo: true
        })
        this.tweens.add({
            targets: transitionstar3,
            angle: 4,
            ease: 'Linear',
            duration: 300,
            repeat: -1,
            yoyo: true
        })
        this.tweens.add({
            targets: transitionstar4,
            angle: 4,
            ease: 'Linear',
            duration: 300,
            repeat: -1,
            yoyo: true
        })
        this.tweens.add({
            targets: transitionstar5,
            angle: 4,
            ease: 'Linear',
            duration: 300,
            repeat: -1,
            yoyo: true
        })
        this.time.addEvent({
            delay: 1000,
            callback: transitionstart11,
            callbackScope: this
        });

        function transitionstart11() {
            this.tweens.add({
                targets: transitionbackground1,
                alpha: 0,
                ease: 'Linear',
                duration: 700
            })
            this.tweens.add({
                targets: transitionbackground2,
                alpha: 0,
                ease: 'Linear',
                duration: 700
            })
            this.tweens.add({
                targets: transitionbackground3,
                alpha: 0,
                ease: 'Linear',
                duration: 700
            })
            this.tweens.add({
                targets: transitionpanel,
                alpha: 0,
                ease: 'Linear',
                duration: 700
            })
            this.tweens.add({
                targets: transitiondress,
                alpha: 0,
                ease: 'Linear',
                duration: 700
            })
            this.tweens.add({
                targets: transitiontitle1,
                alpha: 0,
                ease: 'Linear',
                duration: 700
            })
            this.tweens.add({
                targets: transitiontitle2,
                alpha: 0,
                ease: 'Linear',
                duration: 700
            })
            this.tweens.add({
                targets: transitiontitle3,
                alpha: 0,
                ease: 'Linear',
                duration: 700
            })
            this.tweens.add({
                targets: transitionstar1,
                alpha: 0,
                ease: 'Linear',
                duration: 700
            })
            this.tweens.add({
                targets: transitionstar2,
                alpha: 0,
                ease: 'Linear',
                duration: 700
            })
            this.tweens.add({
                targets: transitionstar3,
                alpha: 0,
                ease: 'Linear',
                duration: 700
            })
            this.tweens.add({
                targets: transitionstar4,
                alpha: 0,
                ease: 'Linear',
                duration: 700
            })
            this.tweens.add({
                targets: transitionstar5,
                alpha: 0,
                ease: 'Linear',
                duration: 700
            })
        }
        dressup2start()
        hitobject = this.add.image(400, 300, 'hitobject').setOrigin(0.5, 0.5).setInteractive({
            useHandCursor: true
        })
        hitobject.visible = false
        hitobject.on('pointerdown', hitobjectdown)

        function hitobjectdown() {}
        this.load.on('complete', function () {
            loadFinish = true
        });
        this.load.start();
    }
});

function dressup2start() {
    arielclrarr1[1] = 1
    arielclrarr1[2] = 1
    arielclrarr1[3] = 1
    arielclrarr1[4] = 1
    arielclrarr1[5] = 1
    arielclrarr1[6] = 1
    arielclrarr1[7] = 1
    arielclrarr1[8] = 1
    arielclrarr1[9] = 1
    arielclrarr1[10] = 1
    arielclrarr1[11] = 1
    tailorbtn.setScale(1.05, 1.05)
    game['color' + 1].setScale(1.05, 1.05)
    tailorbtn.setFrame(1)
    handindication = true
    arielline.visible = true
    arielhit1.on('pointerover', arielsegment1overstartclick)
    arielhit1.on('pointerout', arielsegment1outstartclick)
    arielhit1.on('pointerdown', arielsegment1startclick)

    function arielsegment1overstartclick() {
        if (arielclrarr1[1] > 0 && (colorindication || arielclrarr1[1] != arielclrarr2[1] || arielsegment1dress1.frame.name != doll.frame.name + 1)) {
            arielsegment1dress1.alpha = 0.6
            if (!colorindication) {
                arielsegment1dress1.tint = colorcode[arielclrarr1[1]]
            } else {
                arielsegment1dress1.tint = colorcode[arielclrarr2[1]]
            }
            if (colorindication) {} else {
                arielsegment1dress1.setFrame(doll.frame.name + 1)
            }
            if (arielovrarr[1] == 0) {
                arielsegment1dress1.alpha = 0.6
                if (!colorindication) {
                    arielsegment1dress1.tint = colorcode[arielclrarr1[1]]
                } else if (arielclrarr2[1] == 0) {
                    arielsegment1dress1.alpha = 0
                    arielsegment1dress1.tint = colorcode[arielclrarr2[1]]
                }
            }
        } else if (colorindication && arielclrarr2[1] > 0) {
            arielsegment1dress1.alpha = 0.6
        }
    }

    function arielsegment1outstartclick() {
        if (arielclrarr1[1] > 0 && (colorindication || arielclrarr1[1] != arielclrarr2[1] || arielsegment1dress1.frame.name == doll.frame.name + 1)) {
            if (arielovrarr[1] == 0 && arielclrarr2[1] == 0) {
                arielsegment1dress1.alpha = 0
            } else {
                arielsegment1dress1.alpha = 1
                arielsegment1dress1.tint = colorcode[arielclrarr2[1]]
                arielsegment1dress1.setFrame(arieldresarr1[1])
            }
        } else if (colorindication && arielclrarr2[1] > 0) {
            arielsegment1dress1.alpha = 1
        } else if (colorindication) {
            arielsegment1dress1.alpha = 0
        }
    }

    function arielsegment1startclick() {
        if (music.isPlaying == false) {
            clicksound.stop()
        } else {
            clicksound.play()
        }
        if (colorindication) {
            arielovrarr[1] = 0
            arielclrarr1[1] = 0
            arielclrarr2[1] = 0
            arielsegment1dress1.alpha = 0
        }
        if (arielclrarr1[1] > 0) {
            done.visible = true
            arielsegment1dress1.alpha = 1
            arieldresarr1[1] = 1
            arielsegment1dress1.tint = colorcode[arielclrarr1[1]]
            arielsegment1dress1.setFrame(doll.frame.name + 1)
            arieldresarr1[1] = arielsegment1dress1.frame.name
            arielovrarr[1] = 1
            arielcolor[1] = colorcode[arielclrarr1[1]]
            arielclrarr2[1] = arielclrarr1[1]
        } else {
            arielsegment1dress1.alpha = 0
        }
    }
    arielhit2.on('pointerover', arielsegment2overstartclick)
    arielhit2.on('pointerout', arielsegment2outstartclick)
    arielhit2.on('pointerdown', arielsegment2startclick)

    function arielsegment2overstartclick() {
        if (arielclrarr1[2] > 0 && (doll.frame.name + 1 != 1 && doll.frame.name + 1 != 3) && (colorindication || arielclrarr1[2] != arielclrarr2[2] || arielsegment2dress2.frame.name != doll.frame.name + 1)) {
            arielsegment2dress2.alpha = 0.6
            if (!colorindication) {
                arielsegment2dress2.tint = colorcode[arielclrarr1[2]]
            } else {
                arielsegment2dress2.tint = colorcode[arielclrarr2[2]]
            }
            if (colorindication) {} else {
                arielsegment2dress2.setFrame(doll.frame.name + 1)
            }
            if (arielovrarr[2] == 0) {
                arielsegment2dress2.alpha = 0.6
                if (!colorindication) {
                    arielsegment2dress2.tint = colorcode[arielclrarr1[2]]
                } else if (arielclrarr2[2] == 0) {
                    arielsegment2dress2.alpha = 0
                    arielsegment2dress2.tint = colorcode[arielclrarr2[2]]
                }
            }
        } else if (colorindication && arielclrarr2[2] > 0) {
            arielsegment2dress2.alpha = 0.6
        }
    }

    function arielsegment2outstartclick() {
        if (arielclrarr1[2] > 0 && (doll.frame.name + 1 != 1 && doll.frame.name + 1 != 3) && (colorindication || arielclrarr1[2] != arielclrarr2[2] || arielsegment2dress2.frame.name == doll.frame.name + 1)) {
            if (arielovrarr[2] == 0 && (arielclrarr2[2] == 0)) {
                arielsegment2dress2.alpha = 0
            } else {
                arielsegment2dress2.alpha = 1
                arielsegment2dress2.tint = colorcode[arielclrarr2[2]]
                arielsegment2dress2.setFrame(arieldresarr1[2])
            }
        } else if (colorindication && arielclrarr2[2] > 0) {
            arielsegment2dress2.alpha = 1
        } else if (colorindication) {
            arielsegment2dress2.alpha = 0
        }
    }

    function arielsegment2startclick() {
        if (music.isPlaying == false) {
            clicksound.stop()
        } else {
            clicksound.play()
        }
        if (colorindication) {
            arielovrarr[2] = 0
            arielclrarr1[2] = 0
            arielclrarr2[2] = 0
            arielsegment2dress2.alpha = 0
        }
        if (doll.frame.name + 1 != 1 && doll.frame.name + 1 != 3) {
            if (arielclrarr1[2] > 0) {
                done.visible = true
                arielsegment2dress2.alpha = 1
                arieldresarr1[2] = 1
                arielsegment2dress2.setFrame(doll.frame.name + 1)
                arielsegment2dress2.tint = colorcode[arielclrarr1[2]]
                arieldresarr1[2] = arielsegment2dress2.frame.name
                arielovrarr[2] = 1
                arielcolor[2] = colorcode[arielclrarr1[2]]
                arielclrarr2[2] = arielclrarr1[2]
            } else {
                arielsegment2dress2.alpha = 0
            }
        }
    }
    arielhit3.on('pointerover', arielsegment3overstartclick)
    arielhit3.on('pointerout', arielsegment3outstartclick)
    arielhit3.on('pointerdown', arielsegment3startclick)

    function arielsegment3overstartclick() {
        if (arielclrarr1[3] > 0 && (colorindication || arielclrarr1[3] != arielclrarr2[3] || (arielsegment3dress3.frame.name != doll.frame.name + 1))) {
            arielsegment3dress3.alpha = 0.6
            if (!colorindication) {
                arielsegment3dress3.tint = colorcode[arielclrarr1[3]]
            } else {
                arielsegment3dress3.tint = colorcode[arielclrarr2[3]]
            }
            if (colorindication) {} else {
                arielsegment3dress3.setFrame(doll.frame.name + 1)
            }
            if (arielovrarr[3] == 0) {
                arielsegment3dress3.alpha = 0.6
                if (!colorindication) {
                    arielsegment3dress3.tint = colorcode[arielclrarr1[3]]
                } else if (arielclrarr2[3] == 0) {
                    arielsegment3dress3.alpha = 0
                    arielsegment3dress3.tint = colorcode[arielclrarr2[3]]
                }
            }
        } else if (colorindication && arielclrarr2[3] > 0) {
            arielsegment3dress3.alpha = 0.6
        }
    }

    function arielsegment3outstartclick() {
        if (arielclrarr1[3] > 0 && (colorindication || arielclrarr1[3] != arielclrarr2[3] || (arielsegment3dress3.frame.name == doll.frame.name + 1))) {
            if (arielovrarr[3] == 0 && arielclrarr2[3] == 0) {
                arielsegment3dress3.alpha = 0
            } else {
                arielsegment3dress3.alpha = 1
                arielsegment3dress3.tint = colorcode[arielclrarr2[3]]
                arielsegment3dress3.setFrame(arieldresarr1[3])
            }
        } else if (colorindication && arielclrarr2[3] > 0) {
            arielsegment3dress3.alpha = 1
        } else if (colorindication) {
            arielsegment3dress3.alpha = 0
        }
    }

    function arielsegment3startclick() {
        if (music.isPlaying == false) {
            clicksound.stop()
        } else {
            clicksound.play()
        }
        if (colorindication) {
            arielovrarr[3] = 0
            arielclrarr1[3] = 0
            arielclrarr2[3] = 0
            arielsegment3dress3.alpha = 0
        }
        if (arielclrarr1[3] > 0) {
            done.visible = true
            arielsegment3dress3.alpha = 1
            arieldresarr1[3] = 1
            arielsegment3dress3.setFrame(doll.frame.name + 1)
            arielsegment3dress3.tint = colorcode[arielclrarr1[3]]
            arieldresarr1[3] = arielsegment3dress3.frame.name
            arielovrarr[3] = 1
            arielcolor[3] = colorcode[arielclrarr1[3]]
            arielclrarr2[3] = arielclrarr1[3]
        } else {
            arielsegment3dress3.alpha = 0
        }
        if (doll.frame.name + 1 == 2 || doll.frame.name + 1 == 5) {
            arielsegment41dress4.visible = true
            arielsegment4dress4.visible = false
        } else {
            arielsegment41dress4.visible = false
            arielsegment4dress4.visible = true
        }
    }
    arielhit4.on('pointerover', arielsegment4overstartclick)
    arielhit4.on('pointerout', arielsegment4outstartclick)
    arielhit4.on('pointerdown', arielsegment4startclick)

    function arielsegment4overstartclick() {
        if (arielclrarr1[4] > 0 && (doll.frame.name + 1 != 3) && (colorindication || arielclrarr1[4] != arielclrarr2[4] || arielsegment4dress4.frame.name != doll.frame.name + 1)) {
            arielsegment4dress4.alpha = 0.6
            if (!colorindication) {
                arielsegment4dress4.tint = colorcode[arielclrarr1[4]]
                arielsegment41dress4.tint = colorcode[arielclrarr1[4]]
            } else {
                arielsegment4dress4.tint = colorcode[arielclrarr2[4]]
                arielsegment41dress4.tint = colorcode[arielclrarr2[4]]
            }
            if (colorindication) {} else {
                arielsegment4dress4.setFrame(doll.frame.name + 1)
                arielsegment41dress4.setFrame(doll.frame.name + 1)
            }
            if (arielovrarr[4] == 0) {
                arielsegment4dress4.alpha = 0.6
                arielsegment41dress4.alpha = 0.6
                if (!colorindication) {
                    arielsegment4dress4.tint = colorcode[arielclrarr1[4]]
                    arielsegment41dress4.tint = colorcode[arielclrarr1[4]]
                } else if (arielclrarr2[4] == 0) {
                    arielsegment4dress4.alpha = 0
                    arielsegment4dress4.tint = colorcode[arielclrarr2[4]]
                    arielsegment41dress4.alpha = 0
                    arielsegment41dress4.tint = colorcode[arielclrarr2[4]]
                }
            }
        } else if (colorindication && arielclrarr2[4] > 0) {
            arielsegment4dress4.alpha = 0.6
            arielsegment41dress4.alpha = 0.6
        }
    }

    function arielsegment4outstartclick() {
        if (arielclrarr1[4] > 0 && (doll.frame.name + 1 != 3) && (colorindication || arielclrarr1[4] != arielclrarr2[4] || arielsegment4dress4.frame.name == doll.frame.name + 1)) {
            if (arielovrarr[4] == 0 && arielclrarr2[4] == 0) {
                arielsegment4dress4.alpha = 0
                arielsegment41dress4.alpha = 0
            } else {
                arielsegment4dress4.alpha = 1
                arielsegment4dress4.tint = colorcode[arielclrarr2[4]]
                arielsegment4dress4.setFrame(arieldresarr1[4])
                arielsegment41dress4.alpha = 1
                arielsegment41dress4.tint = colorcode[arielclrarr2[4]]
                arielsegment41dress4.setFrame(arieldresarr1[4])
            }
        } else if (colorindication && arielclrarr2[4] > 0) {
            arielsegment4dress4.alpha = 1
            arielsegment41dress4.alpha = 1
        } else if (colorindication) {
            arielsegment4dress4.alpha = 0
            arielsegment41dress4.alpha = 0
        }
    }

    function arielsegment4startclick() {
        if (music.isPlaying == false) {
            clicksound.stop()
        } else {
            clicksound.play()
        }
        if (colorindication) {
            arielovrarr[4] = 0
            arielclrarr1[4] = 0
            arielclrarr2[4] = 0
            arielsegment4dress4.alpha = 0
            arielsegment41dress4.alpha = 0
        }
        if (doll.frame.name + 1 != 3) {
            if (arielclrarr1[4] > 0) {
                done.visible = true
                arielsegment4dress4.alpha = 1
                arielsegment41dress4.alpha = 1
                arieldresarr1[4] = 1
                arielsegment4dress4.setFrame(doll.frame.name + 1)
                arielsegment41dress4.setFrame(doll.frame.name + 1)
                arielsegment4dress4.tint = colorcode[arielclrarr1[4]]
                arielsegment41dress4.tint = colorcode[arielclrarr1[4]]
                arieldresarr1[4] = arielsegment4dress4.frame.name
                arielovrarr[4] = 1
                arielcolor[4] = colorcode[arielclrarr1[4]]
                arielclrarr2[4] = arielclrarr1[4]
            } else {
                arielsegment4dress4.alpha = 0
                arielsegment41dress4.alpha = 0
            }
        }
        if (doll.frame.name + 1 == 2 || doll.frame.name + 1 == 5) {
            arielsegment41dress4.visible = true
            arielsegment4dress4.visible = false
        } else {
            arielsegment41dress4.visible = false
            arielsegment4dress4.visible = true
        }
    }
    arielhit5.on('pointerover', arielsegment5overstartclick)
    arielhit5.on('pointerout', arielsegment5outstartclick)
    arielhit5.on('pointerdown', arielsegment5startclick)

    function arielsegment5overstartclick() {
        if (arielclrarr1[5] > 0 && (doll.frame.name + 1 != 1 && doll.frame.name + 1 != 3) && (colorindication || arielclrarr1[5] != arielclrarr2[5] || arielsegment5dress5.frame.name != doll.frame.name + 1)) {
            arielsegment5dress5.alpha = 0.6
            if (!colorindication) {
                arielsegment5dress5.tint = colorcode[arielclrarr1[5]]
            } else {
                arielsegment5dress5.tint = colorcode[arielclrarr2[5]]
            }
            if (colorindication) {} else {
                arielsegment5dress5.setFrame(doll.frame.name + 1)
            }
            if (arielovrarr[5] == 0) {
                arielsegment5dress5.alpha = 0.6
                if (!colorindication) {
                    arielsegment5dress5.tint = colorcode[arielclrarr1[5]]
                } else if (arielclrarr2[5] == 0) {
                    arielsegment5dress5.alpha = 0
                    arielsegment5dress5.tint = colorcode[arielclrarr2[5]]
                }
            }
        } else if (colorindication && arielclrarr2[5] > 0) {
            arielsegment5dress5.alpha = 0.6
        }
    }

    function arielsegment5outstartclick() {
        if (arielclrarr1[5] > 0 && (doll.frame.name + 1 != 1 && doll.frame.name + 1 != 3) && (colorindication || arielclrarr1[5] != arielclrarr2[5] || arielsegment5dress5.frame.name == doll.frame.name + 1)) {
            if (arielovrarr[5] == 0 && arielclrarr2[5] == 0) {
                arielsegment5dress5.alpha = 0
            } else {
                arielsegment5dress5.alpha = 1
                arielsegment5dress5.tint = colorcode[arielclrarr2[5]]
                arielsegment5dress5.setFrame(arieldresarr1[5])
            }
        } else if (colorindication && arielclrarr2[5] > 0) {
            arielsegment5dress5.alpha = 1
        } else if (colorindication) {
            arielsegment5dress5.alpha = 0
        }
    }

    function arielsegment5startclick() {
        if (music.isPlaying == false) {
            clicksound.stop()
        } else {
            clicksound.play()
        }
        if (colorindication) {
            arielovrarr[5] = 0
            arielclrarr1[5] = 0
            arielclrarr2[5] = 0
            arielsegment5dress5.alpha = 0
        }
        if (doll.frame.name + 1 != 1 && doll.frame.name + 1 != 3) {
            if (arielclrarr1[5] > 0) {
                done.visible = true
                arielsegment5dress5.alpha = 1
                arieldresarr1[5] = 1
                arielsegment5dress5.setFrame(doll.frame.name + 1)
                arielsegment5dress5.tint = colorcode[arielclrarr1[5]]
                arieldresarr1[5] = arielsegment5dress5.frame.name
                arielovrarr[5] = 1
                arielcolor[5] = colorcode[arielclrarr1[5]]
                arielclrarr2[5] = arielclrarr1[5]
            } else {
                arielsegment5dress5.alpha = 0
            }
        }
    }
    arielhit6.on('pointerover', arielsegment6overstartclick)
    arielhit6.on('pointerout', arielsegment6outstartclick)
    arielhit6.on('pointerdown', arielsegment6startclick)

    function arielsegment6overstartclick() {
        if (arielclrarr1[6] > 0 && (doll.frame.name + 1 == 5) && (colorindication || arielclrarr1[6] != arielclrarr2[6] || 5 != doll.frame.name + 1)) {
            arielsegment6dress6.alpha = 0.6
            if (!colorindication) {
                arielsegment6dress6.tint = colorcode[arielclrarr1[6]]
            } else {
                arielsegment6dress6.tint = colorcode[arielclrarr2[6]]
            }
            if (colorindication) {} else {
                arielsegment6dress6.setFrame(1)
            }
            if (arielovrarr[6] == 0) {
                arielsegment6dress6.alpha = 0.6
                if (!colorindication) {
                    arielsegment6dress6.tint = colorcode[arielclrarr1[6]]
                } else if (arielclrarr2[6] == 0) {
                    arielsegment6dress6.alpha = 0
                    arielsegment6dress6.tint = colorcode[arielclrarr2[6]]
                }
                arielsegment6dress6.setFrame(1)
            }
        } else if (colorindication && arielclrarr2[6] > 0) {
            arielsegment6dress6.alpha = 0.6
        }
    }

    function arielsegment6outstartclick() {
        if (arielclrarr1[6] > 0 && (doll.frame.name + 1 == 5) && (colorindication || arielclrarr1[6] != arielclrarr2[6] || 5 == doll.frame.name + 1)) {
            if (arielovrarr[6] == 0 && (arielclrarr2[6] == 0)) {
                arielsegment6dress6.alpha = 0
            } else {
                arielsegment6dress6.alpha = 1
                arielsegment6dress6.tint = colorcode[arielclrarr2[6]]
                arielsegment6dress6.setFrame(1)
            }
        } else if (colorindication && arielclrarr2[6] > 0) {
            arielsegment6dress6.alpha = 1
        } else if (colorindication) {
            arielsegment6dress6.alpha = 0
        }
    }

    function arielsegment6startclick() {
        if (music.isPlaying == false) {
            clicksound.stop()
        } else {
            clicksound.play()
        }
        if (colorindication) {
            arielovrarr[6] = 0
            arielclrarr1[6] = 0
            arielclrarr2[6] = 0
            arielsegment6dress6.alpha = 0
        }
        if (doll.frame.name + 1 == 5) {
            if (arielclrarr1[6] > 0) {
                done.visible = true
                arielsegment6dress6.alpha = 1
                arieldresarr1[6] = 1
                arielsegment6dress6.setFrame(1)
                arielsegment6dress6.tint = colorcode[arielclrarr1[6]]
                arieldresarr1[6] = arielsegment6dress6.frame.name
                arielovrarr[6] = 1
                arielcolor[6] = colorcode[arielclrarr1[6]]
                arielclrarr2[6] = arielclrarr1[6]
            } else {
                arielsegment6dress6.alpha = 0
            }
        }
    }
    arielhit7.on('pointerover', arielsegment7overstartclick)
    arielhit7.on('pointerout', arielsegment7outstartclick)
    arielhit7.on('pointerdown', arielsegment7startclick)

    function arielsegment7overstartclick() {
        if (arielclrarr1[7] > 0 && (doll.frame.name + 1 == 5) && (colorindication || arielclrarr1[7] != arielclrarr2[7] || 5 != doll.frame.name + 1)) {
            arielsegment7dress7.alpha = 0.6
            if (!colorindication) {
                arielsegment7dress7.tint = colorcode[arielclrarr1[7]]
            } else {
                arielsegment7dress7.tint = colorcode[arielclrarr2[7]]
            }
            if (colorindication) {} else {
                arielsegment7dress7.setFrame(1)
            }
            if (arielovrarr[7] == 0) {
                arielsegment7dress7.alpha = 0.6
                if (!colorindication) {
                    arielsegment7dress7.tint = colorcode[arielclrarr1[7]]
                } else if (arielclrarr2[7] == 0) {
                    arielsegment7dress7.alpha = 0
                    arielsegment7dress7.tint = colorcode[arielclrarr2[7]]
                }
                arielsegment7dress7.setFrame(1)
            }
        } else if (colorindication && arielclrarr2[7] > 0) {
            arielsegment7dress7.alpha = 0.6
        }
    }

    function arielsegment7outstartclick() {
        if (arielclrarr1[7] > 0 && (doll.frame.name + 1 == 5) && (colorindication || arielclrarr1[7] != arielclrarr2[7] || 5 == doll.frame.name + 1)) {
            if (arielovrarr[7] == 0 && (arielclrarr2[7] == 0)) {
                arielsegment7dress7.alpha = 0
            } else {
                arielsegment7dress7.alpha = 1
                arielsegment7dress7.tint = colorcode[arielclrarr2[7]]
                arielsegment7dress7.setFrame(1)
            }
        } else if (colorindication && arielclrarr2[7] > 0) {
            arielsegment7dress7.alpha = 1
        } else if (colorindication) {
            arielsegment7dress7.alpha = 0
        }
    }

    function arielsegment7startclick() {
        if (music.isPlaying == false) {
            clicksound.stop()
        } else {
            clicksound.play()
        }
        if (colorindication) {
            arielovrarr[7] = 0
            arielclrarr1[7] = 0
            arielclrarr2[7] = 0
            arielsegment7dress7.alpha = 0
        }
        if (doll.frame.name + 1 == 5) {
            if (arielclrarr1[7] > 0) {
                done.visible = true
                arielsegment7dress7.alpha = 1
                arieldresarr1[7] = 1
                arielsegment7dress7.setFrame(1)
                arielsegment7dress7.tint = colorcode[arielclrarr1[7]]
                arieldresarr1[7] = arielsegment7dress7.frame.name
                arielovrarr[7] = 1
                arielcolor[7] = colorcode[arielclrarr1[7]]
                arielclrarr2[7] = arielclrarr1[7]
            } else {
                arielsegment7dress7.alpha = 0
            }
        }
    }
    arielhit8.on('pointerover', arielsegment8overstartclick)
    arielhit8.on('pointerout', arielsegment8outstartclick)
    arielhit8.on('pointerdown', arielsegment8startclick)

    function arielsegment8overstartclick() {
        if (arielclrarr1[8] > 0 && (doll.frame.name + 1 == 5) && (colorindication || arielclrarr1[8] != arielclrarr2[8] || 5 != doll.frame.name + 1)) {
            arielsegment8dress8.alpha = 0.6
            if (!colorindication) {
                arielsegment8dress8.tint = colorcode[arielclrarr1[8]]
            } else {
                arielsegment8dress8.tint = colorcode[arielclrarr2[8]]
            }
            if (colorindication) {} else {
                arielsegment8dress8.setFrame(1)
            }
            if (arielovrarr[8] == 0) {
                arielsegment8dress8.alpha = 0.6
                if (!colorindication) {
                    arielsegment8dress8.tint = colorcode[arielclrarr1[8]]
                } else if (arielclrarr2[8] == 0) {
                    arielsegment8dress8.alpha = 0
                    arielsegment8dress8.tint = colorcode[arielclrarr2[8]]
                }
                arielsegment8dress8.setFrame(1)
            }
        } else if (colorindication && arielclrarr2[8] > 0) {
            arielsegment8dress8.alpha = 0.6
        }
    }

    function arielsegment8outstartclick() {
        if (arielclrarr1[8] > 0 && (doll.frame.name + 1 == 5) && (colorindication || arielclrarr1[8] != arielclrarr2[8] || 5 == doll.frame.name + 1)) {
            if (arielovrarr[8] == 0 && arielclrarr2[8] == 0) {
                arielsegment8dress8.alpha = 0
            } else {
                arielsegment8dress8.alpha = 1
                arielsegment8dress8.tint = colorcode[arielclrarr2[8]]
                arielsegment8dress8.setFrame(1)
            }
        } else if (colorindication && arielclrarr2[8] > 0) {
            arielsegment8dress8.alpha = 1
        } else if (colorindication) {
            arielsegment8dress8.alpha = 0
        }
    }

    function arielsegment8startclick() {
        if (music.isPlaying == false) {
            clicksound.stop()
        } else {
            clicksound.play()
        }
        if (colorindication) {
            arielovrarr[8] = 0
            arielclrarr1[8] = 0
            arielclrarr2[8] = 0
            arielsegment8dress8.alpha = 0
        }
        if (doll.frame.name + 1 == 5) {
            if (arielclrarr1[8] > 0) {
                done.visible = true
                arielsegment8dress8.alpha = 1
                arieldresarr1[8] = 1
                arielsegment8dress8.setFrame(1)
                arielsegment8dress8.tint = colorcode[arielclrarr1[8]]
                arieldresarr1[8] = arielsegment8dress8.frame.name
                arielovrarr[8] = 1
                arielcolor[8] = colorcode[arielclrarr1[8]]
                arielclrarr2[8] = arielclrarr1[8]
            } else {
                arielsegment8dress8.alpha = 0
            }
        }
    }
    arielhit9.on('pointerover', arielsegment9overstartclick)
    arielhit9.on('pointerout', arielsegment9outstartclick)
    arielhit9.on('pointerdown', arielsegment9startclick)

    function arielsegment9overstartclick() {
        if (arielclrarr1[9] > 0 && (doll.frame.name + 1 == 5) && (colorindication || arielclrarr1[9] != arielclrarr2[9] || 5 != doll.frame.name + 1)) {
            arielsegment9dress9.alpha = 0.6
            if (!colorindication) {
                arielsegment9dress9.tint = colorcode[arielclrarr1[9]]
            } else {
                arielsegment9dress9.tint = colorcode[arielclrarr2[9]]
            }
            if (colorindication) {} else {
                arielsegment9dress9.setFrame(1)
            }
            if (arielovrarr[9] == 0) {
                arielsegment9dress9.alpha = 0.6
                if (!colorindication) {
                    arielsegment9dress9.tint = colorcode[arielclrarr1[9]]
                } else if (arielclrarr2[9] == 0) {
                    arielsegment9dress9.alpha = 0
                    arielsegment9dress9.tint = colorcode[arielclrarr2[9]]
                }
                arielsegment9dress9.setFrame(1)
            }
        } else if (colorindication && arielclrarr2[9] > 0) {
            arielsegment9dress9.alpha = 0.6
        }
    }

    function arielsegment9outstartclick() {
        if (arielclrarr1[9] > 0 && (doll.frame.name + 1 == 5) && (colorindication || arielclrarr1[9] != arielclrarr2[9] || 5 == doll.frame.name + 1)) {
            if (arielovrarr[9] == 0 && (arielclrarr2[9] == 0)) {
                arielsegment9dress9.alpha = 0
            } else {
                arielsegment9dress9.alpha = 1
                arielsegment9dress9.tint = colorcode[arielclrarr2[9]]
                arielsegment9dress9.setFrame(1)
            }
        } else if (colorindication && arielclrarr2[9] > 0) {
            arielsegment9dress9.alpha = 1
        } else if (colorindication) {
            arielsegment9dress9.alpha = 0
        }
    }

    function arielsegment9startclick() {
        if (music.isPlaying == false) {
            clicksound.stop()
        } else {
            clicksound.play()
        }
        if (colorindication) {
            arielovrarr[9] = 0
            arielclrarr1[9] = 0
            arielclrarr2[9] = 0
            arielsegment9dress9.alpha = 0
        }
        if (doll.frame.name + 1 == 5) {
            if (arielclrarr1[9] > 0) {
                done.visible = true
                arielsegment9dress9.alpha = 1
                arieldresarr1[9] = 1
                arielsegment9dress9.setFrame(1)
                arielsegment9dress9.tint = colorcode[arielclrarr1[9]]
                arieldresarr1[9] = arielsegment9dress9.frame.name
                arielovrarr[9] = 1
                arielcolor[9] = colorcode[arielclrarr1[9]]
                arielclrarr2[9] = arielclrarr1[9]
            } else {
                arielsegment9dress9.alpha = 0
            }
        }
    }
    arielhit10.on('pointerover', arielsegment10overstartclick)
    arielhit10.on('pointerout', arielsegment10outstartclick)
    arielhit10.on('pointerdown', arielsegment10startclick)

    function arielsegment10overstartclick() {
        if (arielclrarr1[10] > 0 && (doll.frame.name + 1 == 5) && (colorindication || arielclrarr1[10] != arielclrarr2[10] || 5 != doll.frame.name + 1)) {
            arielsegment10dress10.alpha = 0.6
            if (!colorindication) {
                arielsegment10dress10.tint = colorcode[arielclrarr1[10]]
            } else {
                arielsegment10dress10.tint = colorcode[arielclrarr2[10]]
            }
            if (colorindication) {} else {
                arielsegment10dress10.setFrame(1)
            }
            if (arielovrarr[10] == 0) {
                arielsegment10dress10.alpha = 0.6
                if (!colorindication) {
                    arielsegment10dress10.tint = colorcode[arielclrarr1[10]]
                } else if (arielclrarr2[10] == 0) {
                    arielsegment10dress10.alpha = 0
                    arielsegment10dress10.tint = colorcode[arielclrarr2[10]]
                }
                arielsegment10dress10.setFrame(1)
            }
        } else if (colorindication && arielclrarr2[10] > 0) {
            arielsegment10dress10.alpha = 0.6
        }
    }

    function arielsegment10outstartclick() {
        if (arielclrarr1[10] > 0 && (doll.frame.name + 1 == 5) && (colorindication || arielclrarr1[10] != arielclrarr2[10] || 5 == doll.frame.name + 1)) {
            if (arielovrarr[10] == 0 && (arielclrarr2[10] == 0)) {
                arielsegment10dress10.alpha = 0
            } else {
                arielsegment10dress10.alpha = 1
                arielsegment10dress10.tint = colorcode[arielclrarr2[10]]
                arielsegment10dress10.setFrame(1)
            }
        } else if (colorindication && arielclrarr2[10] > 0) {
            arielsegment10dress10.alpha = 1
        } else if (colorindication) {
            arielsegment10dress10.alpha = 0
        }
    }

    function arielsegment10startclick() {
        if (music.isPlaying == false) {
            clicksound.stop()
        } else {
            clicksound.play()
        }
        if (colorindication) {
            arielovrarr[10] = 0
            arielclrarr1[10] = 0
            arielclrarr2[10] = 0
            arielsegment10dress10.alpha = 0
        }
        if (doll.frame.name + 1 == 5) {
            if (arielclrarr1[10] > 0) {
                done.visible = true
                arielsegment10dress10.alpha = 1
                arielsegment10dress10.setFrame(1)
                arieldresarr1[10] = 1
                arielsegment10dress10.tint = colorcode[arielclrarr1[10]]
                arieldresarr1[10] = arielsegment10dress10.frame.name
                arielovrarr[10] = 1
                arielcolor[10] = colorcode[arielclrarr1[10]]
                arielclrarr2[10] = arielclrarr1[10]
            } else {
                arielsegment10dress10.alpha = 0
            }
        }
    }
    arielhit11.on('pointerover', arielsegment11overstartclick)
    arielhit11.on('pointerout', arielsegment11outstartclick)
    arielhit11.on('pointerdown', arielsegment11startclick)

    function arielsegment11overstartclick() {
        if (arielclrarr1[11] > 0 && (doll.frame.name + 1 == 5) && (colorindication || arielclrarr1[11] != arielclrarr2[11] || 5 != doll.frame.name + 1)) {
            arielsegment11dress11.alpha = 0.6
            if (!colorindication) {
                arielsegment11dress11.tint = colorcode[arielclrarr1[11]]
            } else {
                arielsegment11dress11.tint = colorcode[arielclrarr2[11]]
            }
            if (colorindication) {} else {
                arielsegment11dress11.setFrame(1)
            }
            if (arielovrarr[11] == 0) {
                arielsegment11dress11.alpha = 0.6
                if (!colorindication) {
                    arielsegment11dress11.tint = colorcode[arielclrarr1[11]]
                } else if (arielclrarr2[11] == 0) {
                    arielsegment11dress11.alpha = 0
                    arielsegment11dress11.tint = colorcode[arielclrarr2[11]]
                }
                arielsegment11dress11.setFrame(1)
            }
        } else if (colorindication && arielclrarr2[11] > 0) {
            arielsegment11dress11.alpha = 0.6
        }
    }

    function arielsegment11outstartclick() {
        if (arielclrarr1[11] > 0 && (doll.frame.name + 1 == 5) && (colorindication || arielclrarr1[11] != arielclrarr2[11] || 5 == doll.frame.name + 1)) {
            if (arielovrarr[11] == 0 && (arielclrarr2[11] == 0)) {
                arielsegment11dress11.alpha = 0
            } else {
                arielsegment11dress11.alpha = 1
                arielsegment11dress11.tint = colorcode[arielclrarr2[11]]
                arielsegment11dress11.setFrame(1)
            }
        } else if (colorindication && arielclrarr2[11] > 0) {
            arielsegment11dress11.alpha = 1
        } else if (colorindication) {
            arielsegment11dress11.alpha = 0
        }
    }

    function arielsegment11startclick() {
        if (music.isPlaying == false) {
            clicksound.stop()
        } else {
            clicksound.play()
        }
        if (colorindication) {
            arielovrarr[11] = 0
            arielclrarr1[11] = 0
            arielclrarr2[11] = 0
            arielsegment11dress11.alpha = 0
        }
        if (doll.frame.name + 1 == 5) {
            if (arielclrarr1[11] > 0) {
                done.visible = true
                arielsegment11dress11.alpha = 1
                arieldresarr1[11] = 1
                arielsegment11dress11.setFrame(1)
                arielsegment11dress11.tint = colorcode[arielclrarr1[11]]
                arieldresarr1[11] = arielsegment11dress11.frame.name
                arielovrarr[11] = 1
                arielcolor[11] = colorcode[arielclrarr1[11]]
                arielclrarr2[11] = arielclrarr1[11]
            } else {
                arielsegment11dress11.alpha = 0
            }
        }
    }
    arielsegment1dress1.on('pointerdown', arielsegment1dress1startclick)

    function arielsegment1dress1startclick() {
        if (colorindication) {
            if (music.isPlaying == false) {
                clicksound.stop()
            } else {
                clicksound.play()
            }
            arielsegment1dress1.alpha = 0
            arielovrarr[1] = 0
            arielclrarr2[1] = 0
        }
    }
    arielsegment2dress2.on('pointerdown', arielsegment2dress2startclick)

    function arielsegment2dress2startclick() {
        if (colorindication) {
            if (music.isPlaying == false) {
                clicksound.stop()
            } else {
                clicksound.play()
            }
            arielsegment2dress2.alpha = 0
            arielovrarr[2] = 0
            arielclrarr2[2] = 0
        }
    }
    arielsegment3dress3.on('pointerdown', arielsegment3dress3startclick)

    function arielsegment3dress3startclick() {
        if (colorindication) {
            if (music.isPlaying == false) {
                clicksound.stop()
            } else {
                clicksound.play()
            }
            arielsegment3dress3.alpha = 0
            arielovrarr[3] = 0
            arielclrarr2[3] = 0
        }
    }
    arielsegment4dress4.on('pointerdown', arielsegment4dress4startclick)
    arielsegment41dress4.on('pointerdown', arielsegment4dress4startclick)

    function arielsegment4dress4startclick() {
        if (colorindication) {
            if (music.isPlaying == false) {
                clicksound.stop()
            } else {
                clicksound.play()
            }
            arielsegment4dress4.alpha = 0
            arielsegment41dress4.alpha = 0
            arielovrarr[4] = 0
            arielclrarr2[4] = 0
        }
    }
    arielsegment5dress5.on('pointerdown', arielsegment5dress5startclick)

    function arielsegment5dress5startclick() {
        if (colorindication) {
            if (music.isPlaying == false) {
                clicksound.stop()
            } else {
                clicksound.play()
            }
            arielsegment5dress5.alpha = 0
            arielovrarr[5] = 0
            arielclrarr2[5] = 0
        }
    }
    arielsegment6dress6.on('pointerdown', arielsegment6dress6startclick)

    function arielsegment6dress6startclick() {
        if (colorindication) {
            if (music.isPlaying == false) {
                clicksound.stop()
            } else {
                clicksound.play()
            }
            arielsegment6dress6.alpha = 0
            arielovrarr[6] = 0
            arielclrarr2[6] = 0
        }
    }
    arielsegment7dress7.on('pointerdown', arielsegment7dress7startclick)

    function arielsegment7dress7startclick() {
        if (colorindication) {
            if (music.isPlaying == false) {
                clicksound.stop()
            } else {
                clicksound.play()
            }
            arielsegment7dress7.alpha = 0
            arielovrarr[7] = 0
            arielclrarr2[7] = 0
        }
    }
    arielsegment8dress8.on('pointerdown', arielsegment8dress8startclick)

    function arielsegment8dress8startclick() {
        if (colorindication) {
            if (music.isPlaying == false) {
                clicksound.stop()
            } else {
                clicksound.play()
            }
            arielsegment8dress8.alpha = 0
            arielovrarr[8] = 0
            arielclrarr2[8] = 0
        }
    }
    arielsegment9dress9.on('pointerdown', arielsegment9dress9startclick)

    function arielsegment9dress9startclick() {
        if (colorindication) {
            if (music.isPlaying == false) {
                clicksound.stop()
            } else {
                clicksound.play()
            }
            arielsegment9dress9.alpha = 0
            arielovrarr[9] = 0
            arielclrarr2[9] = 0
        }
    }
    arielsegment10dress10.on('pointerdown', arielsegment10dress10startclick)

    function arielsegment10dress10startclick() {
        if (colorindication) {
            if (music.isPlaying == false) {
                clicksound.stop()
            } else {
                clicksound.play()
            }
            arielsegment10dress10.alpha = 0
            arielovrarr[10] = 0
            arielclrarr2[10] = 0
        }
    }
    arielsegment11dress11.on('pointerdown', arielsegment11dress11startclick)

    function arielsegment11dress11startclick() {
        if (colorindication) {
            if (music.isPlaying == false) {
                clicksound.stop()
            } else {
                clicksound.play()
            }
            arielsegment11dress11.alpha = 0
            arielovrarr[11] = 0
            arielclrarr2[11] = 0
        }
    }
    for (i = 1; i <= 17; i++) {
        game['color' + i].on('pointerover', annacoloroverstartclick)
        game['color' + i].on('pointerout', annacoloroutstartclick)
        game['color' + i].on('pointerdown', arielcolorstartclick)
    }

    function annacoloroverstartclick() {
        if (arielclrarr1[7] != this.texture.key.substr(5)) {
            game['color' + this.texture.key.substr(5)].setScale(1.05, 1.05)
        }
    }

    function annacoloroutstartclick() {
        if (arielclrarr1[7] != this.texture.key.substr(5)) {
            game['color' + this.texture.key.substr(5)].setScale(1, 1)
        }
    }

    function arielcolorstartclick() {
        if (handindication) {
            if (music.isPlaying == false) {
                clicksound.stop()
            } else {
                clicksound.play()
            }
            sno = this.texture.key.substr(5)
            arielclrarr1[1] = parseInt(sno)
            arielclrarr1[2] = parseInt(sno)
            arielclrarr1[3] = parseInt(sno)
            arielclrarr1[4] = parseInt(sno)
            arielclrarr1[5] = parseInt(sno)
            arielclrarr1[6] = parseInt(sno)
            arielclrarr1[7] = parseInt(sno)
            arielclrarr1[8] = parseInt(sno)
            arielclrarr1[9] = parseInt(sno)
            arielclrarr1[10] = parseInt(sno)
            arielclrarr1[11] = parseInt(sno)
            arielovrarr[1] = 0
            arielovrarr[2] = 0
            arielovrarr[3] = 0
            arielovrarr[4] = 0
            arielovrarr[5] = 0
            arielovrarr[6] = 0
            arielovrarr[7] = 0
            arielovrarr[7] = 0
            arielovrarr[8] = 0
            arielovrarr[9] = 0
            arielovrarr[10] = 0
            arielovrarr[11] = 0
            for (i = 1; i <= 17; i++) {
                game['color' + i].setScale(1, 1)
            }
            game['color' + arielclrarr1[7]].setScale(1.05, 1.05)
        }
    }
    tailorbtn.on('pointerdown', tailorbtnstartclick)

    function tailorbtnstartclick() {
        if (music.isPlaying == false) {
            clicksound.stop()
        } else {
            clicksound.play()
        }
        if (!handindication) {
            colorindication = false
            arielline.visible = true
            handindication = true
            tailorbtn.setScale(1.05, 1.05)
            cutbtn.setScale(1, 1)
            cutbtn.setFrame(0)
            tailorbtn.setFrame(1)
            arielhit1.setInteractive({
                pixelPerfect: true,
                useHandCursor: true
            })
            arielhit2.setInteractive({
                pixelPerfect: true,
                useHandCursor: true
            })
            arielhit3.setInteractive({
                pixelPerfect: true,
                useHandCursor: true
            })
            arielhit4.setInteractive({
                pixelPerfect: true,
                useHandCursor: true
            })
            arielhit5.setInteractive({
                pixelPerfect: true,
                useHandCursor: true
            })
            arielhit6.setInteractive({
                pixelPerfect: true,
                useHandCursor: true
            })
            arielhit7.setInteractive({
                pixelPerfect: true,
                useHandCursor: true
            })
            arielhit8.setInteractive({
                pixelPerfect: true,
                useHandCursor: true
            })
            arielhit9.setInteractive({
                pixelPerfect: true,
                useHandCursor: true
            })
            arielhit10.setInteractive({
                pixelPerfect: true,
                useHandCursor: true
            })
            arielhit11.setInteractive({
                pixelPerfect: true,
                useHandCursor: true
            })
            arielsegment1dress1.disableInteractive();
            arielsegment2dress2.disableInteractive();
            arielsegment3dress3.disableInteractive();
            arielsegment4dress4.disableInteractive();
            arielsegment41dress4.disableInteractive();
            arielsegment5dress5.disableInteractive();
            arielsegment6dress6.disableInteractive();
            arielsegment7dress7.disableInteractive();
            arielsegment8dress8.disableInteractive();
            arielsegment9dress9.disableInteractive();
            arielsegment10dress10.disableInteractive();
            arielsegment11dress11.disableInteractive();
        }
    }
    cutbtn.on('pointerdown', cutbtnstartclick)

    function cutbtnstartclick() {
        if (music.isPlaying == false) {
            clicksound.stop()
        } else {
            clicksound.play()
        }
        arielline.visible = false
        handindication = false
        colorindication = true
        cutbtn.setScale(1.05, 1.05)
        tailorbtn.setScale(1, 1)
        cutbtn.setFrame(1)
        tailorbtn.setFrame(0)
        arielhit1.disableInteractive();
        arielhit2.disableInteractive();
        arielhit3.disableInteractive();
        arielhit4.disableInteractive();
        arielhit5.disableInteractive();
        arielhit6.disableInteractive();
        arielhit7.disableInteractive();
        arielhit8.disableInteractive();
        arielhit9.disableInteractive();
        arielhit10.disableInteractive();
        arielhit11.disableInteractive();
        arielsegment1dress1.setInteractive({
            pixelPerfect: true,
            useHandCursor: true
        })
        arielsegment2dress2.setInteractive({
            pixelPerfect: true,
            useHandCursor: true
        })
        arielsegment3dress3.setInteractive({
            pixelPerfect: true,
            useHandCursor: true
        })
        arielsegment4dress4.setInteractive({
            pixelPerfect: true,
            useHandCursor: true
        })
        arielsegment41dress4.setInteractive({
            pixelPerfect: true,
            useHandCursor: true
        })
        arielsegment5dress5.setInteractive({
            pixelPerfect: true,
            useHandCursor: true
        })
        arielsegment6dress6.setInteractive({
            pixelPerfect: true,
            useHandCursor: true
        })
        arielsegment7dress7.setInteractive({
            pixelPerfect: true,
            useHandCursor: true
        })
        arielsegment8dress8.setInteractive({
            pixelPerfect: true,
            useHandCursor: true
        })
        arielsegment9dress9.setInteractive({
            pixelPerfect: true,
            useHandCursor: true
        })
        arielsegment10dress10.setInteractive({
            pixelPerfect: true,
            useHandCursor: true
        })
        arielsegment11dress11.setInteractive({
            pixelPerfect: true,
            useHandCursor: true
        })
    }
    rarrow.on('pointerdown', rarrowstartclick)

    function rarrowstartclick() {
        if (music.isPlaying == false) {
            clicksound.stop()
        } else {
            clicksound.play()
        }
        if (doll.frame.name == 0) {
            doll.setFrame(1)
        } else if (doll.frame.name == 1) {
            doll.setFrame(2)
        } else if (doll.frame.name == 2) {
            doll.setFrame(3)
        } else if (doll.frame.name == 3) {
            doll.setFrame(4)
        } else if (doll.frame.name == 4) {
            doll.setFrame(5)
        } else if (doll.frame.name == 5) {
            doll.setFrame(0)
        }
    }
    larrow.on('pointerdown', larrowstartclick)

    function larrowstartclick() {
        if (music.isPlaying == false) {
            clicksound.stop()
        } else {
            clicksound.play()
        }
        if (doll.frame.name == 0) {
            doll.setFrame(5)
        } else if (doll.frame.name == 5) {
            doll.setFrame(4)
        } else if (doll.frame.name == 4) {
            doll.setFrame(3)
        } else if (doll.frame.name == 3) {
            doll.setFrame(2)
        } else if (doll.frame.name == 2) {
            doll.setFrame(1)
        } else if (doll.frame.name == 1) {
            doll.setFrame(0)
        }
    }
}
var startgame9 = false
var handindication = false
var colorindication = false
var annastorearr = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
var annaclrarr1 = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
var annaclrarr2 = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
var annaovrarr = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
var annadresarr1 = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
var annacolor = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
var darr3 = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
var colorcode = [, ' 0xff3838', ' 0xfe7d3b', ' 0xffe33b', ' 0x9cdc3b', ' 0x3ba13b', ' 0x3bd8c8', ' 0x3bdcff', ' 0x3b91ed', ' 0x3f3ce6', ' 0x7443f6', ' 0xfeb463', ' 0x9b9b9b', ' 0xb04ab1', ' 0xff8be6', '0xed3b8a', ' 0xfff2f2', ' 0x3b2e2e']
var Level7 = new Phaser.Class({
    Extends: Phaser.Scene,
    initialize: function Level7() {
        Phaser.Scene.call(this, {
            key: 'Level7'
        });
    },
    preload: function () {
        colorcode = [, ' 0xff3838', ' 0xfe7d3b', ' 0xffe33b', ' 0x9cdc3b', ' 0x3ba13b', ' 0x3bd8c8', ' 0x3bdcff', ' 0x3b91ed', ' 0x3f3ce6', ' 0x7443f6', ' 0xfeb463', ' 0x9b9b9b', ' 0xb04ab1', ' 0xff8be6', '0xed3b8a', ' 0xfff2f2', ' 0x3b2e2e']
        darr3 = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        annacolor = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        startgame9 = false
        loadFinish = false
        clrarr = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        annaclrarr1 = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        annaclrarr2 = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        annaovrarr = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        annadresarr1 = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        annastorearr = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        handindication = false
        colorindication = false
    },
    create: function () {
        annabackground1 = this.add.image(400, 300, 'annabackground1').setOrigin(0.5, 0.5)
        dressuppanel = this.add.image(26, 40, 'dressuppanel').setOrigin(0, 0)
        colortext = this.add.image(83, 302, 'colortext').setOrigin(0, 0)
        tailorbtn = this.add.image(81, 136, 'tailorbtn').setOrigin(0.5, 0.5).setInteractive({
            pixelPerfect: true,
            useHandCursor: true,
        })
        tailorbtn.x += parseFloat(tailorbtn.width / 2)
        tailorbtn.y += parseFloat(tailorbtn.height / 2)
        cutbtn = this.add.image(150, 221, 'cutbtn').setOrigin(0.5, 0.5).setInteractive({
            pixelPerfect: true,
            useHandCursor: true,
        })
        cutbtn.x += parseFloat(cutbtn.width / 2)
        cutbtn.y += parseFloat(cutbtn.height / 2)
        var coloxarr = [, 80, 130, 178, 228, 277, 328, 79, 129, 178, 228, 276, 326, 128, 179, 229, 279, 327]
        var coloyarr = [, 369, 370, 370, 370, 370, 370, 424, 424, 424, 424, 424, 424, 477, 477, 477, 477, 477]
        for (i = 1; i <= 17; i++) {
            game['color' + i] = this.add.image(coloxarr[i], coloyarr[i], 'color' + i).setOrigin(0.5, 0.5).setInteractive({
                pixelPerfect: true,
                useHandCursor: true,
            })
            game['color' + i].x += parseFloat(game['color' + i].width / 2)
            game['color' + i].y += parseFloat(game['color' + i].height / 2)
        }
        dressphotoframe = this.add.image(251, 89, 'dressphotoframe').setOrigin(0, 0)
        rarrow = this.add.image(412, 143, 'arrow').setOrigin(0, 0).setInteractive({
            pixelPerfect: true,
            useHandCursor: true,
        })
        larrow = this.add.image(262, 147, 'arrow').setOrigin(0, 0).setInteractive({
            pixelPerfect: true,
            useHandCursor: true,
        })
        larrow.setScale(-1, 1)
        larrow.angle = -7
        this.tweens.add({
            targets: rarrow,
            x: 416,
            ease: 'Linear',
            duration: 400,
            repeat: -1,
            yoyo: true,
        })
        this.tweens.add({
            targets: larrow,
            x: 258,
            ease: 'Linear',
            duration: 400,
            repeat: -1,
            yoyo: true,
        })
        doll = this.add.image(252, 76, 'annadoll').setOrigin(0, 0)
        annabody = this.add.image(54, 42, 'annabody').setOrigin(0, 0)
        annalips = this.add.image(154, 102, 'annalips').setOrigin(0, 0)
        annaeye1 = this.add.image(154, 79, 'annaeye1').setOrigin(0, 0)
        annaeye2 = this.add.image(154, 79, 'annaeye2').setOrigin(0, 0)
        annablush = this.add.image(145, 88, 'annablush').setOrigin(0, 0)
        annatoplayer = this.add.image(150, 74, 'annatoplayer').setOrigin(0, 0)
        annaeshade = this.add.image(147, 75, 'annaeshade').setOrigin(0, 0)
        annaelash = this.add.image(147, 74, 'annaelash').setOrigin(0, 0)
        annaebrow = this.add.image(150, 69, 'annaebrow').setOrigin(0, 0)
        annahair = this.add.image(117, 20, 'annahair').setOrigin(0, 0)
        annahand = this.add.image(221, 146, 'annahand').setOrigin(0, 0)
        annahand1 = this.add.image(223, 146, 'annahand1').setOrigin(0, 0)
        annasegment1dress1 = this.add.image(111, 125, 'annasegment1dress1').setOrigin(0, 0)
        annasegment2dress2 = this.add.image(153, 207, 'annasegment2dress2').setOrigin(0, 0)
        annasegment3dress3 = this.add.image(100, 226, 'annasegment3dress3').setOrigin(0, 0)
        annasegment4dress4 = this.add.image(97, 346, 'annasegment4dress4').setOrigin(0, 0)
        annasegment41dress4 = this.add.image(97, 346, 'annasegment4dress4').setOrigin(0, 0)
        annasegment5dress5 = this.add.image(98, 399, 'annasegment5dress5').setOrigin(0, 0)
        annasegment6dress6 = this.add.image(71, 351, 'annasegment6dress6').setOrigin(0, 0)
        annasegment7dress7 = this.add.image(41, 414, 'annasegment7dress7').setOrigin(0, 0)
        annasegment8dress8 = this.add.image(213, 335, 'annasegment8dress8').setOrigin(0, 0)
        annasegment9dress9 = this.add.image(224, 419, 'annasegment9dress9').setOrigin(0, 0)
        annaelash.setFrame(marr3[0])
        annablush.setFrame(marr3[1])
        annalips.setFrame(marr3[2])
        annaeye1.setFrame(marr3[3])
        annaeye2.setFrame(marr3[3])
        annaeshade.setFrame(marr3[4])
        annaebrow.setFrame(marr3[5])
        annahair.setFrame(marr3[6])
        var annadollgroup = this.add.container(0, 0);
        annadollgroup.add(annasegment9dress9)
        annadollgroup.add(annasegment8dress8)
        annadollgroup.add(annasegment7dress7)
        annadollgroup.add(annasegment6dress6)
        annadollgroup.add(annabody)
        annadollgroup.add(annalips)
        annadollgroup.add(annaeye1)
        annadollgroup.add(annaeye2)
        annadollgroup.add(annatoplayer)
        annadollgroup.add(annaeshade)
        annadollgroup.add(annaelash)
        annadollgroup.add(annablush)
        annadollgroup.add(annaebrow)
        annadollgroup.add(annasegment1dress1)
        annadollgroup.add(annasegment5dress5)
        annadollgroup.add(annasegment2dress2)
        annadollgroup.add(annasegment4dress4)
        annadollgroup.add(annasegment3dress3)
        annadollgroup.add(annasegment41dress4)
        annadollgroup.add(annahand)
        annadollgroup.add(annahair)
        annadollgroup.add(annahand1)
        annadollgroup.x = 433
        annadollgroup.y = 40
        annasegment41dress4.visible = false
        annaline = this.add.image(445, 180, 'elsaline').setOrigin(0, 0)
        annaline.visible = false
        annahit1 = this.add.image(624.95, 220, 'elsahit1').setOrigin(0.5, 0.5).setInteractive({
            pixelperfect: true,
            useHandCursor: true
        })
        annahit2 = this.add.image(625.95, 258, 'elsahit1').setOrigin(0.5, 0.5).setInteractive({
            pixelperfect: true,
            useHandCursor: true
        })
        annahit2.setScale(0.64)
        annahit3 = this.add.image(617.95, 337, 'elsahit1').setOrigin(0.5, 0.5).setInteractive({
            pixelperfect: true,
            useHandCursor: true
        })
        annahit3.setScale(2.02)
        annahit3.angle = 90
        annahit4 = this.add.image(624.95, 453, 'elsahit1').setOrigin(0.5, 0.5).setInteractive({
            pixelperfect: true,
            useHandCursor: true
        })
        annahit4.setScale(1.86)
        annahit5 = this.add.image(627.95, 552, 'elsahit1').setOrigin(0.5, 0.5).setInteractive({
            pixelperfect: true,
            useHandCursor: true
        })
        annahit5.setScale(2.25)
        annahit6 = this.add.image(528.05, 425, 'elsahit1').setOrigin(0.5, 0.5).setInteractive({
            pixelperfect: true,
            useHandCursor: true
        })
        annahit6.setScale(1.2)
        annahit6.angle = 108
        annahit7 = this.add.image(506.95, 519.7, 'elsahit1').setOrigin(0.5, 0.5).setInteractive({
            pixelperfect: true,
            useHandCursor: true
        })
        annahit7.setScale(1.29)
        annahit7.angle = 101
        annahit8 = this.add.image(712.05, 425, 'elsahit1').setOrigin(0.5, 0.5).setInteractive({
            pixelperfect: true,
            useHandCursor: true
        })
        annahit8.setScale(1.2)
        annahit8.angle = 73
        annahit9 = this.add.image(748.95, 510.7, 'elsahit1').setOrigin(0.5, 0.5).setInteractive({
            pixelperfect: true,
            useHandCursor: true
        })
        annahit9.setScale(1.29)
        annahit9.angle = 63
        var tween1 = this.tweens.add({
            targets: annaline,
            alpha: 0.3,
            ease: 'Linear',
            duration: 1000,
            repeat: -1,
            yoyo: true,
        });
        var tween1 = this.tweens.add({
            targets: annaeye1,
            x: annaeye1.x + 2,
            ease: 'Linear',
            duration: 300,
            delay: 2400,
            callbackScope: this,
        });
        var tween2 = this.tweens.add({
            targets: annaeye2,
            x: annaeye2.x + 2,
            ease: 'Linear',
            duration: 300,
            delay: 2400,
            onComplete: annaeyestartani1,
            callbackScope: this
        });

        function annaeyestartani1() {
            this.tweens.add({
                targets: annaeye1,
                x: annaeye1.x - 2,
                ease: 'Linear',
                duration: 300,
                delay: 3000,
                callbackScope: this
            });
            this.tweens.add({
                targets: annaeye2,
                x: annaeye2.x - 2,
                ease: 'Linear',
                duration: 300,
                delay: 3000,
                onComplete: annaeyestartani2,
                callbackScope: this,
            })
        }

        function annaeyestartani2() {
            this.tweens.add({
                targets: annaeye1,
                x: annaeye1.x + 2,
                ease: 'Linear',
                duration: 300,
                delay: 2400,
                callbackScope: this
            });
            this.tweens.add({
                targets: annaeye2,
                x: annaeye2.x + 2,
                ease: 'Linear',
                duration: 300,
                delay: 2400,
                onComplete: annaeyestartani1,
                callbackScope: this,
            })
        }
        annaeshad = this.time.addEvent({
            delay: 1000,
            callback: annaeblinkstart1,
            callbackScope: this
        });

        function annaeblinkstart1() {
            annaeshade.setFrame(marr3[4] + 1)
            annaelash.visible = false
            annaeshad = this.time.addEvent({
                delay: 200,
                callback: annaeblinkstart2,
                callbackScope: this
            });
        }

        function annaeblinkstart2() {
            annaeshade.setFrame(marr3[4])
            annaelash.visible = true
            annaeshad = this.time.addEvent({
                delay: 4000,
                callback: annaeblinkstart1,
                callbackScope: this
            });
        }
        annalip = this.time.addEvent({
            delay: 1000,
            callback: annalipsstart1,
            callbackScope: this
        });

        function annalipsstart1() {
            annalips.setFrame(marr3[2] + 1)
            annalip = this.time.addEvent({
                delay: 100,
                callback: annalipsstart2,
                callbackScope: this
            });
        }

        function annalipsstart2() {
            annalips.setFrame(marr3[2] + 2)
            annalip = this.time.addEvent({
                delay: 2500,
                callback: annalipsstart3,
                callbackScope: this
            });
        }

        function annalipsstart3() {
            annalips.setFrame(marr3[2] + 1)
            annalip = this.time.addEvent({
                delay: 100,
                callback: annalipsstart4,
                callbackScope: this
            });
        }

        function annalipsstart4() {
            annalips.setFrame(marr3[2])
            annalip = this.time.addEvent({
                delay: 3000,
                callback: annalipsstart1,
                callbackScope: this
            });
        }
        clicksound = this.sound.add('clickss');
        done = this.add.image(790, 590, 'uiButton').setOrigin(1, 1).setInteractive({
            pixelPerfect: true,
            useHandCursor: true
        })
        done.setFrame("button_next")
        done.visible = false
        done.on('pointerover', doneoverclick1);
        done.on('pointerout', doneoutclick1);
        done.on('pointerdown', donedownclick1);
        done.on('pointerup', doneupclick1);

        function doneoverclick1() {
            done.setScale(1.05, 1.05)
        }

        function doneoutclick1() {
            done.setScale(1, 1)
        }

        function doneupclick1() {
            if (startgame9) {
                done.setScale(1.05, 1.05)
            }
        }

        function donedownclick1() {
            if (!startgame9 && loadFinish) {
                startgame9 = true
                level = 7
                if (music.isPlaying == false) {
                    clicksound.stop()
                } else {
                    clicksound.play()
                }
                done.setScale(1, 1)
                annastorearr[2] = annasegment9dress9.frame.name
                annastorearr[3] = annasegment8dress8.frame.name
                annastorearr[4] = annasegment7dress7.frame.name
                annastorearr[5] = annasegment6dress6.frame.name
                annastorearr[6] = annasegment1dress1.frame.name
                annastorearr[7] = annasegment5dress5.frame.name
                annastorearr[8] = annasegment4dress4.frame.name
                annastorearr[9] = annasegment3dress3.frame.name
                annastorearr[10] = annasegment2dress2.frame.name
                annastorearr[13] = annasegment9dress9.alpha
                annastorearr[14] = annasegment8dress8.alpha
                annastorearr[15] = annasegment7dress7.alpha
                annastorearr[16] = annasegment6dress6.alpha
                annastorearr[17] = annasegment1dress1.alpha
                annastorearr[18] = annasegment5dress5.alpha
                annastorearr[19] = annasegment4dress4.alpha
                annastorearr[20] = annasegment3dress3.alpha
                annastorearr[21] = annasegment2dress2.alpha
                annastorearr[22] = annasegment4dress4.visible
                transitionbackground1.alpha = 1
                transitionbackground2.alpha = 1
                transitionbackground3.alpha = 1
                transitionpanel.alpha = 1
                transitiondress.alpha = 1
                transitiontitle1.alpha = 1
                transitiontitle2.alpha = 1
                transitiontitle3.alpha = 1
                transitionstar1.alpha = 1
                transitionstar2.alpha = 1
                transitionstar3.alpha = 1
                transitionstar4.alpha = 1
                transitionstar5.alpha = 1
                transitionstar1.setScale(0)
                transitionstar2.setScale(0)
                transitionstar3.setScale(0)
                transitionstar4.setScale(0)
                transitionstar5.setScale(0)
                transitionbackground1.visible = true
                transitionbackground2.visible = true
                transitionbackground3.visible = true
                transitiondress.visible = false
                transitionpanel.visible = false
                transitiontitle1.visible = false
                transitiontitle2.visible = false
                transitiontitle3.visible = false
                transitionstar1.visible = false
                transitionstar2.visible = false
                transitionstar3.visible = false
                transitionstar4.visible = false
                transitionstar5.visible = false
                transitionbackground1.alpha = 0
                transitionbackground2.alpha = 0
                transitionbackground3.alpha = 0
                this.scene.tweens.add({
                    targets: transitionbackground1,
                    alpha: 1,
                    ease: 'Linear',
                    duration: 800,
                    onComplete: transitionstart1,
                    callbackScope: this
                })

                function transitionstart1() {
                    transitiondress.visible = true
                    transitiondress.alpha = 0
                    this.scene.tweens.add({
                        targets: transitiondress,
                        alpha: 1,
                        ease: 'Linear',
                        duration: 400,
                        onComplete: transitionstart2,
                        callbackScope: this
                    })
                    this.scene.tweens.add({
                        targets: transitionbackground2,
                        alpha: 1,
                        ease: 'Linear',
                        duration: 800,
                    })
                }

                function transitionstart2() {
                    transitionpanel.visible = true
                    transitionpanel.setScale(0)
                    this.scene.tweens.add({
                        targets: transitionpanel,
                        scaleX: 1,
                        scaleY: 1,
                        ease: 'Linear',
                        duration: 500,
                        onComplete: transitionstart3,
                        callbackScope: this
                    })
                }

                function transitionstart3() {
                    this.scene.tweens.add({
                        targets: transitionbackground3,
                        alpha: 1,
                        ease: 'Linear',
                        duration: 800,
                        onComplete: transitionstart4,
                        callbackScope: this
                    })
                }

                function transitionstart4() {
                    transitiontitle1.visible = true
                    transitiontitle2.visible = true
                    transitiontitle3.visible = true
                    transitiontitle1.alpha = 0
                    transitiontitle2.alpha = 0
                    transitiontitle3.alpha = 0
                    transitiontitle2.x = transitiontitle2.x + 100
                    transitiontitle3.x = transitiontitle3.x - 100
                    this.scene.tweens.add({
                        targets: transitiontitle1,
                        alpha: 1,
                        ease: 'Linear',
                        duration: 800,
                        onComplete: transitionstart5,
                        callbackScope: this
                    })
                    this.scene.tweens.add({
                        targets: transitiontitle2,
                        alpha: 1,
                        x: transitiontitle2.x - 100,
                        ease: 'Linear',
                        duration: 800,
                    })
                    this.scene.tweens.add({
                        targets: transitiontitle3,
                        alpha: 1,
                        x: transitiontitle3.x + 100,
                        ease: 'Linear',
                        duration: 800,
                    })
                }

                function transitionstart5() {
                    transitionstar1.setScale(0)
                    transitionstar2.setScale(0)
                    transitionstar3.setScale(0)
                    transitionstar4.setScale(0)
                    transitionstar5.setScale(0)
                    transitionstar1.visible = true
                    transitionstar2.visible = true
                    transitionstar3.visible = true
                    transitionstar4.visible = true
                    transitionstar5.visible = true
                    this.scene.tweens.add({
                        targets: transitionstar1,
                        scaleX: 1,
                        scaleY: 1,
                        ease: 'Linear',
                        duration: 300,
                    })
                    this.scene.tweens.add({
                        targets: transitionstar2,
                        scaleX: 0.9,
                        scaleY: 0.9,
                        ease: 'Linear',
                        duration: 300,
                    })
                    this.scene.tweens.add({
                        targets: transitionstar3,
                        scaleX: 0.9,
                        scaleY: 0.9,
                        ease: 'Linear',
                        duration: 300,
                    })
                    this.scene.tweens.add({
                        targets: transitionstar4,
                        scaleX: 0.8,
                        scaleY: 0.8,
                        ease: 'Linear',
                        duration: 300,
                    })
                    this.scene.tweens.add({
                        targets: transitionstar5,
                        scaleX: 0.65,
                        scaleY: 0.65,
                        ease: 'Linear',
                        duration: 300,
                        onComplete: transitionstar6,
                        callbackScope: this
                    })
                }

                function transitionstar6() {
                    this.scene.scene.stop('Level7')
                    game.scene.run('levelSelect');
                }
            }
        }
        mute = this.add.sprite(790, 10, 'uiButton').setFrame("button_music_on").setInteractive({
            pixelPerfect: true,
            useHandCursor: true
        })
        mute.setOrigin(1, 0)
        mute.on('pointerdown', function () {
            if (!isMuted) {
                isMuted = true
                music.pause()
                mute.setFrame("button_music_off")
            } else {
                isMuted = false
                music.resume()
                mute.setFrame("button_music_on")
            }
            var tween2 = this.tweens.add({
                targets: mute,
                scaleX: 0.9,
                scaleY: 0.9,
                ease: 'Linear',
                duration: 80,
                yoyo: true,
                repeat: 1,
            });
            if (music.isPlaying == false) {
                clicksound.stop()
            } else {
                clicksound.play()
            }
        }, this);
        if (music.isPlaying == false) {
            mute.setFrame("button_music_off")
        } else {
            mute.setFrame("button_music_on")
        }
        if (brandingMaterials.inGameLogo.display == true && brandingMaterials.inGameLogo.image != undefined) {
            inGameLogo1 = this.add.image(10, 590, 'inGameLogo1').setOrigin(0, 1).setInteractive({
                pixelPerfect: true,
                useHandCursor: true
            })
            inGameLogo1.on('pointerdown', brandingMaterials.inGameLogo.clickURL);
            inGameLogoani = this.time.addEvent({
                delay: 2000,
                callback: inGameLogoani,
                callbackScope: this
            });

            function inGameLogoani() {
                this.tweens.add({
                    targets: inGameLogo1,
                    scaleX: 1.05,
                    scaleY: 1.05,
                    ease: 'Linear',
                    duration: 100,
                    repeat: 1,
                    yoyo: true,
                    delay: 3000,
                    onComplete: inGameLogoani1,
                    callbackScope: this
                })
            }

            function inGameLogoani1() {
                this.tweens.add({
                    targets: inGameLogo1,
                    scaleX: 1.05,
                    scaleY: 1.05,
                    ease: 'Linear',
                    duration: 100,
                    repeat: 1,
                    yoyo: true,
                    delay: 3000,
                    onComplete: inGameLogoani1,
                    callbackScope: this
                })
            }
        }
        transitionbackground1 = this.add.image(400, 300, 'transitionbackground1').setOrigin(0.5, 0.5).setInteractive({
            pixelPerfect: true,
            useHandCursor: true
        })
        transitionbackground2 = this.add.image(400, 300, 'transitionbackground2').setOrigin(0.5, 0.5)
        transitionbackground3 = this.add.image(400, 300, 'transitionbackground3').setOrigin(0.5, 0.5)
        transitionpanel = this.add.image(393.5, 314, 'transitionpanel').setOrigin(0.5)
        transitiondress = this.add.image(267, 141, 'transitiondress').setOrigin(0, 0)
        transitiontitle1 = this.add.image(400, 329, 'title1');
        transitiontitle1.setOrigin(0.5);
        transitiontitle2 = this.add.image(380, 399, 'title2');
        transitiontitle2.setOrigin(0.5);
        transitiontitle3 = this.add.image(380, 459, 'title3');
        transitiontitle3.setOrigin(0.5);
        transitionstar1 = this.add.image(616.5, 168, 'transitionstar');
        transitionstar1.setOrigin(0.5);
        transitionstar2 = this.add.image(156.5, 168, 'transitionstar');
        transitionstar2.setOrigin(0.5);
        transitionstar2.setScale(0.9)
        transitionstar3 = this.add.image(596.5, 468, 'transitionstar');
        transitionstar3.setOrigin(0.5);
        transitionstar3.setScale(0.9)
        transitionstar4 = this.add.image(116.5, 468, 'transitionstar');
        transitionstar4.setOrigin(0.5);
        transitionstar4.setScale(0.8)
        transitionstar5 = this.add.image(400, 558, 'transitionstar');
        transitionstar5.setOrigin(0.5);
        transitionstar5.setScale(0.65)
        this.tweens.add({
            targets: transitionstar1,
            angle: 4,
            ease: 'Linear',
            duration: 300,
            repeat: -1,
            yoyo: true
        })
        this.tweens.add({
            targets: transitionstar2,
            angle: 4,
            ease: 'Linear',
            duration: 300,
            repeat: -1,
            yoyo: true
        })
        this.tweens.add({
            targets: transitionstar3,
            angle: 4,
            ease: 'Linear',
            duration: 300,
            repeat: -1,
            yoyo: true
        })
        this.tweens.add({
            targets: transitionstar4,
            angle: 4,
            ease: 'Linear',
            duration: 300,
            repeat: -1,
            yoyo: true
        })
        this.tweens.add({
            targets: transitionstar5,
            angle: 4,
            ease: 'Linear',
            duration: 300,
            repeat: -1,
            yoyo: true
        })
        this.time.addEvent({
            delay: 1000,
            callback: transitionstart11,
            callbackScope: this
        });

        function transitionstart11() {
            this.tweens.add({
                targets: transitionbackground1,
                alpha: 0,
                ease: 'Linear',
                duration: 700
            })
            this.tweens.add({
                targets: transitionbackground2,
                alpha: 0,
                ease: 'Linear',
                duration: 700
            })
            this.tweens.add({
                targets: transitionbackground3,
                alpha: 0,
                ease: 'Linear',
                duration: 700
            })
            this.tweens.add({
                targets: transitionpanel,
                alpha: 0,
                ease: 'Linear',
                duration: 700
            })
            this.tweens.add({
                targets: transitiondress,
                alpha: 0,
                ease: 'Linear',
                duration: 700
            })
            this.tweens.add({
                targets: transitiontitle1,
                alpha: 0,
                ease: 'Linear',
                duration: 700
            })
            this.tweens.add({
                targets: transitiontitle2,
                alpha: 0,
                ease: 'Linear',
                duration: 700
            })
            this.tweens.add({
                targets: transitiontitle3,
                alpha: 0,
                ease: 'Linear',
                duration: 700
            })
            this.tweens.add({
                targets: transitionstar1,
                alpha: 0,
                ease: 'Linear',
                duration: 700
            })
            this.tweens.add({
                targets: transitionstar2,
                alpha: 0,
                ease: 'Linear',
                duration: 700
            })
            this.tweens.add({
                targets: transitionstar3,
                alpha: 0,
                ease: 'Linear',
                duration: 700
            })
            this.tweens.add({
                targets: transitionstar4,
                alpha: 0,
                ease: 'Linear',
                duration: 700
            })
            this.tweens.add({
                targets: transitionstar5,
                alpha: 0,
                ease: 'Linear',
                duration: 700
            })
        }
        dressup3start()
        hitobject = this.add.image(400, 300, 'hitobject').setOrigin(0.5, 0.5).setInteractive({
            useHandCursor: true
        })
        hitobject.visible = false
        hitobject.on('pointerdown', hitobjectdown)

        function hitobjectdown() {}
        this.load.on('complete', function () {
            loadFinish = true
        });
        this.load.start();
    }
});

function dressup3start() {
    annaclrarr1[1] = 1
    annaclrarr1[2] = 1
    annaclrarr1[3] = 1
    annaclrarr1[4] = 1
    annaclrarr1[5] = 1
    annaclrarr1[6] = 1
    annaclrarr1[7] = 1
    annaclrarr1[8] = 1
    annaclrarr1[9] = 1
    annaclrarr1[10] = 1
    annaclrarr1[11] = 1
    tailorbtn.setScale(1.05, 1.05)
    game['color' + 1].setScale(1.05, 1.05)
    tailorbtn.setFrame(1)
    handindication = true
    annaline.visible = true
    annahit1.on('pointerover', annasegment1overstartclick)
    annahit1.on('pointerout', annasegment1outstartclick)
    annahit1.on('pointerdown', annasegment1startclick)

    function annasegment1overstartclick() {
        if (annaclrarr1[1] > 0 && (colorindication || annaclrarr1[1] != annaclrarr2[1] || annasegment1dress1.frame.name != doll.frame.name + 1)) {
            annasegment1dress1.alpha = 0.6
            if (!colorindication) {
                annasegment1dress1.tint = colorcode[annaclrarr1[1]]
            } else {
                annasegment1dress1.tint = colorcode[annaclrarr2[1]]
            }
            if (colorindication) {} else {
                annasegment1dress1.setFrame(doll.frame.name + 1)
            }
            if (annaovrarr[1] == 0) {
                annasegment1dress1.alpha = 0.6
                if (!colorindication) {
                    annasegment1dress1.tint = colorcode[annaclrarr1[1]]
                } else if (annaclrarr2[1] == 0) {
                    annasegment1dress1.alpha = 0
                    annasegment1dress1.tint = colorcode[annaclrarr2[1]]
                }
            }
        } else if (colorindication && annaclrarr2[1] > 0) {
            annasegment1dress1.alpha = 0.6
        }
    }

    function annasegment1outstartclick() {
        if (annaclrarr1[1] > 0 && (colorindication || annaclrarr1[1] != annaclrarr2[1] || annasegment1dress1.frame.name == doll.frame.name + 1)) {
            if (annaovrarr[1] == 0 && annaclrarr2[1] == 0) {
                annasegment1dress1.alpha = 0
            } else {
                annasegment1dress1.alpha = 1
                annasegment1dress1.tint = colorcode[annaclrarr2[1]]
                annasegment1dress1.setFrame(annadresarr1[1])
            }
        } else if (colorindication && annaclrarr2[1] > 0) {
            annasegment1dress1.alpha = 1
        } else if (colorindication) {
            annasegment1dress1.alpha = 0
        }
    }

    function annasegment1startclick() {
        if (music.isPlaying == false) {
            clicksound.stop()
        } else {
            clicksound.play()
        }
        if (colorindication) {
            annaovrarr[1] = 0
            annaclrarr1[1] = 0
            annaclrarr2[1] = 0
            annasegment1dress1.alpha = 0
        }
        if (annaclrarr1[1] > 0) {
            done.visible = true
            annasegment1dress1.alpha = 1
            annadresarr1[1] = 1
            annasegment1dress1.setFrame(doll.frame.name + 1)
            annasegment1dress1.tint = colorcode[annaclrarr1[1]]
            annadresarr1[1] = annasegment1dress1.frame.name
            annaovrarr[1] = 1
            annacolor[1] = colorcode[annaclrarr1[1]]
            annaclrarr2[1] = annaclrarr1[1]
        } else {
            annasegment1dress1.alpha = 0
        }
    }
    annahit2.on('pointerover', annasegment2overstartclick)
    annahit2.on('pointerout', annasegment2outstartclick)
    annahit2.on('pointerdown', annasegment2startclick)

    function annasegment2overstartclick() {
        if (annaclrarr1[2] > 0 && (doll.frame.name + 1 != 2 && doll.frame.name + 1 != 6) && (colorindication || annaclrarr1[2] != annaclrarr2[2] || annasegment2dress2.frame.name != doll.frame.name + 1)) {
            annasegment2dress2.alpha = 0.6
            if (!colorindication) {
                annasegment2dress2.tint = colorcode[annaclrarr1[2]]
            } else {
                annasegment2dress2.tint = colorcode[annaclrarr2[2]]
            }
            if (colorindication) {} else {
                annasegment2dress2.setFrame(doll.frame.name + 1)
            }
            if (annaovrarr[2] == 0) {
                annasegment2dress2.alpha = 0.6
                if (!colorindication) {
                    annasegment2dress2.tint = colorcode[annaclrarr1[2]]
                } else if (annaclrarr2[2] == 0) {
                    annasegment2dress2.alpha = 0
                    annasegment2dress2.tint = colorcode[annaclrarr2[2]]
                }
            }
        } else if (colorindication && annaclrarr2[2] > 0) {
            annasegment2dress2.alpha = 0.6
        }
    }

    function annasegment2outstartclick() {
        if (annaclrarr1[2] > 0 && (doll.frame.name + 1 != 2 && doll.frame.name + 1 != 6) && (colorindication || annaclrarr1[2] != annaclrarr2[2] || annasegment2dress2.frame.name == doll.frame.name + 1)) {
            if (annaovrarr[2] == 0 && (annaclrarr2[2] == 0)) {
                annasegment2dress2.alpha = 0
            } else {
                annasegment2dress2.alpha = 1
                annasegment2dress2.tint = colorcode[annaclrarr2[2]]
                annasegment2dress2.setFrame(annadresarr1[2])
            }
        } else if (colorindication && annaclrarr2[2] > 0) {
            annasegment2dress2.alpha = 1
        } else if (colorindication) {
            annasegment2dress2.alpha = 0
        }
    }

    function annasegment2startclick() {
        if (music.isPlaying == false) {
            clicksound.stop()
        } else {
            clicksound.play()
        }
        if (colorindication) {
            annaovrarr[2] = 0
            annaclrarr1[2] = 0
            annaclrarr2[2] = 0
            annasegment2dress2.alpha = 0
        }
        if (doll.frame.name + 1 != 2 && doll.frame.name + 1 != 6) {
            if (annaclrarr1[2] > 0) {
                done.visible = true
                annasegment2dress2.alpha = 1
                annadresarr1[2] = 1
                annasegment2dress2.setFrame(doll.frame.name + 1)
                annasegment2dress2.tint = colorcode[annaclrarr1[2]]
                annadresarr1[2] = annasegment2dress2.frame.name
                annaovrarr[2] = 1
                annacolor[2] = colorcode[annaclrarr1[2]]
                annaclrarr2[2] = annaclrarr1[2]
            } else {
                annasegment2dress2.alpha = 0
            }
        }
    }
    annahit3.on('pointerover', annasegment3overstartclick)
    annahit3.on('pointerout', annasegment3outstartclick)
    annahit3.on('pointerdown', annasegment3startclick)

    function annasegment3overstartclick() {
        if (annaclrarr1[3] > 0 && (colorindication || annaclrarr1[3] != annaclrarr2[3] || (annasegment3dress3.frame.name != doll.frame.name + 1))) {
            annasegment3dress3.alpha = 0.6
            if (!colorindication) {
                annasegment3dress3.tint = colorcode[annaclrarr1[3]]
            } else {
                annasegment3dress3.tint = colorcode[annaclrarr2[3]]
            }
            if (colorindication) {} else {
                annasegment3dress3.setFrame(doll.frame.name + 1)
            }
            if (annaovrarr[3] == 0) {
                annasegment3dress3.alpha = 0.6
                if (!colorindication) {
                    annasegment3dress3.tint = colorcode[annaclrarr1[3]]
                } else if (annaclrarr2[3] == 0) {
                    annasegment3dress3.alpha = 0
                    annasegment3dress3.tint = colorcode[annaclrarr2[3]]
                }
            }
        } else if (colorindication && annaclrarr2[3] > 0) {
            annasegment3dress3.alpha = 0.6
        }
    }

    function annasegment3outstartclick() {
        if (annaclrarr1[3] > 0 && (colorindication || annaclrarr1[3] != annaclrarr2[3] || (annasegment3dress3.frame.name == doll.frame.name + 1))) {
            if (annaovrarr[3] == 0 && annaclrarr2[3] == 0) {
                annasegment3dress3.alpha = 0
            } else {
                annasegment3dress3.alpha = 1
                annasegment3dress3.tint = colorcode[annaclrarr2[3]]
                annasegment3dress3.setFrame(annadresarr1[3])
            }
        } else if (colorindication && annaclrarr2[3] > 0) {
            annasegment3dress3.alpha = 1
        } else if (colorindication) {
            annasegment3dress3.alpha = 0
        }
    }

    function annasegment3startclick() {
        if (music.isPlaying == false) {
            clicksound.stop()
        } else {
            clicksound.play()
        }
        if (colorindication) {
            annaovrarr[3] = 0
            annaclrarr1[3] = 0
            annaclrarr2[3] = 0
            annasegment3dress3.alpha = 0
        }
        if (annaclrarr1[3] > 0) {
            done.visible = true
            annasegment3dress3.alpha = 1
            annadresarr1[3] = 1
            annasegment3dress3.setFrame(doll.frame.name + 1)
            annasegment3dress3.tint = colorcode[annaclrarr1[3]]
            annadresarr1[3] = annasegment3dress3.frame.name
            annaovrarr[3] = 1
            annacolor[3] = colorcode[annaclrarr1[3]]
            annaclrarr2[3] = annaclrarr1[3]
        } else {
            annasegment3dress3.alpha = 0
        }
        if (doll.frame.name + 1 == 3 || doll.frame.name + 1 == 5 || doll.frame.name + 1 == 6) {
            annasegment41dress4.visible = true
            annasegment4dress4.visible = false
        } else {
            annasegment41dress4.visible = false
            annasegment4dress4.visible = true
        }
    }
    annahit4.on('pointerover', annasegment4overstartclick)
    annahit4.on('pointerout', annasegment4outstartclick)
    annahit4.on('pointerdown', annasegment4startclick)

    function annasegment4overstartclick() {
        if (annaclrarr1[4] > 0 && (doll.frame.name + 1 != 1) && (colorindication || annaclrarr1[4] != annaclrarr2[4] || annasegment4dress4.frame.name != doll.frame.name + 1)) {
            annasegment4dress4.alpha = 0.6
            annasegment41dress4.alpha = 0.6
            if (!colorindication) {
                annasegment4dress4.tint = colorcode[annaclrarr1[4]]
                annasegment41dress4.tint = colorcode[annaclrarr1[4]]
            } else {
                annasegment4dress4.tint = colorcode[annaclrarr2[4]]
                annasegment41dress4.tint = colorcode[annaclrarr2[4]]
            }
            if (colorindication) {} else {
                annasegment4dress4.setFrame(doll.frame.name + 1)
                annasegment41dress4.setFrame(doll.frame.name + 1)
            }
            if (annaovrarr[4] == 0) {
                annasegment4dress4.alpha = 0.6
                annasegment41dress4.alpha = 0.6
                if (!colorindication) {
                    annasegment4dress4.tint = colorcode[annaclrarr1[4]]
                } else if (annaclrarr2[4] == 0) {
                    annasegment4dress4.alpha = 0
                    annasegment41dress4.alpha = 0
                    annasegment4dress4.tint = colorcode[annaclrarr2[4]]
                    annasegment41dress4.tint = colorcode[annaclrarr2[4]]
                }
            }
        } else if (colorindication && annaclrarr2[4] > 0) {
            annasegment4dress4.alpha = 0.6
            annasegment41dress4.alpha = 0.6
        }
    }

    function annasegment4outstartclick() {
        if (annaclrarr1[4] > 0 && (doll.frame.name + 1 != 1) && (colorindication || annaclrarr1[4] != annaclrarr2[4] || annasegment4dress4.frame.name == doll.frame.name + 1)) {
            if (annaovrarr[4] == 0 && annaclrarr2[4] == 0) {
                annasegment4dress4.alpha = 0
                annasegment41dress4.alpha = 0
            } else {
                annasegment4dress4.alpha = 1
                annasegment4dress4.tint = colorcode[annaclrarr2[4]]
                annasegment4dress4.setFrame(annadresarr1[4])
                annasegment41dress4.alpha = 1
                annasegment41dress4.tint = colorcode[annaclrarr2[4]]
                annasegment41dress4.setFrame(annadresarr1[4])
            }
        } else if (colorindication && annaclrarr2[4] > 0) {
            annasegment4dress4.alpha = 1
            annasegment41dress4.alpha = 1
        } else if (colorindication) {
            annasegment4dress4.alpha = 0
            annasegment41dress4.alpha = 0
        }
    }

    function annasegment4startclick() {
        if (music.isPlaying == false) {
            clicksound.stop()
        } else {
            clicksound.play()
        }
        if (colorindication) {
            annaovrarr[4] = 0
            annaclrarr1[4] = 0
            annaclrarr2[4] = 0
            annasegment4dress4.alpha = 0
            annasegment41dress4.alpha = 0
        }
        if (doll.frame.name + 1 != 1) {
            if (annaclrarr1[4] > 0) {
                done.visible = true
                annasegment4dress4.alpha = 1
                annasegment41dress4.alpha = 1
                annadresarr1[4] = 1
                annasegment4dress4.setFrame(doll.frame.name + 1)
                annasegment41dress4.setFrame(doll.frame.name + 1)
                annasegment4dress4.tint = colorcode[annaclrarr1[4]]
                annasegment41dress4.tint = colorcode[annaclrarr1[4]]
                annadresarr1[4] = annasegment4dress4.frame.name
                annaovrarr[4] = 1
                annacolor[4] = colorcode[annaclrarr1[4]]
                annaclrarr2[4] = annaclrarr1[4]
            } else {
                annasegment4dress4.alpha = 0
                annasegment41dress4.alpha = 0
            }
        }
        if (doll.frame.name + 1 == 3 || doll.frame.name + 1 == 5 || doll.frame.name + 1 == 6) {
            annasegment41dress4.visible = true
            annasegment4dress4.visible = false
        } else {
            annasegment41dress4.visible = false
            annasegment4dress4.visible = true
        }
    }
    annahit5.on('pointerover', annasegment5overstartclick)
    annahit5.on('pointerout', annasegment5outstartclick)
    annahit5.on('pointerdown', annasegment5startclick)

    function annasegment5overstartclick() {
        if (annaclrarr1[5] > 0 && (doll.frame.name + 1 != 1 && doll.frame.name + 1 != 2 && doll.frame.name + 1 != 3 && doll.frame.name + 1 != 5) && (colorindication || annaclrarr1[5] != annaclrarr2[5] || annasegment5dress5.frame.name != doll.frame.name + 1)) {
            annasegment5dress5.alpha = 0.6
            if (!colorindication) {
                annasegment5dress5.tint = colorcode[annaclrarr1[5]]
            } else {
                annasegment5dress5.tint = colorcode[annaclrarr2[5]]
            }
            if (colorindication) {} else {
                annasegment5dress5.setFrame(doll.frame.name + 1)
            }
            if (annaovrarr[5] == 0) {
                annasegment5dress5.alpha = 0.6
                if (!colorindication) {
                    annasegment5dress5.tint = colorcode[annaclrarr1[5]]
                } else if (annaclrarr2[5] == 0) {
                    annasegment5dress5.alpha = 0
                    annasegment5dress5.tint = colorcode[annaclrarr2[5]]
                }
            }
        } else if (colorindication && annaclrarr2[5] > 0) {
            annasegment5dress5.alpha = 0.6
        }
    }

    function annasegment5outstartclick() {
        if (annaclrarr1[5] > 0 && (doll.frame.name + 1 != 1 && doll.frame.name + 1 != 2 && doll.frame.name + 1 != 3 && doll.frame.name + 1 != 5) && (colorindication || annaclrarr1[5] != annaclrarr2[5] || annasegment5dress5.frame.name == doll.frame.name + 1)) {
            if (annaovrarr[5] == 0 && annaclrarr2[5] == 0) {
                annasegment5dress5.alpha = 0
            } else {
                annasegment5dress5.alpha = 1
                annasegment5dress5.tint = colorcode[annaclrarr2[5]]
                annasegment5dress5.setFrame(annadresarr1[5])
            }
        } else if (colorindication && annaclrarr2[5] > 0) {
            annasegment5dress5.alpha = 1
        } else if (colorindication) {
            annasegment5dress5.alpha = 0
        }
    }

    function annasegment5startclick() {
        if (music.isPlaying == false) {
            clicksound.stop()
        } else {
            clicksound.play()
        }
        if (colorindication) {
            annaovrarr[5] = 0
            annaclrarr1[5] = 0
            annaclrarr2[5] = 0
            annasegment5dress5.alpha = 0
        }
        if (doll.frame.name + 1 != 1 && doll.frame.name + 1 != 2 && doll.frame.name + 1 != 3 && doll.frame.name + 1 != 5) {
            if (annaclrarr1[5] > 0) {
                done.visible = true
                annasegment5dress5.alpha = 1
                annadresarr1[5] = 1
                annasegment5dress5.setFrame(doll.frame.name + 1)
                annasegment5dress5.tint = colorcode[annaclrarr1[5]]
                annadresarr1[5] = annasegment5dress5.frame.name
                annaovrarr[5] = 1
                annacolor[5] = colorcode[annaclrarr1[5]]
                annaclrarr2[5] = annaclrarr1[5]
            } else {
                annasegment5dress5.alpha = 0
            }
        }
    }
    annahit6.on('pointerover', annasegment6overstartclick)
    annahit6.on('pointerout', annasegment6outstartclick)
    annahit6.on('pointerdown', annasegment6startclick)

    function annasegment6overstartclick() {
        if (annaclrarr1[6] > 0 && (doll.frame.name + 1 == 4) && (colorindication || annaclrarr1[6] != annaclrarr2[6] || 4 != doll.frame.name + 1)) {
            annasegment6dress6.alpha = 0.6
            if (!colorindication) {
                annasegment6dress6.tint = colorcode[annaclrarr1[6]]
            } else {
                annasegment6dress6.tint = colorcode[annaclrarr2[6]]
            }
            if (colorindication) {} else {
                annasegment6dress6.setFrame(1)
            }
            if (annaovrarr[6] == 0) {
                annasegment6dress6.alpha = 0.6
                if (!colorindication) {
                    annasegment6dress6.tint = colorcode[annaclrarr1[6]]
                } else if (annaclrarr2[6] == 0) {
                    annasegment6dress6.alpha = 0
                    annasegment6dress6.tint = colorcode[annaclrarr2[6]]
                }
                annasegment6dress6.setFrame(1)
            }
        } else if (colorindication && annaclrarr2[6] > 0) {
            annasegment6dress6.alpha = 0.6
        }
    }

    function annasegment6outstartclick() {
        if (annaclrarr1[6] > 0 && (doll.frame.name + 1 == 4) && (colorindication || annaclrarr1[6] != annaclrarr2[6] || 4 == doll.frame.name + 1)) {
            if (annaovrarr[6] == 0 && (annaclrarr2[6] == 0)) {
                annasegment6dress6.alpha = 0
            } else {
                annasegment6dress6.alpha = 1
                annasegment6dress6.tint = colorcode[annaclrarr2[6]]
                annasegment6dress6.setFrame(1)
            }
        } else if (colorindication && annaclrarr2[6] > 0) {
            annasegment6dress6.alpha = 1
        } else if (colorindication) {
            annasegment6dress6.alpha = 0
        }
    }

    function annasegment6startclick() {
        if (music.isPlaying == false) {
            clicksound.stop()
        } else {
            clicksound.play()
        }
        if (colorindication) {
            annaovrarr[6] = 0
            annaclrarr1[6] = 0
            annaclrarr2[6] = 0
            annasegment6dress6.alpha = 0
        }
        if (doll.frame.name + 1 == 4) {
            if (annaclrarr1[6] > 0) {
                done.visible = true
                annasegment6dress6.alpha = 1
                annadresarr1[6] = 1
                annasegment6dress6.setFrame(1)
                annasegment6dress6.tint = colorcode[annaclrarr1[6]]
                annadresarr1[6] = annasegment6dress6.frame.name
                annaovrarr[6] = 1
                annacolor[6] = colorcode[annaclrarr1[6]]
                annaclrarr2[6] = annaclrarr1[6]
            } else {
                annasegment6dress6.alpha = 0
            }
        }
    }
    annahit7.on('pointerover', annasegment7overstartclick)
    annahit7.on('pointerout', annasegment7outstartclick)
    annahit7.on('pointerdown', annasegment7startclick)

    function annasegment7overstartclick() {
        if (annaclrarr1[7] > 0 && (doll.frame.name + 1 == 4) && (colorindication || annaclrarr1[7] != annaclrarr2[7] || 4 != doll.frame.name + 1)) {
            annasegment7dress7.alpha = 0.6
            if (!colorindication) {
                annasegment7dress7.tint = colorcode[annaclrarr1[7]]
            } else {
                annasegment7dress7.tint = colorcode[annaclrarr2[7]]
            }
            if (colorindication) {} else {
                annasegment7dress7.setFrame(1)
            }
            if (annaovrarr[7] == 0) {
                annasegment7dress7.alpha = 0.6
                if (!colorindication) {
                    annasegment7dress7.tint = colorcode[annaclrarr1[7]]
                } else if (annaclrarr2[7] == 0) {
                    annasegment7dress7.alpha = 0
                    annasegment7dress7.tint = colorcode[annaclrarr2[7]]
                }
                annasegment7dress7.setFrame(1)
            }
        } else if (colorindication && annaclrarr2[7] > 0) {
            annasegment7dress7.alpha = 0.6
        }
    }

    function annasegment7outstartclick() {
        if (annaclrarr1[7] > 0 && (doll.frame.name + 1 == 4) && (colorindication || annaclrarr1[7] != annaclrarr2[7] || 4 == doll.frame.name + 1)) {
            if (annaovrarr[7] == 0 && (annaclrarr2[7] == 0)) {
                annasegment7dress7.alpha = 0
            } else {
                annasegment7dress7.alpha = 1
                annasegment7dress7.tint = colorcode[annaclrarr2[7]]
                annasegment7dress7.setFrame(1)
            }
        } else if (colorindication && annaclrarr2[7] > 0) {
            annasegment7dress7.alpha = 1
        } else if (colorindication) {
            annasegment7dress7.alpha = 0
        }
    }

    function annasegment7startclick() {
        if (music.isPlaying == false) {
            clicksound.stop()
        } else {
            clicksound.play()
        }
        if (colorindication) {
            annaovrarr[7] = 0
            annaclrarr1[7] = 0
            annaclrarr2[7] = 0
            annasegment7dress7.alpha = 0
        }
        if (doll.frame.name + 1 == 4) {
            if (annaclrarr1[7] > 0) {
                done.visible = true
                annasegment7dress7.alpha = 1
                annadresarr1[7] = 1
                annasegment7dress7.setFrame(1)
                annasegment7dress7.tint = colorcode[annaclrarr1[7]]
                annadresarr1[7] = annasegment7dress7.frame.name
                annaovrarr[7] = 1
                annacolor[7] = colorcode[annaclrarr1[7]]
                annaclrarr2[7] = annaclrarr1[7]
            } else {
                annasegment7dress7.alpha = 0
            }
        }
    }
    annahit8.on('pointerover', annasegment8overstartclick)
    annahit8.on('pointerout', annasegment8outstartclick)
    annahit8.on('pointerdown', annasegment8startclick)

    function annasegment8overstartclick() {
        if (annaclrarr1[8] > 0 && (doll.frame.name + 1 == 4) && (colorindication || annaclrarr1[8] != annaclrarr2[8] || 4 != doll.frame.name + 1)) {
            annasegment8dress8.alpha = 0.6
            if (!colorindication) {
                annasegment8dress8.tint = colorcode[annaclrarr1[8]]
            } else {
                annasegment8dress8.tint = colorcode[annaclrarr2[8]]
            }
            if (colorindication) {} else {
                annasegment8dress8.setFrame(1)
            }
            if (annaovrarr[8] == 0) {
                annasegment8dress8.alpha = 0.6
                if (!colorindication) {
                    annasegment8dress8.tint = colorcode[annaclrarr1[8]]
                } else if (annaclrarr2[8] == 0) {
                    annasegment8dress8.alpha = 0
                    annasegment8dress8.tint = colorcode[annaclrarr2[8]]
                }
                annasegment8dress8.setFrame(1)
            }
        } else if (colorindication && annaclrarr2[8] > 0) {
            annasegment8dress8.alpha = 0.6
        }
    }

    function annasegment8outstartclick() {
        if (annaclrarr1[8] > 0 && (doll.frame.name + 1 == 4) && (colorindication || annaclrarr1[8] != annaclrarr2[8] || 4 == doll.frame.name + 1)) {
            if (annaovrarr[8] == 0 && annaclrarr2[8] == 0) {
                annasegment8dress8.alpha = 0
            } else {
                annasegment8dress8.alpha = 1
                annasegment8dress8.tint = colorcode[annaclrarr2[8]]
                annasegment8dress8.setFrame(1)
            }
        } else if (colorindication && annaclrarr2[8] > 0) {
            annasegment8dress8.alpha = 1
        } else if (colorindication) {
            annasegment8dress8.alpha = 0
        }
    }

    function annasegment8startclick() {
        if (music.isPlaying == false) {
            clicksound.stop()
        } else {
            clicksound.play()
        }
        if (colorindication) {
            annaovrarr[8] = 0
            annaclrarr1[8] = 0
            annaclrarr2[8] = 0
            annasegment8dress8.alpha = 0
        }
        if (doll.frame.name + 1 == 4) {
            if (annaclrarr1[8] > 0) {
                done.visible = true
                annasegment8dress8.alpha = 1
                annadresarr1[8] = 1
                annasegment8dress8.setFrame(1)
                annasegment8dress8.tint = colorcode[annaclrarr1[8]]
                annadresarr1[8] = annasegment8dress8.frame.name
                annaovrarr[8] = 1
                annacolor[8] = colorcode[annaclrarr1[8]]
                annaclrarr2[8] = annaclrarr1[8]
            } else {
                annasegment8dress8.alpha = 0
            }
        }
    }
    annahit9.on('pointerover', annasegment9overstartclick)
    annahit9.on('pointerout', annasegment9outstartclick)
    annahit9.on('pointerdown', annasegment9startclick)

    function annasegment9overstartclick() {
        if (annaclrarr1[9] > 0 && (doll.frame.name + 1 == 4) && (colorindication || annaclrarr1[9] != annaclrarr2[9] || 4 != doll.frame.name + 1)) {
            annasegment9dress9.alpha = 0.6
            if (!colorindication) {
                annasegment9dress9.tint = colorcode[annaclrarr1[9]]
            } else {
                annasegment9dress9.tint = colorcode[annaclrarr2[9]]
            }
            if (colorindication) {} else {
                annasegment9dress9.setFrame(1)
            }
            if (annaovrarr[9] == 0) {
                annasegment9dress9.alpha = 0.6
                if (!colorindication) {
                    annasegment9dress9.tint = colorcode[annaclrarr1[9]]
                } else if (annaclrarr2[9] == 0) {
                    annasegment9dress9.alpha = 0
                    annasegment9dress9.tint = colorcode[annaclrarr2[9]]
                }
                annasegment9dress9.setFrame(1)
            }
        } else if (colorindication && annaclrarr2[9] > 0) {
            annasegment9dress9.alpha = 0.6
        }
    }

    function annasegment9outstartclick() {
        if (annaclrarr1[9] > 0 && (doll.frame.name + 1 == 4) && (colorindication || annaclrarr1[9] != annaclrarr2[9] || 4 == doll.frame.name + 1)) {
            if (annaovrarr[9] == 0 && (annaclrarr2[9] == 0)) {
                annasegment9dress9.alpha = 0
            } else {
                annasegment9dress9.alpha = 1
                annasegment9dress9.tint = colorcode[annaclrarr2[9]]
                annasegment9dress9.setFrame(1)
            }
        } else if (colorindication && annaclrarr2[9] > 0) {
            annasegment9dress9.alpha = 1
        } else if (colorindication) {
            annasegment9dress9.alpha = 0
        }
    }

    function annasegment9startclick() {
        if (music.isPlaying == false) {
            clicksound.stop()
        } else {
            clicksound.play()
        }
        if (colorindication) {
            annaovrarr[9] = 0
            annaclrarr1[9] = 0
            annaclrarr2[9] = 0
            annasegment9dress9.alpha = 0
        }
        if (doll.frame.name + 1 == 4) {
            if (annaclrarr1[9] > 0) {
                done.visible = true
                annasegment9dress9.alpha = 1
                annadresarr1[9] = 1
                annasegment9dress9.setFrame(1)
                annasegment9dress9.tint = colorcode[annaclrarr1[9]]
                annadresarr1[9] = annasegment9dress9.frame.name
                annaovrarr[9] = 1
                annacolor[9] = colorcode[annaclrarr1[9]]
                annaclrarr2[9] = annaclrarr1[9]
            } else {
                annasegment9dress9.alpha = 0
            }
        }
    }
    annasegment1dress1.on('pointerdown', annasegment1dress1startclick)

    function annasegment1dress1startclick() {
        if (colorindication) {
            if (music.isPlaying == false) {
                clicksound.stop()
            } else {
                clicksound.play()
            }
            annasegment1dress1.alpha = 0
            annaovrarr[1] = 0
            annaclrarr2[1] = 0
        }
    }
    annasegment2dress2.on('pointerdown', annasegment2dress2startclick)

    function annasegment2dress2startclick() {
        if (colorindication) {
            if (music.isPlaying == false) {
                clicksound.stop()
            } else {
                clicksound.play()
            }
            annasegment2dress2.alpha = 0
            annaovrarr[2] = 0
            annaclrarr2[2] = 0
        }
    }
    annasegment3dress3.on('pointerdown', annasegment3dress3startclick)

    function annasegment3dress3startclick() {
        if (colorindication) {
            if (music.isPlaying == false) {
                clicksound.stop()
            } else {
                clicksound.play()
            }
            annasegment3dress3.alpha = 0
            annaovrarr[3] = 0
            annaclrarr2[3] = 0
        }
    }
    annasegment4dress4.on('pointerdown', annasegment4dress4startclick)
    annasegment41dress4.on('pointerdown', annasegment4dress4startclick)

    function annasegment4dress4startclick() {
        if (colorindication) {
            if (music.isPlaying == false) {
                clicksound.stop()
            } else {
                clicksound.play()
            }
            annasegment4dress4.alpha = 0
            annasegment41dress4.alpha = 0
            annaovrarr[4] = 0
            annaclrarr2[4] = 0
        }
    }
    annasegment5dress5.on('pointerdown', annasegment5dress5startclick)

    function annasegment5dress5startclick() {
        if (colorindication) {
            if (music.isPlaying == false) {
                clicksound.stop()
            } else {
                clicksound.play()
            }
            annasegment5dress5.alpha = 0
            annaovrarr[5] = 0
            annaclrarr2[5] = 0
        }
    }
    annasegment6dress6.on('pointerdown', annasegment6dress6startclick)

    function annasegment6dress6startclick() {
        if (colorindication) {
            if (music.isPlaying == false) {
                clicksound.stop()
            } else {
                clicksound.play()
            }
            annasegment6dress6.alpha = 0
            annaovrarr[6] = 0
            annaclrarr2[6] = 0
        }
    }
    annasegment7dress7.on('pointerdown', annasegment7dress7startclick)

    function annasegment7dress7startclick() {
        if (colorindication) {
            if (music.isPlaying == false) {
                clicksound.stop()
            } else {
                clicksound.play()
            }
            annasegment7dress7.alpha = 0
            annaovrarr[7] = 0
            annaclrarr2[7] = 0
        }
    }
    annasegment8dress8.on('pointerdown', annasegment8dress8startclick)

    function annasegment8dress8startclick() {
        if (colorindication) {
            if (music.isPlaying == false) {
                clicksound.stop()
            } else {
                clicksound.play()
            }
            annasegment8dress8.alpha = 0
            annaovrarr[8] = 0
            annaclrarr2[8] = 0
        }
    }
    annasegment9dress9.on('pointerdown', annasegment9dress9startclick)

    function annasegment9dress9startclick() {
        if (colorindication) {
            if (music.isPlaying == false) {
                clicksound.stop()
            } else {
                clicksound.play()
            }
            annasegment9dress9.alpha = 0
            annaovrarr[9] = 0
            annaclrarr2[9] = 0
        }
    }
    for (i = 1; i <= 17; i++) {
        game['color' + i].on('pointerover', annacoloroverstartclick)
        game['color' + i].on('pointerout', annacoloroutstartclick)
        game['color' + i].on('pointerdown', annacolorstartclick)
    }

    function annacoloroverstartclick() {
        if (annaclrarr1[7] != this.texture.key.substr(5)) {
            game['color' + this.texture.key.substr(5)].setScale(1.05, 1.05)
        }
    }

    function annacoloroutstartclick() {
        if (annaclrarr1[7] != this.texture.key.substr(5)) {
            game['color' + this.texture.key.substr(5)].setScale(1, 1)
        }
    }

    function annacolorstartclick() {
        if (handindication) {
            if (music.isPlaying == false) {
                clicksound.stop()
            } else {
                clicksound.play()
            }
            sno = this.texture.key.substr(5)
            annaclrarr1[1] = parseInt(sno)
            annaclrarr1[2] = parseInt(sno)
            annaclrarr1[3] = parseInt(sno)
            annaclrarr1[4] = parseInt(sno)
            annaclrarr1[5] = parseInt(sno)
            annaclrarr1[6] = parseInt(sno)
            annaclrarr1[7] = parseInt(sno)
            annaclrarr1[8] = parseInt(sno)
            annaclrarr1[9] = parseInt(sno)
            annaovrarr[1] = 0
            annaovrarr[2] = 0
            annaovrarr[3] = 0
            annaovrarr[4] = 0
            annaovrarr[5] = 0
            annaovrarr[6] = 0
            annaovrarr[7] = 0
            annaovrarr[7] = 0
            annaovrarr[8] = 0
            annaovrarr[9] = 0
            for (i = 1; i <= 17; i++) {
                game['color' + i].setScale(1, 1)
            }
            game['color' + annaclrarr1[7]].setScale(1.05, 1.05)
        }
    }
    tailorbtn.on('pointerdown', tailorbtnstartclick)

    function tailorbtnstartclick() {
        if (music.isPlaying == false) {
            clicksound.stop()
        } else {
            clicksound.play()
        }
        if (!handindication) {
            colorindication = false
            annaline.visible = true
            handindication = true
            tailorbtn.setScale(1.05, 1.05)
            cutbtn.setScale(1, 1)
            cutbtn.setFrame(0)
            tailorbtn.setFrame(1)
            annahit1.setInteractive({
                pixelPerfect: true,
                useHandCursor: true
            })
            annahit2.setInteractive({
                pixelPerfect: true,
                useHandCursor: true
            })
            annahit3.setInteractive({
                pixelPerfect: true,
                useHandCursor: true
            })
            annahit4.setInteractive({
                pixelPerfect: true,
                useHandCursor: true
            })
            annahit5.setInteractive({
                pixelPerfect: true,
                useHandCursor: true
            })
            annahit6.setInteractive({
                pixelPerfect: true,
                useHandCursor: true
            })
            annahit7.setInteractive({
                pixelPerfect: true,
                useHandCursor: true
            })
            annahit8.setInteractive({
                pixelPerfect: true,
                useHandCursor: true
            })
            annahit9.setInteractive({
                pixelPerfect: true,
                useHandCursor: true
            })
            annasegment1dress1.disableInteractive();
            annasegment2dress2.disableInteractive();
            annasegment3dress3.disableInteractive();
            annasegment4dress4.disableInteractive();
            annasegment41dress4.disableInteractive();
            annasegment5dress5.disableInteractive();
            annasegment6dress6.disableInteractive();
            annasegment7dress7.disableInteractive();
            annasegment8dress8.disableInteractive();
            annasegment9dress9.disableInteractive();
        }
    }
    cutbtn.on('pointerdown', cutbtnstartclick)

    function cutbtnstartclick() {
        if (music.isPlaying == false) {
            clicksound.stop()
        } else {
            clicksound.play()
        }
        annaline.visible = false
        handindication = false
        colorindication = true
        cutbtn.setScale(1.05, 1.05)
        tailorbtn.setScale(1, 1)
        cutbtn.setFrame(1)
        tailorbtn.setFrame(0)
        annahit1.disableInteractive();
        annahit2.disableInteractive();
        annahit3.disableInteractive();
        annahit4.disableInteractive();
        annahit5.disableInteractive();
        annahit6.disableInteractive();
        annahit7.disableInteractive();
        annahit8.disableInteractive();
        annahit9.disableInteractive();
        annasegment1dress1.setInteractive({
            pixelPerfect: true,
            useHandCursor: true
        })
        annasegment2dress2.setInteractive({
            pixelPerfect: true,
            useHandCursor: true
        })
        annasegment3dress3.setInteractive({
            pixelPerfect: true,
            useHandCursor: true
        })
        annasegment4dress4.setInteractive({
            pixelPerfect: true,
            useHandCursor: true
        })
        annasegment41dress4.setInteractive({
            pixelPerfect: true,
            useHandCursor: true
        })
        annasegment5dress5.setInteractive({
            pixelPerfect: true,
            useHandCursor: true
        })
        annasegment6dress6.setInteractive({
            pixelPerfect: true,
            useHandCursor: true
        })
        annasegment7dress7.setInteractive({
            pixelPerfect: true,
            useHandCursor: true
        })
        annasegment8dress8.setInteractive({
            pixelPerfect: true,
            useHandCursor: true
        })
        annasegment9dress9.setInteractive({
            pixelPerfect: true,
            useHandCursor: true
        })
    }
    rarrow.on('pointerdown', rarrowstartclick)

    function rarrowstartclick() {
        if (music.isPlaying == false) {
            clicksound.stop()
        } else {
            clicksound.play()
        }
        if (doll.frame.name == 0) {
            doll.setFrame(1)
        } else if (doll.frame.name == 1) {
            doll.setFrame(2)
        } else if (doll.frame.name == 2) {
            doll.setFrame(3)
        } else if (doll.frame.name == 3) {
            doll.setFrame(4)
        } else if (doll.frame.name == 4) {
            doll.setFrame(5)
        } else if (doll.frame.name == 5) {
            doll.setFrame(0)
        }
    }
    larrow.on('pointerdown', larrowstartclick)

    function larrowstartclick() {
        if (music.isPlaying == false) {
            clicksound.stop()
        } else {
            clicksound.play()
        }
        if (doll.frame.name == 0) {
            doll.setFrame(5)
        } else if (doll.frame.name == 5) {
            doll.setFrame(4)
        } else if (doll.frame.name == 4) {
            doll.setFrame(3)
        } else if (doll.frame.name == 3) {
            doll.setFrame(2)
        } else if (doll.frame.name == 2) {
            doll.setFrame(1)
        } else if (doll.frame.name == 1) {
            doll.setFrame(0)
        }
    }
}
var startgame10 = false
var handindication = false
var colorindication = false
var rapunstorearr = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
var rapunclrarr1 = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
var rapunclrarr2 = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
var rapunovrarr = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
var rapundresarr1 = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
var darr4 = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
var rapuncolor = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
var colorcode = [, ' 0xff3838', ' 0xfe7d3b', ' 0xffe33b', ' 0x9cdc3b', ' 0x3ba13b', ' 0x3bd8c8', ' 0x3bdcff', ' 0x3b91ed', ' 0x3f3ce6', ' 0x7443f6', ' 0xfeb463', ' 0x9b9b9b', ' 0xb04ab1', ' 0xff8be6', '0xed3b8a', ' 0xfff2f2', ' 0x3b2e2e']
var Level8 = new Phaser.Class({
    Extends: Phaser.Scene,
    initialize: function Level8() {
        Phaser.Scene.call(this, {
            key: 'Level8'
        });
    },
    preload: function () {
        rapuncolor = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        colorcode = [, ' 0xff3838', ' 0xfe7d3b', ' 0xffe33b', ' 0x9cdc3b', ' 0x3ba13b', ' 0x3bd8c8', ' 0x3bdcff', ' 0x3b91ed', ' 0x3f3ce6', ' 0x7443f6', ' 0xfeb463', ' 0x9b9b9b', ' 0xb04ab1', ' 0xff8be6', '0xed3b8a', ' 0xfff2f2', ' 0x3b2e2e']
        darr4 = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        startgame10 = false
        loadFinish = false
        clrarr = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        rapunclrarr1 = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        rapunclrarr2 = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        rapunovrarr = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        rapunstorearr = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        rapundresarr1 = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        handindication = false
        colorindication = false
    },
    create: function () {
        rapunbackground1 = this.add.image(400, 300, 'rapunbackground1').setOrigin(0.5, 0.5)
        dressuppanel = this.add.image(26, 40, 'dressuppanel').setOrigin(0, 0)
        colortext = this.add.image(83, 302, 'colortext').setOrigin(0, 0)
        tailorbtn = this.add.image(81, 136, 'tailorbtn').setOrigin(0.5, 0.5).setInteractive({
            pixelPerfect: true,
            useHandCursor: true,
        })
        tailorbtn.x += parseFloat(tailorbtn.width / 2)
        tailorbtn.y += parseFloat(tailorbtn.height / 2)
        cutbtn = this.add.image(150, 221, 'cutbtn').setOrigin(0.5, 0.5).setInteractive({
            pixelPerfect: true,
            useHandCursor: true,
        })
        cutbtn.x += parseFloat(cutbtn.width / 2)
        cutbtn.y += parseFloat(cutbtn.height / 2)
        var coloxarr = [, 80, 130, 178, 228, 277, 328, 79, 129, 178, 228, 276, 326, 128, 179, 229, 279, 327]
        var coloyarr = [, 369, 370, 370, 370, 370, 370, 424, 424, 424, 424, 424, 424, 477, 477, 477, 477, 477]
        for (i = 1; i <= 17; i++) {
            game['color' + i] = this.add.image(coloxarr[i], coloyarr[i], 'color' + i).setOrigin(0.5, 0.5).setInteractive({
                pixelPerfect: true,
                useHandCursor: true,
            })
            game['color' + i].x += parseFloat(game['color' + i].width / 2)
            game['color' + i].y += parseFloat(game['color' + i].height / 2)
        }
        dressphotoframe = this.add.image(251, 89, 'dressphotoframe').setOrigin(0, 0)
        rarrow = this.add.image(412, 143, 'arrow').setOrigin(0, 0).setInteractive({
            pixelPerfect: true,
            useHandCursor: true,
        })
        larrow = this.add.image(262, 147, 'arrow').setOrigin(0, 0).setInteractive({
            pixelPerfect: true,
            useHandCursor: true,
        })
        larrow.setScale(-1, 1)
        larrow.angle = -7
        this.tweens.add({
            targets: rarrow,
            x: 416,
            ease: 'Linear',
            duration: 400,
            repeat: -1,
            yoyo: true,
        })
        this.tweens.add({
            targets: larrow,
            x: 258,
            ease: 'Linear',
            duration: 400,
            repeat: -1,
            yoyo: true,
        })
        doll = this.add.image(284, 74, 'rapundoll').setOrigin(0, 0)
        rapunbhair = this.add.sprite(151, 98, 'rapunbhair').setOrigin(0, 0)
        rapunbody = this.add.sprite(99, 46, 'rapunbody').setOrigin(0, 0)
        rapunlips = this.add.sprite(198, 115, 'rapunlips').setOrigin(0, 0)
        rapuneye1 = this.add.sprite(200, 98, 'rapuneye1').setOrigin(0, 0)
        rapuneye2 = this.add.sprite(200, 98, 'rapuneye2').setOrigin(0, 0)
        rapunblush = this.add.sprite(196, 111, 'rapunblush').setOrigin(0, 0)
        rapuntoplayer = this.add.sprite(191, 86, 'rapuntoplayer').setOrigin(0, 0)
        rapuneshade = this.add.sprite(191, 91, 'rapuneshade').setOrigin(0, 0)
        rapunelash = this.add.sprite(193, 94, 'rapunelash').setOrigin(0, 0)
        rapunebrow = this.add.sprite(190, 83, 'rapunebrow').setOrigin(0, 0)
        rapunhair = this.add.sprite(146, 34, 'rapunhair').setOrigin(0, 0)
        rapunsegment1dress1 = this.add.sprite(164, 154, 'rapunsegment1dress1').setOrigin(0, 0)
        rapunsegment2dress2 = this.add.sprite(188, 227, 'rapunsegment2dress2').setOrigin(0, 0)
        rapunsegment3dress3 = this.add.sprite(139, 247, 'rapunsegment3dress3').setOrigin(0, 0)
        rapunsegment4dress4 = this.add.sprite(132, 349, 'rapunsegment4dress4').setOrigin(0, 0)
        rapunsegment41dress4 = this.add.sprite(132, 349, 'rapunsegment4dress4').setOrigin(0, 0)
        rapunsegment5dress5 = this.add.sprite(107, 429, 'rapunsegment5dress5').setOrigin(0, 0)
        rapunsegment51dress5 = this.add.sprite(107, 429, 'rapunsegment5dress5').setOrigin(0, 0)
        rapunsegment6dress6 = this.add.sprite(90, 329, 'rapunsegment6dress6').setOrigin(0, 0)
        rapunsegment7dress7 = this.add.sprite(256, 326, 'rapunsegment7dress7').setOrigin(0, 0)
        rapunelash.setFrame(marr4[0])
        rapunblush.setFrame(marr4[1])
        rapunlips.setFrame(marr4[2])
        rapuneye1.setFrame(marr4[3])
        rapuneye2.setFrame(marr4[3])
        rapuneshade.setFrame(marr4[4])
        rapunebrow.setFrame(marr4[5])
        rapunhair.setFrame(marr4[6])
        rapunbhair.setFrame(marr4[6])
        var rapundollgroup = this.add.container(0, 0);
        rapundollgroup.add(rapunsegment7dress7)
        rapundollgroup.add(rapunsegment6dress6)
        rapundollgroup.add(rapunbhair)
        rapundollgroup.add(rapunbody)
        rapundollgroup.add(rapunlips)
        rapundollgroup.add(rapuneye1)
        rapundollgroup.add(rapuneye2)
        rapundollgroup.add(rapuntoplayer)
        rapundollgroup.add(rapuneshade)
        rapundollgroup.add(rapunelash)
        rapundollgroup.add(rapunblush)
        rapundollgroup.add(rapunebrow)
        rapundollgroup.add(rapunsegment5dress5)
        rapundollgroup.add(rapunsegment2dress2)
        rapundollgroup.add(rapunsegment4dress4)
        rapundollgroup.add(rapunsegment3dress3)
        rapundollgroup.add(rapunsegment41dress4)
        rapundollgroup.add(rapunsegment51dress5)
        rapundollgroup.add(rapunsegment1dress1)
        rapundollgroup.add(rapunhair)
        rapundollgroup.x = 397
        rapundollgroup.y = 10
        rapunsegment41dress4.visible = false
        rapunline = this.add.image(445, 180, 'elsaline').setOrigin(0, 0)
        rapunline.visible = false
        rapunhit1 = this.add.image(624.95, 220, 'elsahit1').setOrigin(0.5, 0.5).setInteractive({
            pixelperfect: true,
            useHandCursor: true
        })
        rapunhit2 = this.add.image(625.95, 258, 'elsahit1').setOrigin(0.5, 0.5).setInteractive({
            pixelperfect: true,
            useHandCursor: true
        })
        rapunhit2.setScale(0.64)
        rapunhit3 = this.add.image(617.95, 337, 'elsahit1').setOrigin(0.5, 0.5).setInteractive({
            pixelperfect: true,
            useHandCursor: true
        })
        rapunhit3.setScale(2.02)
        rapunhit3.angle = 90
        rapunhit4 = this.add.image(624.95, 453, 'elsahit1').setOrigin(0.5, 0.5).setInteractive({
            pixelperfect: true,
            useHandCursor: true
        })
        rapunhit4.setScale(1.86)
        rapunhit5 = this.add.image(627.95, 552, 'elsahit1').setOrigin(0.5, 0.5).setInteractive({
            pixelperfect: true,
            useHandCursor: true
        })
        rapunhit5.setScale(2.25)
        rapunhit6 = this.add.image(528.05, 425, 'elsahit1').setOrigin(0.5, 0.5).setInteractive({
            pixelperfect: true,
            useHandCursor: true
        })
        rapunhit6.setScale(1.2)
        rapunhit6.angle = 108
        rapunhit7 = this.add.image(712.05, 425, 'elsahit1').setOrigin(0.5, 0.5).setInteractive({
            pixelperfect: true,
            useHandCursor: true
        })
        rapunhit7.setScale(1.2)
        rapunhit7.angle = 73
        var tween1 = this.tweens.add({
            targets: rapunline,
            alpha: 0.3,
            ease: 'Linear',
            duration: 1000,
            repeat: -1,
            yoyo: true,
        });
        var tween1 = this.tweens.add({
            targets: rapuneye1,
            x: rapuneye1.x + 2,
            ease: 'Linear',
            duration: 300,
            delay: 2400,
            callbackScope: this,
        });
        var tween2 = this.tweens.add({
            targets: rapuneye2,
            x: rapuneye2.x + 2,
            ease: 'Linear',
            duration: 300,
            delay: 2400,
            onComplete: rapuneyestartani1,
            callbackScope: this
        });

        function rapuneyestartani1() {
            this.tweens.add({
                targets: rapuneye1,
                x: rapuneye1.x - 2,
                ease: 'Linear',
                duration: 300,
                delay: 3000,
                callbackScope: this
            });
            this.tweens.add({
                targets: rapuneye2,
                x: rapuneye2.x - 2,
                ease: 'Linear',
                duration: 300,
                delay: 3000,
                onComplete: rapuneyestartani2,
                callbackScope: this,
            })
        }

        function rapuneyestartani2() {
            this.tweens.add({
                targets: rapuneye1,
                x: rapuneye1.x + 2,
                ease: 'Linear',
                duration: 300,
                delay: 2400,
                callbackScope: this
            });
            this.tweens.add({
                targets: rapuneye2,
                x: rapuneye2.x + 2,
                ease: 'Linear',
                duration: 300,
                delay: 2400,
                onComplete: rapuneyestartani1,
                callbackScope: this,
            })
        }
        rapuneshad = this.time.addEvent({
            delay: 1000,
            callback: rapuneblinkstart1,
            callbackScope: this
        });

        function rapuneblinkstart1() {
            rapuneshade.setFrame(marr4[4] + 1)
            rapunelash.visible = false
            rapuneshad = this.time.addEvent({
                delay: 200,
                callback: rapuneblinkstart2,
                callbackScope: this
            });
        }

        function rapuneblinkstart2() {
            rapuneshade.setFrame(marr4[4])
            rapunelash.visible = true
            rapuneshad = this.time.addEvent({
                delay: 4000,
                callback: rapuneblinkstart1,
                callbackScope: this
            });
        }
        rapunlip = this.time.addEvent({
            delay: 1000,
            callback: rapunlipsstart1,
            callbackScope: this
        });

        function rapunlipsstart1() {
            rapunlips.setFrame(marr4[2] + 1)
            rapunlip = this.time.addEvent({
                delay: 100,
                callback: rapunlipsstart2,
                callbackScope: this
            });
        }

        function rapunlipsstart2() {
            rapunlips.setFrame(marr4[2] + 2)
            rapunlip = this.time.addEvent({
                delay: 2500,
                callback: rapunlipsstart3,
                callbackScope: this
            });
        }

        function rapunlipsstart3() {
            rapunlips.setFrame(marr4[2] + 1)
            rapunlip = this.time.addEvent({
                delay: 100,
                callback: rapunlipsstart4,
                callbackScope: this
            });
        }

        function rapunlipsstart4() {
            rapunlips.setFrame(marr4[2])
            rapunlip = this.time.addEvent({
                delay: 3000,
                callback: rapunlipsstart1,
                callbackScope: this
            });
        }
        clicksound = this.sound.add('clickss');
        done = this.add.image(790, 590, 'uiButton').setOrigin(1, 1).setInteractive({
            pixelPerfect: true,
            useHandCursor: true
        })
        done.setFrame("button_next")
        done.visible = false
        done.on('pointerover', doneoverclick1);
        done.on('pointerout', doneoutclick1);
        done.on('pointerdown', donedownclick1);
        done.on('pointerup', doneupclick1);

        function doneoverclick1() {
            done.setScale(1.05, 1.05)
        }

        function doneoutclick1() {
            done.setScale(1, 1)
        }

        function doneupclick1() {
            if (startgame10) {
                done.setScale(1.05, 1.05)
            }
        }

        function donedownclick1() {
            if (!startgame10 && loadFinish) {
                startgame10 = true
                if (music.isPlaying == false) {
                    clicksound.stop()
                } else {
                    clicksound.play()
                }
                done.setScale(1, 1)
                rapunstorearr[4] = rapunsegment7dress7.frame.name
                rapunstorearr[5] = rapunsegment6dress6.frame.name
                rapunstorearr[6] = rapunsegment1dress1.frame.name
                rapunstorearr[7] = rapunsegment5dress5.frame.name
                rapunstorearr[8] = rapunsegment4dress4.frame.name
                rapunstorearr[9] = rapunsegment3dress3.frame.name
                rapunstorearr[10] = rapunsegment2dress2.frame.name
                rapunstorearr[15] = rapunsegment7dress7.alpha
                rapunstorearr[16] = rapunsegment6dress6.alpha
                rapunstorearr[17] = rapunsegment1dress1.alpha
                rapunstorearr[18] = rapunsegment5dress5.alpha
                rapunstorearr[19] = rapunsegment4dress4.alpha
                rapunstorearr[20] = rapunsegment3dress3.alpha
                rapunstorearr[21] = rapunsegment2dress2.alpha
                rapunstorearr[22] = rapunsegment4dress4.visible
                rapunstorearr[23] = rapunsegment5dress5.visible
                transitionbackground1.alpha = 1
                transitionbackground2.alpha = 1
                transitionbackground3.alpha = 1
                transitionpanel.alpha = 1
                transitiondress.alpha = 1
                transitiontitle1.alpha = 1
                transitiontitle2.alpha = 1
                transitiontitle3.alpha = 1
                transitionstar1.alpha = 1
                transitionstar2.alpha = 1
                transitionstar3.alpha = 1
                transitionstar4.alpha = 1
                transitionstar5.alpha = 1
                transitionstar1.setScale(0)
                transitionstar2.setScale(0)
                transitionstar3.setScale(0)
                transitionstar4.setScale(0)
                transitionstar5.setScale(0)
                transitionbackground1.visible = true
                transitionbackground2.visible = true
                transitionbackground3.visible = true
                transitiondress.visible = false
                transitionpanel.visible = false
                transitiontitle1.visible = false
                transitiontitle2.visible = false
                transitiontitle3.visible = false
                transitionstar1.visible = false
                transitionstar2.visible = false
                transitionstar3.visible = false
                transitionstar4.visible = false
                transitionstar5.visible = false
                transitionbackground1.alpha = 0
                transitionbackground2.alpha = 0
                transitionbackground3.alpha = 0
                this.scene.tweens.add({
                    targets: transitionbackground1,
                    alpha: 1,
                    ease: 'Linear',
                    duration: 800,
                    onComplete: transitionstart1,
                    callbackScope: this
                })

                function transitionstart1() {
                    transitiondress.visible = true
                    transitiondress.alpha = 0
                    this.scene.tweens.add({
                        targets: transitiondress,
                        alpha: 1,
                        ease: 'Linear',
                        duration: 400,
                        onComplete: transitionstart2,
                        callbackScope: this
                    })
                    this.scene.tweens.add({
                        targets: transitionbackground2,
                        alpha: 1,
                        ease: 'Linear',
                        duration: 800,
                    })
                }

                function transitionstart2() {
                    transitionpanel.visible = true
                    transitionpanel.setScale(0)
                    this.scene.tweens.add({
                        targets: transitionpanel,
                        scaleX: 1,
                        scaleY: 1,
                        ease: 'Linear',
                        duration: 500,
                        onComplete: transitionstart3,
                        callbackScope: this
                    })
                }

                function transitionstart3() {
                    this.scene.tweens.add({
                        targets: transitionbackground3,
                        alpha: 1,
                        ease: 'Linear',
                        duration: 800,
                        onComplete: transitionstart4,
                        callbackScope: this
                    })
                }

                function transitionstart4() {
                    transitiontitle1.visible = true
                    transitiontitle2.visible = true
                    transitiontitle3.visible = true
                    transitiontitle1.alpha = 0
                    transitiontitle2.alpha = 0
                    transitiontitle3.alpha = 0
                    transitiontitle2.x = transitiontitle2.x + 100
                    transitiontitle3.x = transitiontitle3.x - 100
                    this.scene.tweens.add({
                        targets: transitiontitle1,
                        alpha: 1,
                        ease: 'Linear',
                        duration: 800,
                        onComplete: transitionstart5,
                        callbackScope: this
                    })
                    this.scene.tweens.add({
                        targets: transitiontitle2,
                        alpha: 1,
                        x: transitiontitle2.x - 100,
                        ease: 'Linear',
                        duration: 800,
                    })
                    this.scene.tweens.add({
                        targets: transitiontitle3,
                        alpha: 1,
                        x: transitiontitle3.x + 100,
                        ease: 'Linear',
                        duration: 800,
                    })
                }

                function transitionstart5() {
                    transitionstar1.setScale(0)
                    transitionstar2.setScale(0)
                    transitionstar3.setScale(0)
                    transitionstar4.setScale(0)
                    transitionstar5.setScale(0)
                    transitionstar1.visible = true
                    transitionstar2.visible = true
                    transitionstar3.visible = true
                    transitionstar4.visible = true
                    transitionstar5.visible = true
                    this.scene.tweens.add({
                        targets: transitionstar1,
                        scaleX: 1,
                        scaleY: 1,
                        ease: 'Linear',
                        duration: 300,
                    })
                    this.scene.tweens.add({
                        targets: transitionstar2,
                        scaleX: 0.9,
                        scaleY: 0.9,
                        ease: 'Linear',
                        duration: 300,
                    })
                    this.scene.tweens.add({
                        targets: transitionstar3,
                        scaleX: 0.9,
                        scaleY: 0.9,
                        ease: 'Linear',
                        duration: 300,
                    })
                    this.scene.tweens.add({
                        targets: transitionstar4,
                        scaleX: 0.8,
                        scaleY: 0.8,
                        ease: 'Linear',
                        duration: 300,
                    })
                    this.scene.tweens.add({
                        targets: transitionstar5,
                        scaleX: 0.65,
                        scaleY: 0.65,
                        ease: 'Linear',
                        duration: 300,
                        onComplete: transitionstar6,
                        callbackScope: this
                    })
                }

                function transitionstar6() {
                    this.scene.scene.stop('Level8')
                    game.scene.run('endScreen');
                }
            }
        }
        mute = this.add.sprite(790, 10, 'uiButton').setFrame("button_music_on").setInteractive({
            pixelPerfect: true,
            useHandCursor: true
        })
        mute.setOrigin(1, 0)
        mute.on('pointerdown', function () {
            if (!isMuted) {
                isMuted = true
                music.pause()
                mute.setFrame("button_music_off")
            } else {
                isMuted = false
                music.resume()
                mute.setFrame("button_music_on")
            }
            var tween2 = this.tweens.add({
                targets: mute,
                scaleX: 0.9,
                scaleY: 0.9,
                ease: 'Linear',
                duration: 80,
                yoyo: true,
                repeat: 1,
            });
            if (music.isPlaying == false) {
                clicksound.stop()
            } else {
                clicksound.play()
            }
        }, this);
        if (music.isPlaying == false) {
            mute.setFrame("button_music_off")
        } else {
            mute.setFrame("button_music_on")
        }
        if (brandingMaterials.inGameLogo.display == true && brandingMaterials.inGameLogo.image != undefined) {
            inGameLogo1 = this.add.image(10, 590, 'inGameLogo1').setOrigin(0, 1).setInteractive({
                pixelPerfect: true,
                useHandCursor: true
            })
            inGameLogo1.on('pointerdown', brandingMaterials.inGameLogo.clickURL);
            inGameLogoani = this.time.addEvent({
                delay: 2000,
                callback: inGameLogoani,
                callbackScope: this
            });

            function inGameLogoani() {
                this.tweens.add({
                    targets: inGameLogo1,
                    scaleX: 1.05,
                    scaleY: 1.05,
                    ease: 'Linear',
                    duration: 100,
                    repeat: 1,
                    yoyo: true,
                    delay: 3000,
                    onComplete: inGameLogoani1,
                    callbackScope: this
                })
            }

            function inGameLogoani1() {
                this.tweens.add({
                    targets: inGameLogo1,
                    scaleX: 1.05,
                    scaleY: 1.05,
                    ease: 'Linear',
                    duration: 100,
                    repeat: 1,
                    yoyo: true,
                    delay: 3000,
                    onComplete: inGameLogoani1,
                    callbackScope: this
                })
            }
        }
        transitionbackground1 = this.add.image(400, 300, 'transitionbackground1').setOrigin(0.5, 0.5).setInteractive({
            pixelPerfect: true,
            useHandCursor: true
        })
        transitionbackground2 = this.add.image(400, 300, 'transitionbackground2').setOrigin(0.5, 0.5)
        transitionbackground3 = this.add.image(400, 300, 'transitionbackground3').setOrigin(0.5, 0.5)
        transitionpanel = this.add.image(393.5, 314, 'transitionpanel').setOrigin(0.5)
        transitiondress = this.add.image(267, 141, 'transitiondress').setOrigin(0, 0)
        transitiontitle1 = this.add.image(400, 329, 'title1');
        transitiontitle1.setOrigin(0.5);
        transitiontitle2 = this.add.image(380, 399, 'title2');
        transitiontitle2.setOrigin(0.5);
        transitiontitle3 = this.add.image(380, 459, 'title3');
        transitiontitle3.setOrigin(0.5);
        transitionstar1 = this.add.image(616.5, 168, 'transitionstar');
        transitionstar1.setOrigin(0.5);
        transitionstar2 = this.add.image(156.5, 168, 'transitionstar');
        transitionstar2.setOrigin(0.5);
        transitionstar2.setScale(0.9)
        transitionstar3 = this.add.image(596.5, 468, 'transitionstar');
        transitionstar3.setOrigin(0.5);
        transitionstar3.setScale(0.9)
        transitionstar4 = this.add.image(116.5, 468, 'transitionstar');
        transitionstar4.setOrigin(0.5);
        transitionstar4.setScale(0.8)
        transitionstar5 = this.add.image(400, 558, 'transitionstar');
        transitionstar5.setOrigin(0.5);
        transitionstar5.setScale(0.65)
        this.tweens.add({
            targets: transitionstar1,
            angle: 4,
            ease: 'Linear',
            duration: 300,
            repeat: -1,
            yoyo: true
        })
        this.tweens.add({
            targets: transitionstar2,
            angle: 4,
            ease: 'Linear',
            duration: 300,
            repeat: -1,
            yoyo: true
        })
        this.tweens.add({
            targets: transitionstar3,
            angle: 4,
            ease: 'Linear',
            duration: 300,
            repeat: -1,
            yoyo: true
        })
        this.tweens.add({
            targets: transitionstar4,
            angle: 4,
            ease: 'Linear',
            duration: 300,
            repeat: -1,
            yoyo: true
        })
        this.tweens.add({
            targets: transitionstar5,
            angle: 4,
            ease: 'Linear',
            duration: 300,
            repeat: -1,
            yoyo: true
        })
        this.time.addEvent({
            delay: 1000,
            callback: transitionstart11,
            callbackScope: this
        });

        function transitionstart11() {
            this.tweens.add({
                targets: transitionbackground1,
                alpha: 0,
                ease: 'Linear',
                duration: 700
            })
            this.tweens.add({
                targets: transitionbackground2,
                alpha: 0,
                ease: 'Linear',
                duration: 700
            })
            this.tweens.add({
                targets: transitionbackground3,
                alpha: 0,
                ease: 'Linear',
                duration: 700
            })
            this.tweens.add({
                targets: transitionpanel,
                alpha: 0,
                ease: 'Linear',
                duration: 700
            })
            this.tweens.add({
                targets: transitiondress,
                alpha: 0,
                ease: 'Linear',
                duration: 700
            })
            this.tweens.add({
                targets: transitiontitle1,
                alpha: 0,
                ease: 'Linear',
                duration: 700
            })
            this.tweens.add({
                targets: transitiontitle2,
                alpha: 0,
                ease: 'Linear',
                duration: 700
            })
            this.tweens.add({
                targets: transitiontitle3,
                alpha: 0,
                ease: 'Linear',
                duration: 700
            })
            this.tweens.add({
                targets: transitionstar1,
                alpha: 0,
                ease: 'Linear',
                duration: 700
            })
            this.tweens.add({
                targets: transitionstar2,
                alpha: 0,
                ease: 'Linear',
                duration: 700
            })
            this.tweens.add({
                targets: transitionstar3,
                alpha: 0,
                ease: 'Linear',
                duration: 700
            })
            this.tweens.add({
                targets: transitionstar4,
                alpha: 0,
                ease: 'Linear',
                duration: 700
            })
            this.tweens.add({
                targets: transitionstar5,
                alpha: 0,
                ease: 'Linear',
                duration: 700
            })
        }
        dressup4start()
        hitobject = this.add.image(400, 300, 'hitobject').setOrigin(0.5, 0.5).setInteractive({
            useHandCursor: true
        })
        hitobject.visible = false
        hitobject.on('pointerdown', hitobjectdown)

        function hitobjectdown() {}
        this.load.on('complete', function () {
            loadFinish = true
        });
        this.load.start();
    }
});

function dressup4start() {
    rapunclrarr1[1] = 1
    rapunclrarr1[2] = 1
    rapunclrarr1[3] = 1
    rapunclrarr1[4] = 1
    rapunclrarr1[5] = 1
    rapunclrarr1[6] = 1
    rapunclrarr1[7] = 1
    rapunclrarr1[8] = 1
    rapunclrarr1[9] = 1
    rapunclrarr1[10] = 1
    rapunclrarr1[11] = 1
    tailorbtn.setScale(1.05, 1.05)
    game['color' + 1].setScale(1.05, 1.05)
    tailorbtn.setFrame(1)
    handindication = true
    rapunline.visible = true
    rapunhit1.on('pointerover', rapunsegment1overstartclick)
    rapunhit1.on('pointerout', rapunsegment1outstartclick)
    rapunhit1.on('pointerdown', rapunsegment1startclick)

    function rapunsegment1overstartclick() {
        if (rapunclrarr1[1] > 0 && (colorindication || rapunclrarr1[1] != rapunclrarr2[1] || rapunsegment1dress1.frame.name != doll.frame.name + 1)) {
            rapunsegment1dress1.alpha = 0.6
            if (!colorindication) {
                rapunsegment1dress1.tint = colorcode[rapunclrarr1[1]]
            } else {
                rapunsegment1dress1.tint = colorcode[rapunclrarr2[1]]
            }
            if (colorindication) {} else {
                rapunsegment1dress1.setFrame(doll.frame.name + 1)
            }
            if (rapunovrarr[1] == 0) {
                rapunsegment1dress1.alpha = 0.6
                if (!colorindication) {
                    rapunsegment1dress1.tint = colorcode[rapunclrarr1[1]]
                } else if (rapunclrarr2[1] == 0) {
                    rapunsegment1dress1.alpha = 0
                    rapunsegment1dress1.tint = colorcode[rapunclrarr2[1]]
                }
            }
        } else if (colorindication && rapunclrarr2[1] > 0) {
            rapunsegment1dress1.alpha = 0.6
        }
    }

    function rapunsegment1outstartclick() {
        if (rapunclrarr1[1] > 0 && (colorindication || rapunclrarr1[1] != rapunclrarr2[1] || rapunsegment1dress1.frame.name == doll.frame.name + 1)) {
            if (rapunovrarr[1] == 0 && rapunclrarr2[1] == 0) {
                rapunsegment1dress1.alpha = 0
            } else {
                rapunsegment1dress1.alpha = 1
                rapunsegment1dress1.tint = colorcode[rapunclrarr2[1]]
                rapunsegment1dress1.setFrame(rapundresarr1[1])
            }
        } else if (colorindication && rapunclrarr2[1] > 0) {
            rapunsegment1dress1.alpha = 1
        } else if (colorindication) {
            rapunsegment1dress1.alpha = 0
        }
    }

    function rapunsegment1startclick() {
        if (music.isPlaying == false) {
            clicksound.stop()
        } else {
            clicksound.play()
        }
        if (colorindication) {
            rapunovrarr[1] = 0
            rapunclrarr1[1] = 0
            rapunclrarr2[1] = 0
            rapunsegment1dress1.alpha = 0
        }
        if (rapunclrarr1[1] > 0) {
            done.visible = true
            rapunsegment1dress1.alpha = 1
            rapundresarr1[1] = 1
            rapunsegment1dress1.setFrame(doll.frame.name + 1)
            rapunsegment1dress1.tint = colorcode[rapunclrarr1[1]]
            rapundresarr1[1] = rapunsegment1dress1.frame.name
            rapunovrarr[1] = 1
            rapuncolor[1] = colorcode[rapunclrarr1[1]]
            rapunclrarr2[1] = rapunclrarr1[1]
        } else {
            rapunsegment1dress1.alpha = 0
        }
    }
    rapunhit2.on('pointerover', rapunsegment2overstartclick)
    rapunhit2.on('pointerout', rapunsegment2outstartclick)
    rapunhit2.on('pointerdown', rapunsegment2startclick)

    function rapunsegment2overstartclick() {
        if (rapunclrarr1[2] > 0 && (doll.frame.name + 1 != 1 && doll.frame.name + 1 != 2 && doll.frame.name + 1 != 3) && (colorindication || rapunclrarr1[2] != rapunclrarr2[2] || rapunsegment2dress2.frame.name != doll.frame.name + 1)) {
            rapunsegment2dress2.alpha = 0.6
            if (!colorindication) {
                rapunsegment2dress2.tint = colorcode[rapunclrarr1[2]]
            } else {
                rapunsegment2dress2.tint = colorcode[rapunclrarr2[2]]
            }
            if (colorindication) {} else {
                rapunsegment2dress2.setFrame(doll.frame.name + 1)
            }
            if (rapunovrarr[2] == 0) {
                rapunsegment2dress2.alpha = 0.6
                if (!colorindication) {
                    rapunsegment2dress2.tint = colorcode[rapunclrarr1[2]]
                } else if (rapunclrarr2[2] == 0) {
                    rapunsegment2dress2.alpha = 0
                    rapunsegment2dress2.tint = colorcode[rapunclrarr2[2]]
                }
            }
        } else if (colorindication && rapunclrarr2[2] > 0) {
            rapunsegment2dress2.alpha = 0.6
        }
    }

    function rapunsegment2outstartclick() {
        if (rapunclrarr1[2] > 0 && (doll.frame.name + 1 != 1 && doll.frame.name + 1 != 2 && doll.frame.name + 1 != 3) && (colorindication || rapunclrarr1[2] != rapunclrarr2[2] || rapunsegment2dress2.frame.name == doll.frame.name + 1)) {
            if (rapunovrarr[2] == 0 && (rapunclrarr2[2] == 0)) {
                rapunsegment2dress2.alpha = 0
            } else {
                rapunsegment2dress2.alpha = 1
                rapunsegment2dress2.tint = colorcode[rapunclrarr2[2]]
                rapunsegment2dress2.setFrame(rapundresarr1[2])
            }
        } else if (colorindication && rapunclrarr2[2] > 0) {
            rapunsegment2dress2.alpha = 1
        } else if (colorindication) {
            rapunsegment2dress2.alpha = 0
        }
    }

    function rapunsegment2startclick() {
        if (music.isPlaying == false) {
            clicksound.stop()
        } else {
            clicksound.play()
        }
        if (colorindication) {
            rapunovrarr[2] = 0
            rapunclrarr1[2] = 0
            rapunclrarr2[2] = 0
            rapunsegment2dress2.alpha = 0
        }
        if (doll.frame.name + 1 != 1 && doll.frame.name + 1 != 2 && doll.frame.name + 1 != 3) {
            if (rapunclrarr1[2] > 0) {
                done.visible = true
                rapunsegment2dress2.alpha = 1
                rapunsegment2dress2.setFrame(doll.frame.name + 1)
                rapundresarr1[2] = 1
                rapunsegment2dress2.tint = colorcode[rapunclrarr1[2]]
                rapundresarr1[2] = rapunsegment2dress2.frame.name
                rapunovrarr[2] = 1
                rapuncolor[2] = colorcode[rapunclrarr1[2]]
                rapunclrarr2[2] = rapunclrarr1[2]
            } else {
                rapunsegment2dress2.alpha = 0
            }
        }
    }
    rapunhit3.on('pointerover', rapunsegment3overstartclick)
    rapunhit3.on('pointerout', rapunsegment3outstartclick)
    rapunhit3.on('pointerdown', rapunsegment3startclick)

    function rapunsegment3overstartclick() {
        if (rapunclrarr1[3] > 0 && (colorindication || rapunclrarr1[3] != rapunclrarr2[3] || (rapunsegment3dress3.frame.name != doll.frame.name + 1))) {
            rapunsegment3dress3.alpha = 0.6
            if (!colorindication) {
                rapunsegment3dress3.tint = colorcode[rapunclrarr1[3]]
            } else {
                rapunsegment3dress3.tint = colorcode[rapunclrarr2[3]]
            }
            if (colorindication) {} else {
                rapunsegment3dress3.setFrame(doll.frame.name + 1)
            }
            if (rapunovrarr[3] == 0) {
                rapunsegment3dress3.alpha = 0.6
                if (!colorindication) {
                    rapunsegment3dress3.tint = colorcode[rapunclrarr1[3]]
                } else if (rapunclrarr2[3] == 0) {
                    rapunsegment3dress3.alpha = 0
                    rapunsegment3dress3.tint = colorcode[rapunclrarr2[3]]
                }
            }
        } else if (colorindication && rapunclrarr2[3] > 0) {
            rapunsegment3dress3.alpha = 0.6
        }
    }

    function rapunsegment3outstartclick() {
        if (rapunclrarr1[3] > 0 && (colorindication || rapunclrarr1[3] != rapunclrarr2[3] || (rapunsegment3dress3.frame.name == doll.frame.name + 1))) {
            if (rapunovrarr[3] == 0 && rapunclrarr2[3] == 0) {
                rapunsegment3dress3.alpha = 0
            } else {
                rapunsegment3dress3.alpha = 1
                rapunsegment3dress3.tint = colorcode[rapunclrarr2[3]]
                rapunsegment3dress3.setFrame(rapundresarr1[3])
            }
        } else if (colorindication && rapunclrarr2[3] > 0) {
            rapunsegment3dress3.alpha = 1
        } else if (colorindication) {
            rapunsegment3dress3.alpha = 0
        }
    }

    function rapunsegment3startclick() {
        if (music.isPlaying == false) {
            clicksound.stop()
        } else {
            clicksound.play()
        }
        if (colorindication) {
            rapunovrarr[3] = 0
            rapunclrarr1[3] = 0
            rapunclrarr2[3] = 0
            rapunsegment3dress3.alpha = 0
        }
        if (rapunclrarr1[3] > 0) {
            done.visible = true
            rapunsegment3dress3.alpha = 1
            rapundresarr1[3] = 1
            rapunsegment3dress3.setFrame(doll.frame.name + 1)
            rapunsegment3dress3.tint = colorcode[rapunclrarr1[3]]
            rapundresarr1[3] = rapunsegment3dress3.frame.name
            rapunovrarr[3] = 1
            rapuncolor[3] = colorcode[rapunclrarr1[3]]
            rapunclrarr2[3] = rapunclrarr1[3]
        } else {
            rapunsegment3dress3.alpha = 0
        }
        if (doll.frame.name + 1 == 3) {
            rapunsegment41dress4.visible = true
            rapunsegment4dress4.visible = false
        } else {
            rapunsegment41dress4.visible = false
            rapunsegment4dress4.visible = true
        }
    }
    rapunhit4.on('pointerover', rapunsegment4overstartclick)
    rapunhit4.on('pointerout', rapunsegment4outstartclick)
    rapunhit4.on('pointerdown', rapunsegment4startclick)

    function rapunsegment4overstartclick() {
        if (rapunclrarr1[4] > 0 && (doll.frame.name + 1 != 1) && (colorindication || rapunclrarr1[4] != rapunclrarr2[4] || rapunsegment4dress4.frame.name != doll.frame.name + 1)) {
            rapunsegment4dress4.alpha = 0.6
            rapunsegment41dress4.alpha = 0.6
            if (!colorindication) {
                rapunsegment4dress4.tint = colorcode[rapunclrarr1[4]]
                rapunsegment41dress4.tint = colorcode[rapunclrarr1[4]]
            } else {
                rapunsegment4dress4.tint = colorcode[rapunclrarr2[4]]
                rapunsegment41dress4.tint = colorcode[rapunclrarr2[4]]
            }
            if (colorindication) {} else {
                rapunsegment4dress4.setFrame(doll.frame.name + 1)
                rapunsegment41dress4.setFrame(doll.frame.name + 1)
            }
            if (rapunovrarr[4] == 0) {
                rapunsegment4dress4.alpha = 0.6
                rapunsegment41dress4.alpha = 0.6
                if (!colorindication) {
                    rapunsegment4dress4.tint = colorcode[rapunclrarr1[4]]
                    rapunsegment41dress4.tint = colorcode[rapunclrarr1[4]]
                } else if (rapunclrarr2[4] == 0) {
                    rapunsegment4dress4.alpha = 0
                    rapunsegment41dress4.alpha = 0
                    rapunsegment4dress4.tint = colorcode[rapunclrarr2[4]]
                    rapunsegment41dress4.tint = colorcode[rapunclrarr2[4]]
                }
            }
        } else if (colorindication && rapunclrarr2[4] > 0) {
            rapunsegment4dress4.alpha = 0.6
            rapunsegment41dress4.alpha = 0.6
        }
    }

    function rapunsegment4outstartclick() {
        if (rapunclrarr1[4] > 0 && (doll.frame.name + 1 != 1) && (colorindication || rapunclrarr1[4] != rapunclrarr2[4] || rapunsegment4dress4.frame.name == doll.frame.name + 1)) {
            if (rapunovrarr[4] == 0 && rapunclrarr2[4] == 0) {
                rapunsegment4dress4.alpha = 0
                rapunsegment41dress4.alpha = 0
            } else {
                rapunsegment4dress4.alpha = 1
                rapunsegment4dress4.tint = colorcode[rapunclrarr2[4]]
                rapunsegment4dress4.setFrame(rapundresarr1[4])
                rapunsegment41dress4.alpha = 1
                rapunsegment41dress4.tint = colorcode[rapunclrarr2[4]]
                rapunsegment41dress4.setFrame(rapundresarr1[4])
            }
        } else if (colorindication && rapunclrarr2[4] > 0) {
            rapunsegment4dress4.alpha = 1
            rapunsegment41dress4.alpha = 1
        } else if (colorindication) {
            rapunsegment4dress4.alpha = 0
            rapunsegment41dress4.alpha = 0
        }
    }

    function rapunsegment4startclick() {
        if (music.isPlaying == false) {
            clicksound.stop()
        } else {
            clicksound.play()
        }
        if (colorindication) {
            rapunovrarr[4] = 0
            rapunclrarr1[4] = 0
            rapunclrarr2[4] = 0
            rapunsegment4dress4.alpha = 0
            rapunsegment41dress4.alpha = 0
        }
        if (doll.frame.name + 1 != 1) {
            if (rapunclrarr1[4] > 0) {
                done.visible = true
                rapunsegment4dress4.alpha = 1
                rapunsegment41dress4.alpha = 1
                rapundresarr1[4] = 1
                rapunsegment4dress4.setFrame(doll.frame.name + 1)
                rapunsegment41dress4.setFrame(doll.frame.name + 1)
                rapunsegment4dress4.tint = colorcode[rapunclrarr1[4]]
                rapunsegment41dress4.tint = colorcode[rapunclrarr1[4]]
                rapundresarr1[4] = rapunsegment4dress4.frame.name
                rapunovrarr[4] = 1
                rapuncolor[4] = colorcode[rapunclrarr1[4]]
                rapunclrarr2[4] = rapunclrarr1[4]
            } else {
                rapunsegment4dress4.alpha = 0
            }
        }
        if (doll.frame.name + 1 == 3) {
            rapunsegment41dress4.visible = true
            rapunsegment4dress4.visible = false
        } else {
            rapunsegment41dress4.visible = false
            rapunsegment4dress4.visible = true
        }
        if (doll.frame.name + 1 == 2) {
            rapunsegment51dress5.visible = true
            rapunsegment5dress5.visible = false
        } else {
            rapunsegment51dress5.visible = false
            rapunsegment5dress5.visible = true
        }
    }
    rapunhit5.on('pointerover', rapunsegment5overstartclick)
    rapunhit5.on('pointerout', rapunsegment5outstartclick)
    rapunhit5.on('pointerdown', rapunsegment5startclick)

    function rapunsegment5overstartclick() {
        if (rapunclrarr1[5] > 0 && (doll.frame.name + 1 != 1 && doll.frame.name + 1 != 5) && (colorindication || rapunclrarr1[5] != rapunclrarr2[5] || rapunsegment5dress5.frame.name != doll.frame.name + 1)) {
            rapunsegment5dress5.alpha = 0.6
            rapunsegment51dress5.alpha = 0.6
            if (!colorindication) {
                rapunsegment5dress5.tint = colorcode[rapunclrarr1[5]]
                rapunsegment51dress5.tint = colorcode[rapunclrarr1[5]]
            } else {
                rapunsegment5dress5.tint = colorcode[rapunclrarr2[5]]
                rapunsegment51dress5.tint = colorcode[rapunclrarr2[5]]
            }
            if (colorindication) {} else {
                rapunsegment5dress5.setFrame(doll.frame.name + 1)
                rapunsegment51dress5.setFrame(doll.frame.name + 1)
            }
            if (rapunovrarr[5] == 0) {
                rapunsegment5dress5.alpha = 0.6
                rapunsegment51dress5.alpha = 0.6
                if (!colorindication) {
                    rapunsegment5dress5.tint = colorcode[rapunclrarr1[5]]
                    rapunsegment51dress5.tint = colorcode[rapunclrarr1[5]]
                } else if (rapunclrarr2[5] == 0) {
                    rapunsegment5dress5.alpha = 0
                    rapunsegment51dress5.alpha = 0
                    rapunsegment5dress5.tint = colorcode[rapunclrarr2[5]]
                    rapunsegment51dress5.tint = colorcode[rapunclrarr2[5]]
                }
            }
        } else if (colorindication && rapunclrarr2[5] > 0) {
            rapunsegment5dress5.alpha = 0.6
            rapunsegment51dress5.alpha = 0.6
        }
    }

    function rapunsegment5outstartclick() {
        if (rapunclrarr1[5] > 0 && (doll.frame.name + 1 != 1 && doll.frame.name + 1 != 5) && (colorindication || rapunclrarr1[5] != rapunclrarr2[5] || rapunsegment5dress5.frame.name == doll.frame.name + 1)) {
            if (rapunovrarr[5] == 0 && rapunclrarr2[5] == 0) {
                rapunsegment5dress5.alpha = 0
                rapunsegment51dress5.alpha = 0
            } else {
                rapunsegment5dress5.alpha = 1
                rapunsegment5dress5.tint = colorcode[rapunclrarr2[5]]
                rapunsegment5dress5.setFrame(rapundresarr1[5])
                rapunsegment51dress5.alpha = 1
                rapunsegment51dress5.tint = colorcode[rapunclrarr2[5]]
                rapunsegment51dress5.setFrame(rapundresarr1[5])
            }
        } else if (colorindication && rapunclrarr2[5] > 0) {
            rapunsegment5dress5.alpha = 1
            rapunsegment51dress5.alpha = 1
        } else if (colorindication) {
            rapunsegment5dress5.alpha = 0
            rapunsegment51dress5.alpha = 0
        }
    }

    function rapunsegment5startclick() {
        if (music.isPlaying == false) {
            clicksound.stop()
        } else {
            clicksound.play()
        }
        if (colorindication) {
            rapunovrarr[5] = 0
            rapunclrarr1[5] = 0
            rapunclrarr2[5] = 0
            rapunsegment5dress5.alpha = 0
            rapunsegment51dress5.alpha = 0
        }
        if (doll.frame.name + 1 != 1 && doll.frame.name + 1 != 5) {
            if (rapunclrarr1[5] > 0) {
                done.visible = true
                rapunsegment5dress5.alpha = 1
                rapunsegment51dress5.alpha = 1
                rapundresarr1[5] = 1
                rapunsegment5dress5.setFrame(doll.frame.name + 1)
                rapunsegment51dress5.setFrame(doll.frame.name + 1)
                rapunsegment5dress5.tint = colorcode[rapunclrarr1[5]]
                rapunsegment51dress5.tint = colorcode[rapunclrarr1[5]]
                rapundresarr1[5] = rapunsegment5dress5.frame.name
                rapunovrarr[5] = 1
                rapuncolor[5] = colorcode[rapunclrarr1[5]]
                rapunclrarr2[5] = rapunclrarr1[5]
            } else {
                rapunsegment5dress5.alpha = 0
                rapunsegment51dress5.alpha = 0
            }
        }
        if (doll.frame.name + 1 == 2) {
            rapunsegment51dress5.visible = true
            rapunsegment5dress5.visible = false
        } else {
            rapunsegment51dress5.visible = false
            rapunsegment5dress5.visible = true
        }
    }
    rapunhit6.on('pointerover', rapunsegment6overstartclick)
    rapunhit6.on('pointerout', rapunsegment6outstartclick)
    rapunhit6.on('pointerdown', rapunsegment6startclick)

    function rapunsegment6overstartclick() {
        if (rapunclrarr1[6] > 0 && (doll.frame.name + 1 == 3) && (colorindication || rapunclrarr1[6] != rapunclrarr2[6] || 3 != doll.frame.name + 1)) {
            rapunsegment6dress6.alpha = 0.6
            if (!colorindication) {
                rapunsegment6dress6.tint = colorcode[rapunclrarr1[6]]
            } else {
                rapunsegment6dress6.tint = colorcode[rapunclrarr2[6]]
            }
            if (colorindication) {} else {
                rapunsegment6dress6.setFrame(1)
            }
            if (rapunovrarr[6] == 0) {
                rapunsegment6dress6.alpha = 0.6
                if (!colorindication) {
                    rapunsegment6dress6.tint = colorcode[rapunclrarr1[6]]
                } else if (rapunclrarr2[6] == 0) {
                    rapunsegment6dress6.alpha = 0
                    rapunsegment6dress6.tint = colorcode[rapunclrarr2[6]]
                }
                rapunsegment6dress6.setFrame(1)
            }
        } else if (colorindication && rapunclrarr2[6] > 0) {
            rapunsegment6dress6.alpha = 0.6
        }
    }

    function rapunsegment6outstartclick() {
        if (rapunclrarr1[6] > 0 && (doll.frame.name + 1 == 3) && (colorindication || rapunclrarr1[6] != rapunclrarr2[6] || 3 == doll.frame.name + 1)) {
            if (rapunovrarr[6] == 0 && (rapunclrarr2[6] == 0)) {
                rapunsegment6dress6.alpha = 0
            } else {
                rapunsegment6dress6.alpha = 1
                rapunsegment6dress6.tint = colorcode[rapunclrarr2[6]]
                rapunsegment6dress6.setFrame(1)
            }
        } else if (colorindication && rapunclrarr2[6] > 0) {
            rapunsegment6dress6.alpha = 1
        } else if (colorindication) {
            rapunsegment6dress6.alpha = 0
        }
    }

    function rapunsegment6startclick() {
        if (music.isPlaying == false) {
            clicksound.stop()
        } else {
            clicksound.play()
        }
        if (colorindication) {
            rapunovrarr[6] = 0
            rapunclrarr1[6] = 0
            rapunclrarr2[6] = 0
            rapunsegment6dress6.alpha = 0
        }
        if (doll.frame.name + 1 == 3) {
            if (rapunclrarr1[6] > 0) {
                done.visible = true
                rapunsegment6dress6.alpha = 1
                rapundresarr1[6] = 1
                rapunsegment6dress6.setFrame(1)
                rapunsegment6dress6.tint = colorcode[rapunclrarr1[6]]
                rapundresarr1[6] = rapunsegment6dress6.frame.name
                rapunovrarr[6] = 1
                rapuncolor[6] = colorcode[rapunclrarr1[6]]
                rapunclrarr2[6] = rapunclrarr1[6]
            } else {
                rapunsegment6dress6.alpha = 0
            }
        }
    }
    rapunhit7.on('pointerover', rapunsegment7overstartclick)
    rapunhit7.on('pointerout', rapunsegment7outstartclick)
    rapunhit7.on('pointerdown', rapunsegment7startclick)

    function rapunsegment7overstartclick() {
        if (rapunclrarr1[7] > 0 && (doll.frame.name + 1 == 3) && (colorindication || rapunclrarr1[7] != rapunclrarr2[7] || 3 != doll.frame.name + 1)) {
            rapunsegment7dress7.alpha = 0.6
            if (!colorindication) {
                rapunsegment7dress7.tint = colorcode[rapunclrarr1[7]]
            } else {
                rapunsegment7dress7.tint = colorcode[rapunclrarr2[7]]
            }
            if (colorindication) {} else {
                rapunsegment7dress7.setFrame(1)
            }
            if (rapunovrarr[7] == 0) {
                rapunsegment7dress7.alpha = 0.6
                if (!colorindication) {
                    rapunsegment7dress7.tint = colorcode[rapunclrarr1[7]]
                } else if (rapunclrarr2[7] == 0) {
                    rapunsegment7dress7.alpha = 0
                    rapunsegment7dress7.tint = colorcode[rapunclrarr2[7]]
                }
                rapunsegment7dress7.setFrame(1)
            }
        } else if (colorindication && rapunclrarr2[7] > 0) {
            rapunsegment7dress7.alpha = 0.6
        }
    }

    function rapunsegment7outstartclick() {
        if (rapunclrarr1[7] > 0 && (doll.frame.name + 1 == 3) && (colorindication || rapunclrarr1[7] != rapunclrarr2[7] || 3 == doll.frame.name + 1)) {
            if (rapunovrarr[7] == 0 && (rapunclrarr2[7] == 0)) {
                rapunsegment7dress7.alpha = 0
            } else {
                rapunsegment7dress7.alpha = 1
                rapunsegment7dress7.tint = colorcode[rapunclrarr2[7]]
                rapunsegment7dress7.setFrame(1)
            }
        } else if (colorindication && rapunclrarr2[7] > 0) {
            rapunsegment7dress7.alpha = 1
        } else if (colorindication) {
            rapunsegment7dress7.alpha = 0
        }
    }

    function rapunsegment7startclick() {
        if (music.isPlaying == false) {
            clicksound.stop()
        } else {
            clicksound.play()
        }
        if (colorindication) {
            rapunovrarr[7] = 0
            rapunclrarr1[7] = 0
            rapunclrarr2[7] = 0
            rapunsegment7dress7.alpha = 0
        }
        if (doll.frame.name + 1 == 3) {
            if (rapunclrarr1[7] > 0) {
                done.visible = true
                rapunsegment7dress7.alpha = 1
                rapundresarr1[7] = 1
                rapunsegment7dress7.setFrame(1)
                rapunsegment7dress7.tint = colorcode[rapunclrarr1[7]]
                rapundresarr1[7] = rapunsegment7dress7.frame.name
                rapunovrarr[7] = 1
                rapuncolor[7] = colorcode[rapunclrarr1[7]]
                rapunclrarr2[7] = rapunclrarr1[7]
            } else {
                rapunsegment7dress7.alpha = 0
            }
        }
    }
    for (i = 1; i <= 17; i++) {
        game['color' + i].on('pointerover', annacoloroverstartclick)
        game['color' + i].on('pointerout', annacoloroutstartclick)
        game['color' + i].on('pointerdown', annacolorstartclick)
    }

    function annacoloroverstartclick() {
        if (rapunclrarr1[7] != this.texture.key.substr(5)) {
            game['color' + this.texture.key.substr(5)].setScale(1.05, 1.05)
        }
    }

    function annacoloroutstartclick() {
        if (rapunclrarr1[7] != this.texture.key.substr(5)) {
            game['color' + this.texture.key.substr(5)].setScale(1, 1)
        }
    }

    function annacolorstartclick() {
        if (handindication) {
            if (music.isPlaying == false) {
                clicksound.stop()
            } else {
                clicksound.play()
            }
            sno = this.texture.key.substr(5)
            rapunclrarr1[1] = parseInt(sno)
            rapunclrarr1[2] = parseInt(sno)
            rapunclrarr1[3] = parseInt(sno)
            rapunclrarr1[4] = parseInt(sno)
            rapunclrarr1[5] = parseInt(sno)
            rapunclrarr1[6] = parseInt(sno)
            rapunclrarr1[7] = parseInt(sno)
            rapunovrarr[1] = 0
            rapunovrarr[2] = 0
            rapunovrarr[3] = 0
            rapunovrarr[4] = 0
            rapunovrarr[5] = 0
            rapunovrarr[6] = 0
            rapunovrarr[7] = 0
            rapunovrarr[7] = 0
            for (i = 1; i <= 17; i++) {
                game['color' + i].setScale(1, 1)
            }
            game['color' + rapunclrarr1[7]].setScale(1.05, 1.05)
        }
    }
    rapunsegment1dress1.on('pointerdown', rapunsegment1dress1startclick)

    function rapunsegment1dress1startclick() {
        if (colorindication) {
            if (music.isPlaying == false) {
                clicksound.stop()
            } else {
                clicksound.play()
            }
            rapunsegment1dress1.alpha = 0
            rapunovrarr[1] = 0
            rapunclrarr2[1] = 0
        }
    }
    rapunsegment2dress2.on('pointerdown', rapunsegment2dress2startclick)

    function rapunsegment2dress2startclick() {
        if (colorindication) {
            if (music.isPlaying == false) {
                clicksound.stop()
            } else {
                clicksound.play()
            }
            rapunsegment2dress2.alpha = 0
            rapunovrarr[2] = 0
            rapunclrarr2[2] = 0
        }
    }
    rapunsegment3dress3.on('pointerdown', rapunsegment3dress3startclick)

    function rapunsegment3dress3startclick() {
        if (colorindication) {
            if (music.isPlaying == false) {
                clicksound.stop()
            } else {
                clicksound.play()
            }
            rapunsegment3dress3.alpha = 0
            rapunovrarr[3] = 0
            rapunclrarr2[3] = 0
        }
    }
    rapunsegment4dress4.on('pointerdown', rapunsegment4dress4startclick)
    rapunsegment41dress4.on('pointerdown', rapunsegment4dress4startclick)

    function rapunsegment4dress4startclick() {
        if (colorindication) {
            if (music.isPlaying == false) {
                clicksound.stop()
            } else {
                clicksound.play()
            }
            rapunsegment4dress4.alpha = 0
            rapunsegment41dress4.alpha = 0
            rapunovrarr[4] = 0
            rapunclrarr2[4] = 0
        }
    }
    rapunsegment5dress5.on('pointerdown', rapunsegment5dress5startclick)
    rapunsegment51dress5.on('pointerdown', rapunsegment5dress5startclick)

    function rapunsegment5dress5startclick() {
        if (colorindication) {
            if (music.isPlaying == false) {
                clicksound.stop()
            } else {
                clicksound.play()
            }
            rapunsegment5dress5.alpha = 0
            rapunsegment51dress5.alpha = 0
            rapunovrarr[5] = 0
            rapunclrarr2[5] = 0
        }
    }
    rapunsegment6dress6.on('pointerdown', rapunsegment6dress6startclick)

    function rapunsegment6dress6startclick() {
        if (colorindication) {
            if (music.isPlaying == false) {
                clicksound.stop()
            } else {
                clicksound.play()
            }
            rapunsegment6dress6.alpha = 0
            rapunovrarr[6] = 0
            rapunclrarr2[6] = 0
        }
    }
    rapunsegment7dress7.on('pointerdown', rapunsegment7dress7startclick)

    function rapunsegment7dress7startclick() {
        if (colorindication) {
            if (music.isPlaying == false) {
                clicksound.stop()
            } else {
                clicksound.play()
            }
            rapunsegment7dress7.alpha = 0
            rapunovrarr[7] = 0
            rapunclrarr2[7] = 0
        }
    }
    tailorbtn.on('pointerdown', tailorbtnstartclick)

    function tailorbtnstartclick() {
        if (music.isPlaying == false) {
            clicksound.stop()
        } else {
            clicksound.play()
        }
        if (!handindication) {
            colorindication = false
            rapunline.visible = true
            handindication = true
            tailorbtn.setScale(1.05, 1.05)
            cutbtn.setScale(1, 1)
            cutbtn.setFrame(0)
            tailorbtn.setFrame(1)
            rapunhit1.setInteractive({
                pixelPerfect: true,
                useHandCursor: true
            })
            rapunhit2.setInteractive({
                pixelPerfect: true,
                useHandCursor: true
            })
            rapunhit3.setInteractive({
                pixelPerfect: true,
                useHandCursor: true
            })
            rapunhit4.setInteractive({
                pixelPerfect: true,
                useHandCursor: true
            })
            rapunhit5.setInteractive({
                pixelPerfect: true,
                useHandCursor: true
            })
            rapunhit6.setInteractive({
                pixelPerfect: true,
                useHandCursor: true
            })
            rapunhit7.setInteractive({
                pixelPerfect: true,
                useHandCursor: true
            })
            rapunsegment1dress1.disableInteractive();
            rapunsegment2dress2.disableInteractive();
            rapunsegment3dress3.disableInteractive();
            rapunsegment4dress4.disableInteractive();
            rapunsegment41dress4.disableInteractive();
            rapunsegment5dress5.disableInteractive();
            rapunsegment51dress5.disableInteractive();
            rapunsegment6dress6.disableInteractive();
            rapunsegment7dress7.disableInteractive();
        }
    }
    cutbtn.on('pointerdown', cutbtnstartclick)

    function cutbtnstartclick() {
        if (music.isPlaying == false) {
            clicksound.stop()
        } else {
            clicksound.play()
        }
        rapunline.visible = false
        handindication = false
        colorindication = true
        cutbtn.setScale(1.05, 1.05)
        tailorbtn.setScale(1, 1)
        cutbtn.setFrame(1)
        tailorbtn.setFrame(0)
        rapunhit1.disableInteractive();
        rapunhit2.disableInteractive();
        rapunhit3.disableInteractive();
        rapunhit4.disableInteractive();
        rapunhit5.disableInteractive();
        rapunhit6.disableInteractive();
        rapunhit7.disableInteractive();
        rapunsegment1dress1.setInteractive({
            pixelPerfect: true,
            useHandCursor: true
        })
        rapunsegment2dress2.setInteractive({
            pixelPerfect: true,
            useHandCursor: true
        })
        rapunsegment3dress3.setInteractive({
            pixelPerfect: true,
            useHandCursor: true
        })
        rapunsegment4dress4.setInteractive({
            pixelPerfect: true,
            useHandCursor: true
        })
        rapunsegment5dress5.setInteractive({
            pixelPerfect: true,
            useHandCursor: true
        })
        rapunsegment41dress4.setInteractive({
            pixelPerfect: true,
            useHandCursor: true
        })
        rapunsegment51dress5.setInteractive({
            pixelPerfect: true,
            useHandCursor: true
        })
        rapunsegment6dress6.setInteractive({
            pixelPerfect: true,
            useHandCursor: true
        })
        rapunsegment7dress7.setInteractive({
            pixelPerfect: true,
            useHandCursor: true
        })
    }
    rarrow.on('pointerdown', rarrowstartclick)

    function rarrowstartclick() {
        if (music.isPlaying == false) {
            clicksound.stop()
        } else {
            clicksound.play()
        }
        if (doll.frame.name == 0) {
            doll.setFrame(1)
        } else if (doll.frame.name == 1) {
            doll.setFrame(2)
        } else if (doll.frame.name == 2) {
            doll.setFrame(3)
        } else if (doll.frame.name == 3) {
            doll.setFrame(4)
        } else if (doll.frame.name == 4) {
            doll.setFrame(5)
        } else if (doll.frame.name == 5) {
            doll.setFrame(0)
        }
    }
    larrow.on('pointerdown', larrowstartclick)

    function larrowstartclick() {
        if (music.isPlaying == false) {
            clicksound.stop()
        } else {
            clicksound.play()
        }
        if (doll.frame.name == 0) {
            doll.setFrame(5)
        } else if (doll.frame.name == 5) {
            doll.setFrame(4)
        } else if (doll.frame.name == 4) {
            doll.setFrame(3)
        } else if (doll.frame.name == 3) {
            doll.setFrame(2)
        } else if (doll.frame.name == 2) {
            doll.setFrame(1)
        } else if (doll.frame.name == 1) {
            doll.setFrame(0)
        }
    }
}
var stargame13 = false
var endScreen = new Phaser.Class({
    Extends: Phaser.Scene,
    initialize: function endScreen() {
        Phaser.Scene.call(this, {
            key: 'endScreen'
        });
    },
    preload: function () {
        stargame13 = false
        loadFinish = false
    },
    create: function () {
        WitchSDK.recordEvent(WitchSDK.Events.GAME_OVER);
        endbackground = this.add.image(400, 300, 'endbackground').setOrigin(0.5, 0.5)
        bar = this.add.image(0, 0, 'bar').setOrigin(0, 0)
        stand1 = this.add.image(30, 477, 'stand1').setOrigin(0, 0)
        light1 = this.add.image(78, 492, 'light').setOrigin(0.1, 0.95)
        stand2 = this.add.image(38, 493, 'stand2').setOrigin(0, 0)
        var light1container = this.add.container(0, 0)
        light1container.add(stand1)
        light1container.add(light1)
        light1container.add(stand2)
        this.tweens.add({
            targets: light1,
            angle: 14,
            ease: 'Linear',
            duration: 1000,
            repeat: -1,
            yoyo: true,
        })
        stand11 = this.add.image(30, 477, 'stand1').setOrigin(0, 0)
        light2 = this.add.image(78, 492, 'light').setOrigin(0.1, 0.95)
        stand22 = this.add.image(38, 493, 'stand2').setOrigin(0, 0)
        var light2container = this.add.container(0, 0)
        light2container.add(stand11)
        light2container.add(light2)
        light2container.add(stand22)
        light2container.x = -40
        light2container.y = 80
        this.tweens.add({
            targets: light2,
            angle: 28,
            ease: 'Linear',
            duration: 1200,
            delay: 1000,
            repeat: -1,
            yoyo: true,
        })
        stand12 = this.add.image(30, 477, 'stand1').setOrigin(0, 0)
        light3 = this.add.image(78, 492, 'light').setOrigin(0.1, 0.95)
        stand23 = this.add.image(38, 493, 'stand2').setOrigin(0, 0)
        var light3container = this.add.container(0, 0)
        light3container.add(stand12)
        light3container.add(light3)
        light3container.add(stand23)
        light3container.x = 800
        light3container.setScale(-1, 1)
        this.tweens.add({
            targets: light3,
            angle: 14,
            ease: 'Linear',
            duration: 1000,
            repeat: -1,
            yoyo: true,
        })
        stand13 = this.add.image(30, 477, 'stand1').setOrigin(0, 0)
        light4 = this.add.image(78, 492, 'light').setOrigin(0.1, 0.95)
        stand24 = this.add.image(38, 493, 'stand2').setOrigin(0, 0)
        var light4container = this.add.container(0, 0)
        light4container.add(stand13)
        light4container.add(light4)
        light4container.add(stand24)
        light4container.x = 840
        light4container.y = 80
        light4container.setScale(-1, 1)
        this.tweens.add({
            targets: light4,
            angle: 28,
            ease: 'Linear',
            duration: 1200,
            delay: 1000,
            repeat: -1,
            yoyo: true,
        })
        stand14 = this.add.image(30, 477, 'stand1').setOrigin(0, 0)
        light5 = this.add.image(78, 492, 'light').setOrigin(0.1, 0.95)
        stand25 = this.add.image(38, 493, 'stand2').setOrigin(0, 0)
        var light5container = this.add.container(0, 0)
        light5container.add(stand14)
        light5container.add(light5)
        light5container.add(stand25)
        light5container.x = 190
        light5container.y = 460
        light5container.angle = -14
        light5container.setScale(0.8, -0.8)
        this.tweens.add({
            targets: light5,
            angle: 7,
            ease: 'Linear',
            duration: 1200,
            delay: 1000,
            repeat: -1,
            yoyo: true,
        })
        stand15 = this.add.image(30, 477, 'stand1').setOrigin(0, 0)
        light6 = this.add.image(78, 492, 'light').setOrigin(0.1, 0.95)
        stand26 = this.add.image(38, 493, 'stand2').setOrigin(0, 0)
        var light6container = this.add.container(0, 0)
        light6container.add(stand15)
        light6container.add(light6)
        light6container.add(stand26)
        light6container.x = 90
        light6container.y = 505
        light6container.angle = -14
        light6container.setScale(0.8, -0.8)
        this.tweens.add({
            targets: light6,
            angle: 7,
            ease: 'Linear',
            duration: 1200,
            repeat: -1,
            yoyo: true,
        })
        stand16 = this.add.image(30, 477, 'stand1').setOrigin(0, 0)
        light7 = this.add.image(78, 492, 'light').setOrigin(0.1, 0.95)
        stand27 = this.add.image(38, 493, 'stand2').setOrigin(0, 0)
        var light7container = this.add.container(0, 0)
        light7container.add(stand16)
        light7container.add(light7)
        light7container.add(stand27)
        light7container.x = 600
        light7container.y = 460
        light7container.angle = 18
        light7container.setScale(-0.8, -0.8)
        this.tweens.add({
            targets: light7,
            angle: 7,
            ease: 'Linear',
            duration: 1200,
            delay: 1000,
            repeat: -1,
            yoyo: true,
        })
        stand17 = this.add.image(30, 477, 'stand1').setOrigin(0, 0)
        light8 = this.add.image(78, 492, 'light').setOrigin(0.1, 0.95)
        stand28 = this.add.image(38, 493, 'stand2').setOrigin(0, 0)
        var light8container = this.add.container(0, 0)
        light8container.add(stand17)
        light8container.add(light8)
        light8container.add(stand28)
        light8container.x = 680
        light8container.y = 500
        light8container.angle = 18
        light8container.setScale(-0.8, -0.8)
        this.tweens.add({
            targets: light8,
            angle: 7,
            ease: 'Linear',
            duration: 1200,
            repeat: -1,
            yoyo: true,
        })
        elsabody = this.add.image(101, 37, 'elsabody').setOrigin(0, 0)
        elsalips = this.add.image(193, 113, 'elsalips').setOrigin(0, 0)
        elsaeye1 = this.add.image(192, 90, 'elsaeye1').setOrigin(0, 0)
        elsaeye2 = this.add.image(192, 90, 'elsaeye2').setOrigin(0, 0)
        elsatoplayer = this.add.image(185, 85, 'elsatoplayer').setOrigin(0, 0)
        elsaeshade = this.add.image(184, 87, 'elsaeshade').setOrigin(0, 0)
        elsaelash = this.add.image(185, 86, 'elsaelash').setOrigin(0, 0)
        elsablush = this.add.image(188, 101, 'elsablush').setOrigin(0, 0)
        elsaebrow = this.add.image(185, 76, 'elsaebrow').setOrigin(0, 0)
        elsasegment1dress1 = this.add.image(181, 147, 'elsasegment1dress1').setOrigin(0, 0)
        elsasegment2dress2 = this.add.image(188, 215, 'elsasegment2dress2').setOrigin(0, 0)
        elsasegment3dress3 = this.add.image(143, 233, 'elsasegment3dress3').setOrigin(0, 0)
        elsasegment4dress4 = this.add.image(144, 352, 'elsasegment4dress4').setOrigin(0, 0)
        elsasegment5dress5 = this.add.image(131, 443, 'elsasegment5dress5').setOrigin(0, 0)
        elsasegment6dress6 = this.add.image(121, 264, 'elsasegment6dress6').setOrigin(0, 0)
        elsasegment7dress7 = this.add.image(95, 341, 'elsasegment7dress7').setOrigin(0, 0)
        elsasegment8dress8 = this.add.image(74, 427, 'elsasegment8dress8').setOrigin(0, 0)
        elsasegment9dress9 = this.add.image(245, 269, 'elsasegment9dress9').setOrigin(0, 0)
        elsasegment10dress10 = this.add.image(254, 343, 'elsasegment10dress10').setOrigin(0, 0)
        elsasegment11dress11 = this.add.image(272, 431, 'elsasegment11dress11').setOrigin(0, 0)
        elsahair = this.add.image(139, 12, 'elsahair').setOrigin(0, 0)
        elsaelash.setFrame(marr1[0])
        elsablush.setFrame(marr1[1])
        elsalips.setFrame(marr1[2])
        elsaeye1.setFrame(marr1[3])
        elsaeye2.setFrame(marr1[3])
        elsaeshade.setFrame(marr1[4])
        elsaebrow.setFrame(marr1[5])
        elsahair.setFrame(marr1[6])
        var elsadollgroup = this.add.container(0, 0);
        elsadollgroup.add(elsasegment11dress11)
        elsadollgroup.add(elsasegment10dress10)
        elsadollgroup.add(elsasegment9dress9)
        elsadollgroup.add(elsasegment8dress8)
        elsadollgroup.add(elsasegment7dress7)
        elsadollgroup.add(elsasegment6dress6)
        elsadollgroup.add(elsabody)
        elsadollgroup.add(elsalips)
        elsadollgroup.add(elsaeye1)
        elsadollgroup.add(elsaeye2)
        elsadollgroup.add(elsatoplayer)
        elsadollgroup.add(elsaeshade)
        elsadollgroup.add(elsaelash)
        elsadollgroup.add(elsablush)
        elsadollgroup.add(elsaebrow)
        elsadollgroup.add(elsasegment2dress2)
        elsadollgroup.add(elsasegment1dress1)
        elsadollgroup.add(elsasegment5dress5)
        elsadollgroup.add(elsasegment4dress4)
        elsadollgroup.add(elsasegment3dress3)
        elsadollgroup.add(elsahair)
        elsadollgroup.x = 160
        elsadollgroup.y = 20
        var tween1 = this.tweens.add({
            targets: elsaeye1,
            x: elsaeye1.x + 2,
            ease: 'Linear',
            duration: 300,
            delay: 2400,
            callbackScope: this,
        });
        var tween2 = this.tweens.add({
            targets: elsaeye2,
            x: elsaeye2.x + 2,
            ease: 'Linear',
            duration: 300,
            delay: 2400,
            onComplete: elsaeyestartani1,
            callbackScope: this
        });

        function elsaeyestartani1() {
            this.tweens.add({
                targets: elsaeye1,
                x: elsaeye1.x - 2,
                ease: 'Linear',
                duration: 300,
                delay: 3000,
                callbackScope: this
            });
            this.tweens.add({
                targets: elsaeye2,
                x: elsaeye2.x - 2,
                ease: 'Linear',
                duration: 300,
                delay: 3000,
                onComplete: elsaeyestartani2,
                callbackScope: this,
            })
        }

        function elsaeyestartani2() {
            this.tweens.add({
                targets: elsaeye1,
                x: elsaeye1.x + 2,
                ease: 'Linear',
                duration: 300,
                delay: 2400,
                callbackScope: this
            });
            this.tweens.add({
                targets: elsaeye2,
                x: elsaeye2.x + 2,
                ease: 'Linear',
                duration: 300,
                delay: 2400,
                onComplete: elsaeyestartani1,
                callbackScope: this,
            })
        }
        elsaeshad = this.time.addEvent({
            delay: 1000,
            callback: elsaeblinkstart1,
            callbackScope: this
        });

        function elsaeblinkstart1() {
            elsaeshade.setFrame(marr1[4] + 1)
            elsaelash.visible = false
            elsaeshad = this.time.addEvent({
                delay: 200,
                callback: elsaeblinkstart2,
                callbackScope: this
            });
        }

        function elsaeblinkstart2() {
            elsaeshade.setFrame(marr1[4])
            elsaelash.visible = true
            elsaeshad = this.time.addEvent({
                delay: 4000,
                callback: elsaeblinkstart1,
                callbackScope: this
            });
        }
        elsalip = this.time.addEvent({
            delay: 1000,
            callback: elsalipsstart1,
            callbackScope: this
        });

        function elsalipsstart1() {
            elsalips.setFrame(marr1[2] + 1)
            elsalip = this.time.addEvent({
                delay: 100,
                callback: elsalipsstart2,
                callbackScope: this
            });
        }

        function elsalipsstart2() {
            elsalips.setFrame(marr1[2] + 2)
            elsalip = this.time.addEvent({
                delay: 2500,
                callback: elsalipsstart3,
                callbackScope: this
            });
        }

        function elsalipsstart3() {
            elsalips.setFrame(marr1[2] + 1)
            elsalip = this.time.addEvent({
                delay: 100,
                callback: elsalipsstart4,
                callbackScope: this
            });
        }

        function elsalipsstart4() {
            elsalips.setFrame(marr1[2])
            elsalip = this.time.addEvent({
                delay: 3000,
                callback: elsalipsstart1,
                callbackScope: this
            });
        }
        arielbhair = this.add.image(140, 112, 'arielbhair').setOrigin(0, 0)
        arielbody = this.add.image(73, 43, 'arielbody').setOrigin(0, 0)
        ariellips = this.add.image(161, 105, 'ariellips').setOrigin(0, 0)
        arieleye1 = this.add.image(161, 83, 'arieleye1').setOrigin(0, 0)
        arieleye2 = this.add.image(161, 83, 'arieleye2').setOrigin(0, 0)
        arielblush = this.add.image(156, 96, 'arielblush').setOrigin(0, 0)
        arieltoplayer = this.add.image(158, 79, 'arieltoplayer').setOrigin(0, 0)
        arieleshade = this.add.image(154, 80, 'arieleshade').setOrigin(0, 0)
        arielelash = this.add.image(153, 80, 'arielelash').setOrigin(0, 0)
        arielebrow = this.add.image(157, 72, 'arielebrow').setOrigin(0, 0)
        arielhair = this.add.image(124, 28, 'arielhair').setOrigin(0, 0)
        arielsegment1dress1 = this.add.image(130, 145, 'arielsegment1dress1').setOrigin(0, 0)
        arielsegment2dress2 = this.add.image(165, 198, 'arielsegment2dress2').setOrigin(0, 0)
        arielsegment3dress3 = this.add.image(121, 224, 'arielsegment3dress3').setOrigin(0, 0)
        arielsegment4dress4 = this.add.image(96, 333, 'arielsegment4dress4').setOrigin(0, 0)
        arielsegment41dress4 = this.add.image(96, 333, 'arielsegment4dress4').setOrigin(0, 0)
        arielsegment5dress5 = this.add.image(99, 424, 'arielsegment5dress5').setOrigin(0, 0)
        arielsegment6dress6 = this.add.image(79, 257, 'arielsegment6dress6').setOrigin(0, 0)
        arielsegment7dress7 = this.add.image(78, 330, 'arielsegment7dress7').setOrigin(0, 0)
        arielsegment8dress8 = this.add.image(61, 429, 'arielsegment8dress8').setOrigin(0, 0)
        arielsegment9dress9 = this.add.image(211, 254, 'arielsegment9dress9').setOrigin(0, 0)
        arielsegment10dress10 = this.add.image(224, 325, 'arielsegment10dress10').setOrigin(0, 0)
        arielsegment11dress11 = this.add.image(247, 414, 'arielsegment11dress11').setOrigin(0, 0)
        arielelash.setFrame(marr2[0])
        arielblush.setFrame(marr2[1])
        ariellips.setFrame(marr2[2])
        arieleye1.setFrame(marr2[3])
        arieleye2.setFrame(marr2[3])
        arieleshade.setFrame(marr2[4])
        arielebrow.setFrame(marr2[5])
        arielhair.setFrame(marr2[6])
        if (marr2[6] > 0) {
            arielbhair.setFrame(1)
        } else {
            arielbhair.setFrame(0)
        }
        var arieldollgroup = this.add.container(0, 0);
        arieldollgroup.add(arielsegment11dress11)
        arieldollgroup.add(arielsegment10dress10)
        arieldollgroup.add(arielsegment9dress9)
        arieldollgroup.add(arielsegment8dress8)
        arieldollgroup.add(arielsegment7dress7)
        arieldollgroup.add(arielsegment6dress6)
        arieldollgroup.add(arielbhair)
        arieldollgroup.add(arielbody)
        arieldollgroup.add(ariellips)
        arieldollgroup.add(arieleye1)
        arieldollgroup.add(arieleye2)
        arieldollgroup.add(arieltoplayer)
        arieldollgroup.add(arieleshade)
        arieldollgroup.add(arielelash)
        arieldollgroup.add(arielblush)
        arieldollgroup.add(arielebrow)
        arieldollgroup.add(arielsegment1dress1)
        arieldollgroup.add(arielsegment2dress2)
        arieldollgroup.add(arielsegment5dress5)
        arieldollgroup.add(arielsegment41dress4)
        arieldollgroup.add(arielsegment3dress3)
        arieldollgroup.add(arielsegment4dress4)
        arieldollgroup.add(arielhair)
        arieldollgroup.x = 170
        arieldollgroup.y = 20
        var tween1 = this.tweens.add({
            targets: arieleye1,
            x: arieleye1.x + 2,
            ease: 'Linear',
            duration: 300,
            delay: 2400,
            callbackScope: this,
        });
        var tween2 = this.tweens.add({
            targets: arieleye2,
            x: arieleye2.x + 2,
            ease: 'Linear',
            duration: 300,
            delay: 2400,
            onComplete: arieleyestartani1,
            callbackScope: this
        });

        function arieleyestartani1() {
            this.tweens.add({
                targets: arieleye1,
                x: arieleye1.x - 2,
                ease: 'Linear',
                duration: 300,
                delay: 3000,
                callbackScope: this
            });
            this.tweens.add({
                targets: arieleye2,
                x: arieleye2.x - 2,
                ease: 'Linear',
                duration: 300,
                delay: 3000,
                onComplete: arieleyestartani2,
                callbackScope: this,
            })
        }

        function arieleyestartani2() {
            this.tweens.add({
                targets: arieleye1,
                x: arieleye1.x + 2,
                ease: 'Linear',
                duration: 300,
                delay: 2400,
                callbackScope: this
            });
            this.tweens.add({
                targets: arieleye2,
                x: arieleye2.x + 2,
                ease: 'Linear',
                duration: 300,
                delay: 2400,
                onComplete: arieleyestartani1,
                callbackScope: this,
            })
        }
        arieleshad = this.time.addEvent({
            delay: 1000,
            callback: arieleblinkstart1,
            callbackScope: this
        });

        function arieleblinkstart1() {
            arieleshade.setFrame(marr2[4] + 1)
            arielelash.visible = false
            arieleshad = this.time.addEvent({
                delay: 200,
                callback: arieleblinkstart2,
                callbackScope: this
            });
        }

        function arieleblinkstart2() {
            arieleshade.setFrame(marr2[4])
            arielelash.visible = true
            arieleshad = this.time.addEvent({
                delay: 4000,
                callback: arieleblinkstart1,
                callbackScope: this
            });
        }
        ariellip = this.time.addEvent({
            delay: 1000,
            callback: ariellipsstart1,
            callbackScope: this
        });

        function ariellipsstart1() {
            ariellips.setFrame(marr2[2] + 1)
            ariellip = this.time.addEvent({
                delay: 100,
                callback: ariellipsstart2,
                callbackScope: this
            });
        }

        function ariellipsstart2() {
            ariellips.setFrame(marr2[2] + 2)
            ariellip = this.time.addEvent({
                delay: 2500,
                callback: ariellipsstart3,
                callbackScope: this
            });
        }

        function ariellipsstart3() {
            ariellips.setFrame(marr2[2] + 1)
            ariellip = this.time.addEvent({
                delay: 100,
                callback: ariellipsstart4,
                callbackScope: this
            });
        }

        function ariellipsstart4() {
            ariellips.setFrame(marr2[2])
            ariellip = this.time.addEvent({
                delay: 3000,
                callback: ariellipsstart1,
                callbackScope: this
            });
        }
        annabody = this.add.image(54, 42, 'annabody').setOrigin(0, 0)
        annalips = this.add.image(154, 102, 'annalips').setOrigin(0, 0)
        annaeye1 = this.add.image(154, 79, 'annaeye1').setOrigin(0, 0)
        annaeye2 = this.add.image(154, 79, 'annaeye2').setOrigin(0, 0)
        annablush = this.add.image(145, 88, 'annablush').setOrigin(0, 0)
        annatoplayer = this.add.image(150, 74, 'annatoplayer').setOrigin(0, 0)
        annaeshade = this.add.image(147, 75, 'annaeshade').setOrigin(0, 0)
        annaelash = this.add.image(147, 74, 'annaelash').setOrigin(0, 0)
        annaebrow = this.add.image(150, 69, 'annaebrow').setOrigin(0, 0)
        annahair = this.add.image(117, 20, 'annahair').setOrigin(0, 0)
        annahand = this.add.image(221, 146, 'annahand').setOrigin(0, 0)
        annahand1 = this.add.image(223, 146, 'annahand1').setOrigin(0, 0)
        annasegment1dress1 = this.add.image(111, 125, 'annasegment1dress1').setOrigin(0, 0)
        annasegment2dress2 = this.add.image(153, 207, 'annasegment2dress2').setOrigin(0, 0)
        annasegment3dress3 = this.add.image(100, 226, 'annasegment3dress3').setOrigin(0, 0)
        annasegment4dress4 = this.add.image(97, 346, 'annasegment4dress4').setOrigin(0, 0)
        annasegment41dress4 = this.add.image(97, 346, 'annasegment4dress4').setOrigin(0, 0)
        annasegment5dress5 = this.add.image(98, 399, 'annasegment5dress5').setOrigin(0, 0)
        annasegment6dress6 = this.add.image(71, 351, 'annasegment6dress6').setOrigin(0, 0)
        annasegment7dress7 = this.add.image(41, 414, 'annasegment7dress7').setOrigin(0, 0)
        annasegment8dress8 = this.add.image(213, 335, 'annasegment8dress8').setOrigin(0, 0)
        annasegment9dress9 = this.add.image(224, 419, 'annasegment9dress9').setOrigin(0, 0)
        annaelash.setFrame(marr3[0])
        annablush.setFrame(marr3[1])
        annalips.setFrame(marr3[2])
        annaeye1.setFrame(marr3[3])
        annaeye2.setFrame(marr3[3])
        annaeshade.setFrame(marr3[4])
        annaebrow.setFrame(marr3[5])
        annahair.setFrame(marr3[6])
        var tween1 = this.tweens.add({
            targets: annaeye1,
            x: annaeye1.x + 2,
            ease: 'Linear',
            duration: 300,
            delay: 2400,
            callbackScope: this,
        });
        var tween2 = this.tweens.add({
            targets: annaeye2,
            x: annaeye2.x + 2,
            ease: 'Linear',
            duration: 300,
            delay: 2400,
            onComplete: annaeyestartani1,
            callbackScope: this
        });

        function annaeyestartani1() {
            this.tweens.add({
                targets: annaeye1,
                x: annaeye1.x - 2,
                ease: 'Linear',
                duration: 300,
                delay: 3000,
                callbackScope: this
            });
            this.tweens.add({
                targets: annaeye2,
                x: annaeye2.x - 2,
                ease: 'Linear',
                duration: 300,
                delay: 3000,
                onComplete: annaeyestartani2,
                callbackScope: this,
            })
        }

        function annaeyestartani2() {
            this.tweens.add({
                targets: annaeye1,
                x: annaeye1.x + 2,
                ease: 'Linear',
                duration: 300,
                delay: 2400,
                callbackScope: this
            });
            this.tweens.add({
                targets: annaeye2,
                x: annaeye2.x + 2,
                ease: 'Linear',
                duration: 300,
                delay: 2400,
                onComplete: annaeyestartani1,
                callbackScope: this,
            })
        }
        annaeshad = this.time.addEvent({
            delay: 1000,
            callback: annaeblinkstart1,
            callbackScope: this
        });

        function annaeblinkstart1() {
            annaeshade.setFrame(marr3[4] + 1)
            annaelash.visible = false
            annaeshad = this.time.addEvent({
                delay: 200,
                callback: annaeblinkstart2,
                callbackScope: this
            });
        }

        function annaeblinkstart2() {
            annaeshade.setFrame(marr3[4])
            annaelash.visible = true
            annaeshad = this.time.addEvent({
                delay: 4000,
                callback: annaeblinkstart1,
                callbackScope: this
            });
        }
        annalip = this.time.addEvent({
            delay: 1000,
            callback: annalipsstart1,
            callbackScope: this
        });

        function annalipsstart1() {
            annalips.setFrame(marr3[2] + 1)
            annalip = this.time.addEvent({
                delay: 100,
                callback: annalipsstart2,
                callbackScope: this
            });
        }

        function annalipsstart2() {
            annalips.setFrame(marr3[2] + 2)
            annalip = this.time.addEvent({
                delay: 2500,
                callback: annalipsstart3,
                callbackScope: this
            });
        }

        function annalipsstart3() {
            annalips.setFrame(marr3[2] + 1)
            annalip = this.time.addEvent({
                delay: 100,
                callback: annalipsstart4,
                callbackScope: this
            });
        }

        function annalipsstart4() {
            annalips.setFrame(marr3[2])
            annalip = this.time.addEvent({
                delay: 3000,
                callback: annalipsstart1,
                callbackScope: this
            });
        }
        var annadollgroup = this.add.container(0, 0);
        annadollgroup.add(annasegment9dress9)
        annadollgroup.add(annasegment8dress8)
        annadollgroup.add(annasegment7dress7)
        annadollgroup.add(annasegment6dress6)
        annadollgroup.add(annabody)
        annadollgroup.add(annalips)
        annadollgroup.add(annaeye1)
        annadollgroup.add(annaeye2)
        annadollgroup.add(annatoplayer)
        annadollgroup.add(annaeshade)
        annadollgroup.add(annaelash)
        annadollgroup.add(annablush)
        annadollgroup.add(annaebrow)
        annadollgroup.add(annasegment1dress1)
        annadollgroup.add(annasegment5dress5)
        annadollgroup.add(annasegment2dress2)
        annadollgroup.add(annasegment4dress4)
        annadollgroup.add(annasegment3dress3)
        annadollgroup.add(annasegment41dress4)
        annadollgroup.add(annahand)
        annadollgroup.add(annahair)
        annadollgroup.add(annahand1)
        annadollgroup.x = 170
        annadollgroup.y = 20
        rapunbhair = this.add.sprite(151, 98, 'rapunbhair').setOrigin(0, 0)
        rapunbody = this.add.sprite(99, 46, 'rapunbody').setOrigin(0, 0)
        rapunlips = this.add.sprite(198, 115, 'rapunlips').setOrigin(0, 0)
        rapuneye1 = this.add.sprite(200, 98, 'rapuneye1').setOrigin(0, 0)
        rapuneye2 = this.add.sprite(200, 98, 'rapuneye2').setOrigin(0, 0)
        rapunblush = this.add.sprite(196, 111, 'rapunblush').setOrigin(0, 0)
        rapuntoplayer = this.add.sprite(191, 86, 'rapuntoplayer').setOrigin(0, 0)
        rapuneshade = this.add.sprite(191, 91, 'rapuneshade').setOrigin(0, 0)
        rapunelash = this.add.sprite(193, 94, 'rapunelash').setOrigin(0, 0)
        rapunebrow = this.add.sprite(190, 83, 'rapunebrow').setOrigin(0, 0)
        rapunhair = this.add.sprite(146, 34, 'rapunhair').setOrigin(0, 0)
        rapunsegment1dress1 = this.add.sprite(164, 154, 'rapunsegment1dress1').setOrigin(0, 0)
        rapunsegment2dress2 = this.add.sprite(188, 227, 'rapunsegment2dress2').setOrigin(0, 0)
        rapunsegment3dress3 = this.add.sprite(139, 247, 'rapunsegment3dress3').setOrigin(0, 0)
        rapunsegment4dress4 = this.add.sprite(132, 349, 'rapunsegment4dress4').setOrigin(0, 0)
        rapunsegment41dress4 = this.add.sprite(132, 349, 'rapunsegment4dress4').setOrigin(0, 0)
        rapunsegment5dress5 = this.add.sprite(107, 429, 'rapunsegment5dress5').setOrigin(0, 0)
        rapunsegment51dress5 = this.add.sprite(107, 429, 'rapunsegment5dress5').setOrigin(0, 0)
        rapunsegment6dress6 = this.add.sprite(90, 329, 'rapunsegment6dress6').setOrigin(0, 0)
        rapunsegment7dress7 = this.add.sprite(256, 326, 'rapunsegment7dress7').setOrigin(0, 0)
        rapunelash.setFrame(marr4[0])
        rapunblush.setFrame(marr4[1])
        rapunlips.setFrame(marr4[2])
        rapuneye1.setFrame(marr4[3])
        rapuneye2.setFrame(marr4[3])
        rapuneshade.setFrame(marr4[4])
        rapunebrow.setFrame(marr4[5])
        rapunhair.setFrame(marr4[6])
        rapunbhair.setFrame(marr4[6])
        var rapundollgroup = this.add.container(0, 0);
        rapundollgroup.add(rapunsegment7dress7)
        rapundollgroup.add(rapunsegment6dress6)
        rapundollgroup.add(rapunbhair)
        rapundollgroup.add(rapunbody)
        rapundollgroup.add(rapunlips)
        rapundollgroup.add(rapuneye1)
        rapundollgroup.add(rapuneye2)
        rapundollgroup.add(rapuntoplayer)
        rapundollgroup.add(rapuneshade)
        rapundollgroup.add(rapunelash)
        rapundollgroup.add(rapunblush)
        rapundollgroup.add(rapunebrow)
        rapundollgroup.add(rapunsegment5dress5)
        rapundollgroup.add(rapunsegment2dress2)
        rapundollgroup.add(rapunsegment4dress4)
        rapundollgroup.add(rapunsegment3dress3)
        rapundollgroup.add(rapunsegment41dress4)
        rapundollgroup.add(rapunsegment51dress5)
        rapundollgroup.add(rapunsegment1dress1)
        rapundollgroup.add(rapunhair)
        rapundollgroup.x = 170
        rapundollgroup.y = 10
        rapunsegment1dress1.tint = rapuncolor[1]
        rapunsegment2dress2.tint = rapuncolor[2]
        rapunsegment3dress3.tint = rapuncolor[3]
        rapunsegment4dress4.tint = rapuncolor[4]
        rapunsegment41dress4.tint = rapuncolor[4]
        rapunsegment5dress5.tint = rapuncolor[5]
        rapunsegment51dress5.tint = rapuncolor[5]
        rapunsegment6dress6.tint = rapuncolor[6]
        rapunsegment7dress7.tint = rapuncolor[7]
        elsasegment1dress1.tint = elsacolor[1]
        elsasegment2dress2.tint = elsacolor[2]
        elsasegment3dress3.tint = elsacolor[3]
        elsasegment4dress4.tint = elsacolor[4]
        elsasegment5dress5.tint = elsacolor[5]
        elsasegment6dress6.tint = elsacolor[6]
        elsasegment7dress7.tint = elsacolor[7]
        elsasegment8dress8.tint = elsacolor[8]
        elsasegment9dress9.tint = elsacolor[9]
        elsasegment10dress10.tint = elsacolor[10]
        elsasegment11dress11.tint = elsacolor[11]
        annasegment1dress1.tint = annacolor[1]
        annasegment2dress2.tint = annacolor[2]
        annasegment3dress3.tint = annacolor[3]
        annasegment4dress4.tint = annacolor[4]
        annasegment41dress4.tint = annacolor[4]
        annasegment5dress5.tint = annacolor[5]
        annasegment6dress6.tint = annacolor[6]
        annasegment7dress7.tint = annacolor[7]
        annasegment8dress8.tint = annacolor[8]
        annasegment9dress9.tint = annacolor[9]
        arielsegment1dress1.tint = arielcolor[1]
        arielsegment2dress2.tint = arielcolor[2]
        arielsegment3dress3.tint = arielcolor[3]
        arielsegment4dress4.tint = arielcolor[4]
        arielsegment41dress4.tint = arielcolor[4]
        arielsegment5dress5.tint = arielcolor[5]
        arielsegment6dress6.tint = arielcolor[6]
        arielsegment7dress7.tint = arielcolor[7]
        arielsegment8dress8.tint = arielcolor[8]
        arielsegment9dress9.tint = arielcolor[9]
        arielsegment10dress10.tint = arielcolor[10]
        arielsegment11dress11.tint = arielcolor[11]
        var tween1 = this.tweens.add({
            targets: rapuneye1,
            x: rapuneye1.x + 2,
            ease: 'Linear',
            duration: 300,
            delay: 2400,
            callbackScope: this,
        });
        var tween2 = this.tweens.add({
            targets: rapuneye2,
            x: rapuneye2.x + 2,
            ease: 'Linear',
            duration: 300,
            delay: 2400,
            onComplete: rapuneyestartani1,
            callbackScope: this
        });

        function rapuneyestartani1() {
            this.tweens.add({
                targets: rapuneye1,
                x: rapuneye1.x - 2,
                ease: 'Linear',
                duration: 300,
                delay: 3000,
                callbackScope: this
            });
            this.tweens.add({
                targets: rapuneye2,
                x: rapuneye2.x - 2,
                ease: 'Linear',
                duration: 300,
                delay: 3000,
                onComplete: rapuneyestartani2,
                callbackScope: this,
            })
        }

        function rapuneyestartani2() {
            this.tweens.add({
                targets: rapuneye1,
                x: rapuneye1.x + 2,
                ease: 'Linear',
                duration: 300,
                delay: 2400,
                callbackScope: this
            });
            this.tweens.add({
                targets: rapuneye2,
                x: rapuneye2.x + 2,
                ease: 'Linear',
                duration: 300,
                delay: 2400,
                onComplete: rapuneyestartani1,
                callbackScope: this,
            })
        }
        rapuneshad = this.time.addEvent({
            delay: 1000,
            callback: rapuneblinkstart1,
            callbackScope: this
        });

        function rapuneblinkstart1() {
            rapuneshade.setFrame(marr4[4] + 1)
            rapunelash.visible = false
            rapuneshad = this.time.addEvent({
                delay: 200,
                callback: rapuneblinkstart2,
                callbackScope: this
            });
        }

        function rapuneblinkstart2() {
            rapuneshade.setFrame(marr4[4])
            rapunelash.visible = true
            rapuneshad = this.time.addEvent({
                delay: 4000,
                callback: rapuneblinkstart1,
                callbackScope: this
            });
        }
        rapunlip = this.time.addEvent({
            delay: 1000,
            callback: rapunlipsstart1,
            callbackScope: this
        });

        function rapunlipsstart1() {
            rapunlips.setFrame(marr4[2] + 1)
            rapunlip = this.time.addEvent({
                delay: 100,
                callback: rapunlipsstart2,
                callbackScope: this
            });
        }

        function rapunlipsstart2() {
            rapunlips.setFrame(marr4[2] + 2)
            rapunlip = this.time.addEvent({
                delay: 2500,
                callback: rapunlipsstart3,
                callbackScope: this
            });
        }

        function rapunlipsstart3() {
            rapunlips.setFrame(marr4[2] + 1)
            rapunlip = this.time.addEvent({
                delay: 100,
                callback: rapunlipsstart4,
                callbackScope: this
            });
        }

        function rapunlipsstart4() {
            rapunlips.setFrame(marr4[2])
            rapunlip = this.time.addEvent({
                delay: 3000,
                callback: rapunlipsstart1,
                callbackScope: this
            });
        }
        elsadollgroup.x = -400
        arieldollgroup.x = -400
        annadollgroup.x = -400
        rapundollgroup.x = -400
        this.tweens.add({
            targets: elsadollgroup,
            x: 180,
            ease: 'Back',
            duration: 700,
            delay: 2500,
            onComplete: elsadollgroupanistart11,
            callbackScope: this,
        })

        function elsadollgroupanistart11() {
            this.time.addEvent({
                delay: 4000,
                callback: elsadollgroupanistart,
                callbackScope: this
            });
        }

        function elsadollgroupanistart() {
            this.tweens.add({
                targets: elsadollgroup,
                x: 870,
                ease: 'Back',
                duration: 700,
                onComplete: elsadollgroupanistart1,
                callbackScope: this,
            })
        }

        function elsadollgroupanistart1() {
            this.tweens.add({
                targets: arieldollgroup,
                x: 190,
                ease: 'Back',
                duration: 700,
                onComplete: elsadollgroupanistart2,
                callbackScope: this,
            })
        }

        function elsadollgroupanistart2() {
            this.time.addEvent({
                delay: 4000,
                callback: elsadollgroupanistart3,
                callbackScope: this
            });
        }

        function elsadollgroupanistart3() {
            this.tweens.add({
                targets: arieldollgroup,
                x: 800,
                ease: 'Back',
                duration: 700,
                onComplete: elsadollgroupanistart4,
                callbackScope: this,
            })
        }

        function elsadollgroupanistart4() {
            this.tweens.add({
                targets: annadollgroup,
                x: 210,
                ease: 'Back',
                duration: 700,
                onComplete: elsadollgroupanistart5,
                callbackScope: this,
            })
        }

        function elsadollgroupanistart5() {
            this.time.addEvent({
                delay: 4000,
                callback: elsadollgroupanistart6,
                callbackScope: this
            });
        }

        function elsadollgroupanistart6() {
            this.tweens.add({
                targets: annadollgroup,
                x: 800,
                ease: 'Back',
                duration: 700,
                onComplete: elsadollgroupanistart7,
                callbackScope: this,
            })
        }

        function elsadollgroupanistart7() {
            this.tweens.add({
                targets: rapundollgroup,
                x: 170,
                ease: 'Back',
                duration: 700,
                onComplete: elsadollgroupanistart8,
                callbackScope: this,
            })
        }

        function elsadollgroupanistart8() {
            this.time.addEvent({
                delay: 4000,
                callback: elsadollgroupanistart9,
                callbackScope: this
            });
        }

        function elsadollgroupanistart9() {
            this.tweens.add({
                targets: rapundollgroup,
                x: 800,
                ease: 'Back',
                duration: 700,
                onComplete: elsadollgroupanistart10,
                callbackScope: this,
            })
        }

        function elsadollgroupanistart10() {
            elsadollgroup.x = -400
            arieldollgroup.x = -400
            annadollgroup.x = -400
            rapundollgroup.x = -400
            this.tweens.add({
                targets: elsadollgroup,
                x: 180,
                ease: 'Back',
                duration: 700,
                onComplete: elsadollgroupanistart11,
                callbackScope: this,
            })
        }
        elsasegment11dress11.setFrame(elsastorearr[0])
        elsasegment10dress10.setFrame(elsastorearr[1])
        elsasegment9dress9.setFrame(elsastorearr[2])
        elsasegment8dress8.setFrame(elsastorearr[3])
        elsasegment7dress7.setFrame(elsastorearr[4])
        elsasegment6dress6.setFrame(elsastorearr[5])
        elsasegment1dress1.setFrame(elsastorearr[6])
        elsasegment5dress5.setFrame(elsastorearr[7])
        elsasegment4dress4.setFrame(elsastorearr[8])
        elsasegment3dress3.setFrame(elsastorearr[9])
        elsasegment2dress2.setFrame(elsastorearr[10])
        elsasegment11dress11.alpha = elsastorearr[11]
        elsasegment10dress10.alpha = elsastorearr[12]
        elsasegment9dress9.alpha = elsastorearr[13]
        elsasegment8dress8.alpha = elsastorearr[14]
        elsasegment7dress7.alpha = elsastorearr[15]
        elsasegment6dress6.alpha = elsastorearr[16]
        elsasegment1dress1.alpha = elsastorearr[17]
        elsasegment5dress5.alpha = elsastorearr[18]
        elsasegment4dress4.alpha = elsastorearr[19]
        elsasegment3dress3.alpha = elsastorearr[20]
        elsasegment2dress2.alpha = elsastorearr[21]
        arielsegment11dress11.setFrame(arielstorearr[0])
        arielsegment10dress10.setFrame(arielstorearr[1])
        arielsegment9dress9.setFrame(arielstorearr[2])
        arielsegment8dress8.setFrame(arielstorearr[3])
        arielsegment7dress7.setFrame(arielstorearr[4])
        arielsegment6dress6.setFrame(arielstorearr[5])
        arielsegment1dress1.setFrame(arielstorearr[6])
        arielsegment5dress5.setFrame(arielstorearr[7])
        arielsegment4dress4.setFrame(arielstorearr[8])
        arielsegment41dress4.setFrame(arielstorearr[8])
        arielsegment3dress3.setFrame(arielstorearr[9])
        arielsegment2dress2.setFrame(arielstorearr[10])
        arielsegment11dress11.alpha = arielstorearr[11]
        arielsegment10dress10.alpha = arielstorearr[12]
        arielsegment9dress9.alpha = arielstorearr[13]
        arielsegment8dress8.alpha = arielstorearr[14]
        arielsegment7dress7.alpha = arielstorearr[15]
        arielsegment6dress6.alpha = arielstorearr[16]
        arielsegment1dress1.alpha = arielstorearr[17]
        arielsegment5dress5.alpha = arielstorearr[18]
        arielsegment4dress4.alpha = arielstorearr[19]
        arielsegment41dress4.alpha = arielstorearr[19]
        arielsegment3dress3.alpha = arielstorearr[20]
        arielsegment2dress2.alpha = arielstorearr[21]
        if (arielstorearr[22] == false) {
            arielsegment4dress4.visible = false
            arielsegment41dress4.visible = true
        } else {
            arielsegment4dress4.visible = true
            arielsegment41dress4.visible = false
        }
        annasegment9dress9.setFrame(annastorearr[2])
        annasegment8dress8.setFrame(annastorearr[3])
        annasegment7dress7.setFrame(annastorearr[4])
        annasegment6dress6.setFrame(annastorearr[5])
        annasegment1dress1.setFrame(annastorearr[6])
        annasegment5dress5.setFrame(annastorearr[7])
        annasegment4dress4.setFrame(annastorearr[8])
        annasegment41dress4.setFrame(annastorearr[8])
        annasegment3dress3.setFrame(annastorearr[9])
        annasegment2dress2.setFrame(annastorearr[10])
        annasegment9dress9.alpha = annastorearr[13]
        annasegment8dress8.alpha = annastorearr[14]
        annasegment7dress7.alpha = annastorearr[15]
        annasegment6dress6.alpha = annastorearr[16]
        annasegment1dress1.alpha = annastorearr[17]
        annasegment5dress5.alpha = annastorearr[18]
        annasegment4dress4.alpha = annastorearr[19]
        annasegment41dress4.alpha = annastorearr[19]
        annasegment3dress3.alpha = annastorearr[20]
        annasegment2dress2.alpha = annastorearr[21]
        if (annastorearr[22] == false) {
            annasegment4dress4.visible = false
            annasegment41dress4.visible = true
        } else {
            annasegment4dress4.visible = true
            annasegment41dress4.visible = false
        }
        rapunsegment7dress7.setFrame(rapunstorearr[4])
        rapunsegment6dress6.setFrame(rapunstorearr[5])
        rapunsegment1dress1.setFrame(rapunstorearr[6])
        rapunsegment5dress5.setFrame(rapunstorearr[7])
        rapunsegment51dress5.setFrame(rapunstorearr[7])
        rapunsegment4dress4.setFrame(rapunstorearr[8])
        rapunsegment41dress4.setFrame(rapunstorearr[8])
        rapunsegment3dress3.setFrame(rapunstorearr[9])
        rapunsegment2dress2.setFrame(rapunstorearr[10])
        rapunsegment7dress7.alpha = rapunstorearr[15]
        rapunsegment6dress6.alpha = rapunstorearr[16]
        rapunsegment1dress1.alpha = rapunstorearr[17]
        rapunsegment5dress5.alpha = rapunstorearr[18]
        rapunsegment51dress5.alpha = rapunstorearr[18]
        rapunsegment4dress4.alpha = rapunstorearr[19]
        rapunsegment41dress4.alpha = rapunstorearr[19]
        rapunsegment3dress3.alpha = rapunstorearr[20]
        rapunsegment2dress2.alpha = rapunstorearr[21]
        if (rapunstorearr[22] == false) {
            rapunsegment4dress4.visible = false
            rapunsegment41dress4.visible = true
        } else {
            rapunsegment4dress4.visible = true
            rapunsegment41dress4.visible = false
        }
        if (rapunstorearr[23] == false) {
            rapunsegment5dress5.visible = false
            rapunsegment51dress5.visible = true
        } else {
            rapunsegment5dress5.visible = true
            rapunsegment51dress5.visible = false
        }
        save = this.add.image(710, 265, 'uiButton').setOrigin(0.5, 0.5).setInteractive({
            pixelPerfect: true,
            useHandCursor: true
        })
        save.setFrame("button-save-pic")
        save.on('pointerover', function () {
            save.setScale(1.05, 1.05)
        });
        save.on('pointerout', function () {
            save.setScale(1, 1)
        });
        save.on('pointerup', function () {
            save.setScale(1.05, 1.05)
        });
        save.on('pointerdown', function () {
            if (music.isPlaying == false) {
                clicksound.stop()
            } else {
                clicksound.play()
            }
            save.setScale(1, 1)
            saveContainer.visible = false
            setTimeout(hideAction, 500)

            function hideAction() {
                saveContainer.visible = true
            }
            var canvas;

            function exportCanvasAsPNG(id, fileName, dataUrl) {
                var canvasElement = document.getElementById(id);
                var MIME_TYPE = "image/png";
                var imgURL = dataUrl;
                var dlLink = document.createElement('a');
                dlLink.download = fileName;
                dlLink.href = imgURL;
                dlLink.dataset.downloadurl = [MIME_TYPE, dlLink.download, dlLink.href].join(':');
                document.body.appendChild(dlLink);
                dlLink.click();
                document.body.removeChild(dlLink);
            }
            game.renderer.snapshot(function (image) {
                var imgSrcs;
                imgSrcs = image.src
                exportCanvasAsPNG(canvas, 'princess_girls_oscars_design', imgSrcs);
            });
        });
        replay = this.add.image(710, 375, 'uiButton').setOrigin(0.5, 0.5).setInteractive({
            pixelPerfect: true,
            useHandCursor: true
        })
        replay.setFrame("button_replay")
        replay.on('pointerover', function () {
            replay.setScale(1.05, 1.05)
        });
        replay.on('pointerout', function () {
            replay.setScale(1, 1)
        });
        replay.on('pointerup', function () {
            if (stargame13) {
                replay.setScale(1.05, 1.05)
            }
        });
        replay.once('pointerdown', function () {
            if (!stargame13) {
                stargame13 = true
                if (music.isPlaying == false) {
                    clicksound.stop()
                } else {
                    clicksound.play()
                }
                replay.setScale(1, 1)
                endCount = 1
                transitionbackground1.alpha = 1
                transitionbackground2.alpha = 1
                transitionbackground3.alpha = 1
                transitionpanel.alpha = 1
                transitiondress.alpha = 1
                transitiontitle1.alpha = 1
                transitiontitle2.alpha = 1
                transitiontitle3.alpha = 1
                transitionstar1.alpha = 1
                transitionstar2.alpha = 1
                transitionstar3.alpha = 1
                transitionstar4.alpha = 1
                transitionstar5.alpha = 1
                transitionstar1.setScale(0)
                transitionstar2.setScale(0)
                transitionstar3.setScale(0)
                transitionstar4.setScale(0)
                transitionstar5.setScale(0)
                transitionbackground1.visible = true
                transitionbackground2.visible = true
                transitionbackground3.visible = true
                transitiondress.visible = false
                transitionpanel.visible = false
                transitiontitle1.visible = false
                transitiontitle2.visible = false
                transitiontitle3.visible = false
                transitionstar1.visible = false
                transitionstar2.visible = false
                transitionstar3.visible = false
                transitionstar4.visible = false
                transitionstar5.visible = false
                transitionbackground1.alpha = 0
                transitionbackground2.alpha = 0
                transitionbackground3.alpha = 0
                this.scene.tweens.add({
                    targets: transitionbackground1,
                    alpha: 1,
                    ease: 'Linear',
                    duration: 800,
                    onComplete: transitionstart1,
                    callbackScope: this
                })

                function transitionstart1() {
                    transitiondress.visible = true
                    transitiondress.alpha = 0
                    this.scene.tweens.add({
                        targets: transitiondress,
                        alpha: 1,
                        ease: 'Linear',
                        duration: 400,
                        onComplete: transitionstart2,
                        callbackScope: this
                    })
                    this.scene.tweens.add({
                        targets: transitionbackground2,
                        alpha: 1,
                        ease: 'Linear',
                        duration: 800,
                    })
                }

                function transitionstart2() {
                    transitionpanel.visible = true
                    transitionpanel.setScale(0)
                    this.scene.tweens.add({
                        targets: transitionpanel,
                        scaleX: 1,
                        scaleY: 1,
                        ease: 'Linear',
                        duration: 500,
                        onComplete: transitionstart3,
                        callbackScope: this
                    })
                }

                function transitionstart3() {
                    this.scene.tweens.add({
                        targets: transitionbackground3,
                        alpha: 1,
                        ease: 'Linear',
                        duration: 800,
                        onComplete: transitionstart4,
                        callbackScope: this
                    })
                }

                function transitionstart4() {
                    transitiontitle1.visible = true
                    transitiontitle2.visible = true
                    transitiontitle3.visible = true
                    transitiontitle1.alpha = 0
                    transitiontitle2.alpha = 0
                    transitiontitle3.alpha = 0
                    transitiontitle2.x = transitiontitle2.x + 100
                    transitiontitle3.x = transitiontitle3.x - 100
                    this.scene.tweens.add({
                        targets: transitiontitle1,
                        alpha: 1,
                        ease: 'Linear',
                        duration: 800,
                        onComplete: transitionstart5,
                        callbackScope: this
                    })
                    this.scene.tweens.add({
                        targets: transitiontitle2,
                        alpha: 1,
                        x: transitiontitle2.x - 100,
                        ease: 'Linear',
                        duration: 800,
                    })
                    this.scene.tweens.add({
                        targets: transitiontitle3,
                        alpha: 1,
                        x: transitiontitle3.x + 100,
                        ease: 'Linear',
                        duration: 800,
                    })
                }

                function transitionstart5() {
                    transitionstar1.setScale(0)
                    transitionstar2.setScale(0)
                    transitionstar3.setScale(0)
                    transitionstar4.setScale(0)
                    transitionstar5.setScale(0)
                    transitionstar1.visible = true
                    transitionstar2.visible = true
                    transitionstar3.visible = true
                    transitionstar4.visible = true
                    transitionstar5.visible = true
                    this.scene.tweens.add({
                        targets: transitionstar1,
                        scaleX: 1,
                        scaleY: 1,
                        ease: 'Linear',
                        duration: 300,
                    })
                    this.scene.tweens.add({
                        targets: transitionstar2,
                        scaleX: 0.9,
                        scaleY: 0.9,
                        ease: 'Linear',
                        duration: 300,
                    })
                    this.scene.tweens.add({
                        targets: transitionstar3,
                        scaleX: 0.9,
                        scaleY: 0.9,
                        ease: 'Linear',
                        duration: 300,
                    })
                    this.scene.tweens.add({
                        targets: transitionstar4,
                        scaleX: 0.8,
                        scaleY: 0.8,
                        ease: 'Linear',
                        duration: 300,
                    })
                    this.scene.tweens.add({
                        targets: transitionstar5,
                        scaleX: 0.65,
                        scaleY: 0.65,
                        ease: 'Linear',
                        duration: 300,
                        onComplete: transitionstar6,
                        callbackScope: this
                    })
                }

                function transitionstar6() {
                    this.scene.scene.stop('endScreen')
                    this.scene.scene.run('titleScreen')
                }
            }
        });
        if (brandingMaterials.thumbnails[0].display == true && brandingMaterials.thumbnails[0].image != undefined) {
            thumbnails1 = this.add.sprite(30, 570, 'thumbnails1').setInteractive({
                pixelPerfect: true,
                useHandCursor: true
            })
            thumbnails1.setOrigin(0, 1)
            thumbnails1.on("pointerdown", brandingMaterials.thumbnails[0].clickURL)
        }
        if (brandingMaterials.thumbnails[1].display == true && brandingMaterials.thumbnails[1].image != undefined) {
            thumbnails2 = this.add.sprite(770, 570, 'thumbnails2').setInteractive({
                pixelPerfect: true,
                useHandCursor: true
            })
            thumbnails2.setOrigin(1, 1)
            thumbnails2.on("pointerdown", brandingMaterials.thumbnails[1].clickURL)
        }
        if (brandingMaterials.moreGamesButton.display == true && brandingMaterials.moreGamesButton.image != undefined) {
            moreGamesButton1 = this.add.sprite(400, 570, 'moreGamesButton1').setInteractive({
                pixelPerfect: true,
                useHandCursor: true
            })
            moreGamesButton1.setOrigin(0.5, 1)
            moreGamesButton1.on("pointerdown", brandingMaterials.moreGamesButton.clickURL)
        }
        mute = this.add.sprite(790, 10, 'uiButton').setFrame("button_music_on").setInteractive({
            pixelPerfect: true,
            useHandCursor: true
        })
        mute.setOrigin(1, 0)
        mute.on('pointerdown', function () {
            if (!isMuted) {
                isMuted = true
                music.pause()
                mute.setFrame("button_music_off")
            } else {
                isMuted = false
                music.resume()
                mute.setFrame("button_music_on")
            }
            var tween2 = this.tweens.add({
                targets: mute,
                scaleX: 0.9,
                scaleY: 0.9,
                ease: 'Linear',
                duration: 80,
                yoyo: true,
                repeat: 1,
            });
            if (music.isPlaying == false) {
                clicksound.stop()
            } else {
                clicksound.play()
            }
        }, this);
        if (music.isPlaying == false) {
            mute.setFrame("button_music_off")
        } else {
            mute.setFrame("button_music_on")
        }
        var saveContainer = this.add.container(0, 0);
        saveContainer.add([save, replay, thumbnails1, thumbnails2, moreGamesButton1, mute])
        transitionbackground1 = this.add.image(400, 300, 'transitionbackground1').setOrigin(0.5, 0.5).setInteractive({
            pixelPerfect: true,
            useHandCursor: true
        })
        transitionbackground2 = this.add.image(400, 300, 'transitionbackground2').setOrigin(0.5, 0.5)
        transitionbackground3 = this.add.image(400, 300, 'transitionbackground3').setOrigin(0.5, 0.5)
        transitionpanel = this.add.image(393.5, 314, 'transitionpanel').setOrigin(0.5)
        transitiondress = this.add.image(267, 141, 'transitiondress').setOrigin(0, 0)
        transitiontitle1 = this.add.image(400, 329, 'title1');
        transitiontitle1.setOrigin(0.5);
        transitiontitle2 = this.add.image(380, 399, 'title2');
        transitiontitle2.setOrigin(0.5);
        transitiontitle3 = this.add.image(380, 459, 'title3');
        transitiontitle3.setOrigin(0.5);
        transitionstar1 = this.add.image(616.5, 168, 'transitionstar');
        transitionstar1.setOrigin(0.5);
        transitionstar2 = this.add.image(156.5, 168, 'transitionstar');
        transitionstar2.setOrigin(0.5);
        transitionstar2.setScale(0.9)
        transitionstar3 = this.add.image(596.5, 468, 'transitionstar');
        transitionstar3.setOrigin(0.5);
        transitionstar3.setScale(0.9)
        transitionstar4 = this.add.image(116.5, 468, 'transitionstar');
        transitionstar4.setOrigin(0.5);
        transitionstar4.setScale(0.8)
        transitionstar5 = this.add.image(400, 558, 'transitionstar');
        transitionstar5.setOrigin(0.5);
        transitionstar5.setScale(0.65)
        hitobject = this.add.image(400, 300, 'hitobject').setOrigin(0.5, 0.5).setInteractive({
            useHandCursor: true
        })
        hitobject.visible = false
        hitobject.on('pointerdown', hitobjectdown)

        function hitobjectdown() {}
        this.tweens.add({
            targets: transitionstar1,
            angle: 4,
            ease: 'Linear',
            duration: 300,
            repeat: -1,
            yoyo: true
        })
        this.tweens.add({
            targets: transitionstar2,
            angle: 4,
            ease: 'Linear',
            duration: 300,
            repeat: -1,
            yoyo: true
        })
        this.tweens.add({
            targets: transitionstar3,
            angle: 4,
            ease: 'Linear',
            duration: 300,
            repeat: -1,
            yoyo: true
        })
        this.tweens.add({
            targets: transitionstar4,
            angle: 4,
            ease: 'Linear',
            duration: 300,
            repeat: -1,
            yoyo: true
        })
        this.tweens.add({
            targets: transitionstar5,
            angle: 4,
            ease: 'Linear',
            duration: 300,
            repeat: -1,
            yoyo: true
        })
        this.time.addEvent({
            delay: 1000,
            callback: transitionstart11,
            callbackScope: this
        });

        function transitionstart11() {
            this.tweens.add({
                targets: transitionbackground1,
                alpha: 0,
                ease: 'Linear',
                duration: 700
            })
            this.tweens.add({
                targets: transitionbackground2,
                alpha: 0,
                ease: 'Linear',
                duration: 700
            })
            this.tweens.add({
                targets: transitionbackground3,
                alpha: 0,
                ease: 'Linear',
                duration: 700
            })
            this.tweens.add({
                targets: transitionpanel,
                alpha: 0,
                ease: 'Linear',
                duration: 700
            })
            this.tweens.add({
                targets: transitiondress,
                alpha: 0,
                ease: 'Linear',
                duration: 700
            })
            this.tweens.add({
                targets: transitiontitle1,
                alpha: 0,
                ease: 'Linear',
                duration: 700
            })
            this.tweens.add({
                targets: transitiontitle2,
                alpha: 0,
                ease: 'Linear',
                duration: 700
            })
            this.tweens.add({
                targets: transitiontitle3,
                alpha: 0,
                ease: 'Linear',
                duration: 700
            })
            this.tweens.add({
                targets: transitionstar1,
                alpha: 0,
                ease: 'Linear',
                duration: 700
            })
            this.tweens.add({
                targets: transitionstar2,
                alpha: 0,
                ease: 'Linear',
                duration: 700
            })
            this.tweens.add({
                targets: transitionstar3,
                alpha: 0,
                ease: 'Linear',
                duration: 700
            })
            this.tweens.add({
                targets: transitionstar4,
                alpha: 0,
                ease: 'Linear',
                duration: 700
            })
            this.tweens.add({
                targets: transitionstar5,
                alpha: 0,
                ease: 'Linear',
                duration: 700
            })
        }
    }
});
var config = {
    type: Phaser.AUTO,
    width: 800,
    height: 600,
    backgroundColor: 'rgb(255, 255, 255)',
    scale: {
        parent: 'phaser-example',
        mode: Phaser.Scale.FIT,
        width: 800,
        height: 600,
    },
    parent: 'spinner',
    scene: [bootState, preLoader, titleScreen, levelSelect, Level1, Level5, Level2, Level6, Level3, Level7, Level4, Level8, endScreen],
}
var game = new Phaser.Game(config);
